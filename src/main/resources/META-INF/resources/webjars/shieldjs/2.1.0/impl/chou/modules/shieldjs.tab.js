/**
* multiTab
* @fileOverview 多页签,依赖jQuery
* @author kevin
* @email donggongai@126.com
* @version 1.0
* @date 2019-09-25
* Copyright (c) 2019-2020 kevin
*/
;(function($){
	/**
	* @author kevin
	* @constructor multiTab
	* @description 美化table
	* @see The <a href="#">kevin</a>
	* @example
	*	$(".tabContainer").multiTab();
	* @since version 1.0
	*/
	$.fn.multiTab = function(options){
		// debug('multiTab count: ' + this.size());
		
		if (!jQuery().contextMenu) {
			ShieldJS.error("检测到multi-tab_plugin元素缺少依赖JS库：jquery.contextmenu.r2.js！");
			return;
		}
		
		/**
		* @description {Json} 设置参数选项
		* @field
		*/
		var options = $.extend({},$.fn.multiTab.defaults, options);
		/**
		* @description 处理函数
		*/
		this.each(function() {
	        var $this = $(this);
	        $this.data("options", options); //将设置缓存
	        
	        var multiTitleContainerE = getTitleE($this, options);
	        var multiContentContainerE = getContentE($this, options);
	        
	        multiTitleContainerE.parent().addClass(options.cssHeader); //增加父容器
	        
	        multiTitleContainerE.find("."+options.cssTitleItem).each(function() {
	        	var itemE = $(this);
	        	dealItem(itemE, multiTitleContainerE, multiContentContainerE, options);
	        	// 完成后的回调
	        	if (options.onload && $.isFunction(options.onload)){
					options.onload($this, multiTitleContainerE, multiContentContainerE);
                }
            });
	        $this.attr("multiTabInit", true); //标记为已经初始化
	        
		});
		
	};
	// 如果容器未初始化进行初始化操作
	function initPlugin(containerE) {
		if (!hasAttr(containerE, "multiTabInit")) { //未初始化先初始化
			containerE.multiTab();
        }
    }
	/**
	 * 添加页签
	 * @param tabid {String} 页签唯一标识符，字符串
	 * @param tabTitle {String} 标题
	 * @param content {String} 内容
	 * @param index {Intger} 下标从零开始，-1或者忽略该参数时表示插入到最后
	 * @param url {String} url，异步加载使用
	 * @param params {json} url的附加参数，json字符串
	 * @param relType {String} 打开类型，iframe或内部
	 */
	$.fn.multiTabAdd = function(tabid, tabTitle, content, index, url, params, relType) {
		var containerE = $(this);
		initPlugin(containerE);  //未初始化先初始化
		if (typeof index === 'string') { //index不传时
			relType = params;
			params = url;
			url = index;
			index = null;
        }
		var paramsStr = params;
		if (typeof params == 'object') { //js对象
			paramsStr = JSON.stringify(params);
    	} 
		var result = {titleE : null, contentE :null, index : null}
		
		var options = $.extend({},$.fn.multiTab.defaults, containerE.data("options"));
		var multiTitleContainerE = getTitleE(containerE, options);
        var multiContentContainerE = getContentE(containerE, options);
        
        var tabContentShowEle; //内容容器
        
        var relEle = multiTitleContainerE.find("[tabid='"+tabid+"']"); //根据id获取
        
        if (relEle.length == 0) { //没有，需新建
        	var titleHtml = '<li class="'+options.cssTitleItem+'" tabid="'+tabid+'" ';
        	titleHtml += '><a class="titletxt">'+tabTitle+'</a></li>';
        	relEle = $(titleHtml);
        	if (url) {
        		relEle.attr("data-url",url);
        		if (params) {
        			relEle.attr('shieldParams', paramsStr);
        		}
        	}
        	if ((index && index!=-1) || index==0) { //插入到指定位置
        		var itemPreE = getTitleItem(multiTitleContainerE, index, options);
				if(itemPreE.length){ //有
					itemPreE.before(relEle);
				} else { //没有，则加到最后
					multiTitleContainerE.append(relEle);
				}
        		
        	} else {
        		multiTitleContainerE.append(relEle);
				
			}
        	
        	var contentHtml = '<div class="'+options.cssContentItem+'" style="display: none;">';
        	contentHtml += content;
        	contentHtml += '</div>';
        	tabContentShowEle = $(contentHtml);
        	if ((index && index!=-1) || index==0) { //插入到指定位置
        		var itemPreE = getContentItem(multiContentContainerE, index, options);
        		
				if(itemPreE.length){ //有
					itemPreE.before(tabContentShowEle);
				} else { //没有，则加到最后
					multiContentContainerE.append(tabContentShowEle);
				}
        	} else {
        		multiContentContainerE.append(tabContentShowEle);
			}
        	// 绑定条目处理
        	dealItem(relEle, multiTitleContainerE, multiContentContainerE, options);
        	
        } else { //如果存在tabid相同的直接返回
        	if (url) {
        		relEle.data("url",url);
        		if (params) {
        			relEle.attr('shieldParams', paramsStr);
            	}
        	}
        	var thisIndex = getTitleItemIndex(multiTitleContainerE, relEle, options);
        	tabContentShowEle = getContentItem(multiContentContainerE, thisIndex, options);
        }
        relEle.find(".titletxt").text(tabTitle);
    	tabContentShowEle.html(content);
    	
    	result.titleE = relEle;
    	if (relType) {
	    	relEle.data("relType", relType);
    	}
    	result.contentE = tabContentShowEle;
    	result.index = getTitleItemIndex(multiTitleContainerE, relEle, options);
    	return result;
    	
	}
	/**
	 * 更新页签如果没有就创建，并显示
	 * @param tabid {String} 页签唯一标识符，字符串
	 * @param tabTitle {String} 标题
	 * @param content {String} 默认显示内容
	 * @param index {Intger} 下标从零开始，-1或者忽略该参数时表示插入到最后
	 * @param url {String} url，异步加载使用，非异步传空或者不传
	 * @param params {json} url的附加参数，json字符串
	 * @param relType {String} 打开类型，iframe或内部
	 */
	$.fn.multiTabAddOrUpd = function(tabid, tabTitle, content, index, url, params, relType) {
		var containerE = $(this);
		var result = containerE.multiTabAdd(tabid, tabTitle, content, index, url, params, relType);
		result.titleE.triggerHandler("refresh"); //刷新
    	return result;
    	
	}
	/**
	 * 添加页签
	 * @param value {Object} 参数，可以是下标，也可以是tabid的值，还可以是头部jQuery对象
	 */
	$.fn.multiTabClose = function(value) {
		initPlugin(containerE);  //未初始化先初始化
		
	    var containerE = $(this);
		var options = $.extend({},$.fn.multiTab.defaults, containerE.data("options"));
		
		var multiTitleContainerE = getTitleE(containerE, options);
        var multiContentContainerE = getContentE(containerE, options);
        
        var itemE = null;
        if(value instanceof jQuery){ // jquery对象
        	itemE = value;
		} else if (typeof value === 'number' && !isNaN(value)) { //数字，下标从0开始
			var index = value;
			itemE = getTitleItem(multiTitleContainerE, index, options);
        } else if (typeof value === 'string') { //tabid
        	var tabid = value;
			itemE = multiTitleContainerE.find("[tabid='"+tabid+"']");
        }
        
        if (itemE) {
        	itemE.triggerHandler("close");
        }
        
	}
	// 获取tab容器的title容器
	function getTitleE(containerE, options) {
		var multiTitleContainerE = containerE.find("."+options.cssTitle);
		return multiTitleContainerE;
    }
	// 获取tab容器的content容器
	function getContentE(containerE, options) {
        var multiContentContainerE = containerE.find("."+options.cssContent);
        if (multiContentContainerE.length == 0) { //没有则创建
        	var multiTitleContainerE = getTitleE(containerE, options);
 	        var leng = multiTitleContainerE.find("."+options.cssTitleItem).length;
 	        if (leng > 0) {
 	        	var contentHtml = '<div class="'+options.cssContent+'">';
 	        	for (var i = 0; i < leng; i++) {
 	        		contentHtml += '<div class="mtab-item';
 	        		if (i==0) {
 	        			contentHtml += ' multi-show ';
                    }
 	        		contentHtml += '" style="display: none;"></div>';
                }
 	        	contentHtml += '</div>';
            }
        	
        	multiContentContainerE = $(contentHtml);
        	containerE.append(multiContentContainerE);
        }
        return multiContentContainerE;
    }
	// 获取给定标题对象在同类元素中的下标
	function getTitleItemIndex(multiTitleContainerE, titleEle, options) {
		var thisIndex = multiTitleContainerE.children("."+options.cssTitleItem).index(titleEle);
		return thisIndex;
    }
	// 获取给定下标的title条目对象
	function getTitleItem(multiTitleContainerE, index, options) {
		var titleEle = multiTitleContainerE.children("."+options.cssTitleItem).eq(index);
		return titleEle;
    }
	// 获取给定下标的content条目对象
	function getContentItem(multiContentContainerE, index, options) {
		var contentEle = multiContentContainerE.children("."+options.cssContentItem).eq(index);
        return contentEle;
    }
	// 获取给定标题对象的标题
	function getTitleItemTitle(titleEle) {
		var title = titleEle.text();
		return title;
    }
	// 是否可以关闭
	function hasClose(titleEle) {
        return titleEle.data("close")+""!="false";
    }
	// 是否可以刷新
	function hasRefresh(titleEle) {
        return titleEle.data("refresh")+""!="false";
    }
	
	// 处理单个条目，itemE为标题条目
	function dealItem(itemE, multiTitleContainerE , multiContentContainerE, options) {
    	var url = itemE.data("url");
    	if (url && hasRefresh(itemE)) {
    		var refreshE = itemE.find(".refresh");
    		if (refreshE.length==0) {
    			itemE.append('<span class="refresh" title="刷新"></span>');
    		}
    		refreshE = itemE.find(".refresh");
    		refreshE.click(function() { //刷新即执行点击操作
    			itemE.triggerHandler("refresh");
    			return false; //阻止冒泡
            });
    	}
    	
    	if ($("#multi-tab_plugin_popmenu").length==0) { //右键
    		$("body").append('<div class="contextMenu" id="multi-tab_plugin_popmenu" style="position: absolute;display: none;">' +
    			    '<ul>' +
    			      '<li id="mul-tab_refresh">刷新</li>' +
    			      '<li id="mul-tab_close">关闭</li>' +
    			      '<li id="mul-tab_closeothers">关闭其他</li>' +
    			    '</ul>' +
    			'</div>');
    	}
    	// 关闭
    	itemE.bind("close", function(event, showPrev) {
    		if (itemE.data("close")+""!="false") { //不显示“关闭”按钮,加“是因为默认返回boolean型
    			var titleEle = $(this);
    			
    			showPrev = showPrev||"true"; //默认显示前一个  ele.triggerHandler("close",["false"])
            	if (showPrev=="true") { // dataload有值表示加载过
		    		var prevEle = titleEle.prev();
		    		if (prevEle.is(":visible")) {
    					prevEle.triggerHandler("click");
    				}
            	}
    			
    			var thisIndex = getTitleItemIndex(multiTitleContainerE, titleEle, options);
    			titleEle.remove();
    			
    			var contentEle = getContentItem(multiContentContainerE, thisIndex, options);;
    			contentEle.remove();
    			
    			dealUI(multiTitleContainerE , multiContentContainerE, options);
    		}
    	});
    	// 关闭其他
    	itemE.bind("closeothers", function() {
    		var titleEle = $(this);
        	var thisIndex = getTitleItemIndex(multiTitleContainerE, titleEle, options);
        	titleEle.triggerHandler("click");
        	multiTitleContainerE.find("."+options.cssTitleItem).not(titleEle).each(function() {
        		$(this).triggerHandler("close", ["false"]); //不显示前一个
            });
        	var containerE = multiTitleContainerE.closest("."+options.cssHeader); //增加父容器
        	multiTitleContainerE.css({left : ""}); // 移除left属性，恢复默认
    	});
    	// 显示，只显示如果为ajax不发起请求
    	itemE.bind("show", function() {
    		var titleEle = $(this);
        	var thisIndex = getTitleItemIndex(multiTitleContainerE, titleEle, options);
        	multiTitleContainerE.find("."+options.cssTitleItem).removeClass(options.cssTitleActive);
        	titleEle.addClass(options.cssTitleActive);
        	
        	multiContentContainerE.children("."+options.cssContentItem).hide();
        	var contentEle = getContentItem(multiContentContainerE, thisIndex, options);
        	contentEle.addClass(options.cssContentActive);
        	contentEle.show();
        	
        	var containerE = multiTitleContainerE.closest("."+options.cssHeader); //增加父容器
        	// 更多的处理
        	var moreUlEle = containerE.find(".mul-more");
        	if (moreUlEle.length > 0) {
        		moreUlEle.find("li").removeClass(options.cssTitleActive);
        		var moreLiEle = moreUlEle.find("li").eq(thisIndex);
        		moreLiEle.addClass(options.cssTitleActive);
            }
        	var offsetLeft = itemE.offset().left;
        	var arrowRightE = containerE.find('.arrow-r');
			var arrowLeftE = containerE.find('.arrow-l');
			if (arrowRightE.length >0 ) { 
				//可视范围内
				if (offsetLeft < arrowLeftE.offset().left) { //可视范围内
					showLeft(multiTitleContainerE, itemE,  options);
				} else if (offsetLeft+50> arrowRightE.offset().left) {
					showRight(multiTitleContainerE, itemE,  options);
				}
			}
    	});
    	
    	// 刷新
    	itemE.bind("refresh", function() {
    		itemE.triggerHandler("click",["true"])
    	});
    	if (!hasAttr(itemE, "title")) {
    		var title = getTitleItemTitle(itemE);
    		itemE.attr("title", title);
        }
    	// 点击，加载事件
        itemE.bind("click", function(event, refresh) {
        	var titleEle = $(this);
        	var thisIndex = getTitleItemIndex(multiTitleContainerE, titleEle, options);
        	var contentEle = getContentItem(multiContentContainerE, thisIndex, options); //当前条目的内容区
        	
        	titleEle.triggerHandler("show");
        	// 无url的直接显示,有url的发起ajax请求
        	var url = titleEle.data("url");
        	refresh = refresh||"false"; //刷新  ele.triggerHandler("click",["true"])
        	if (url && (!titleEle.data("dataload") || refresh=="true")) { // dataload有值表示加载过
        		var params={};
				var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector(titleEle, params); //参数选择器
				if (!checkParams) {
					return true;
				} else {
					params = checkParams;
				}
				params = getParams(titleEle, params);
				
				var relType = "";
				var method = titleEle.data("method")||"get";
				if(titleEle.data("relType")){
					relType = titleEle.data("relType"); 
				}
				if (relType === 'iframe') { // 处理iframe的情况
					var tabid = 'shieldtab_'+new Date().getTime();
					changeFrameHeight = function (tabid) {
						var ifm = $("#iframe" + tabid);
						if (ifm.length > 0) {
							var rootE;
							try {
								rootE = ifm.contents().find(":root"); //跨域获取不到
		                    } catch (e) {
		                    	// %c表示样式%s表示字符串
		                    	window.console && window.console.log && console.log("%c%s","color: red; background: yellow;","错误：");
		                    	ShieldJS.debug(e);
		                    }
							var iframe = ifm[0];
							var bHeight = iframe.contentWindow.document.body.scrollHeight;
							var dHeight = iframe.contentWindow.document.documentElement.scrollHeight; //有<!DOCTYPE..>标签时
							//console.log("bHeight="+bHeight+" dHeight="+dHeight+" rootE.height()="+rootE.height())
							var height = Math.max(bHeight, dHeight);
							ifm.height(height);
							var iframeResizeFn = ShieldJS.winResizes["iframeResize"+tabid]; //resize置空
							if (!iframeResizeFn) {
								iframeResizeFn = function() {
									var ifm = $("#iframe" + tabid);
									if (ifm.length > 0) {
										changeFrameHeight(tabid);
									}
									return false;
		                        }
								ShieldJS.winResizes["iframeResize"+tabid] = iframeResizeFn;
		                    }
						} else {
							ShieldJS.error("id为iframe" + tabid + "的iframe不存在！");
						}
						// alert($(ifm.document).find("body").length);
					}
					var relHeight;
					if(titleEle.data("height")){
						relHeight = parseInt(titleEle.data("height")); // 高度
					}
					var relHeightStr = ' onload="changeFrameHeight(\'' + tabid + '\')" ';
					if (relHeight) {
						relHeightStr = ' height="'+relHeight+'" ';
					}
					contentEle.html("").append('<iframe name="iframe' + tabid + '" id="iframe' + tabid + '" width="100%" ' + relHeightStr + ' frameborder="0" scrolling="auto">'+ShieldJS.loadingHtml+'</iframe>');
					var ifrmaeE = $("#iframe" + tabid, contentEle);
					ifrmaeE.contents().find('body').html(ShieldJS.loadingHtml);
					ifrmaeE.on('load', function() { //绑定事件
		                var iframeContentE = ifrmaeE.contents().find(":root"); //搜索根元素
		                console.log("iframeContentE=="+iframeContentE.height())
		                ShieldJS.core.contentAreaDeal(iframeContentE)
		                titleEle.data("dataload", "fill"); //表示直接执行过加载
		            });
					var html = '<form id="from' + tabid + '" action="' + url + '" target="iframe' + tabid + '" method="post">';
					html += "<input type='hidden' name='ifrran' value='" + Math.random() + "'>";
					for(var key in params){
						if($.isArray(params[key])){
							for(var i=0;i<params[key].length;i++) {
								html += "<input type='hidden' name='" + key + "' value='" + params[key][i] + "'>"; //直接取是逗号连接的字符串
							}
						} else {
							html += "<input type='hidden' name='" + key + "' value='" + params[key] + "'>";
						}
					}
					html+= "</form>";
					contentEle.append(html);
					var formE = contentEle.find("#from" + tabid);
					if (method == "post") { // post请求
					} else { //get
						formE.attr("method", "get");
//						ifrmaeE.attr("src", url+"?"+$.param(params)+"&ifrran="+Math.random()); //需要考虑post请求的处理
					}
					formE.submit();
				} else {
					if (method == "post") {
						ShieldJS.ajax.post(contentEle, url, params,  function() {
							//内容区处理
							ShieldJS.core.contentAreaDeal(contentEle, null, null);
							// 自定义点击回调
							ShieldJS.HtmlAction.clickCallback($this);
							titleEle.data("dataload", "fill"); //表示直接执行过加载
						});
					} else {
						ShieldJS.ajax.get(contentEle, url, params,  function() {
							//内容区处理
							ShieldJS.core.contentAreaDeal(contentEle, null, null);
							// 自定义点击回调
							ShieldJS.HtmlAction.clickCallback(titleEle);
							titleEle.data("dataload", "fill");
						});
					}
				}
				
				
        	} 
        });
        // 右键
        itemE.contextMenu('multi-tab_plugin_popmenu', {
	    	onContextMenu: function(e) {	
	    		// 是否显示
	        	// if ($(e.target).hasClass('bluebg')) return true;
	    		return true;
	      	},
	      	onShowMenu: function(e, menu) {
	      		// alert(event.ucid);
	      		var refreshE = itemE.find(".refresh");
	    		if (refreshE.length==0 || !hasRefresh(itemE)) { //无刷新ele 不显示“刷新”按钮
	    			$('#mul-tab_refresh', menu).remove();
	    		}
	    		if (!hasClose(itemE)) { //不显示“关闭”按钮,加“是因为默认返回boolean型
	    			$('#mul-tab_close', menu).remove();
	    		}
	        	return menu;
	      	},
	      	bindings: {
        		'mul-tab_refresh': function(t) { // 刷新
        			itemE.triggerHandler("refresh");
        		},
        		'mul-tab_close': function(t) { // 关闭
        			itemE.triggerHandler("close");
        		},
        		'mul-tab_closeothers': function(t) { // 关闭其他
        			itemE.triggerHandler("closeothers");
        		}
  			}		
	    });
        
        dealUI(multiTitleContainerE , multiContentContainerE, options);
        var load = itemE.data("load");
        if (load=="auto") { //自动加载
        	itemE.click();
        }
    	
    }
	// 右箭头展示
	function showRight(multiTitleContainerE, rightE,  options) {
		if (rightE && rightE.length>0) {
			var containerE = multiTitleContainerE.closest("."+options.cssHeader); //增加父容器
			var arrowRight = containerE.find('.arrow-r');
			var arrowLeft = containerE.find('.arrow-l');
			var offsetLeft = arrowRight.offset().left - arrowRight.width() - rightE.width() - rightE.position().left;;
			multiTitleContainerE.offset({left : offsetLeft});
			arrowRight.attr("index", getTitleItemIndex(multiTitleContainerE, rightE, options));
			arrowLeft.removeAttr("index"); //移除左箭头的下标记录
			arrowLeft.removeClass("disabled");
		}
    }
	// 左箭头展示
	function showLeft(multiTitleContainerE, leftE, options) {
		if (leftE && leftE.length>0) {
			var containerE = multiTitleContainerE.closest("."+options.cssHeader); //增加父容器
			var arrowRight = containerE.find('.arrow-r');
			var arrowLeft = containerE.find('.arrow-l');
			var offsetLeft = arrowLeft.offset().left + arrowLeft.width() - leftE.position().left;
			//console.log("offsetLeft="+offsetLeft);
			multiTitleContainerE.offset({left : offsetLeft});
			arrowLeft.attr("index", getTitleItemIndex(multiTitleContainerE, leftE, options));
			arrowRight.removeAttr("index"); //移除右箭头的下标记录
			arrowRight.removeClass("disabled");
		}
    }
	// 处理ui显示，显示更多和左右箭头等
	function dealUI(multiTitleContainerE , multiContentContainerE, options) {
		var containerE = multiTitleContainerE.closest("."+options.cssHeader); //增加父容器
		//console.log(containerE.width());
    	var liwidth = 0;
    	multiTitleContainerE.children("."+options.cssTitleItem).each(function() {
    		var $this = $(this);
    		liwidth += $this.outerWidth(); //包含内外边距
        })
        //console.log("itemWidth:::"+liwidth);
    	
    	var mareShow = false;
    	if (liwidth > containerE.width()) { //宽度超过父元素，显示左右箭头
    		mareShow = true;
    		multiTitleContainerE.addClass("mtab-title-l");
    		// 左右箭头
    		var activeIndex = multiTitleContainerE.find("."+options.cssTitleItem+"."+options.cssTitleActive).index();
    		var maxIndex = multiTitleContainerE.find("."+options.cssTitleItem).length-1;
    		var arrowLeftE = multiTitleContainerE.find('arrow-r');
			var arrowLeftE = multiTitleContainerE.find('arrow-l');
    		if (containerE.find(".more").length == 0 ) {
    			arrowRightE = $('<div class="arrow-r more"><a ></a></div>');
    			arrowLeftE = $('<div class="arrow-l more"><a ></a></div>');
    			multiTitleContainerE.before(arrowLeftE);
    			multiTitleContainerE.after(arrowRightE);
    			
    			arrowRightE.attr("maxIndex", maxIndex);
    			if (activeIndex == 0) {
    				arrowLeftE.addClass("disabled");
                } else if (activeIndex == maxIndex) {
                	arrowRightE.addClass("disabled");
				}
    			
    			// 向右移动一个标题
    			arrowRightE.click(function() {
    				if (!arrowRightE.hasClass("disabled")) { // 未设置为不可用
    					var index = arrowRightE.attr("index");
    					var rightE;
    					if (index || index=="0") { //有值，当值为0时index返回false所以需要单独判断
    						index++;
    						rightE = getTitleItem(multiTitleContainerE, index, options);
    					} else {
    						// 左侧隐藏
    						multiTitleContainerE.children("."+options.cssTitleItem).each(function(i, n) {
    							if (!rightE) {
    								var $this = $(this);
    								// console.log(i+" "+$(n).html()+" "+$this.offset().left+" "+$this.outerWidth()+" "+arrowRightE.offset().left);
    								if ($this.offset().left + $this.outerWidth()>arrowRightE.offset().left) { //包含内外边距
    									//alert(i+" "+$this.html());
    									rightE = $this;
    									index = i;
    									return; //跳不出循环
    								}
    							}
    						});
    						
    					}
    					if (!rightE || rightE.length==0) { //没有下一个了
    						arrowRightE.addClass("disabled");
    						index = arrowRightE.attr("maxIndex");
    						rightE = getTitleItem(multiTitleContainerE, index, options); //取最后一个
    					}
    					showRight(multiTitleContainerE, rightE, options);
                    }
                });
    			// 向左移动一个标题
    			arrowLeftE.click(function() {
    				if (!arrowLeftE.hasClass("disabled")) { //未设置为不可用
    					var index = arrowLeftE.attr("index");
    					var leftE;
    					if (index || index=="0") { //有值，当值为0时index返回false所以需要单独判断
    						index--;
    						if (index<0) { //负值会从后往前选
    							index = 0;
    						}
    						leftE = getTitleItem(multiTitleContainerE, index, options);
    					} else {
    						var items = multiTitleContainerE.children("."+options.cssTitleItem);
    						for (var i = items.length-1;i>=0;i--) {
    							if (!leftE) {
    								var $this = $(items[i]);
    								// console.log(i+" "+$this.html()+" "+$this.offset().left+" "+$this.outerWidth()+" "+arrowLeftE.offset().left);
    								if ($this.offset().left -5 < arrowLeftE.offset().left) { //包含内外边距
    									// alert(i+" "+$this.html());
    									leftE = $this;
    									index = i;
    									break;
    								}
    							}
    						}
    						
    					}
    					if (!leftE || leftE.length==0 || index ==0) { //没有下一个了
    						arrowLeftE.addClass("disabled");
    						index = 0;
    						leftE = getTitleItem(multiTitleContainerE, 0, options); //取第一个
    					}
    					showLeft(multiTitleContainerE, leftE, options);
    				}
                });
    		} else {
    			if (activeIndex == maxIndex) {
                	arrowRightE.addClass("disabled");
				}
    			arrowRightE.attr("maxIndex", maxIndex);
			}
    		if (options.more=="none") {
    			arrowRightE.addClass("arrow-r-n"); // 右侧居右
            }
    		
        } else {
        	containerE.find(".more").remove();
        	if (options.more != "always") { //不一直显示则移除
        		containerE.find(".arrow-x").remove();
            }
        	multiTitleContainerE.removeClass("mtab-title-l");
		}
    	if ((options.more=="auto" && mareShow) || options.more=="always") { // 自动 或  //总是显示
    		var arrowMoreE; //更多
    		var arrowMoreHtml = '<div class="arrow-x"><a class="arrow bg-on" title="点击查看更多"></a>' +
    		'<ul class="mul-more" style="display:none;">';
    		
    		multiTitleContainerE.find("."+options.cssTitleItem).each(function() {
    			var itemE = $(this);
    			var title = getTitleItemTitle(itemE);
    			//console.log(title)
    			arrowMoreHtml += '	<li ';
    			if (itemE.hasClass(options.cssTitleActive)) {
    				arrowMoreHtml += '	class="' + options.cssTitleActive + '" ';
    			}
    			arrowMoreHtml += '	><a >' + title + '</a></li>';
    		});
    		arrowMoreHtml += '	</ul></div>'
    			arrowMoreE = $(arrowMoreHtml);
    		containerE.find(".arrow-x").remove();
    		multiTitleContainerE.after(arrowMoreE);
    		var moreUlEle = arrowMoreE.find(".mul-more");
    		arrowMoreE.find(".arrow").click(function() { // 显示/隐藏更多
    			moreUlEle.toggle();
    			return false;
    		});
    		
    		$(document).click(function() {
    			moreUlEle.hide();
    		});
    		// 点击
    		moreUlEle.find("li").click(function() {
    			var thisIndex = $(this).index();
    			getTitleItem(multiTitleContainerE, thisIndex, options).click();
    		});
        } 
    	
	}
	/**
	* @description {Json} 默认参数
	* @field
	*/
	$.fn.multiTab.defaults = {
		/** 头部容器样式，包含更多，标题，箭头等的容器 */
		cssHeader : "multi-tab-header"
		/** 标题容器样式 */
		,cssTitle : "mtab-title"
		/** 标题条目样式 */
		,cssTitleItem : "title"
		/** 内容容器样式 */
		,cssContent : "mtab-content"
		/** 内容条目样式 */
		,cssContentItem : "mtab-item"
		/** 内容条目选中样式 */
		,cssContentActive : "multi-show" 
		/** 标题条目选中样式，及更多条目选中样式 */
		,cssTitleActive : "on" 
		/** 更多的显示状态，auto表示自动处理超过宽度时显示，always表示一直显示，none表示不显示 */
		,more : "auto"
		/** 完成后的回调方法 */
		,onload:null
	}
	/**
	* @description 输出选中对象的个数到控制台
	* @param {jQueryObject} $obj 选中对象
	*/
	function debug(msg) {
		if (window.console && window.console.log){
			window.console.log('debug log: ' + msg);
		}
	};
})(jQuery);