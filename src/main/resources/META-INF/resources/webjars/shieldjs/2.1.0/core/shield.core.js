;(function($){
	/**
	 * 主函数，可直接调用，建议通过ShieldJS.init调用
	 * @author 吕凯
	 * @param {json} options - 配置参数{@link ShieldJS.core._defaults}
	 * 
	 */
	ShieldJS.core.main = function(options){
		for(var optionKey in options){
			if(!_defaults[optionKey]) {
				ShieldJS.error("参数"+optionKey+"不存在，请检查是否配置正确！");
			}
		}
		ShieldJS.core.options = $.extend({},_defaults, options);
		getPageFrameEle(ShieldJS.core.options); //页面框架元素获取
		//通用化
		$("body").shieldBindHtmlAction(); //初始化页面内容
		ShieldJS.core.initMainPage(ShieldJS.topBarE, ShieldJS.topMenuE);
	};
	/**
	 * 初始化之后的参数
	 * @author 吕凯
	 * @type {json} 
	 * 
	 */
	ShieldJS.core.options = {};
	//-----------------------------------------------------------------------------
	//-------------------------------以下为公开方法---------------------------------
	//-----------------------------------------------------------------------------
	
	/**
	 * 绑定Html动作处理
	 * @author 吕凯
	 * @param {Array} methodNames - 调用方法名，字符串数组，【非必需】，不写则调用全部方法
	 * @param {jQueryObject} contentE - 内容dom【必需】jQuery对象
	 * @param {jQueryObject} menuliSelectE - 左侧栏选中的menu【非必需】jQuery对象
	 * @param {jQueryObject} topMenuSelectE - 导航栏选中的menu【非必需】jQuery对象
	 * @param {jQueryObject} searchForm -搜索表单【非必需】jQuery对象
	 * @param {ShieldJS.dialog} shieldDialog - 弹出框对象【非必需】shieldDialog对象
	 */
	ShieldJS.core.bindHtmlAction = function(methodNames, contentE, menuliSelectE, topMenuSelectE, searchForm, shieldDialog){
		var methodNames = arguments[0];
		if (!$.isArray(methodNames)) { //未指定调用方法
			shieldDialog = arguments[4];
			searchForm = arguments[3];
			topMenuSelectE = arguments[2];
			menuliSelectE = arguments[1];
        	contentE = arguments[0];
        	methodNames = null;
		}
		var actions = $.extend({},ShieldJS.HtmlAction.HTMLEleActions);
		var options = ShieldJS.core.options;
		var foreachObj = actions;
		//获取html元素action处理
		if (methodNames) {
			foreachObj = methodNames;
        }
		var hasError = false;
		var bindBeforeCallbackE = findAndMarkEle(contentE, '[bindBefore]'); //绑定前处理
		
		if (bindBeforeCallbackE.length > 0) { // 有搜索表单时的处理(class="searchForm")
			var callbackFn = bindBeforeCallbackE.attr("bindBefore");
			callbackFn = eval(callbackFn); //字符串转换为方法
			if ($.isFunction(callbackFn)) {
				callbackFn(bindBeforeCallbackE); //需要自定义
			}
		} 
		
		for(var actionName in foreachObj){
			if ($.isArray(foreachObj) && !foreachObj.hasOwnProperty(actionName)) { //未指定调用方法
				continue ; //跳出本次循环
			}
			if (methodNames) {
				actionName = foreachObj[actionName]; //数组返回的下标
	        } 
			try {
				// 无checkFn函数的直接调用
				if (!$.isFunction(actions[actionName].checkFn)) {
					actions[actionName].fn(contentE, menuliSelectE, topMenuSelectE, searchForm, shieldDialog);
				} else if ($.isFunction(actions[actionName].checkFn)) {
					// 有checkFn函数的检测完后调用
					var ele = actions[actionName].checkFn(contentE, menuliSelectE, topMenuSelectE, searchForm, shieldDialog);
					if (ele) {
						actions[actionName].fn(ele, menuliSelectE, topMenuSelectE, searchForm, shieldDialog);
					}
				}
			} catch (e) {
				printCallStack(e);
				hasError = true;
				ShieldJS.debug("方法出错！")
				ShieldJS.alert("消息提示", "方法异常："+actionName+" "+e, "error");
				if(options && $.isFunction(options.bindHtmlErrorCallback) ){ //成功错误回调函数
					options.bindHtmlErrorCallback(actionName, e);
				}
				break;
//				return false;
			}
		}
		var bindAfterCallbackE = findAndMarkEle(contentE, '[bindAfter]'); //绑定后处理
		if (bindAfterCallbackE.length > 0) { // 自定义回调处理
			var callbackFn = bindAfterCallbackE.attr("bindAfter");
			callbackFn = eval(callbackFn); //字符串转换为方法
			if ($.isFunction(callbackFn)) {
				callbackFn(bindAfterCallbackE); //需要自定义
			}
		}
		if(!hasError && options && $.isFunction(options.bindHtmlSuccCallback) ){ //成功回调函数
			options.bindHtmlSuccCallback();
		}
		if(options && $.isFunction(options.bindHtmlCallback) ){ //回调函数
			options.bindHtmlCallback();
		}
	};
	/**
	 * 页面框架化处理：：：处理整个内容区
	 * @author 吕凯
	 * @param {jQueryObject} contentE - 内容dom【必需】（以下均为jquery对象）
	 * @param {jQueryObject} siderMenuClickEle - 左侧栏选中的menu【非必需】
	 * @param {jQueryObject} topMenuSelectE - 头部导航栏选中的menu【非必需】
	 */
	ShieldJS.core.contentAreaDeal = function(contentE, siderMenuClickEle, topMenuClickEle){
		var comPeriodContentE = findAndMarkEle(contentE, '.shieldTabContent'); //多页签处理
		if (comPeriodContentE.length > 0) {//多页签处理
			var shieldHeader = contentE.find('.shieldHeader');
			shieldAClickFn(comPeriodContentE, shieldHeader, siderMenuClickEle, topMenuClickEle);
		} else { //普通页签
			contentWithSubmitDeal(contentE, siderMenuClickEle, topMenuClickEle);
		}
		return true;
	};
	/**
	 * 页面框架化处理：：：主页面操作 默认实现如果不满足实际使用可以覆盖对应的方法
	 * @param topBarE {jQueryObject} 头部bar，用于与内容页的交互跳转
	 * @param topMenuE {jQueryObject} 头部菜单
	 */
	ShieldJS.core.initMainPage = function(topBarE, topMenuE){
		if(!topMenuE){
			topMenuE = $("#navMenu");
		}
		var topMenus = findAndMarkEle(topMenuE, ".menu")
		topMenus.click(function(event, doClick){ //doclick我传参
			doClick = doClick||'true'; //是否执行点击，默认true
			
			var topMenuClickEle = $(this); //头部导航栏选中
			ShieldJS.activeTopMenuE = topMenuClickEle;
			
			if (hasAttr(topMenuClickEle, "clickExtendFn")) { //自身的扩展点击事件，返回false则不执行原方法，返回true则继续原方法
				var clickExtendFnName = topMenuClickEle.attr("clickExtendFn");
				clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
				if ($.isFunction(clickExtendFnName)) {
					if(!clickExtendFnName(event, topMenuClickEle, topMenuE, contentE)){
						return false;
					}
				}
			} else if (hasAttr(topMenuE, "clickExtendFn")) { //父类的扩展点击事件，建议直接绑定到父类，方便管理
				var clickExtendFnName = topMenuE.attr("clickExtendFn");
				clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
				if ($.isFunction(clickExtendFnName)) {
					if(!clickExtendFnName(event, topMenuClickEle, topMenuE, contentE)){
						return false;
					}
				}
			}
			
			topMenuE.find(".menu").removeClass(ShieldJS.core.options.navMenuCurClass);
			topMenuClickEle.addClass(ShieldJS.core.options.navMenuCurClass);
			var url = topMenuClickEle.data("url");
			if (topMenuClickEle.data("url")) {
				ShieldJS.ajax.get(ShieldJS.siderE, url, {}, function(){
					ShieldJS.core.bindHtmlAction(ShieldJS.siderE); //初始化左侧栏内容
					ShieldJS.core.initLeftMenu(ShieldJS.siderE, topBarE, topMenuClickEle, true);
				});
			} else { //无url
				var noUrlMenuEles = topMenuE.find(".menu:not([data-url])");
				var menuIndex = noUrlMenuEles.index(topMenuClickEle);
				var liMenuContainerE = ShieldJS.siderE.find(".shield-menus").hide().eq(menuIndex);
				liMenuContainerE.show();
				if (hasAttr(topMenuClickEle, "action")) { //使用action来异步加载
					var actionUrl = topMenuClickEle.attr("action");
					ShieldJS.ajax.get(liMenuContainerE, actionUrl, {}, function(){
						liMenuContainerE.removeClass("shieldMarked_shield-menus");
						ShieldJS.core.bindHtmlAction(liMenuContainerE); //初始化左侧栏内容
						ShieldJS.core.initLeftMenu(ShieldJS.siderE, topBarE, topMenuClickEle, true);
						//选中默认左侧菜单节点
						if (doClick == 'true') { //执行点击操作
							var siderLiMenusE = liMenuContainerE.find('.shield-menu');
							setDefaultSelectNode(liMenuContainerE, siderLiMenusE, topMenuClickEle, ShieldJS.siderE, true);
						}
					});
                } else {
                	//选中默认左侧菜单节点
    				if (doClick == 'true') { //执行点击操作
    					var siderLiMenusE = liMenuContainerE.find('.shield-menu');
    					setDefaultSelectNode(liMenuContainerE, siderLiMenusE, topMenuClickEle, ShieldJS.siderE, true);
    				}
				}
			}
			// 执行父类点击操作
			var doParClickEle = null;
			if (hasAttr(topMenuClickEle,"doParClickEle")) {
				doParClickEle = topMenuClickEle.attr("doParClickEle");
			}
			if (doParClickEle) {
				var parEle ;
				if (doParClickEle == "..") { //直接上级元素
					parEle = topMenuClickEle.parent();
                } else {
                	parEle = topMenuClickEle.closest(doParClickEle);
				}
				parEle.click();
            }
//			return false; //阻止冒泡
		});
		ShieldJS.core.initLeftMenu(ShieldJS.siderE, topBarE, null, false); //初始化菜单用于非ajax获取的左侧菜单，不设定默认选中的条目
		//选中
		setTimeout(function() { //延时，给其他事件的绑定时间
			var selectIndex = 0;
			if (hasAttr(topMenuE, "selectNode")) { //默认选中节点，从0开始
				selectIndex = Number.parseInt(topMenuE.attr("selectNode"));
			}
			if (selectIndex != -1) { //-1表示不选中
				var topMenu = topMenuE.find(".menu")
				if (topMenu.length >0) {
					var clickEle = topMenu.eq(selectIndex);
					var doclick = topMenuE.data("doclick") || "true";
					console.log('doclick=='+doclick+" "+topMenuE.data("doclick"));
					if (doclick == "true") {
						clickEle.click();
					}
                }
            }
		}, 10);
	}
	/**
	 * 左侧栏初始化，内置了3中菜单处理，1、普通菜单（选择器.shield-menus，方法ShieldJS.core.initLeftCommonMenu） 
	 * 2、树形菜单（选择器.shieldZTreeMenu,方法ShieldJS.core.initLeftZtreeMenu） 3、懒加载菜单（选择器.shieldLazyMenu,方法ShieldJS.core.initLeftExtendMenu）
	 * 以上三个方法的默认实现如果不满足实际使用可以覆盖对应的方法，如果不在这三种里可以扩展（ShieldJS.core.initLeftExtendMenu）
	 * @param siderE {jQueryObject} 左侧菜单栏对象
	 * @param topBarE {jQueryObject} 头部bar
	 * @param topMenuClickEle {jQueryObject} 当前选中的头部菜单
	 * @param setDefaultSelect {Boolean} 是否设定默认选中项
	 */
	ShieldJS.core.initLeftMenu = function(siderE, topBarE, topMenuClickEle, setDefaultSelect){
		//左侧栏菜单
		var liMenuContainerE = findAndMarkEle(siderE, ".shield-menus"); //普通菜单容器，一般为ul>li
		var ztreeMenuContainerE = findAndMarkEle(siderE, ".shieldZTreeMenu"); //树形菜单容器
		var shieldLazyMenuContainerE = findAndMarkEle(siderE, ".shieldLazyMenu"); //延迟加载的菜单

		//标准菜单ul、li
		if (liMenuContainerE.length>0) {
			ShieldJS.core.initLeftCommonMenu(liMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect);
		//树形菜单li
		} else if (ztreeMenuContainerE.length > 0 ) {
			ShieldJS.core.initLeftZtreeMenu(ztreeMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect);
		// 懒加载菜单
		} else if (shieldLazyMenuContainerE.length > 0 ) {
			ShieldJS.core.initLeftLazyMenu(shieldLazyMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect);
		} else {
			ShieldJS.core.initLeftExtendMenu(shieldLazyMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect);
		}
	};
	/**
	 * 左侧栏初始化::普通菜单的处理，一般为ul>li
	 * @param liMenuContainerE {jQueryObject} 左侧菜单栏普通菜单对象容器
	 * @param siderE {jQueryObject} 左侧菜单栏容器对象
	 * @param topBarE {jQueryObject} 头部bar
	 * @param topMenuClickEle {jQueryObject} 当前选中的头部菜单
	 * @param setDefaultSelect {Boolean} 是否设定默认选中项
	 */
	ShieldJS.core.initLeftCommonMenu = function(liMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect){
		siderE.closest(".shield-side").removeClass("admin113-side-big");
		ShieldJS.contentMainE.css("left",siderE.closest(".shield-side").width());
		var siderLiMenusE = findAndMarkEle(liMenuContainerE, '.shield-menu');
		
		if (siderLiMenusE.length > 0) {
			siderLiMenusE.each(function(event) {
				var siderMenuClickEle = $(this); //左侧菜单栏选中
				
				siderMenuClickEle.click(function() {
					
					ShieldJS.activeSiderE = siderMenuClickEle;
					
					if (hasAttr(siderMenuClickEle, "clickExtendFn")) { //自身的扩展点击事件，返回false则不执行原方法，返回true则继续原方法
						var clickExtendFnName = siderMenuClickEle.attr("clickExtendFn");
						try {
							clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
						} catch (e) {
							ShieldJS.error("菜单自定义点击事件方法"+clickExtendFnName+"出错，请检查方法定义！");
						}
						if ($.isFunction(clickExtendFnName)) {
							if(!clickExtendFnName(event, liMenuContainerE, siderMenuClickEle, ShieldJS.contentMainE)){
								return false;
							}
						}
					} else if (hasAttr(liMenuContainerE, "clickExtendFn")) { //父类的扩展点击事件，建议直接绑定到父类，方便管理
						var clickExtendFnName = liMenuContainerE.attr("clickExtendFn");
						try {
							clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
						} catch (e) {
							ShieldJS.error("菜单自定义点击事件方法"+clickExtendFnName+"出错，请检查方法定义！");
						}
						if ($.isFunction(clickExtendFnName)) {
							if(!clickExtendFnName(event, liMenuContainerE, siderMenuClickEle, ShieldJS.contentMainE)){
								return false;
							}
						}
					}
					ShieldJS.core.leftCommonMenuClickDoing(event, liMenuContainerE, siderMenuClickEle, ShieldJS.contentMainE);
//				siderLiMenusE.removeClass(ShieldJS.core.options.liMenuCurClass);
//				siderMenuClickEle.addClass(ShieldJS.core.options.liMenuCurClass); //重置选中样式
				});
				
			});
			
			//菜单默默认点击事件，区分通过导向菜单过来的
			setDefaultSelectNode(liMenuContainerE, siderLiMenusE, topMenuClickEle, siderE, setDefaultSelect);
        }
	};
	/**
	 * @description 设定默认选中的节点（执行点击）
	 * @field  （均为jquery对象）
	 * options：配置参数
	 */
	var setDefaultSelectNode = function(containerE, selectEles, topMenuClickEle, siderE, setDefaultSelect){
		// 头部信息不为空，点击了导向菜单shieldForward
		if (selectEles && selectEles.length >0) {
			if (setDefaultSelect) {
				var shieldForwardEles = $("body").find(".shieldForward");
				if (shieldForwardEles.length > 0) {
					shieldForwardEles.each(function(){
						var shieldForwardEle = $(this);
						if (hasAttr(shieldForwardEle, "toShieldMenu") && shieldForwardEle.data("forwarclickdo")) {//有属性才处理
							var toShieldMenuArr = shieldForwardEle.attr("toShieldMenu").split(">");
							if (toShieldMenuArr.length > 1) {
								var toNavMenuE = $(toShieldMenuArr[0]);
								if (topMenuClickEle.is(toNavMenuE)) { //头部菜单是同一个对象
									siderE.find(toShieldMenuArr[1]).click(); //左侧菜单点击
									//siderE.scrollTop(0);
									var t_a = siderE.find(toShieldMenuArr[1]).offset();
									//siderE.offset().top距顶部的高度 siderE.height()【siderE[0].offsetHeight】为可视高度 siderE[0].scrollHeight为实际高度
									//scrollTop卷起的高度，头部0底部实际高度
									// a.offset().top+siderE.scrollTop()=siderE.height()/2+siderE.offset().top+需要滚动的距离
									siderE.animate({scrollTop:(t_a.top+siderE.scrollTop()-siderE.height()/2-siderE.offset().top)+ "px"}, 1000); //滚动到窗口中间
									if (toShieldMenuArr.length == 2) { //2级导航到此结束，如果超过2级导航则后面继续处理，如多页签的情况等
										shieldForwardEle.removeData("forwarclickdo"); //跳转完成移除标记
									}
									setDefaultSelect =  false; //
								}
							}
						}
					});
				}
	        }
			if (setDefaultSelect) { //菜单默默认点击事件，区分通过导向菜单过来的
				setTimeout(function() { //延时，给其他事件的绑定时间
					var selectIndex = 0;
					if (hasAttr(containerE, "selectNode")) { //默认选中节点，从0开始
						selectIndex = parseInt(containerE.attr("selectNode"));
					}
					if (selectIndex != -1 && selectEles.eq(selectIndex).length > 0) {
						selectEles.eq(selectIndex).click();
					}
				}, 100);
			}
		}
	} 
	/**
	 * 左侧普通菜单栏点击事件::普通菜单的处理，一般为ul>li
	 * @param event {Event} 事件对象
	 * @param liMenuContainerE {jQueryObject} 左侧菜单栏普通菜单对象容器
	 * @param siderMenuClickEle {jQueryObject} 左侧菜单点击的菜单对象
	 * @param contentMainE {jQueryObject} 内容区
	 */
	ShieldJS.core.leftCommonMenuClickDoing  = function(ele, liMenuContainerE, siderMenuClickEle, contentMainE){
		
	}
	/**
	 * 左侧栏初始化::树形菜单的处理，默认采用ztree
	 * @param liMenuContainerE {jQueryObject} 左侧菜单栏普通菜单对象容器
	 * @param siderE {jQueryObject} 左侧菜单栏容器对象
	 * @param topBarE {jQueryObject} 头部bar
	 * @param topMenuClickEle {jQueryObject} 当前选中的头部菜单
	 * @param setDefaultSelect {Boolean} 是否设定默认选中项
	 */
	ShieldJS.core.initLeftZtreeMenu = function(ztreeMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect){
		ShieldJS.debug("tree");
		siderE.closest(".shield-side").addClass("admin113-side-big");
		ShieldJS.contentMainE.css("left",siderE.closest(".shield-side").width());
		var getFont = function(treeId, node) {
			return node.font ? node.font : {};
		}
		var jsTreeSetting = {
			data : {
				simpleData : {
					enable : true
				}
			},
			view : {
				showIcon : false,
				fontCss: getFont,
				nameIsHTML: true
			},
			callback : {
				onClick : function(e, treeId, treeNode) {
					if(treeNode){
						var containerE = $("#"+treeId);
						if (hasAttr(containerE,"clickExtendFn")) { //扩展点击事件
							var clickExtendFnName = containerE.attr("clickExtendFn");
							clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
							if($.isFunction(clickExtendFnName)){
								if(!clickExtendFnName(e, treeId, treeNode, contentE)){
									return false;
								}
							}
						}
						
						var url = treeNode.urlNode;
						if (url) { //内容中自带链接
							var params = {};
							ShieldJS.ajax.get(contentE, url, params, function() {
								//内容区处理
								if(!ShieldJS.core.contentAreaDeal(contentE, null, topMenuClickEle)){
									return false;
								}
								
							});
						} else if (containerE.data("urlNode")) { //ul中公共链接
							var url = containerE.data("urlNode");
							var paramAdapter = containerE.attr("paramAdapter");
							var params = {};
							if (paramAdapter) {
								//paramAdapter="typeId=id#newsType"
								var paramAdapters = paramAdapter.split("#"); //#号分隔
								for(var i=0;i<paramAdapters.length;i++){
									var paramName = paramAdapters[i];
									if (paramName.indexOf("=")!=-1) {
										var paramNames = paramName.split("="); //如果key与提交不一致用=号分隔
										var paramsKey = paramNames[0];
										var treeNodeKey = paramNames[1];
										params[paramsKey] = treeNode[treeNodeKey];
									} else {
										params[paramName] = treeNode[paramName];
									}
								}
								ShieldJS.ajax.get(colContentEle, url, params, function() {
									//内容区处理
									ShieldJS.contentMainE.css("left", siderE.closest(".shield-side").width()+colContentEle.width());
									if(!ShieldJS.core.contentAreaDeal(colContentEle, null, topMenuClickEle)){
										return false;
									}
									
								});
							} else {
								ShieldJS.error("未定义参数选择属性：请在"+treeId+"容器上添加paramAdapter属性并添加参数名，多个参数用#号连接");
								return false;
							}
						} 
					}
				}
			}
		};
		var searchEles = findAndMarkEle(siderE, '.shieldZTreeSearch'); //搜按钮
		var loadZtree = function() {
			ztreeMenuContainerE.each(function(i, n){
				var treeE = $(this);
				if(!$.fn.zTree){
					ShieldJS.error("缺少依赖JS库：ZTree！"+treeE.html());
					return false;
				}
				if(typeof initZTree != "undefined" && $.isFunction(eval("initZTree"))){
					initZTree(treeE, jsTreeSetting); //初始化，可做参数或其他预处理
				}
				if(treeE.data("url")){ //有链接才处理
					if(hasAttr(treeE, "asyncsetting")){ //异步请求
						jsTreeSetting.async = eval(treeE.attr("asyncsetting"));
					}
					var url = treeE.data("url");
					var params = {};
					if (hasAttr(treeE,"paramSelector")) { //参数选择器
						params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params, siderE); //参数选择器 siderE.find(paramSelector).serializeJson();
						if ($.isEmptyObject(params)) {
							ShieldJS.alert("消息提示", "参数为空！", "error");
							return false;
						}
					}
					params.timestamp = new Date().getTime(); //时间戳防缓存
					params = getParams(treeE, params);
					$.getJSON(url, params, function(json) {
						if (json && json.length > 0) {
							$.fn.zTree.init(treeE, jsTreeSetting, json);
							var treeEId = treeE.attr("id");
							var treeObj = $.fn.zTree.getZTreeObj(treeEId);
							if(hasAttr(treeE,"selectNode")){ //默认选中节点
								var selectNode = treeE.attr("selectNode");
								var defaultName = "rownum"; //默认属性
								var selectNodeValue= selectNode;
								if (selectNode.indexOf("=") != -1) {
									defaultName = selectNode.split("=")[0];
									selectNodeValue = selectNode.split("=")[1];
								}
								var node = treeObj.getNodeByParam(defaultName, selectNodeValue);
								treeObj.selectNode(node);
								treeObj.setting.callback.onClick(null, treeObj.setting.treeId, node);
							}
						} else {
							treeE.html('没有数据！');
						}
					});
					if (searchEles.length > 0) {
						var searchE = searchEles.eq(i);
						searchE.click(function(event){
							var params = {};
							if (hasAttr(treeE,"paramSelector")) { //参数选择器
								params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params, siderE); //参数选择器siderE.find(paramSelector).serializeJson();
								if ($.isEmptyObject(params)) {
									ShieldJS.alert("消息提示", "参数为空！", "error");
									return false;
								}
							}
							params.timestamp = new Date().getTime(); //时间戳防缓存
							params = getParams(treeE, params);
							if (hasAttr(searchE,"clickExtendFn")) { //扩展点击事件
								var clickExtendFnName = searchE.attr("clickExtendFn");
								clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
								if($.isFunction(clickExtendFnName)){
									if(!clickExtendFnName(event, params, treeE, jsTreeSetting)){
										return false;
									}
								}
							}
							//未扩展，仍执行原查询
							$.getJSON(url, params, function(json) {
								if (json && json.length > 0) {
									$.fn.zTree.init(treeE, jsTreeSetting, json);
								} else {
									treeE.html('没有数据！');
								}
							});
						});
					}
				}
				
			});
		};
		loadZtree();
	};
	/**
	 * 左侧栏初始化::懒加载菜单的处理
	 * @param liMenuContainerE {jQueryObject} 左侧菜单栏普通菜单对象
	 * @param siderE {jQueryObject} 左侧菜单栏容器对象
	 * @param topBarE {jQueryObject} 头部bar
	 * @param topMenuClickEle {jQueryObject} 当前选中的头部菜单
	 * @param setDefaultSelect {Boolean} 是否设定默认选中项
	 */
	ShieldJS.core.initLeftLazyMenu = function(shieldLazyMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect){
		siderE.closest(".shield-side").addClass("admin113-side-big");
		ShieldJS.contentMainE.css("left",siderE.closest(".shield-side").width());
		
		var targetEPar = null;
		if (hasAttr(shieldLazyMenuContainerE,"target")) { //ul中公共链接
			targetEPar = getjQueryObj(shieldLazyMenuContainerE.attr("target"));
		} 
		var siderLiMenusE = findAndMarkEle(shieldLazyMenuContainerE, '.shield-menu');
		siderLiMenusE.each(function(event) {
			
			var siderMenuClickEle = $(this); //左侧菜单栏选中
			
			if (hasAttr(siderMenuClickEle,"clickExtendFn")) { //扩展点击事件
				var clickExtendFnName = liMenuContainerE.attr("clickExtendFn");
				clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
				if ($.isFunction(clickExtendFnName)) {
					if(!clickExtendFnName(event, liMenuContainerE, siderMenuClickEle, contentE)){
						return false;
					}
				}
			}
			
			if (siderMenuClickEle.data("url")) {//有链接才处理
				siderMenuClickEle.click(function() {
					ShieldJS.activeSiderE = siderMenuClickEle;
					
					var params = {};
					if (hasAttr(siderMenuClickEle,"paramSelector")) { //参数选择器
						params = ShieldJS.HtmlAction.checkAnGetParamSelector(siderMenuClickEle, params, siderMenuClickEle); //参数选择器
						if ($.isEmptyObject(params)) {
							ShieldJS.alert("消息提示","参数为空！", "error");
							return false;
						}
					}
					var hrefmenu = siderMenuClickEle.data("url");
					ShieldJS.ajax.get(targetEPar, hrefmenu, params, function() {
						//内容区处理
						contentMainE.css("left", siderE.closest(".shield-side").width() + colContentEle.width());
						if(!ShieldJS.core.contentAreaDeal(targetEPar, siderMenuClickEle, topMenuClickEle)){
							return false;
						}
						
					});
					return false; //阻止冒泡
                });
				//siderE.find("li").removeClass(ShieldJS.core.options.liMenuCurClass);
				//siderMenuClickEle.addClass(ShieldJS.core.options.liMenuCurClass); //选中
			}
		});
	};
	/**
	 * 左侧栏初始化::扩展菜单（空方法，用于框架扩展）
	 * @param liMenuContainerE {jQueryObject} 左侧菜单栏普通菜单对象
	 * @param siderE {jQueryObject} 左侧菜单栏容器对象
	 * @param topBarE {jQueryObject} 头部bar
	 * @param topMenuClickEle {jQueryObject} 当前选中的头部菜单
	 * @param setDefaultSelect {Boolean} 是否设定默认选中项
	 */
	ShieldJS.core.initLeftExtendMenu = function(shieldLazyMenuContainerE, siderE, topBarE, topMenuClickEle, setDefaultSelect){
	}
	//-----------------------------------------------------------------------------
	//-------------------------------以下为私有方法---------------------------------
	//-----------------------------------------------------------------------------
	
	/**
	 * 私有变量，框架的默认配置，外部不可见
	* @namespace ShieldJS.core._defaults
	* @property {String/jQueryObj}  topBarE     - 头部信息区（jQuery对象或选择器）
	* @property {String/jQueryObj}  topMenuE    - 头部菜单区（jQuery对象或选择器）
	* @property {String/jQueryObj}  siderE      - 左侧栏（jQuery对象或选择器）
	* @property {String/jQueryObj}  contentMainE  - 主内容区（jQuery对象或选择器）
	* @property {String/jQueryObj}  contentNavTabE - 主内容区navTab（jQuery对象或选择器）
	* @property {String/jQueryObj}  contentActiveTabE - 主内容区当前活动Tab（jQuery对象或选择器）
	* @property {String}  navMenuCurClass - 内容区多页签选项卡选中的样式
	* @property {String}  headerCurClass - 内容区多页签选项卡选中的样式
	* @property {String}  liMenuCurClass - 左侧菜单选中的样式
	* @property {String}  bindHtmlErrorCallback - html绑定失败回调方法(actionName, e) 
	* @property {String}  bindHtmlSuccCallback - html绑定成功回调方法
	* @property {String}  bindHtmlCallback - html绑定完成回调方法（成功失败都会调用）
	* @property {String}  templPath - 模板路径，不填找templ目录下的文件，自定义目录时文件名需要保持一致
	*/
	var _defaults = {
		/** 头部信息 */
		topBarE : '#userBar'
		/** 头部菜单区 */
		,topMenuE : '#navMenu' //头部菜单区
		/** 左侧栏 */
		,siderE : '#leftBar'
		/** 主内容区（容器） */
		,contentMainE : '#content'
		/** 主内容区navTab */
		,contentNavTabE : '#contentNavTab'
		/** 主内容区当前活动Tab */
		,contentActiveTabE : '.tab-pane.active'
		/** 菜单栏中菜单选中的样式*/
		,navMenuCurClass : 'cur'
		/** 内容区多页签选项卡选中的样式 */
		,headerCurClass : 'cur'
		/** 左侧菜单选中的样式*/
		,liMenuCurClass : 'active'
		/** html绑定失败回调方法(actionName, e) */
		,bindHtmlErrorCallback : null
		/** html绑定成功回调方法 */
		,bindHtmlSuccCallback : null
		/** html绑定完成回调方法（成功失败都会调用）*/
		,bindHtmlCallback : null
		/** 模板路径，扩展字段默认不需要，用于一些需要模板的实现，不填找templ目录下的文件，自定义目录时文件名需要保持一致 */
		,templPath : 'templ'
	};
	/**
	 * @description 获取框架结构的页面元素
	 * @field  （均为jquery对象）
	 * options：配置参数
	 */
	var getPageFrameEle = function(options) {
		ShieldJS.topBarE = options.topBarE ? getjQueryObj(options.topBarE) : null; //头部信息区
		ShieldJS.topMenuE = options.topMenuE ? getjQueryObj(options.topMenuE) : null; //头部菜单区
		ShieldJS.siderE = options.siderE ? getjQueryObj(options.siderE) : null; //主内容区 //左侧栏
		ShieldJS.contentMainE = options.contentMainE ? getjQueryObj(options.contentMainE) : null; //主内容区
		if(ShieldJS.topBarE && ShieldJS.topBarE.length == 0){
			ShieldJS.error("topBarE定义元素未找到，请检查是否配置正确！");
		}
		if(ShieldJS.topMenuE && ShieldJS.topMenuE.length == 0){
			ShieldJS.error("topMenuE定义元素未找到，请检查是否配置正确！");
		}
		if(ShieldJS.siderE && ShieldJS.siderE.length == 0){
			ShieldJS.error("siderE定义元素未找到，请检查是否配置正确！");
		}
		if(ShieldJS.contentMainE && ShieldJS.contentMainE.length == 0){
			ShieldJS.error("contentMainE定义元素未找到，请检查是否配置正确！");
		}
	}
	/**
	 * @description 处理页面处理（带提交事件）
	 * @field  （均为jquery对象）
	 * contentE：内容dom menuliSelectE：左侧栏选中的menu topMenuSelectE：导航栏选中的menu
	 */
	var contentWithSubmitDeal = function(contentE, menuliSelectE, topMenuSelectE){
		var searchForm = findAndMarkEle(contentE, 'form.searchForm');
		var submitForm = findAndMarkEle(contentE, 'form.submitForm');
		var shieldExtendCallback = findAndMarkEle(contentE, '.shieldExtendCallback'); //自定义处理事件的元素
		
		if (searchForm.length > 0) { // 有搜索表单时的处理(class="searchForm")
			ShieldJS.core.bindHtmlAction(contentE, menuliSelectE, topMenuSelectE, searchForm);
			// 提交表单完成后执行方法
			ShieldJS.form.initSubmit(contentE, function(formE, targetE, data){
				// 初始化页面内容
				ShieldJS.core.bindHtmlAction(contentE, menuliSelectE, topMenuSelectE, searchForm);
			});
			var notAutoSubmit = false;
			if(hasAttr(searchForm, "notAutoSubmit")){ //页面初始化后不自动提交
				notAutoSubmit = searchForm.attr("notAutoSubmit");
				notAutoSubmit = notAutoSubmit=='false' ? false : true;
			}
			if (!notAutoSubmit) { //自动提交
				searchForm.submit();
			} else {
				ShieldJS.core.bindHtmlAction(contentE, menuliSelectE, topMenuSelectE, searchForm);
			}
		} else if (shieldExtendCallback.length > 0) { // 自定义回调处理
			ShieldJS.core.bindHtmlAction(contentE, menuliSelectE, topMenuSelectE);
			var successDo = ShieldJS.HtmlAction.successDo.shieldRefresh;
			if (hasAttr(shieldExtendCallback, "successDo")) { //保存成功后的处理
				successDo = shieldExtendCallback.attr("successDo");
			}
			
			//提交表单完成后执行方法
			ShieldJS.form.initSubmit(contentE, function(formE, targetE, data){
				ShieldJS.HtmlAction.formSubmitCallback(successDo, formE, targetE, data, menuliSelectE, topMenuSelectE);
			});
			
			var callbackFn = shieldExtendCallback.attr("callback");
			callbackFn = eval(callbackFn); //字符串转换为方法
			if ($.isFunction(callbackFn)) {
				callbackFn(shieldExtendCallback, successDo); //需要自定义
			}
		} else {// 没有form的处理，可能为单页修改的页面
			ShieldJS.core.bindHtmlAction(contentE, menuliSelectE, topMenuSelectE);
			// 如果存在shieldDonothing 的类，则什么都不做，默认刷新
			if(contentE.find("."+ShieldJS.HtmlAction.successDo.shieldDonothing).length == 0){
				//提交表单完成后执行方法
				ShieldJS.form.initSubmit(contentE,function(formE, targetE, data){
					if(menuliSelectE){
						menuliSelectE.click();
					}
				});	
			}
		}
	}
	/**
	 * @description 多页签的a点击事件
	 * @field 
	 * （以下均为jquery对象）
	 * comPeriodContentE：内容dom shieldHeader：头部dom
	 * siderMenuClickEle：左侧栏点击菜单 topMenuClickEle：当前选中的头部菜单
	 */
	var shieldAClickFn = function(comPeriodContentE, shieldHeader, siderMenuClickEle, topMenuClickEle){
		var shieldHeaderAEles = findAndMarkEle(shieldHeader, 'a');
		shieldHeaderAEles.click(function() {
			var curHeaderA = $(this);
			shieldHeader.find('a').removeClass(ShieldJS.core.options.headerCurClass);
			curHeaderA.addClass(ShieldJS.core.options.headerCurClass);
			
			if (hasAttr(siderMenuClickEle,"shieldHeaderUrl")) { //父元素有链接（用于公共页面不能直接添加url的处理）
				var shieldHeaderUrl = siderMenuClickEle.attr("shieldHeaderUrl");
				var curHeaderAHref = shieldHeaderUrl;
				var params = {type : curHeaderA.attr('v')}; //用v区分类型（必需）
				params = getParams(curHeaderA,params);
				shieldTabDeal(curHeaderAHref, params, comPeriodContentE, shieldHeader, curHeaderA, siderMenuClickEle, topMenuClickEle);
				
			} else { //普通带href处理
				var juniorResult = checkJuniorE(comPeriodContentE,curHeaderA);
				if(!juniorResult.hasErr){
					var juniorE = juniorResult.juniorE;
					if (juniorE) {
						shieldAClickFn(comPeriodContentE, juniorE, siderMenuClickEle, topMenuClickEle);
					} else {
						if(curHeaderA.data("url")){ //有链接才处理
							var curHeaderAHref = curHeaderA.data("url");
							var params = {type : curHeaderA.attr('v')}; //用v区分类型（非必需）
							params = getParams(curHeaderA, params);
							shieldTabDeal(curHeaderAHref, params, comPeriodContentE, shieldHeader, curHeaderA, siderMenuClickEle, topMenuClickEle);
						} 
					}
				}
			}
			return false;
		});
		shieldHeader.find('a:eq(0):not(.noshield)').click();
	}
	/**
	 * @description 检查并处理下级容器，返回子级容器
	 * @field 
	 * （以下均为jquery对象）
	 * contentE：内容dom curHeaderClickE：当前选中的头部dom
	 */
	var checkJuniorE = function(contentE,curHeaderClickE){
		var result = {
			hasErr : false,
			juniorE :null
		}
		ShieldJS.contentMainE.find(".shieldJunior").each(function(){
			var shieldJuniorE = $(this);
			if (hasAttr(shieldJuniorE,"juniorFor")) {
				var juniorFor = shieldJuniorE.attr("juniorFor");
				if (shieldJuniorE.find(curHeaderClickE).length == 0) { //非父类元素
					if (curHeaderClickE.is($(juniorFor))) {
						shieldJuniorE.show();
						result.juniorE = shieldJuniorE;
						return shieldJuniorE; //不能直接跳出
					} else {
						shieldJuniorE.hide();
					}
				}
			} else {
				ShieldJS.error("shieldJunior下级容器未定义关联到的上级容器：请定义属性juniorFor");
				result.hasErr = true;
				return false;
			}
		});
		return result;
	}
	/**
	 * @description 处理tab格式的页面
	 * @field 
	 * href：链接 params：参数
	 * （以下均为jquery对象）
	 * comPeriodContentE：内容dom shieldHeader：头部dom
	 * curHeaderA：当前选中的头部dom menuliSelectE：左侧栏选中的menu topMenuSelectE：导航栏选中的menu
	 */
	var shieldTabDeal = function(url, params, comPeriodContentE, shieldHeader, curHeaderA, menuliSelectE, topMenuSelectE){
		var thisContentE = comPeriodContentE;
		var errCallbackFn = null;
		var completeCallbackFn = null;
		if (curHeaderA) {
			if (hasAttr(curHeaderA,"errCallbackFn")) { //返回失败方法
				var clickExtendFnName = curHeaderA.attr("errCallbackFn");
				clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
				if ($.isFunction(clickExtendFnName)) {
					errCallbackFn = function(data){
						// 如果是错误，则为json格式，自动格式化
						var json;
						try {
							json = $.parseJSON(data);
						} catch (e) {
							json = '';
						}
						if(!json){ // 转换错误时，赋值为原来的值
							json = data;
						}
						clickExtendFnName(json, thisContentE, shieldHeader, curHeaderA, menuliSelectE, topMenuSelectE);
					};
//				if(!clickExtendFnName(data, thisContentE, shieldHeader, curHeaderA, menuliSelectE, topMenuSelectE)){
//					return false;
//				}
				}
			}
			if (hasAttr(curHeaderA,"completeCallbackFn")) { //完成后回调方法
				var clickExtendFnName = curHeaderA.attr("completeCallbackFn");
				clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
				if ($.isFunction(clickExtendFnName)) {
					completeCallbackFn = function(data){
						clickExtendFnName(data, thisContentE, shieldHeader, curHeaderA, menuliSelectE, topMenuSelectE);
					};
//				if(!clickExtendFnName(data, thisContentE, shieldHeader, curHeaderA, menuliSelectE, topMenuSelectE)){
//					return false;
//				}
				}
			}
			if (hasAttr(curHeaderA,"clickExtendFn")) { //扩展点击事件
				var clickExtendFnName = curHeaderA.attr("clickExtendFn");
				clickExtendFnName = eval(clickExtendFnName); //字符串转换为方法
				if ($.isFunction(clickExtendFnName)) {
					if(!clickExtendFnName(thisContentE, shieldHeader, curHeaderA, menuliSelectE, topMenuSelectE)){
						return false;
					}
				}
			}
        }
		ShieldJS.ajax.get(comPeriodContentE, url, params, function(data) {
			if (!menuliSelectE || menuliSelectE.length == 0 ) { //针对多页签
				menuliSelectE = curHeaderA;
			}
			contentWithSubmitDeal(comPeriodContentE, menuliSelectE, topMenuSelectE);
		}, errCallbackFn, completeCallbackFn);
	}
	
	//-----------------------------------------------------------------------------
	//-------------------------------jQuery插件化----------------------------------
	//-----------------------------------------------------------------------------
	/**
	 * 页面HTML元素处理（绑定HTML处理，只在页面上邦迪html处理，如果包含tab则使用上面的方法）
	* @author kevin Lv <donggongai@126.com>
	* @class $().shieldBindHtmlAction
	* @param {json} options - 配置参数,topBarE为topBar元素或者对应的选择器,topMenu头部导航栏对应的menu元素或者对应的选择器,searchForm为搜索表单元素或者对应的选择器，dialog为弹出框对象(类型为ShieldJS.dialog)
	* 
	* @example
	* $("#content").shieldBindHtmlAction();
	* //带参数的用法 topBarE为topBar元素或者对应的选择器,topMenu头部导航栏对应的menu元素或者对应的选择器,searchForm为搜索表单元素或者对应的选择器，dialog为弹出框对象(类型为ShieldJS.dialog)
	* $("#content").shieldBindHtmlAction({leftMenu:'#leftMenu',topMenu:'#topMenu',searchForm:'.sarchForm',dialog:dialog});
	*
	* @since version 1.0
	*/
	$.fn.shieldBindHtmlAction = function(options){
		if(!options){
			options = {};
		}
		//动态绑定，页面初始化之后加入的也可以绑定
		$("body").off("click", ".shieldForward").on("click", ".shieldForward", function(event){
			var shieldForwardEle = $(this);
			if (!shieldForwardEle.data("forwardclickinit")) { //防止重复处理
				if (hasAttr(shieldForwardEle,"toShieldMenu")) { //有菜单属性才处理
					var toShieldMenuArr = shieldForwardEle.attr("toShieldMenu").split(">");
					if (toShieldMenuArr.length > 1) {
						var toNavMenu = $(toShieldMenuArr[0]); //头部菜单
						shieldForwardEle.data("forwarclickdo",toShieldMenuArr[1]); //添加属性，跳转完成后移除
						toNavMenu.click();
					}
				}
            }
		});
		/**
		* @description 处理函数
		*/
		return this.each(function() {    
			var $this = $(this);
			//使用metadata插件获取元数据
			var ops = $.metadata ? $.extend({}, options, $this.metadata()) : options;
			var leftMenuClickEle = getjQueryObj(ops.leftMenu);
			var topMenuClickEle = getjQueryObj(ops.topMenu);
			var searchFormE = getjQueryObj(ops.sarchForm);
			var shieldDialog = getjQueryObj(ops.dialog);
			ShieldJS.core.bindHtmlAction($this, leftMenuClickEle, topMenuClickEle, searchFormE, shieldDialog);
		}); 
	};
	/**
	 * 左侧菜单处理，并处理内容区
	* @author kevin Lv <donggongai@126.com>
	* @class $().shieldLeftMenu
	* @param {json} options - 配置参数,参考{@link ShieldJS.core._defaults}, topMenuClickEle头部导航栏对应的菜单选项
	* 
	* @example
	* $("#content").shieldLeftMenu();
	* //带参数的用法 topMenuClickEle头部导航栏对应的菜单选项
	* $("#content").shieldLeftMenu({topBarE:'#topBar',topMenuE:'#navMenu'});
	*
	* @since version 1.0
	*/
	$.fn.shieldLeftMenu = function(options){
		/**
		* @description 处理函数
		*/
		return this.each(function() {   
			var $this = $(this);
			//使用metadata插件获取元数据
			var ops = $.metadata ? $.extend({}, options, $this.metadata()) : options;
			ShieldJS.core.options = $.extend({},_defaults, ops);
			getPageFrameEle(ShieldJS.core.options); //页面框架元素获取
			var topMenuClickEle = getjQueryObj(ops.topMenuClickEle);
			ShieldJS.core.initLeftMenu($this, ShieldJS.topBarE, topMenuClickEle);
			$("body").shieldBindHtmlAction(); //初始化页面内容
		}); 
	};
	/**
	* 初始化页面HTML元素处理（与$.fn.shieldBindHtmlAction方法代码类似，区别在于本方法处理多页签，包含form回调方法）
	* @author kevin Lv <donggongai@126.com>
	* @class $().shieldMainContent
	* @param {json} options - 配置参数
	* 
	* @example
	* $("#content").shieldMainContent();
	* //带参数的用法 leftMenuClickE为左侧栏选中的菜单，topMenuClickEle为头部菜单选中的菜单
	* $("#content").shieldMainContent({leftMenuClickE:'#leftMenu',topMenuClickEle:'#topMenu'});
	* 
	* @since version 1.0
	*/
	$.fn.shieldMainContent = function(options){
		if(!options){
			options = {};
		}
		/**
		* @description 处理函数
		*/
		return this.each(function() {    
			var $this = $(this);
			//使用metadata插件获取元数据
			var ops = $.metadata ? $.extend({}, options, $this.metadata()) : options;
			var leftMenuClickEle = getjQueryObj(ops.leftMenuClickE);
			var topMenuClickEle = getjQueryObj(ops.topMenuClickEle);
			ShieldJS.core.contentAreaDeal($this, leftMenuClickEle, topMenuClickEle);
		}); 
	};
})(jQuery);
