#!/bin bash
pwd | xargs cd
java -jar ../../tool/compiler.jar --js core/shield.util.js core/shield.main.js \
 core/shield.core.js core/shield.HtmlAction.js \
 --js_output_file ./shieldjs.min.js