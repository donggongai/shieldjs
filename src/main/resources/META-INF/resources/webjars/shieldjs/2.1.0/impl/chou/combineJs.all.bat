%~d0
cd %~dp0
java -jar ../../../../tool/compiler.jar --js ../../depends/jquery.metadata.js  ^
 ../../depends/ztree/jquery.ztree.core.min.js ../../depends/shieldcron-generator/shieldCronGen.js ../../depends/qtips/jquery.qtip.js ^
 ../../tool/form/jquery.form.min.js ../../tool/doT.js ../../tool/formvalidate.js  ^
 ../../depends/fullcalendar/lib/moment.min.js ../../depends/fullcalendar/lib/lunar.js ../../depends/fullcalendar/fullcalendar.js ../../depends/fullcalendar/locale/zh-cn.js ^
 ../../core/shield.util.js ../../core/shield.main.js ^
 ../../core/shield.core.js ../../core/shield.HtmlAction.js ^
 modules/shieldjs.tab.js shield.chou.HtmlAction.extend.js ^
 --js_output_file shield.chou.all.min.js
