//html标签的js动作处理
(function($){
	ShieldJS.HtmlAction = {
		/**	处理成功后的回调方法名
		 * @class ShieldJS.HtmlAction.successDo */
		successDo : {
			/**	刷新，关闭dialog，并刷新搜索表单(默认)
			 * @type String */
			shieldRefresh: "shieldRefresh",
			/**	什么都不做
			 * @type String */
			shieldDonothing: "shieldDonothing",
			/**	只刷新搜索表单
			 * @type String */
			shieldReSearch: "shieldReSearch",
			/**	只关闭dialog
			 * @type String */
			shieldDialogRemove: "shieldDialogRemove",
			/**	刷新Tab（有dialog也会关闭）,用于多页签内容页的刷新
			 * @type String */
			shieldRefreshTab: "shieldRefreshTab",
			/**	刷新Tab（不关闭dialog）,用于多页签内容页的刷新
			 * @type String */
			shieldRefreshTabIgnoreDialog: "shieldRefreshTabIgnoreDialog",
			/**	跳转到新页面
			 * @type String */
			shieldForward: "shieldForward",
			/**	自定义方法
			 * @type String */
			shieldExtend: "shieldExtend" //
		},
		names : {
			shieldSelectRow : "shieldSelectRow",
			shieldSelectRowByshieldCheck : "shieldSelectRowByshieldCheck"
		},
		/**	处理成功后的回调执行方法
		 * @class ShieldJS.HtmlAction.successDoMethod */
		successDoMethod : {
			shieldRefresh : function(contentE, searchForm, menuliSelect, dialog, oldRefresh){ //刷新，关闭dialog，并刷新搜索表单(默认)
				if (dialog) {
					dialog.remove();
				}
				if (searchForm) {
					if (!oldRefresh) { //兼容旧框架的json返回，不重复刷新
						searchForm.submit();
					}
				} else if (menuliSelect) {
					menuliSelect.click();
				}
			},
			shieldDonothing : function(contentE, searchForm, menuliSelect, dialog, oldRefresh) { //什么都不做
			},
			shieldReSearch : function(contentE, searchForm, menuliSelect, dialog, oldRefresh) { //只刷新搜索表单
				if (searchForm) {
					if (!oldRefresh) { //兼容旧框架的json返回，不重复刷新
						searchForm.submit();
					}
				} else if (menuliSelect) {
					menuliSelect.click();
				}
			},
			shieldDialogRemove : function(contentE, searchForm, menuliSelect, dialog, oldRefresh) { //只关闭dialog
				if (dialog) {
					dialog.remove();
				}
			},
			shieldRefreshTabDealFn : function(contentE, searchForm, menuliSelect, dialog, closeDialog) { //刷新Tab处理函数，内部使用
				var shieldHeader = contentE.find(".shieldHeader");
				if (shieldHeader.length > 0) {
					if (closeDialog && dialog) {
						dialog.remove();
					}
					var headerCurClasses = ShieldJS.core.options.headerCurClass.replace(" ",".");
					var shieldJuniorSelectE = contentE.find(".shieldJunior."+headerCurClasses);
					if(shieldJuniorSelectE.length > 0){
						shieldJuniorSelectE.click();
					} else {
						var shieldHeaderSelectE = shieldHeader.find("."+headerCurClasses);
						if(shieldHeaderSelectE.length > 0){
							shieldHeaderSelectE.click();
						}
					}
				}
			},
			shieldRefreshTab : function(contentE, searchForm, menuliSelect, dialog, oldRefresh) { //刷新Tab,用于多页签内容页的刷新
				ShieldJS.HtmlAction.successDoMethod.shieldRefreshTabDealFn(contentE, searchForm, menuliSelect, dialog, true)
			},
			shieldRefreshTabIgnoreDialog : function(contentE, searchForm, menuliSelect, dialog, oldRefresh) { //刷新Tab（不关闭，dialog）,用于多页签内容页的刷新
				ShieldJS.HtmlAction.successDoMethod.shieldRefreshTabDealFn(contentE, searchForm, menuliSelect, dialog, false)
			},
			shieldForward : function(contentE, searchForm, menuliSelect, dialog, oldRefresh, result) { //自定义
				// TODO 待实现
			},
			shieldExtend : function(contentE, searchForm, menuliSelect, dialog, oldRefresh, result) { //自定义

			}
		},
		/**	检查成功的回调函数是否存在
		 * @param successDoName {String} 成功后的回调方法名
		 * @returns  {Boolean} 方法存在时返回true，否则返回false */
		checkSuccessDoMethodExist : function (successDoName) {
			if(!$.isFunction(ShieldJS.HtmlAction.successDoMethod[successDoName])){
				var suddceeDoExtendFn = eval(successDoName); //自定义的回调方法
				if(!$.isFunction(suddceeDoExtendFn)){
					ShieldJS.error(successDo + "成功后的回调处理函数不存在！");
					return false;
				}
			}
			return true;
		},
		/**	指定成功的回调函数,result为成功返回的结果
		 * @param successDoName {String} 成功后的回调方法名 */
		execSuccessDoMethod : function (successDoName, contentE, searchForm, menuliSelect, dialog, oldRefresh, result) {
			if($.isFunction(ShieldJS.HtmlAction.successDoMethod[successDoName])){ //预定于的回调方法
				ShieldJS.HtmlAction.successDoMethod[successDoName](contentE, searchForm, menuliSelect, dialog, oldRefresh, result);
			} else {
				var suddceeDoExtendFn = eval(successDoName); //自定义的回调方法
				if($.isFunction(suddceeDoExtendFn)){
					suddceeDoExtendFn(contentE, searchForm, menuliSelect, dialog, oldRefresh, result);
				}
			}
		},
		//post提交数据
		postData : function(url, params, contentE, searchForm, menuliSelect, shieldDialog, successDo, methodType, successCallback) {
			var method = "post";
			if ($.isFunction(methodType)) { //是函数，则为successCallback
				successCallback = methodType;
			}else {
				method = methodType;
			}
			if (method == "get") {
				ShieldJS.ajax.get(null, url, params, function(json) {
            		ShieldJS.HtmlAction.execSuccessDoMethod(successDo, contentE, searchForm, menuliSelect, shieldDialog, json.refresh, json);
            		if (successCallback) {
            			successCallback();
                    }
            	}, function(json) {
            		ShieldJS.debug("提交数据出错！" + url + " params: " + $.param(params));
            	});
            } else {
            	ShieldJS.ajax.post(url, params, function(json) {
            		ShieldJS.HtmlAction.execSuccessDoMethod(successDo, contentE, searchForm, menuliSelect, shieldDialog, json.refresh, json);
            		if (successCallback) {
            			successCallback();
                    }
            	}, function(json) {
            		ShieldJS.debug("提交数据出错！" + url + " params: " + $.param(params));
            	}, "json");
			}
			
		},
		//检查并返回参数,如果正常返回params否则返回false
		checkAnGetParamSelector : function(ele, params, contentEle){
			if (!contentEle) {
				contentEle = ShieldJS.contentMainE; //主内容区没有包含弹出框
				if (ShieldJS.dialogE) { //增加弹出框对象 ，注意此处未做回归测试，可能有问题
					contentEle = contentEle.add(ShieldJS.dialogE);
                }
            }
			
			var result = ele.paramSelector({
				/**    提示框，可以覆盖，默认采用alert，type字符型（success、error、info、warning）
			     * @param title {String} 标题
			     * @param message {String} 提示内容
			     * @param type {String} 提示类型包括：success、error、info、warning4种  */
			    alert : function(title, message, type){ 
			    	ShieldJS.alert(title, message, type);
			    },
			    validate : function(formE) { //表单验证方法
			        return ShieldJS.form.validate(formE);
			    },
			    mainEle : contentEle, //关联对象容器
			    checkedClass : ShieldJS.HtmlAction.names.shieldSelectRowByshieldCheck //被选中的复选框或单选框的样式
			});
			if (result) {
				result = $.extend(true, {}, params, result);
            }
			
			return result;
			
			/*if (hasAttr(ele, "paramSelector")) { //参数选择器
				var nodatatips = ele.data("noneTips");
				var paramSelector = ele.attr("paramSelector");
				{selectRows}表示复选框选中行的数据作为参数提交，如果只想选一部分可以在里面增加选择器
				 {selectRows>:checkbox}意思为只提交选中行的的checkbox作为参数,用'>'连接选择器，里面的选择器为jQuery选择器
				if (paramSelector.indexOf("{selectRows") != -1 ) { //选中行用>分隔内部选择器
					// 括号表示组。访问可以用group[index]来访问每组的信息  
					var linkRegx = /.*{(selectRows.*?)},?/;  
					var group = paramSelector.match(linkRegx);
					if (group.length == 2) { //如果包含则为长度为2
						var selectRows = group[1]; //selectRows
						var selectRowsEle = contentEle.find("."+ShieldJS.HtmlAction.names.shieldSelectRowByshieldCheck);
						if(selectRows.indexOf(">") > 0){ //过滤
							var selectRowsArrs = selectRows.split(">");
							selectRowsEle = selectRowsEle.find(selectRowsArrs[1]);
						}
						if (!selectRowsEle.is(":input")) { //如果不是input，继续查找内部的input
							selectRowsEle = selectRowsEle.find(":input");
						}
						if (ShieldJS.form.validate(selectRowsEle)) {
							params = $.extend(true, {}, params, selectRowsEle.serializeJson());
							if ($.isEmptyObject(params)) {
								ShieldJS.alert("消息提示", "至少选中一条信息！", "error");
								return false;
							}
                        } else {
                        	ShieldJS.debug("参数选择器验证未通过！");
                        	return false;
						}
					} else {
						ShieldJS.error("参数选择器参数设置错误，请检查{selectRows}！");
						return false;
					}
					paramSelector = paramSelector.replace(linkRegx, "");
				}
				// 用于tr中的选择器，取当前行内参数
				if (paramSelector.indexOf("{currRows") != -1 ) {//当前行用>分隔内部选择器
					// 括号表示组。访问可以用group[index]来访问每组的信息  
					var linkRegx = /.*{(currRows.*?)},?/;  
					var group = paramSelector.match(linkRegx);
					if (group.length == 2) { //如果包含则为长度为2
						var selectRows = group[1]; //selectRows
						var selectRowsEle = ele.closest("tr");
						if(selectRows.indexOf(">") > 0 ){ //过滤
							var selectRowsArrs = selectRows.split(">");
							selectRowsEle = cselectRowsEle.filter(selectRowsArrs[1]);
						}
						if (!selectRowsEle.is(":input")) { //如果不是input，继续查找内部的input
							selectRowsEle = selectRowsEle.find(":input");
						}
						if (ShieldJS.form.validate(selectRowsEle)) {
							params = $.extend(true, {}, params, selectRowsEle.serializeJson());
                        } else {
                        	ShieldJS.debug("参数选择器验证未通过！");
                        	return false;
						}
					} else {
						ShieldJS.error("参数选择器参数设置错误，请检查{currRows}！");
						return false;
					}
					paramSelector = paramSelector.replace(linkRegx, "");
				} 
				if (paramSelector) { //继续检查
					var selectEles = contentEle.find(paramSelector);
					if (ShieldJS.form.validate(selectEles)) {
						params = $.extend(true, {}, params, selectEles.serializeJson());
						if ($.isEmptyObject(params)) {
							ShieldJS.alert("消息提示", nodatatips||"数据为空或未选中，请返回检查！", "error");
							return false;
						}
					} else {
						ShieldJS.debug("参数选择器验证未通过！");
						return false;
					}
                }
			}
			return params;*/
		},
		// 检查自定义点击回调方法是否存在
		hasClickCallbackFn : function(ele, eventName) {
			var $events = $._data(ele[0], "events");
			if( $events && $events[eventName] ){
				return true;
			}
			return false;
		},
		// 自定义点击回调
		clickCallback : function(ele) {
			var eventName = "clickCallback";
			if (ShieldJS.HtmlAction.hasClickCallbackFn(ele, eventName)) {
				var $events = $._data(ele[0],"events");
				if( $events && $events[eventName] ){
					ele.trigger(eventName);
				}
            }
		},
		/**	表单提交成功后的回调方法
		 * @param successDo    {String} 成功后的回调方法名
		 * @param formE        {jQuery} 表单对象
		 * @param targetE      {jQuery} 表单提交的目标对象
		 * @param data         {Object} 返回结果的对象可能为json也可能为页面字符
		 * @param menuliSelect {jQuery} 左侧栏menu 
		 * @param navMenuSelect{jQuery} 导航栏menu 
		 * @param searchForm   {jQuery} 搜索表单  
		 * @param dialog       {ShieldJS.dialog} 弹出框对象
		 *  */
		formSubmitCallback : function(successDo, formE, targetE, data, menuliSelect, navMenuSelect, searchForm, dialog ) {
			var successDoForm = successDo;
			if (hasAttr(formE, "successDo")) { //form上保存成功后的处理
				successDoForm = formE.attr("successDo");
			}
			if (successDoForm) { //回调
				if (!ShieldJS.HtmlAction.checkSuccessDoMethodExist(successDoForm)) {
					return false;
				} else {
					// 为json格式
					var json;
					if (typeof data == 'object') {
						json = data;
					} else {
						try {
							json = $.parseJSON(data);
						} catch (e) {
							json = '';
						}
					}
					ShieldJS.HtmlAction.execSuccessDoMethod(successDoForm, ShieldJS.contentMainE, searchForm, menuliSelect, dialog, json.refresh, json);
				}
			}
			if (targetE && targetE.is(":visible")) { //未关闭
				ShieldJS.core.bindHtmlAction(targetE, menuliSelect, navMenuSelect, searchForm, dialog); //内部继续加载,已经默认实现
            }  
        },
		/**	html元素的扩展动作方法
		 * @class ShieldJS.HtmlAction.HTMLEleActions
		 *  */
		HTMLEleActions : {
			/**	select默认值处理设置默认值 */
			selectV : {
				expression : 'select:not([fillv])[data-v]',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					findAndMarkEle(contentE, this.expression).each(function() {
						var selectEthis = $(this);
						var value = selectEthis.data("v");
						if(value || value=="0"){
							setInputValue(selectEthis);
						}
					});
				}
				,description : "select默认值处理"
			},
			/**	select的值赋值到其他元素 */
			selectValueCopy : {
				expression : '.selectValueCopy',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					findAndMarkEle(contentE, this.expression).each(function() {
						var selectEthis = $(this);
						selectEthis.change(function(){
							// printCallStack();
							var containerEle = selectEthis.closest(".selectValueCopy_container");
							if (containerEle.length == 0) {
								containerEle = selectEthis.parent();
                            }
							// 赋值给对应元素
							// 如果选中的是空值，则取上一级的编码和名称显示，相当于取消当前级别地区
							var toValueStr = selectEthis.attr("value-to"); //值返回到
							if (toValueStr) {
								var vele = selectEthis;
								var vvalue = selectEthis.val();
								
								var selectRelateds = containerEle.find("select");
								var index = selectRelateds.index(selectEthis);
                               	while (!vvalue && --index >=0) {
                               		vele = selectRelateds.eq(index);
                               		vvalue = vele.val();
                               	}
                               	// 将值赋给input
                               	var toValueEle = containerEle.find(toValueStr);
                               	if (toValueEle.is(":input")) {
									toValueEle.val(vvalue).triggerHandler("change"); //触发自定义change事件
                                } else {
                                	toValueEle.text(vvalue).triggerHandler("change"); //触发自定义change事件
                                }
                               	
                               	var toNameStr = selectEthis.attr("name-to"); //文本返回到
                               	var nameSplitStr = selectEthis.attr("name-split")||""; //文本分割符
                               	var nameIgnoreStr = selectEthis.attr("name-ignore")||"请选择"; //忽略文本
								if (toNameStr) {
									var nameIgnoreReg = new RegExp(nameIgnoreStr,'g'); //正则表达式对象，g表示全局
									var showText = "";
									containerEle.find("option:selected").each(function() {
										var text = $(this).text();
										text = text.replace(nameIgnoreReg, "");
										if (text) {
											if (showText) {
												showText += nameSplitStr;
											}
											showText += text;
                                        }
									});
									showText = showText.replace(nameIgnoreReg,"");
									var toNameEle = containerEle.find(toNameStr);
									if (toNameEle.is(":input")) {
										toNameEle.val(showText).triggerHandler("change"); //触发自定义change事件
                                    } else {
                                    	toNameEle.text(showText).triggerHandler("change"); //触发自定义change事件
                                    }
								}
                            }
						});
					});
				}
				,description : "select默认值处理"
			},
			/**	头像img处理，没有src时显示文字中第一个字 */
			avatar : {
				expression : '.avatar_plugin',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var colors = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#f1c40f",
                	"#e67e22", "#e74c3c", "#eca0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"];
					findAndMarkEle(contentE, this.expression).each(function() {
						var selectEthis = $(this);
						
						var headImgE = selectEthis;
				        if(!headImgE.attr("src") && headImgE.is("img")){ //没有路径时,并且需要是图片
				        	var canvas = document.createElement("canvas");
					        var context = canvas.getContext('2d');
					        if(context){ //支持canvas才继续，否则不需要继续了
					        	var uname = headImgE.data("dashow")||"无名氏";
					        	var width = headImgE.data("dawidth")||headImgE.attr("width")||90;
						        var name = uname.charAt(0);
						        var fontSize =  Math.round(width/2);
						        var fontWeight = 'bold';
						        canvas.width = width;
						        canvas.height = width;
						        
						        var narrs = String(uname).toUpperCase().split(" ");
								// 去第一个字符，如果不存在则返回？
        						var firstchar = narrs.length == 1 ? narrs[0] ? narrs[0].charAt(0) : "?" : narrs[0].charAt(0) + narrs[1].charAt(0);
        						window.devicePixelRatio && (width *= window.devicePixelRatio); //该 Window 属性 devicePixelRatio 能够返回当前显示设备的物理像素分辨率与 CSS 像素分辨率的比率。此值也可以解释为像素大小的比率：一个 CSS 像素的大小与一个物理像素的大小的比值。简单地说，这告诉浏览器应该使用多少个屏幕的实际像素来绘制单个 CSS 像素。
        						var charCode = ("?" == firstchar ? 72 : firstchar.charCodeAt(0) ) - 64;
        						var colorIndexr = charCode % 20;
        						context.fillStyle = colors[colorIndexr - 1];
					        	
						        //context.fillStyle = '#1abc9c';
						        context.fillRect(0, 0, canvas.width, canvas.height);
						        context.fillStyle = '#FFF';
						       	context.font = fontWeight + ' ' + fontSize + 'px sans-serif';
								//context.font = fontWeight + ' ' + fontSize + "px Arial";
						        context.textAlign = 'center';
						        //context.textBaseline="middle";
						        //(text,x,y,maxWidth); text输出的文本。x:开始绘制文本的x坐标位置（相对于画布）。y开始绘制文本的 y 坐标位置（相对于画布） maxWidth:可选。允许的最大文本宽度，以像素计。
						        //var fillx = width/2;
						        //context.fillText(name, fillx, fillx); 
						        if(25 > width ){
						        	context.fillText(firstchar, width / 2, width / 1.37);
						        } else if("U" === firstchar || "V" === firstchar || "W" === firstchar){
						        	context.fillText(firstchar, width / 2, width / 1.42); 
						        } else {
						     		context.fillText(firstchar, width / 2, width / 1.48);
						        }
						        
						        headImgE.attr('src',canvas.toDataURL("image/png")).show().width(width);
					        }
							canvas = null;
				        } else if(!headImgE.is("img")){
				        	ShieldJS.error("avatar组件不是img元素！");
				        }
					});
				}
				,description : "头像处理组件，没有时显示文字中第一个字"
			},
			/**	弹出窗口，添加，显示，修改等；查找带回 */
			shieldDialogOrLookup: {
				expression : '.shieldDialog,.shieldLookup',
				/* selectE：选中的元素dom menuliSelect：左侧栏menu navMenuSelect：导航栏menu searchForm：搜索表单  shieldDialog：弹出框 */
				fn : function(selectE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					//存在checkFn时contentE为checkFn函数的返回值
					selectE.unbind("click").click(function() {
						var $this = $(this);
						if(hasAttr($this,"confirm")){ //确认信息
							var confirmMsg = $this.attr("confirm"); 
							if(!confirm(confirmMsg)){
								return false;
							}
						}
						var relType; //iframe
						if($this.data("relType")){ //打开类型,data-rel-type
							relType = $this.data("relType");
						}
						var viewTitle = "信息";
						if(hasAttr($this,ShieldJS.css.TITLE)){
							viewTitle = $this.attr(ShieldJS.css.TITLE);
						}
						var viewWidth = 500;
						if(hasAttr($this,ShieldJS.css.WIDTH)){
							viewWidth = $this.attr(ShieldJS.css.WIDTH);
						}
						var viewHeight = 600; //iframe打开时用到
						if(hasAttr($this,ShieldJS.css.HEIGHT)){
							viewHeight = $this.attr(ShieldJS.css.HEIGHT);
						}
						var url = $this.data("url");
						var params = {};
						var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
						if (!checkParams) {
							return false;
						} else {
							params = checkParams;
						}
						params = getParams($this,params);
						var successDo = ShieldJS.HtmlAction.successDo.shieldRefresh;
						if (hasAttr($this, "successDo")) { //保存成功后的处理
							successDo = $this.attr("successDo");
						}
						var options = {};
						options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
						ShieldJS.openDialog(url, params, function(ele, dialog) {
							ele.data('dialog', dialog);
							ShieldJS.form.initSubmit(ele, function(formE, targetE, data) {//成功后
								ShieldJS.HtmlAction.formSubmitCallback(successDo, formE, targetE, data, menuliSelect, navMenuSelect, searchForm, dialog);
							});
							 //默认输入光标在第一个输入框内
							var eleInput =  findAndMarkEle(ele, 'input[type="text"],textarea');
							var inputLength = eleInput.length;
							if (inputLength>0) {
								eleInput.eq(0).focus();
							}
						
							 //查找带回
							if($this.hasClass("shieldLookup")){
								var selectBtnEles = findAndMarkEle(ele, ".selectBtn"); //选择按钮
								var formE = $this.closest("form");
								ele.on('click',".selectBtn",function(){
									var selectBtnEle = $(this);
									var thisIndex = formE.find(".shieldLookup").index($this);
									formE.find(':input.shieldLookupId').eq(thisIndex).val(selectBtnEle.data("id")).triggerHandler("change");//触发自定义change事件
									var shieldLookupShowNameEle = formE.find('.shieldLookupName').eq(thisIndex);
									var isInput = shieldLookupShowNameEle.is(":input");
									if(isInput){
										shieldLookupShowNameEle.val(selectBtnEle.data("name"));
									}else{
										shieldLookupShowNameEle.text(selectBtnEle.data("name"));
									}
									shieldLookupShowNameEle.triggerHandler("change");//触发自定义change事件
									console.log("successDo")
									var suddceeDoExtendFn = eval(successDo); //自定义的回调方法
									alert($.isFunction(suddceeDoExtendFn))
									if($.isFunction(suddceeDoExtendFn)){
										suddceeDoExtendFn(selectBtnEle.data("id"),selectBtnEle.data("name"));
									}
									dialog.remove(); //关闭弹出框
									
									
								});
							}
							
							//ShieldJS.core.bindHtmlAction(ele, menuliSelect, navMenuSelect, searchForm, dialog); //内部继续加载,已经默认实现
							
						}, viewTitle, viewWidth, viewHeight, null, relType, options);
						return false;
					});
				},
				checkFn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var dealEle = findAndMarkEle(contentE, this.expression);
					if (dealEle.length > 0) {
						dealEle.each(function(){
							var eleName = "shieldDialog元素";
							var $this = $(this);
							//查找带回
							if ($this.hasClass("shieldLookup")) {
								eleName = "shieldDialog元素";
								var formE = $this.closest("form");
								if (formE.length == 0) {
									ShieldJS.error(eleName + "元素需要包含在form内部！");
									return false;
								}
								var shieldLookupEles = formE.find(".shieldLookup");
								var shieldLookupShowIdEles = formE.find(':input.shieldLookupId');//触发自定义change事件
								var shieldLookupShowNameEles = formE.find('.shieldLookupName');
								
								if (shieldLookupEles.length != shieldLookupShowIdEles.length) {
									ShieldJS.error(eleName + "个数与shieldLookupId个数不相等，请检查！");
									return false;
								} if (shieldLookupEles.length != shieldLookupShowNameEles.length) {
									ShieldJS.error(eleName + "个数与shieldLookupName个数不相等，请检查！");
									return false;
								}
							}
							var url = $this.data("url");
							var target = $this.data("target");
							if(!url && !target){ //有链接才处理
								dealEle = dealEle.not($this);
								ShieldJS.error(eleName + "中data-url和data-target属性至少有一个");
								ShieldJS.console(eleName + "中data-url和data-target属性至少有一个："+$this.prop("outerHTML"));
								return false;
							}
							
						});
					}
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "1、弹出窗口，添加，显示，修改等；2、查找带回"
			},
			/**	ajax请求,返回json格式，删除、更新状态等 */
			shieldAjax : {
				expression : '.shieldAjax'
				,deal : function(ele, contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var $this = ele;
					if($this.data("url")){ //有链接才处理
						var successDo = ShieldJS.HtmlAction.successDo.shieldRefresh;
						if (hasAttr($this,"successDo")) { //保存成功后的处理
							successDo = $this.attr("successDo");
						}
						if (successDo) { //回调
							if(!ShieldJS.HtmlAction.checkSuccessDoMethodExist(successDo)){
								return false;
							}
						}
						var url = $this.data("url");
						var method = $this.data("method")||"post";
						var viewTitle;
						if(hasAttr($this, ShieldJS.css.TITLE)){
							viewTitle = $this.attr(ShieldJS.css.TITLE);
						}
						var params={};
						var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
						if (!checkParams) {
							return false;
						} else {
							params = checkParams;
						}
						params = getParams($this, params);
						// 查找是否存在点击元素和搜索表单
						var clickEle = $this.data("successdo-click");
						if (clickEle) {
							menuliSelect = getjQueryObj(clickEle);
				        }
						var searchFormV = $this.data("successdo-searchform");
						if (searchFormV) {
							searchForm = getjQueryObj(searchFormV);
				        }
						var runnext =true; //继续执行
						if(viewTitle){
							ShieldJS.confirm("确认提示", viewTitle, function() {
								ShieldJS.HtmlAction.postData(url, params, contentE, searchForm, menuliSelect, shieldDialog, successDo, method, ShieldJS.HtmlAction.clickCallback($this));
                            });
						} else {
							ShieldJS.HtmlAction.postData(url, params, contentE, searchForm, menuliSelect, shieldDialog, successDo, method, ShieldJS.HtmlAction.clickCallback($this));
						}
						return false;
					} else {
						ShieldJS.error("shieldAjax元素缺少data-url属性！");
						ShieldJS.console("shieldAjax元素缺少data-url属性！"+$this.prop("outerHTML"));
						return false;
					}
				}
				,fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var dealMethod = this.deal;
					findAndMarkEle(contentE, this.expression).click(function() {
						var $this = $(this);
						dealMethod($this, contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog);
						return false;
					});
				}
				,description : "ajax请求,返回json格式，删除、更新状态等"
			},
			/**	ajax删除的处理 */
			shieldDelete : {
				expression : '.shieldDelete',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					findAndMarkEle(contentE, this.expression).each(function() {
						var $this = $(this);
						if(!hasAttr($this, ShieldJS.css.TITLE)){
							$this.attr(ShieldJS.css.TITLE, "确定删除该信息吗？");
						}
						$this.click(function() {
							var $this = $(this);
							return ShieldJS.HtmlAction.HTMLEleActions.shieldAjax.deal($this, contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog);
						});
					});
				}
				,description : "ajax删除处理。"
			},
			/**	覆盖内容，类似于jQuery的jObj.load() */
			shieldLoad : {
				expression : '.shieldLoad',
				fn : function(selectE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var expression = this.expression;
					var loadContent = function(ele) {
						var $this = ele;						
						
						var url = $this.data("url");
						var targetEle = getjQueryObj($this.data("target"));
						var method = $this.data("method")||"post";
						if (!targetEle) {
							targetEle = $this; //target如果没有则为自身
                        }
						targetEle.data('invoker-load', ele); //load方法的调用者添加进去
						targetEle.off('refresh.shieldjs').on('refresh.shieldjs', function(){
							loadContent($this);
						});
						
						var viewTitle;
						if(hasAttr($this,ShieldJS.css.TITLE)){
							viewTitle = $this.attr(ShieldJS.css.TITLE);
						}
						var params={};
						var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
						if (!checkParams) {
							return false;
						} else {
							params = checkParams;
						}
						params = getParams($this,params);
						// 执行方法
						var doing =  function() {
							targetEle.html(ShieldJS.loadingHtml);
							if (method == "post") {
								ShieldJS.ajax.post(url, params, function(data) {
									dealWidth(targetEle);
									targetEle.html(data).show();
									targetEle.find(".closebtn").on("click", function() {
										closeTarget(targetEle); //关闭
                                    });
									ShieldJS.form.initSubmit(targetEle, function(formE, targetE, data){
										//改写form的submit方法（util.js）
										//页面处理
										ShieldJS.core.bindHtmlAction(targetEle, menuliSelect, navMenuSelect, searchForm);
									});
									//页面处理
									ShieldJS.core.bindHtmlAction(targetEle, menuliSelect, navMenuSelect, searchForm);
									// 自定义点击回调
									ShieldJS.HtmlAction.clickCallback($this);
								}, null, null, true);
							} else {
								ShieldJS.ajax.get(url, params, function(data) {
									dealWidth(targetEle);
									targetEle.html(data).show();
									targetEle.find(".closebtn").on("click", function() {
										closeTarget(targetEle); //关闭
                                    });
									ShieldJS.form.initSubmit(targetEle, function(formE, targetE, data){
										//改写form的submit方法（util.js）
										//页面处理
										ShieldJS.core.bindHtmlAction(targetEle, menuliSelect, navMenuSelect, searchForm);
									});
									//页面处理
									ShieldJS.core.bindHtmlAction(targetEle, menuliSelect, navMenuSelect, searchForm);
									// 自定义点击回调
									ShieldJS.HtmlAction.clickCallback($this);
								}, null, null, true);
							}
						}
						if (viewTitle) {
							ShieldJS.confirm("确认提示", viewTitle, function() {
								doing();
                            });
						}else {
							doing();
						}
                    };
                    // 宽度计算
                    var dealWidth = function(targetEle){
                    	var targetEleWidth = targetEle.data("width");
						if (targetEleWidth) {
							var prevEle = targetEle.prev();
							if (prevEle.find(expression).length>0) { //前一个元素包含load组件时才处理，否则不处理
								var orgWidth = prevEle.data("org--width");
								var widthPrev = orgWidth;
								if (orgWidth) {
									widthPrev = orgWidth;
								} else {
									widthPrev = prevEle.outerWidth(true); //包括外边框
									prevEle.data("org--width", widthPrev);
								}
								var widthTotal = widthPrev ; // + targetEle.outerWidth(true)
								
								targetEle.css("width", targetEleWidth);
								prevEle.width(widthTotal - targetEle.outerWidth(true) -10);
                            }
							
                        }
                    }
                    var closeTarget = function(targetEle){
						var prevEle = targetEle.prev();
						var orgWidth = prevEle.data("org--width");
						if (orgWidth) {
							targetEle.hide();
							prevEle.width(orgWidth);
                        } 
                    }
                    selectE.each(function() {
                    	var $this = $(this);
                    	var autoload = "false";
                    	if(hasAttr($this, "autoload")){ //autoLoad
                    		autoload = $this.attr("autoload")||"true";
                    	}else if(hasAttr($this, "autoLoad")){ //autoLoad
                    		autoload = $this.attr("autoLoad")||"true";
                    	}
						
						$this.off('load.shieldjs').on('load.shieldjs', function(){
							loadContent($this);
						});
						
                    	if (autoload=="true") { //自动加载
                    		$this.triggerHandler('load');
                        }else { 
                        	$this.click(function() {
                        		$this.triggerHandler('load');
//                        		return false;
                        	});
						}
                    });
				},
				checkFn : function(contentE, menuliSelect, navMenuSelect, searchForm){
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "shieldLoad元素";
					if (dealEle.length > 0) {
						dealEle.each(function(i, n){
							var $this = $(this);
							if (!$this.data("url")) {
								ShieldJS.error(eleName + "未定义data-url：请添加该属性！");
								ShieldJS.console(eleName + "未定义data-url：请添加该属性！"+$this.prop("outerHTML"));
								return false;
							} else if (hasAttr($this,"data-target") && $("body").find($this.data("target")).length == 0 ) {
								ShieldJS.error(eleName + "目标元素未找到：请检查 data-target 的定义是否正确！");
								return false;
							}
						});
					}
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "覆盖内容，类似于jQuery的jObj.load()"
			},
			/**	a标签附加参数(如果不是新打开的页面,为Get方式提交需要注意参数长度) */
			shieldA : {
				expression : '.shieldA',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					findAndMarkEle(contentE, this.expression).each(function() {
						var $this = $(this);
						if($this.data("url")){ //有链接才处理
							$this.click(function() {
								var url = $this.data("url");
								var params={};
								var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
								if (!checkParams) {
									return true;
								} else {
									params = checkParams;
								}
								params = getParams($this, params);
								console.log(params);
								var method = $this.data("method")||"get";
								var target = $this.data("target")||"_blank";
								console.log(target);
								if (target == "_blank") {
									openNewPage($this, url, params); //shield.util.js 中的方法
									// 自定义点击回调
									ShieldJS.HtmlAction.clickCallback($this);
								} else {
									var targetE = getjQueryObj(target);
									console.log("post");
									if (method == "post") {
										ShieldJS.ajax.post(targetE, url, params,  function() {
											//内容区处理
											if(!ShieldJS.core.contentAreaDeal(targetE, null, null)){
//											return false;
											}
											// 自定义点击回调
											ShieldJS.HtmlAction.clickCallback($this);
										});
									} else {
										console.log("get");
										ShieldJS.ajax.get(targetE, url, params,  function() {
											//内容区处理
											if(!ShieldJS.core.contentAreaDeal(targetE, null, null)){
//                                			return false;
											}
											// 自定义点击回调
											ShieldJS.HtmlAction.clickCallback($this);
										});
									}
								}
//							return false;
							})
						}
                    });
				}
				,description : "a标签附加参数(如果不是新打开的页面,为Get方式提交需要注意参数长度)"
			},
			/**	打开链接 */
			shieldOpenLink : {
				expression : '.shieldOpenLink',
				fn : function(selectE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					 selectE.unbind("click").click(function() {
						 var $this = $(this);
						 var linkSrcE = $this;
						 var url = '';
						 if (hasAttr($this, "baseOn")) { //基于元素
							 var baseOnValue = $this.attr("baseOn");
							 var baseOnValueArrs = baseOnValue.split(">");
							 if (baseOnValueArrs.length == 2) {
								 linkSrcE = $this.closest(baseOnValueArrs[0]).find(baseOnValueArrs[1]);
								 if (linkSrcE.is(":input")) {
									 url = linkSrcE.val();
								 } else {
									 url = linkSrcE.text();
								 }
							 }
						 } else {
							 var prevE = $this.prev();
							 if (prevE.is(":input")) { // 类型为text的input或select  if (prevE.is(":text") || prevE.is("select")) {
								 url = prevE.val();
							 } else {
								 url = prevE.text();
							 }
						 }
						 url = formatUrl(url);
						 if (url == '') {
							 ShieldJS.alert("消息提示", '空连接,不能打开!', "error");
						 } else {
							 $this.after("<a href='" + url + "' target='_blank'></a>");
							 var a = $this.next();
							 a[0].click(); //直接用a.click()不执行
							 a.remove();
							 // 自定义点击回调
							 ShieldJS.HtmlAction.clickCallback($this);
						 }
						return false;
					});
				},
				checkFn : function(contentE, menuliSelect, navMenuSelect, searchForm){
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "shieldOpenLink元素";
					if (dealEle.length > 0) {
						dealEle.each(function(i, n){
							var $this = $(this);
							if (hasAttr($this, "baseOn")) { //基于元素
								 var baseOnValue = $this.attr("baseOn");
								 var baseOnValueArrs = baseOnValue.split(">");
								 if (baseOnValueArrs.length == 2) {
									 linkSrcE = $this.closest(baseOnValueArrs[0]).find(baseOnValueArrs[1]);
									 if(linkSrcE.length == 0){
										 ShieldJS.error(eleName + "未定义链接源元素：请检查baseOn的值定义是否正确（父元素选择器+“>”+源元素选择器）");
										 ShieldJS.console(eleName + "未定义链接源元素：请检查baseOn的值定义是否正确（父元素选择器+“>”+源元素选择器）"+baseOnValue+$this.prop("outerHTML"));
										 return false;
									 }
								 }
							 }
						});
					}
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "打开链接"
			},
			/** 刷新 */
			shieldRefresh : {
				expression : '.shieldRefresh',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					 findAndMarkEle(contentE, this.expression).unbind("click").click(function() {
						ShieldJS.HtmlAction.successDoMethod.shieldRefresh(contentE, searchForm, menuliSelect, shieldDialog);
						// 自定义点击回调
						ShieldJS.HtmlAction.clickCallback($(this));
						return false;
					});
				}
			 	,description : "刷新"
			},
			/** 带参数提交（form基础上点击不同按钮提交不同结果） */
			shieldSubmit : {
				expression : '.shieldSubmit',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					 findAndMarkEle(contentE, this.expression).unbind("click").click(function() {
						var $this = $(this);
						var params={};
						
						var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
						if (!checkParams) {
							return false;
						} else {
							params = checkParams;
						}
						
						params = getParams($this,params);
						var formE = $this.closest('form');
						if (formE.length > 0){
							for(var key in params){
								formE.find("input[name='"+key+"']").val(params[key]);
							}
							if (!shieldDialog) { //dialog不存在时才处理，存在时默认处理
								ShieldJS.form.initSubmit(contentE, function(formE, targetE, data){
									// 初始化页面内容
									ShieldJS.core.bindHtmlAction(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog);
									//提交完成后刷新当前页面
									var successDo = ShieldJS.HtmlAction.successDo.shieldRefresh;
									ShieldJS.HtmlAction.formSubmitCallback(successDo, formE, targetE, data, menuliSelect, navMenuSelect, searchForm, shieldDialog);
									// 自定义点击回调
									ShieldJS.HtmlAction.clickCallback($this);
								});
							}
							formE.submit();
						}
						return false;
					});
				}
				,description : "带参数提交（form基础上点击不同按钮提交不同结果）"
			},
			/** 回调 */
			shieldDialogCallback : {
				expression : '.shieldDialogCallback',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var shieldDialogCallbackEle = findAndMarkEle(contentE, this.expression);
					if (shieldDialogCallbackEle.length > 0) {
						var methodName = shieldDialogCallbackEle.attr("callback");
						ShieldJS.shieldDialogCallback[methodName](contentE, shieldDialog);
					}
				}
			 	,description : "回调"
			},
			/** 弹出新窗口操作 */
			shieldOpenWindow : {
				expression : 'a.shieldOpenWindow',
				fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					 findAndMarkEle(contentE, this.expression).unbind("click").click(function() {
						var $this = $(this);
						var viewTitle = "信息";
						if(hasAttr($this, ShieldJS.css.TITLE)){
							viewTitle = $this.attr(ShieldJS.css.TITLE);
						}
						var viewWidth = 500;
						if(hasAttr($this, ShieldJS.css.WIDTH)){
							viewWidth = $this.attr(ShieldJS.css.WIDTH);
						}
						if($this.data("url")){ //有链接才处理
							var url = $this.data("url");
							if(hasAttr($this, "openUrl")){
								window.open($this.attr("openUrl"));
							}
							var params={};
							var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
							if (!checkParams) {
								return false;
							} else {
								params = checkParams;
							}
							params = getParams($this, params);
							setTimeout(function() {
								ShieldJS.openDialog(url, params, function(ele, dialog) {
									//提交的表单统一命名为saveForm
									var saveForm = ele.find('form.saveForm');
									
									saveForm.unbind('submit').submit(function() {
										if(saveForm.kvvalid()){ //表单验证
											if(hasAttr($this, "submitTips")){
												setTimeout(function() {
													ele.find('.contentBody').html($this.attr('submitTips'));//改变了form结构
												},10);
											}
											return true;
										}
										return false; //表单验证未通过
									});
									var shieldSubmitAction = ShieldJS.HtmlAction.HTMLEleActions["shieldSubmit"];
									if($.isFunction(shieldSubmitAction)){
										shieldSubmitAction(saveForm);
									}
									
									//ShieldJS.core.bindHtmlAction(ele, menuliSelect, navMenuSelect, searchForm, dialog); //内部继续加载
									// 自定义点击回调
									ShieldJS.HtmlAction.clickCallback($this);
								}, viewTitle, viewWidth);
							},10);
							return false;
						}
					});
				}
				,description : "弹出新窗口操作"
			 },
			 /** 图片上传 */
			 shieldImageUpload : {
				expression : 'input.shieldImageUpload',
				fn : function(selectE, menuliSelect, navMenuSelect, searchForm){
					// 待实现
				},
				checkFn : function(contentE, menuliSelect, navMenuSelect, searchForm){
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "图片上传组件shieldImageUpload";
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "图片上传"
			},
			/** 删除图片 */
			shieldImgDelete : {
				expression : 'input.shieldImgDelete',
				fn : function(selectE, menuliSelect, navMenuSelect, searchForm){
					 // 待实现
				},
				checkFn : function(contentE, menuliSelect, navMenuSelect, searchForm){//检查函数
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "图片删除组件shieldImgDelete";
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "删除图片"
			},
			/** 多图片上传插件 */
			shieldMultiImgUpload : {
				expression : '.shieldMultiImgUpload',
				fn : function(selectE, menuliSelect, navMenuSelect, searchForm) {
					// 待实现
				},
				checkFn : function(contentE, menuliSelect, navMenuSelect, searchForm) { //检查函数
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "图片上传组件shieldMultiImgUpload";
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "多图片上传插件"
			},
			/** 下载附件 */
			shieldDownload : {
					expression : '.shieldDownload',
					fn : function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
						findAndMarkEle(contentE, this.expression).each(function() {
							var $this = $(this);
							console.log($this.data("downloadurl"));
							if($this.data("url")){ //有链接才处理
								$this.click(function() {
									var downloadurl = $this.data("url");
									var existUrl = $this.data("existurl");
									var method = $this.data("method")||"post";
									var params={};
									var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
									if (!checkParams) {
										return true;
									} else {
										params = checkParams;
									}
									params = getParams($this, params);
									console.log(params);
									if(existUrl){
										ShieldJS.HtmlAction.postData(existUrl, params, contentE, searchForm, menuliSelect, shieldDialog, "shieldDonothing", method, function(){
											openNewPage($this, downloadurl, params); //shield.util.js 中的方法
										});
									}else{
										openNewPage($this, downloadurl, params); //shield.util.js 中的方法
									}
								})
							}else
							{
								ShieldJS.alert("消息提示", '下载链接或者检查存在链接不存在！', "error");
							}
	                    });
					}
					,description : "下载附件"
				},
			/** 关联其他元素的select
			 * @example 1
			 * //选择1时显示 id为dealBySpider的元素 选择2是显示id为yaunyinSpider的元素
			 * <select id="state" name="state" class="shieldSelectRelated">
					<option value="">--请选择--</option>
					<option value="1" relatedTo="#dealBySpider">通过</option>
					<option value="2" relatedTo="#yaunyinSpider">退回</option>
				</select>
			 * @example 2
				<!-- 联动select,relatedTo为联动目标（jQuery选择器，本例中为id为job的select），relatedUrl为联动url，其中department={}代表department为当前select选中的值，
				主要为了参数名与select名字不一致时，也可以使用参数选择器paramSelector，paramAdapter为参数适配器，将relatedUrl返回的json（注意必须为json格式）的字段适配为
				select可使用的字段，适配器中id为option的值name为option的显示文本如果字段名本身为id和name，此属性可不写，下面的示例中可用写为paramAdapter="name=jobName"，id省略表示与默认值一致 -->
				<select name="department" id="department" class="shieldSelectRelated" relatedTo="#job" relatedUrl="/resume/getJobList?department={}" paramAdapter="id#name=jobName">
				    <option value="0">请选择</option>
				    <option value="1" relatedTo="#jobspan">技术部</option>
				    <option value="2">信息采编部</option>
				    <option value="3">客服部</option>
				    <option value="4">招商部</option>
				</select>
				<span id="jobspan"><b>招聘职位：</b>
				<select name="job" id="job">
				    <!-- 样式为 noshield，说明联动时不覆盖 -->
				    <option value="0" class="noshield">请选择</option>
				</select></span>

			*/
			shieldSelectRelated : {
				expression : '.shieldSelectRelated',
				fn: function(selectE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
					var shieldSelectRelated = selectE;
					shieldSelectRelated.each(function() {
						var $this = $(this);
						var autochange = $this.data("autochange");
						var hasAutoChange = autochange || (autochange==false) || (autochange=="false");
						if (!hasAutoChange) {
							if ($this.find("option[relatedTo]").length > 0) { // option级别的自动change
								autochange = true;
							} else { //select级别的不自动change
								autochange = false;
							}
						}
						
						$this.change(function(){
							setTimeout(function() {
								var selectVal = $this.val()+"";
								
								var paramNames = {id:"id",name:"name"}; //默认参数配置
								if (hasAttr($this, "relatedTo")) { //select级别联动
									
									var relatedTo = $this.attr("relatedTo");
									var relatedToE = getjQueryObj(relatedTo);
									var relatedUrl = $this.attr("relatedUrl");
									
									if (selectVal!="" && selectVal!="null") {
										
										var paramAdapter = $this.attr("paramAdapter");
										if (paramAdapter) {
											var paramAdapters = paramAdapter.split("#"); //#号分隔
											for (var i=0;i<paramAdapters.length;i++) {
												var paramName = paramAdapters[i];
												if (paramName.indexOf("=")!=-1) {
													var _paramNames = paramName.split("="); //如果key与提交不一致用=号分隔
													var paramsKey = _paramNames[0];
													var jsonResutlKey = _paramNames[1];
													paramNames[paramsKey] = jsonResutlKey;
												} else {
													paramNames[paramName] = paramName;
												}
											}
										}
										//获取数据
										if(relatedUrl.indexOf("{}") != 0){
											relatedUrl = relatedUrl.replace("{}",selectVal);
										}
										var params={};
										var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
										if (!checkParams) {
											return false;
										} else {
											params = checkParams;
										}
										params = getParams($this, params);
										relatedToE.data("sourceEle", $this); //写入触发关系
										if (relatedToE.is("select")) { // 联动对象是select，则应该返回的是json格式
											$.getJSON(relatedUrl, params, function(json) {
												var html = "";
												if (json) {
													for ( var i = 0; i < json.length; i++) {
														html += '<option value="' + json[i][paramNames.id] + '">';
														html += json[i][paramNames.name] + '</option>';
													}
												}
												relatedToE.find("option:not(.noshield)").remove();
												relatedToE.append(html);
												
												if(!relatedToE.data("initareav")){ //赋值
													var value = relatedToE.data("v");
													if (value) {
														if (value == "clear") { //清空值
															value = "";
														}
														//relatedToE.val(value);
														relatedToE.find("option[value='"+value+"']").attr("selected", "selected");
													}
													relatedToE.attr("data-initareav", "initareav");
												}
												relatedToE.triggerHandler("change"); //触发change方法
												
											});
										} else { // 联动对象不是select，直接返回页面
											var method = $this.data("method")||"post";
											if (method == "post") {
												ShieldJS.ajax.post(relatedToE, relatedUrl, params, function(data) {
													ShieldJS.form.initSubmit(relatedToE, function(formE, targetE, data){
														//改写form的submit方法（util.js）
													});
													//页面处理
													ShieldJS.core.bindHtmlAction(relatedToE, menuliSelect, navMenuSelect, searchForm);
													// 自定义点击回调
													ShieldJS.HtmlAction.clickCallback($this);
												}, null, null, true);
											} else {
												ShieldJS.ajax.get(relatedToE, relatedUrl, params,  function() {
													ShieldJS.form.initSubmit(relatedToE, function(formE, targetE, data){
														//改写form的submit方法（util.js）
													});
													//页面处理
													ShieldJS.core.bindHtmlAction(relatedToE, menuliSelect, navMenuSelect, searchForm);
													// 自定义点击回调
													ShieldJS.HtmlAction.clickCallback($this);
												});
											}
										}
                                    } else if (selectVal==""){
                                    	var html = "";
										relatedToE.find("option:not(.noshield)").remove();
										relatedToE.append(html);
										relatedToE.triggerHandler("change"); //触发change方法
    								}
								} 
								//option联动
								$this.find("option").each(function(){
									var $thisOption = $(this);
									var opetionVal = $thisOption.val();
									if (hasAttr($thisOption, "relatedTo")) { //有属性才处理
										var relatedTo = $thisOption.attr("relatedTo");
										var relatedToE = getjQueryObj(relatedTo);
										if (opetionVal === selectVal) {
											enableRelate2E(relatedToE, true);
											relatedToE.show();
										} else {
											//表单设为不可用
											enableRelate2E(relatedToE, false);
											relatedToE.hide();
										}
									}
								});
								
							}, 1); //加定时器是因为通过utils赋值的select不加取不到值
						});
						// 禁用启用元素
						function enableRelate2E(relatedToE, checked) {
							relatedToE.prop("disabled", !checked); //关联对象设置为启用/禁用
							relatedToE.find(":input").each(function() { //关联对象里面的input设置为启用/禁用
								var $thisInput = $(this);
								var disabled = $thisInput.prop("disabled"); // 获取初始状态
								var data_enabled = $thisInput.data("enabled"); //data中存在启用值
								var data_disabled = $thisInput.data("disabled"); //data中存在禁用值
								if (!data_enabled && !data_disabled) { //都没有设置 或者 都是false（后一种不会存在）
									if (disabled) {
										data_disabled = true; //禁用
										$thisInput.data("disabled", true);
									} else {
										$thisInput.data("enabled", true); //该值无实际用处，只是表明已经初始化过了
									}
								}
								if (checked) { //可用，如果选中了，且原来就是禁用的则继续禁用，原来为正常则启用
									$thisInput.prop("disabled", data_disabled?true:false);
								} else { //不可用,如果未选中，则禁用此元素
									$thisInput.prop("disabled", !checked);
								}
							});
						}
						
						// 获取上级数据
						function getHigherLevel(ele){
							
							var higherLevel = ele.data("higher"); //倒推上级
							var higherUrl = ele.data("higher-url"); //倒推上级
							
								
							if (higherUrl) { //select级别联动
								var selectVal = ele.data("ajax-v");
								if (selectVal) {
									var relatedUrl = ele.attr("relatedUrl"); //下级
									var paramAdapter = ele.attr("paramAdapter"); //下级
									var extendClass = ele.data("extendClass");
									var eleId = ele.attr("id");
									try {
										higherIndex ++;
									}catch (e) {
										higherIndex = 0;
									}
									if (!eleId) {
										eleId = "gethigherContainer"+higherIndex;
										ele.attr("id", eleId);
									}
									var paramNames = {id:"id",name:"name", pid:"pid"}; //默认参数配置
									var higherReqUrl = higherUrl;
									
									var paramAdapter = ele.attr("paramAdapter");
									if (paramAdapter) {
										var paramAdapters = paramAdapter.split("#"); //#号分隔
										for (var i=0;i<paramAdapters.length;i++) {
											var paramName = paramAdapters[i];
											if (paramName.indexOf("=")!=-1) {
												var _paramNames = paramName.split("="); //如果key与提交不一致用=号分隔
												var paramsKey = _paramNames[0];
												var jsonResutlKey = _paramNames[1];
												paramNames[paramsKey] = jsonResutlKey;
											} else {
												paramNames[paramName] = paramName;
											}
										}
									}
									//获取数据
									if(higherUrl.indexOf("{}") != 0){
										higherReqUrl = higherReqUrl.replace("{}", selectVal);
									}
									var params={};
									var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector(ele, params); //参数选择器
									if (!checkParams) {
										return false;
									} else {
										params = checkParams;
									}
									params = getParams(ele, params);
									$.getJSON(higherReqUrl, params, function(json) {
										var html = "";
										if (json) {
											var highPid ;
											for ( var i = 0; i < json.length; i++) {
												highPid = json[i][paramNames.pid];
												html += '<option value="' + json[i][paramNames.id] + '">';
												html += json[i][paramNames.name] + '</option>';
											}
											var attrs = {id: "hlselect", "class":"higherSelect selectValueCopy shieldSelectRelated "+extendClass, "data-higher-url":higherUrl, "data-ajax-v":highPid, "relatedTo":"#"+eleId, "relatedUrl":relatedUrl }
											if (paramAdapter) {
												attrs.paramAdapter = paramAdapter;
	                                        }
											higherE =  $("<select/>", attrs);
											higherE.html('<option value="">请选择</option>');
											ele.html(html);
											//higherE.append(html);
											ele.before(higherE);
											
											if(!ele.data("initareav")){ //赋值
												var value = ele.data("ajax-v");
												if (value) {
													if (value == "clear") { //清空值
														value = "";
													}
													ele.val(value);
												}
												ele.attr("data-initareav", "initareav");
											}
											// ele.triggerHandler("change"); //触发change方法 不触发，等向上的线回调完成再触发
											ShieldJS.core.bindHtmlAction(higherE.parent(), menuliSelect, navMenuSelect, searchForm); //绑定元素
										} else{
											ele.parent().find("[data-higher-url]:last").triggerHandler("change"); //触发change方法 不触发，等向上的线回调完成再触发
											ele.remove();
										}
										
										
									});
								} else { //值为空说明到顶了
									ele.parent().find("[data-higher-url]:last").triggerHandler("change"); //触发change方法 不触发，等向上的线回调完成再触发
									ele.remove();
								}
							}
								
						}
						getHigherLevel($this);
						
						if (autochange) {
							$this.triggerHandler("change");//触发自定义change事件
                        }
                    });
				},
				checkFn : function(contentE) { //检查函数
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "联动select元素shieldSelectRelated";
					if(dealEle.length > 0 ) {
						dealEle.each(function(){
							var $this = $(this);
							if (hasAttr($this,"relatedTo")) { //select级别的联动（区别于option级别）
								var relatedTo = $this.attr("relatedTo");
								var relatedToE = getjQueryObj(relatedTo);
								if (relatedToE.length == 0) {
									ShieldJS.error(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！");
									ShieldJS.console(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！"+$this.prop("outerHTML"));
									return false;
								}
								if (!hasAttr($this,"relatedUrl")) {
									ShieldJS.error(eleName + "未定义数据获取地址：relatedUrl属性");
									ShieldJS.console(eleName + "未定义数据获取地址：relatedUrl属性"+$this.prop("outerHTML"));
									return false;
								}
								if (relatedToE.is("select") && !hasAttr($this,"paramAdapter")) {
									ShieldJS.error(eleName + "未定义参数适配：paramAdapter属性");
									ShieldJS.console(eleName + "未定义参数适配：paramAdapter属性"+$this.prop("outerHTML"));
									return false;
								}
							}
						});
					};
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "关联其他元素的select"
			},
			
			/** 关联其他元素的checkbox或radio
			 * @example 
			 * // shieldCheckedRelated关联操作checkbox或radio元素，relatedTo为关联到的元素，relatedToType="disabled"表示将目标元素更改disabled属性（其他值：disabledAndHide及不写）
			 * <sapn style="float: left;padding: 3px;"><input type="checkbox" id="acceptSetBtn" class="shieldCheckedRelated" relatedTo="#buySubmit" relatedToType="disabled">我已阅读并接受此规定</sapn>
			 * <input type="button" id="buySubmit" class="shieldSubmit" value="购买" disabled="disabled">*/
			shieldCheckedRelated : {
				expression : '.shieldCheckedRelated',
				fn: function(selectE){
					
					selectE.each(function(){
						var thisSelectE = $(this);
						
						var shieldCheckedRelated = thisSelectE;
						var changeDealE; //如果是radio需处理为同一父类的radio
						if (shieldCheckedRelated.is(":radio")) { //单选框
							var formE = shieldCheckedRelated.closest("form");
							changeDealE = formE.find(":radio[name='"+shieldCheckedRelated.attr("name")+"']");
						}
						if (!changeDealE) {
							changeDealE = shieldCheckedRelated;
						}
						changeDealE.each(function() {
							var bindE = $(this);
							if (!bindE.hasClass("shieldCheckedRelatedInit")) {
								bindE.bind("change",function(){
									var $thisCheckedE = $(this);
									if (changeDealE) { //radio需处理同一父级的所有radio元素，否则点击其他的radio不会触发上一次选中radio的change事件
										$thisCheckedE = changeDealE;
									}
									$thisCheckedE.filter(":not(:checked)").each(function(){
										var $this = $(this);
										changeEvent($this);
									});
									$thisCheckedE.filter(":checked").each(function(){
										var $this = $(this);
										changeEvent($this);
									});
								});
								bindE.addClass("shieldCheckedRelatedInit");
                            }
                        });
						
						shieldCheckedRelated.triggerHandler("change");//触发自定义change事件
					});
					// 显示隐藏元素
					function showRelate2E(relatedToE, checked) {
						if (checked) {
							relatedToE.show();
                        } else {
                        	relatedToE.hide();
						}
                    }
					// 禁用启用元素
					function enableRelate2E(relatedToE, checked) {
						relatedToE.prop("disabled", !checked); //关联对象设置为启用/禁用
						relatedToE.find(":input").each(function() { //关联对象里面的input设置为启用/禁用
							var $thisInput = $(this);
							var disabled = $thisInput.prop("disabled"); // 获取初始状态
							var data_enabled = $thisInput.data("enabled"); //data中存在启用值
							var data_disabled = $thisInput.data("disabled"); //data中存在禁用值
							if (!data_enabled && !data_disabled) { //都没有设置 或者 都是false（后一种不会存在）
								if (disabled) {
									data_disabled = true; //禁用
									$thisInput.data("disabled", true);
	                            } else {
	                            	$thisInput.data("enabled", true); //该值无实际用处，只是表明已经初始化过了
	                            }
							}
							if (checked) { //可用，如果选中了，且原来就是禁用的则继续禁用，原来为正常则启用
								$thisInput.prop("disabled", data_disabled?true:false);
	                        } else { //不可用,如果未选中，则禁用此元素
	                        	$thisInput.prop("disabled", !checked);
	                        }
	                    });
	                }
					function changeEvent($this) {
						setTimeout(function() {
							var checked = $this.prop("checked");
							
							if (hasAttr($this, "relatedTo")) { //关联元素
								var relatedTo = $this.attr("relatedTo");
								var relatedToE = getjQueryObj(relatedTo);
								
								if (hasAttr($this, "relatedToType")) { //定义关联状态
									var relatedToType = $this.attr("relatedToType").toLowerCase(); //转小写
									
									if (relatedToType == "disabled") { //仅设为不可用
										enableRelate2E(relatedToE, checked);
										
									} else if (relatedToType == "disabledandhide") { //设为不可用并隐藏
										enableRelate2E(relatedToE, checked);
										showRelate2E(relatedToE, checked);
										
									} else { //仅隐藏
										showRelate2E(relatedToE, checked);
									}
								} else { //仅隐藏
									showRelate2E(relatedToE, checked);
								}
								if (checked) {
									var datas = $this.data(); //用data存储
									for ( var key in datas) {
										relatedToE.find('[name="'+key+'"]').val(datas[key]);
									}
                                }
							}
						}, 1); //加定时器是防止utils干扰
                    }
					
					
				},
				checkFn : function(contentE) { //检查函数
					var dealEle = findAndMarkEle(contentE, this.expression);
					var eleName = "联动checkbox或radio元素shieldCheckedRelated";
					if(dealEle.length > 0 ) {
						dealEle.each(function(){
							var $this = $(this);
							if (hasAttr($this,"relatedTo")) {
								var relatedTo = $this.attr("relatedTo");
								var relatedToE = getjQueryObj(relatedTo);
								if (!relatedToE || relatedToE.length == 0) {
									ShieldJS.error(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！");
									ShieldJS.console(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！"+$this.prop("outerHTML"));
									return false;
								}
							}
						});
					};
					return dealEle.length > 0 ? dealEle : false;
				}
				,description : "关联其他元素的checkbox或radio"
			},
			/** 全选 */
			checkall : {
				expression : '#checkall',
				fn: function(contentE){
					findAndMarkEle(contentE, this.expression).change(function() {
						var $this = $(this);
						var checked = $this.is(':checked');
						var rangeE = $this.closest("table");
						if (hasAttr($this, "rangeEle")) {
							var rangeTo = $this.attr("rangeEle");
							rangeE = getjQueryObj(rangeTo);
							if (rangeE.length == 0) {
								ShieldJS.error("全选处理范围元素未找到：请检查rangeEle属性是否定义错误！");
								ShieldJS.console("全选处理范围元素未找到：请检查rangeEle属性是否定义错误！"+$this.prop("outerHTML"));
								return false;
							}
						}
						rangeE.find(':checkbox').not($this).each(function() {
							$(this).prop("checked", checked);
							$(this).triggerHandler("change"); //触发change事件
						});
					});
				}
				,description : "全选"
			},
			/** 用于标记选中行，一般用于checkbox上用于选中行传参 */
			shieldSelectRow : {
				expression : '.shieldSelectRow',
				fn: function(contentE){
					findAndMarkEle(contentE, this.expression).change(function() {
						var $this = $(this);
						var checked = $this.is(':checked');
						var checkClass = ShieldJS.HtmlAction.names.shieldSelectRowByshieldCheck;
						var rowEle = $this.closest("tr");
						rowEle.toggleClass(checkClass);
					});
				}
				,description : "用于标记选中行，一般用于checkbox上用于选中行传参"
			},
			/** 用于表单自动提交 */
			autoSubmitFormPlugin : {
				expression : '.autoSubmitForm_plugin',
				fn: function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
					findAndMarkEle(contentE, this.expression).each(function(i, n) {
						var $this = $(this);
						var successDo = ShieldJS.HtmlAction.successDo.shieldRefresh; // 默认提交成功后的回调方法
						//ShieldJS.form.initSubmit($this, function(formE, targetE, data){
							//改写form的submit方法（util.js）
							ShieldJS.core.bindHtmlAction(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog); //内部继续加载,已经默认实现
							//ShieldJS.HtmlAction.formSubmitCallback(successDo, formE, targetE, data, menuliSelect, navMenuSelect, searchForm, shieldDialog);
						//});
						$this.submit();
						//页面处理
						
					});
				}
				,description : "自动提交表单"
			},
			/** 无数据tr处理 */
			noDataTrPlugin : {
				expression : '.nodatatr_plugin',
				fn: function(contentE) {
					findAndMarkEle(contentE, this.expression).each(function(i, n) {
						var $this = $(this);
						var pervTr = $this.closest("table").find("tr:first");
						$this.find("th,td").attr("colspan", pervTr.find("th,td").length);
					});
				}
				,description : "无数据tr处理"
			},
			/** 用于显示所有已经注册的ShieldJS的html扩展动作方法 */
			showAllShieldJS : {
				expression : '.showAllShieldJS',
				fn: function(contentE){
					findAndMarkEle(contentE, this.expression).each(function(i, n) {
						var $this = $(this);
						var actions = $.extend({},ShieldJS.HtmlAction.HTMLEleActions);
						var tablecls = "allShieldJSHtmlActions "; //table的class
						tablecls += $this.data("class")||"";
						var extendUrl = $this.data("extendUrl"); //扩展url，查看示例
						var htmlStr = '<table class="'+tablecls+'">'+
										 '<tr class="headertr"><th class="shieldjs_headertr_xh"></th><th class="shieldjs_headertr_method">方法名</th><th class="shieldjs_headertr_ex">表达式</th><th class="shieldjs_headertr_des">描述</th>';
						if (extendUrl) {
							htmlStr += '<th class="shieldjs_headertr_demo">示例</th>';
                        }
						htmlStr += '</tr>';
						var index = 0;
						for(var actionName in actions){
							index++;
							htmlStr += '<tr class="datatr"><td class="shieldjs_datatr_xh">'+index+'</td><td class="shieldjs_datatr_method">'+actionName+'</td><td class="shieldjs_datatr_ex">'+(actions[actionName].expression||"")+'</td><td class="shieldjs_datatr_des">'+(actions[actionName].description||"")+'</td>';
							if (extendUrl) {
								htmlStr += '<td class="shieldjs_datatr_demo"><a data-url="'+$this.data("extendUrl")+actionName+'" class="shieldDialog" viewTitle="查看信息" viewWidth="800">查看示例</td>';
	                        }
							htmlStr += '</tr>';
						}
						htmlStr += '</table>';
						var tableE = $(htmlStr);
						tableE.find("td").each(function(i, n) {
							$(this).data("html", $(this).html());
						});
						if(extendUrl){
							ShieldJS.core.bindHtmlAction(["shieldDialogOrLookup"], tableE); //加载shieldDialogOrLookup方法
						}
						var _defaults = {
							/**前景色*/
							foreground: 'red',    
							/**背景色*/
						    background: 'yellow'    
						};
						//搜索
						$(".showAllShieldJSSearch").on("keyup", contentE, function() {
	                        var $search = $(this);
	                        var searchKey = $.trim($search.val());
	                        if (searchKey) {
	                        	var options = $.extend(true, _defaults, getSettings($search));
	                        	tableE.find("tr.datatr").hide();
	                        	tableE.find("tr.datatr").filter(function() {
	                        		if ($(this).text().toLowerCase().indexOf($search.val().toLowerCase())!=-1) {
	                        			var reg = new RegExp("("+$search.val()+")", "gi"); //全局并忽略大小写
	                        			$(this).find("td").not(".shieldjs_datatr_demo").each(function(i, n) { //排除示例
	                        				var newstr = $(n).data("html").replace(reg, '<strong style="background-color: '+options.background+';color: '+options.foreground+';">$1</strong>');
	                        				$(n).html(newstr);
                                        });
	                        			return true;
	                        		}
	                        		return false; 
	                        	}).show();
                            } else {
                            	tableE.find("tr.datatr").show().find("td").not(".shieldjs_datatr_demo").each(function(i, n) {
        							$(this).html($(this).data("html"));
        						});
							}
                        });
						$this.append(tableE);
					});
				}
				,description : "展示所有的注册方法"
			}
		},
		
		/** 添加htmlEle动作处理函数
		 * @param HTMLEleName {String} 方法名称
		 * @param expression {String} 表单式
		 * @param fn {String} 处理函数
		 * @param checkFn {String} 检查函数【可无】
		 * @param description {String} 方法描述 */
		addHTMLEleAction : function (HTMLEleName, expression, fn, checkFn, description) {
			if(this.HTMLEleActions[HTMLEleName]){
				ShieldJS.debug(HTMLEleName + "动作处理方法被覆盖！");
			}
			// 复制对象
		    var copiedObject = $.extend({}, this.HTMLEleActions[HTMLEleName]);
		    var expressionV = arguments[1]; //重载，expression可能不存在
		    var indexRemove = 0;
		    if ($.isFunction(expression)) {
		    	expressionV = "";
		    	indexRemove++;
            }
		    var fnV = arguments[2-indexRemove]; //重载，checkFn函数可能不存在
		    var checkFnV = arguments[3-indexRemove];
		    var descriptionV = arguments[4-indexRemove];
		    if (typeof(checkFnV)=="string") {
		    	descriptionV = checkFnV;
		    	checkFnV = null;
            }
			this.HTMLEleActions[HTMLEleName] = {
				expression : expressionV || "" //表达式
				,fn:fnV //处理函数
				,checkFn:checkFnV //检查函数，是否缺少属性等，如果不合法返回false
				,description : descriptionV || copiedObject.description //方法描述
			};
		}
	}
})(jQuery);
