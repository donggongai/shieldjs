[TOC]

### shieldjs2框架
![](https://img.shields.io/badge/release-2.0.0-blue.svg)
#### 项目说明
- chouui,在原项目基础上做的一套ui系统。


#### 兼容性
##### JS版本兼容性说明
- jQuery             1.12.4   主流浏览器         Jquery类库【核心】
- ShieldJS           2.1.0    主流浏览器         ShieldJS类库【核心】
- arithUtil          1.0.0    主流浏览器         算术工具类，用于浮点型计算
- cronGen            1.0.0    IE8及以上            cron表达式
- doT                -        主流浏览器         js模板语言
- formvalidate       3.0.0    主流浏览器         表单验证
- fullcalendar       3.10.0	  IE9及以上            日历控件  https://fullcalendar.io/support Firefox, Chrome, Safari, IE 9+,Moment 2.9.0+
- jquery.cookie      1.4.1    主流浏览器         cookie操作
- jquery.form        4.2.2    主流浏览器          jquery表单操作
- jquery.metadata    -        主流浏览器         原数据处理
- jquery.tableDnd    1.0.3    IE8及以上           表格拖拽
- highlight          9.13.1   未测试                   代码高亮显示
- layer              3.1.1    主流浏览器         弹出框 
- moment.js          2.10.6   主流浏览器     JavaScript 日期处理类库 参考：http://momentjs.cn
- select2            4.0.3    IE8及以上         选择输入框 IE 8+、Chrome 8+、Firefox 10+、Safari 3+、Opera 10.6+
- qTips2             3.0.3    IE8及以上           消息提示
- timeDropper        -        IE10及以上        时间选择器
- ueditor            1.4.3.3  主流浏览器         富文本编辑器 
- WdatePicker        4.8Beta3 主流浏览器        日期选择器  
- ztree              3.5.18   主流浏览器         树形结构

##### 使用

#### 版本说明
