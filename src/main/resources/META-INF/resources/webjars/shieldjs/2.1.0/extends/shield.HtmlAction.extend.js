//自定义的html标签的js动作处理的扩展方法
/** 
 * @description 示例一，不带checkFn的函数
 * @field
 * 必需：
 *  第一个参数为自定义html动作处理的名字，注意与已有的是否冲突，如果冲突会用新的覆盖旧的（可以用来重写方法而不改变框架原有方法）
 *  第二个参数为需处理的元素的jquery选择器表达式
 *  第三个参数为js动作初始方法
 *  （均为jquery对象）
 *  contentE:处理的内容对象，menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第四个参数为方法描述
 */
ShieldJS.HtmlAction.addHTMLEleAction("selfAction", ".selfAction", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.unbind("click").click(function(){
			ShieldJS.alert("消息提示", "自定义js处理动作示例1", "info");
			return false;
		});
	}
},"自定义示例一，不带checkFn的函数");
/**
 * @description 示例二，带checkFn的函数
 * @field
 * 必需：
 *  第一个参数为自定义html动作处理的名字，注意与已有的是否冲突，如果冲突会用新的覆盖旧的（可以用来重写方法而不改变框架原有方法）
 *  第二个参数为需处理的元素的jquery选择器表达式
 *  第三个参数为js动作初始方法
 *  （均为jQuery对象）
 *  selectE:处理的jquery对象（checkFn中返回的对象），menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第四参数为刷新符合条件的html元素（如是否缺少相关联的父子容器或存储容器），没有符合条件的返回false，有则返回合格条件的jQuery对象
 *  （均为jQuery对象）
 *  contentE:处理的页面对象，menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第五个参数为方法描述
 */
ShieldJS.HtmlAction.addHTMLEleAction("selfAction2", ".selfAction2", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.unbind("click").click(function(){
		ShieldJS.alert("消息提示", "自定义js处理动作示例2", "info");
		return false;
	});
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		// 可进一步筛选
		return dealEle; //返回符合条件的jQuery对象
	}
	return dealEle.length > 0 ? dealEle : false;
},"自定义示例二，带checkFn的函数");
