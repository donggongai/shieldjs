%~d0
cd %~dp0
java -jar ../../../../tool/compiler.jar --js ../../core/shield.util.js ../../core/shield.main.js ^
 ../../core/shield.core.js ../../core/shield.HtmlAction.js ^
 modules/shieldjs.tab.js modules/shieldjs.nav.js modules/shieldjs.page.js modules/shieldjs.checkbox.js modules/shieldjs.radio.js modules/shieldjs.select.js ^
 shield.V2.HtmlAction.extend.js ^
 --js_output_file shield.v2.min.js
