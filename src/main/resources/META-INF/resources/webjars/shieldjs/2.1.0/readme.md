[TOC]

### shieldjs2框架
![](https://img.shields.io/badge/release-2.0.0-blue.svg)
#### 项目说明
- 富客户端框架(jQuery RIA framework)设计目标是简单实用、扩展方便、快速开发、RIA思路、轻量级；
本框架支持用html扩展的方式来代替javascript代码, 只要懂html语法, 再参考使用手册就可以做ajax开发；尽量让程序员少写javascript，也能使用各种页面组件和ajax技术。如果有特定需求也可以扩展框架做定制化开发。
本框架的目标是不需要开发人员去关心javascript怎么写，只要写标准html就可以了。本框架简单扩展了html标准, 给HTML定义了一些特别的 class 和 attribute 。框架会找到当前请求结果中的那些特别的 class 和 attribute, 并自动关联上相应的js处理事件和效果。
;
- 第一次打开页面时载入界面到客户端，之后和服务器的交互只是数据交互，不占用界面相关的网络流量。
- 支持HTML扩展方式来调用组件。
- 标准化Ajax开发, 降低Ajax开发成本。


#### 目录说明
- core    shieldjs2框架的核心库，必须引入，不引入不能使用;
- doc      shieldjs2框架的说明文档;
- docstrap shieldjs2框架的说明文档的样式模板;
- depends shieldjs2框架依赖的js库，可以不引入，不引入部分功能不能使用;
- extends shieldjs2框架的扩展库，不需要不引入，只是做了示例，在具体实现类中扩展;
- impl    shieldjs2框架的具体实现，必须引入，不引入不能使用大部分功能;
- tool    shieldjs2框架包含的模块库，必须引入，不引入不能使用，可独立于该框架作为工具类使用;

#### 命名说明
- 提示框                          ShieldJS.alert(title, msg, type); //type字符型（success、error、info、warning）

- 确认框		     ShieldJS.confirm(title, message, ok, cancel)

- data-url       链接

- data-url-node   节点url，用于树形结构等

- data-url-show定义新增按钮的url【非必需，没有可不填】

- data-url-add定义新增按钮的url【非必需，没有可不填】

- data-url-upd定义修改按钮的url【非必需，没有可不填】

- data-url-del定义删除按钮的url【非必需，没有可不填】

- data-target    目标对象 data-target，搜索按钮，

- paramSelector  参数选择器,动态参数

- paramAdapter   参数适配器,适配 源字段名1>目标字段名1#源字段名2[>目标字段名1]，源字段名与目标字段名一致时目标字段名可省略

- selectNode     选中节点,从0开始

- shieldSettings 插件参数设置

- data-rel-type 类型shieldDialog：iframe ，addtabs：new 

- data-rel-id id用于区分tab等

- 操作列 S|U|D，R代表Show查看，U代表Update更新，D代表Delete删除可以不写值,showName用于该列title前缀【非必需】，没有时取tab标题栏的.title内容 

- 不需要初始化的插件元素，增加样式<mark>noshield</mark>

- 不需要表单验证的input元素，增加样式<mark>ignore</mark>（disabled的也不进行验证）

#### 使用说明
1、绑定html动作前后的回调方法，可以增加bindBefore和bindAfter属性，属性值为方法名，建议方法名写长一些。
```html
<!-- bindAfter表示绑定完成后回调的方法，在页面事件绑定完成后执行，bindBefore是在之前 -->
	<div class="multi-tab_plugin" style="overflow: hidden;" bindAfter="addTab_191009">
		<div class="mtab-box" >
			<ul class="mtab-title clearfix">
				<li class="on title" data-url="${ctxPathA }/system/permission/getPermDetails" shieldParams="{id:'${id }'}">权限信息</li>
				<!-- // on 点击状态 -->
				<li class="title" data-url="${ctxPathA }/system/permission/getHasPermRoles" shieldParams="{id:'${id }'}">拥有权限的角色</li>
			</ul>
			
		</div>
		<div class="mtab-content">
			<!-- //内容部分  -->
			<div class="mtab-item multi-show">
				<%@include file="sysperm_show_permdetail.jsp"%>
			</div>
			<!-- //multi-show 当前标签内容部分 -->
			<div class="mtab-item" style="display: none;">
				<%@include file="sysperm_show_haspermroles.jsp"%>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctxPath}/js/lib/jquery.contextmenu.r2.js?20190903085211"></script>
	<script>
	var addTab_191009 = function(ele){
		// ele为绑定事件的元素
		var $this = ele;
		alert($this.length)
		$this.multiTabAdd("test", "test", "测试", 1, "/software-center/a/system/permission/getHasPermRoles?a=b", "{id:'566578227917619200'}");
        $this.multiTabAdd("test", "test1", "测试1");
        $this.multiTabAdd("test2", "test2搜索", "测试2");
        $this.multiTabAdd("test3", "test3为", "测试3");
        $this.multiTabAdd("test4", "test4释放的", "测试4");
        $this.multiTabAdd("test5", "test5释放的", "测试5");
        $this.multiTabAdd("test6", "test6说法是", "测试6");
        $this.multiTabAdd("test7", "test7释放的", "测试7");
        $this.multiTabAdd("test8", "test8说法是", "测试8");
	}
	</script>
```

#### 插件属性    

  插件属性采用3种方式获取

 - 1、ShieldJS自定义属性采用**“属性名=属性值”**的配置方式，具体属性参考具体使用说明；
```html
<!-- myname为自定义属性名 -->
<a myname="test">示例</a>
```
```javascript
// 获取
ele.attr("myname");
```
 - 2、本框架采用jQuery的data定义属性,大部分的配置可以采用**“data-属性名”**方式配置，取值时使用data(属性名)取，注意data取值默认会把属性名转换为小写，如果需要**大写用“-”连接**，如需要去data("userName"),则需要定义为data-user-name。也支持以json格式设置到shieldSettings中；
 例如：
（1）、需要在js中使用jQueryObject.data("name")，则属性配置为data-name="test";
```html
<a data-name="test">示例</a>
```
```javascript
// 获取
ele.data("name");
```
（2）、需要在js中使用jQueryObject.data("nameType")，属性配置为data-name-type="test";
```html
<a data-name-type="test">示例</a>
```
```javascript
// 获取,注意T需要为大写，反过来如果为大写的字母，属性前一定要用-连接
ele.data("nameType");
```
（3）、配置属性shieldSettings='{name:"test",show:false}'，框架会自动解析并传入options中。
使用 getExtendSettings 获取设置的属性（包含data和shieldSettings都会获取，其中shieldSettings优先级最高）
```html
<!-- 注意本例子中shieldSettings和data-name中都定义了name属性，以shieldSettings优先 -->
<a shieldSettings='{name:"test",show:false}' data-name="myname">示例</a>
```
```javascript
// 获取
var params = {};
getExtendSettings(ele, params);
```
 - 3、第三方插件的属性配置，支持以json格式设置到shieldSettings中，同上。
json回调 一般要求有**是否成功（success）**，**返回信息（message）**，**返回结果**3个字段  
例如：  
```javascript
{  
	"success":true,
    "message":"添加成功！",
	"data":[{},{}]
}  
```

#### 扩展插件

如果默认提供的插件不满足要求可以对插件进行覆盖或增加新的插件
- 扩展插件
扩展插件的2种写法： 
```javascript
/** 
 * @description 示例一，不带checkFn的函数
 * @field
 * 必需：
 *  第一个参数为自定义html动作处理的名字，注意与已有的是否冲突，如果冲突会用新的覆盖旧的（可以用来重写方法而不改变框架原有方法）
 *  第二个参数为需处理的元素的jquery选择器表达式
 *  第三个参数为js动作初始方法
 *  （均为jquery对象）
 *  contentE:处理的内容对象，menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第四个参数为方法描述
 */
ShieldJS.HtmlAction.addHTMLEleAction("selfAction", ".selfAction", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.unbind("click").click(function(){
			ShieldJS.alert("消息提示", "自定义js处理动作示例1", "info");
			return false;
		});
	}
},"自定义示例一，不带checkFn的函数");
/**
 * @description 示例二，带checkFn的函数
 * @field
 * 必需：
 *  第一个参数为自定义html动作处理的名字，注意与已有的是否冲突，如果冲突会用新的覆盖旧的（可以用来重写方法而不改变框架原有方法）
 *  第二个参数为需处理的元素的jquery选择器表达式
 *  第三个参数为js动作初始方法
 *  （均为jQuery对象）
 *  selectE:处理的jquery对象（checkFn中返回的对象），menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第四参数为刷新符合条件的html元素（如是否缺少相关联的父子容器或存储容器），没有符合条件的返回false，有则返回合格条件的jQuery对象
 *  （均为jQuery对象）
 *  contentE:处理的页面对象，menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第五个参数为方法描述
 */
ShieldJS.HtmlAction.addHTMLEleAction("selfAction2", ".selfAction2", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.unbind("click").click(function(){
		ShieldJS.alert("消息提示", "自定义js处理动作示例2", "info");
		return false;
	});
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		// 可进一步筛选
		return dealEle; //返回符合条件的jQuery对象
	}
	return dealEle.length > 0 ? dealEle : false;
},"自定义示例二，带checkFn的函数");
```
- 内置方法
   - 1、获取参数
       - getParams(ele, params, attrName) , attrName可以不写，不写默认找shieldParams属性的定义值（json字符串）
       - getSettings(ele, _default) 取属性shieldSettings的设置值）
       - getExtendSettings(ele, _default) 取属性shieldSettings的设置值覆盖，不存在则取jQuery.data()设置的属性，一般用于第三方插件的扩展原插件属性。
       - ShieldJS.HtmlAction.checkAnGetParamSelector(ele, params, contentEle);
       获取动态参数，ele上定义paramSelector属性（值为jQuery选择器表达式）
       ```html
       <input id="export" type="button" class="shieldA" shieldParams="{'conn':'${conn}'}" paramSelector=":checkbox[name='tableName']" data-method="post" data-url="${ctxPathA}/system/outdatabase/toOutWord" value="导出Word表结构"/>
       <input type="checkbox" name="tableName" value="name1"/>
       <input type="checkbox" name="tableName" value="name2"/>
       
       <javascript>
       var ele = $("#export");
       // 第三个参数contentEle表示查询参数的范围，不写默认查内容页
       ShieldJS.HtmlAction.checkAnGetParamSelector(ele, params);
       </javascript>
       ```

#### 版本说明
##### v2.0.0（2018-08-13）
- 初版，在原来内部使用的框架基础上改造为通用框架; 
