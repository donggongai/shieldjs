ShieldJS.tab = {
	ele : {}
	,css : {/*样式*/
		/*标题*/
		TITLE:'shield-tab-title',
		/*内容*/
		CONTENT:'shield-tab-content',
		/*标题子类*/
		TITLE_CHILD:'shield-tab-title-item',
		/*标题子类更多*/
		TITLE_CHILD_MORE:'more',
		TITLE_CHILD_MORE_CON:'shield-tab-more-child',
		/*内容子类*/
		CONTENT_CHILD:'shield-tab-item',
		CLOSE:'shield-tab-close'
	}
	,selector :{}
	,addClose :{}
	,check : {}
	,init : {}
	,add : {}
}
ShieldJS.tab.selector = {/*选择器*/
	/*标题*/
	TITLE:'.'+ShieldJS.tab.css.TITLE,
	/*内容*/
	CONTENT:'.'+ShieldJS.tab.css.CONTENT,
	/*标题子类*/
	TITLE_CHILD:'.'+ShieldJS.tab.css.TITLE_CHILD,
	/*标题子类更多*/
	TITLE_CHILD_MORE:'.'+ShieldJS.tab.css.TITLE_CHILD_MORE,
	/*标题子类更多 内容容器*/
	TITLE_CHILD_MORE_CON:'.'+ShieldJS.tab.css.TITLE_CHILD_MORE_CON,
	/*内容子类*/
	CONTENT_CHILD:'.'+ShieldJS.tab.css.CONTENT_CHILD,
	CLOSE:'.'+ShieldJS.tab.css.CLOSE
}
//添加关闭按钮
ShieldJS.tab.addClose = function(containerE, titleContainerEle, liE) {
	var allowclose = "false";
	if (hasAttr(containerE,"allowclose")) {//是否允许关闭
		allowclose = containerE.attr("allowclose");
	}
	if (allowclose=="true") {
		var closeHtml = '<i class="shield-icon '+ShieldJS.tab.css.CLOSE+'">&#xe607;</i>';
		var addLiEClose = function(ele) {
			var $this = ele;
			var thisAllowclose = "true";
			if (hasAttr($this,"allowclose")) { //是否允许关闭
				thisAllowclose = $this.attr("allowclose");
			}
			if (thisAllowclose=="true") {
				var iEle = $this.find("i");
				if (iEle.length == 0) { //已经存在则使用存在的
					$this.append(closeHtml);
				}
			}
        }
		if (titleContainerEle && titleContainerEle.length >0) {
			var titleEs = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).add(titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD_MORE).find(ShieldJS.tab.selector.TITLE_CHILD));
			titleEs.each(function(){
				addLiEClose($(this));
			});
        } else if (liE && liE.length >0 ) {
        	addLiEClose(liE);
		}
    }
}
//校验
ShieldJS.tab.check = function(ele) {
	var result = {
		hasErr : false,
		objEle :null
	}
	var shieldTabE = ele;
	var eleName = "tab元素";
	if (shieldTabE.length > 0) {
		shieldTabE.each(function(){
			var shieldTabSelectE = $(this);
			var titleContainerEle = shieldTabSelectE.children(ShieldJS.tab.selector.TITLE);
			var contentContainerEle = shieldTabSelectE.children(ShieldJS.tab.selector.CONTENT);
			if (titleContainerEle.length == 0) {
				ShieldJS.error(eleName + "未定义标题容器：请在对应的元素上添加class："+ShieldJS.tab.css.TITLE + titleContainerEle.prop("outerHTML"));
				result.hasErr = true; //return true 相当于是 continue，而 return false 相当于是 break。 
				return false;
			}
			if (titleContainerEle.length > 0) {
				ShieldJS.tab.addClose(shieldTabSelectE, titleContainerEle); //添加关闭按钮
			}
			if (contentContainerEle.length == 0) {
				ShieldJS.error(eleName + "未定义内容容器：请在对应的元素上添加class："+ShieldJS.tab.css.CONTENT + ele.prop("outerHTML"));
				result.hasErr = true; //return true 相当于是 continue，而 return false 相当于是 break。 
				return false;
			}
			var itemEles = contentContainerEle.children(ShieldJS.tab.selector.CONTENT_CHILD);
			if (itemEles.length == 0) {
				ShieldJS.error(eleName + "未定义内容项：请在对应的元素上添加class："+ShieldJS.tab.css.CONTENT_CHILD + contentContainerEle.prop("outerHTML"));
				result.hasErr = true; //return true 相当于是 continue，而 return false相当于是 break。 
				return false;
			}
			var titleLen = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).length; //标题元素数
			var titleLenMore = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD_MORE).find(ShieldJS.tab.selector.TITLE_CHILD).length;
			var titleLenTotal = titleLen+titleLenMore;
			var contentLen = itemEles.length;
			if (titleLenTotal != contentLen ) {
				ShieldJS.error(eleName + "头部标签数目（" + titleLenTotal + "）与内容标签数目（" + contentLen + "）不相等，请检查！"+ ele.prop("outerHTML"));
				result.hasErr = true; //return true 相当于是 continue，而 return false相当于是 break。 
				return false;
			}
		});
		result.objEle = shieldTabE;
	}
	return result;
}
// 初始化，初始化之前需要校验
ShieldJS.tab.init = function(ele) {
	ele.each(function(){
		var shieldTabSelectE = $(this);
		var titleContainerEle = shieldTabSelectE.children(ShieldJS.tab.selector.TITLE); //标题
		var contentContainerEle = shieldTabSelectE.children(ShieldJS.tab.selector.CONTENT); //内容
		var v = 0; //默认选中的值
		if (hasAttr(shieldTabSelectE, "v")) {
			v = parseInt(shieldTabSelectE.attr("v"))||0;
        }
		if (v < 0) {
	        v = 0;
        }
		var itemLength = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).length;
		if (v >= itemLength) {
	        v = itemLength - 1;
        }
		titleContainerEle.on("click", ShieldJS.tab.selector.TITLE_CHILD, function() {
			var titleEle = $(this);
			var titleEs = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).add(titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD_MORE).find(ShieldJS.tab.selector.TITLE_CHILD));
			var thisIndex = titleEs.index(titleEle);
			itemEle = contentContainerEle.children(ShieldJS.tab.selector.CONTENT_CHILD).eq(thisIndex);
			titleEs.removeClass(ShieldJS.css.THIS);
			titleEle.addClass(ShieldJS.css.THIS);
			contentContainerEle.children(ShieldJS.tab.selector.CONTENT_CHILD).removeClass(ShieldJS.css.SHOW);
			itemEle.addClass(ShieldJS.css.SHOW);
			var moreConEle = titleEle.closest(ShieldJS.tab.selector.TITLE_CHILD_MORE_CON);
			if (moreConEle.length >0) {
				moreConEle.show();
            }
			return false;
        });
		// 关闭事件
		titleContainerEle.on("click", ShieldJS.tab.selector.CLOSE, function() {
			var closeEle = $(this);
			var titleEle = closeEle.closest(ShieldJS.tab.selector.TITLE_CHILD);
			var titleMore = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD_MORE);
			var titleEs = titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).add(titleMore.find(ShieldJS.tab.selector.TITLE_CHILD));
			var thisIndex = titleEs.index(titleEle);
			itemEle = contentContainerEle.children(ShieldJS.tab.selector.CONTENT_CHILD).eq(thisIndex);
			if (titleEle.hasClass(ShieldJS.css.THIS)) { //如果是选中的,重新处理选中事件
				titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).removeClass(ShieldJS.css.THIS);
				titleEle.prev().addClass(ShieldJS.css.THIS);
				contentContainerEle.children(ShieldJS.tab.selector.CONTENT_CHILD).removeClass(ShieldJS.css.SHOW);
				itemEle.prev().addClass(ShieldJS.css.SHOW);
            }
			titleEle.remove();
			itemEle.remove();
			if (titleMore.find(ShieldJS.tab.selector.TITLE_CHILD).length == 0) { //更多里面没有子元素了
				titleMore.remove(); 
            }
			return false; //阻止冒泡
        });
		// 更多
		titleContainerEle.on("click", ShieldJS.tab.selector.TITLE_CHILD_MORE, function() {
			var moreEle = $(this);
			moreEle.find(ShieldJS.tab.selector.TITLE_CHILD_MORE_CON).toggle();
        });
		if (v != 0) { //设置默认选中的值
			titleContainerEle.children(ShieldJS.tab.selector.TITLE_CHILD).get(v).click();
        }
		
	});
}
// 添加tab
ShieldJS.tab.add = function(ele, tabid, tabTitle, content) {
	var result = {
		titleE : null,
		contentE :null
	}
	var tabTitleEle = ele.children(ShieldJS.tab.selector.TITLE);
	var tabContentEle = ele.children(ShieldJS.tab.selector.CONTENT);
	var relEle = tabTitleEle.find("[tabid='"+tabid+"']");
	var titleMore = tabTitleEle.children(ShieldJS.tab.selector.TITLE_CHILD_MORE);
	
	var tabContentShowEle;
	if (relEle.length == 0) { //没有，需新建
		if (titleMore.length == 0) { //没显示更多
			tabTitleEle.append('<li class="'+ShieldJS.tab.css.TITLE_CHILD+'" tabid="'+tabid+'">'+tabTitle+'</li>');
			tabContentEle.append('<div class="'+ShieldJS.tab.css.CONTENT_CHILD+'"></div>');
			var titleWidth = tabTitleEle.width();
			var titleLiWidth = 0;
			tabTitleEle.children(ShieldJS.tab.selector.TITLE_CHILD).each(function() {
				//$(this).width();//只是获取content宽度
				//$('div').innerWidth();//获取content+padding的宽度
				//$('div').outerWidth();//获取content+padding+border的宽度
				titleLiWidth += $(this).outerWidth(true);//获取content+padding+border+margin的宽度
            });
			
			if (titleLiWidth > titleWidth) { //超过长度则显示更多
				
				tabTitleEle.find(ShieldJS.tab.selector.TITLE_CHILD+":last").remove();
				tabTitleEle.append('<li class="more" tabid="more"><i class="shield-icon">&#xe66f;</i>'+
						'<dl class="shield-tab-more-child">'+
						'<dd class="'+ShieldJS.tab.css.TITLE_CHILD+'" tabid="'+tabid+'">'+ tabTitle +'</dd>'+
						' </dl></li>');
            }
        } else { //显示了更多
        	titleMore.find(ShieldJS.tab.selector.TITLE_CHILD_MORE_CON).append('<dd class="'+ShieldJS.tab.css.TITLE_CHILD+'" tabid="'+tabid+'">'+ tabTitle +'</dd>');
			tabContentEle.append('<div class="'+ShieldJS.tab.css.CONTENT_CHILD+'"></div>');
        }
		relEle = tabTitleEle.find(ShieldJS.tab.selector.TITLE_CHILD+":last");
		tabContentShowEle = tabContentEle.children(ShieldJS.tab.selector.CONTENT_CHILD+":last");
		ShieldJS.tab.addClose(ele, null, relEle); //添加关闭按钮
    } else { //如果存在tabid相同的直接返回
    	relEle.text(tabTitle);
    	ShieldJS.tab.addClose(ele, null, relEle); //添加关闭按钮
    	var thisIndex = tabTitleEle.children(ShieldJS.tab.selector.TITLE_CHILD).index(relEle);
    	tabContentShowEle = tabContentEle.children(ShieldJS.tab.selector.CONTENT_CHILD).eq(thisIndex);
    }
	
	relEle.click();
	tabContentShowEle.html(content);
	result.titleE = relEle;
	result.contentE = tabContentShowEle;
	return result;
}
//移除tab
ShieldJS.tab.del = function(ele, tabid) {
	var result = {
		titleE : null,
		contentE :null
	}
	var tabTitleEle = ele.children(ShieldJS.tab.selector.TITLE);
	var relEle = tabTitleEle.children("[tabid='"+tabid+"']");
	
	var tabContentShowEle;
	if (relEle.length > 0) {
    	relEle.children(ShieldJS.tab.selector.CLOSE).click();
    }
	result.titleE = relEle;
	result.contentE = tabContentShowEle;
	return result;
}