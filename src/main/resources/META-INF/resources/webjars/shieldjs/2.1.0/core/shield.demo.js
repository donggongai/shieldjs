/** 
 * 	::::::html扩展使用示例::::::
 *  
 ****************************** 导航菜单（shield-nav）**********************************
 *	典型场景：导航菜单
 *	注意：父元素必需为ul【必需】添加样式shield-nav【必需】，子元素li【必需】添加样式shield-nav-item【必需】
 * 	<ul class="shield-nav shield-layout-left" id="navMenu">
   		<%-- .menu表示框架中头部菜单,doParClickEle执行父类点击操作的jquery选择器,如果为直接父类可以用..表示，不写则表示不执行父类点击操作，此功能为解决冒泡被阻止的问题，选中样式是在父类执行的 --%>
        <li class="shield-nav-item"><a class="menu" doParClickEle=".." data-url="${ctxPath }/test/v2/myInfo/leftBar.jsp"><i class="shield-icon">&#xe64b;</i>账户管理</a></li>
        <li class="shield-nav-item"><a class="menu" doParClickEle=".." data-url="${ctxPath }/test/v2/team/leftBar.jsp"><em class="shield-icon">&#xe723;</em>团队管理</a></li>
 ****************************** 左侧菜单（shield-menu）**********************************
 *	典型场景：左侧菜单
 *	注意：父元素必需为ul【必需】添加样式shield-menu【必需】，子元素li【必需】添加样式shield-menu【必需】，父元素selectNode【非必需】表示默认选中节点，为-1表示默认不选中
 * <ul class="shield-nav shield-nav-tree shield-menu" selectNode="2" lay-filter="test">
	<li class="shield-nav-item shield-nav-itemed">
       	<a class="" href="javascript:;"><i class="shield-icon shield-font16 shield-nav-icon">&#xe623;</i><span class="title">账户相关</span><span class="shield-nav-more"></span></a>
       	<div class="admin113-nav-info"><span class="shield-caret"></span>
			<div class="shield-nav-side-info">
              	<div class="shield-nav-child-scroll">
			       	<dl class="shield-nav-child">
						<dd class="shield-menu shieldNavTab" data-url="${ctxPath }/test/v2/myInfo/account.jsp" rel="myInfo-account"><a><i class="shield-icon">&#xe655;</i><span class="shield-menu-title">账户信息</span></a></dd>
			          	<dd class="shield-menu shieldNavTab" data-url="${ctxPath }/test/v2/myInfo/account.jsp" rel="myInfo-archiveCount"><a><i class="shield-icon">&#xe655;</i><span class="shield-menu-title">档案统计</span></a></dd>
			       	</dl>
              	</div>
 			</div>
		</div>
	</li>
 * 
 ****************************** tab页面打开（shieldNavTab）**********************************
 *	典型场景：左侧菜单点击后在内容区的navTab中新增内容
 *	<%-- 增加样式shieldNavTab【必需】，rel【必需】用来确定navTab的唯一性，rel相同的新的会覆盖旧的，viewTitle【非必需】用于navTab上显示标题（不定义默认确认取子元素.shield-menu-title的文本值）
 *	relType区分是否为iframe relType="iframe" relHeight用于返回后的高度 relHeight="730" --%>
	<dd class="shield-menu shieldNavTab" data-url="${ctxPath }/test/v2/myInfo/account.jsp" rel="myInfo-account"><a><i class="shield-icon">&#xe655;</i><span class="shield-menu-title">账户信息</span></a></dd>
 * 	
 * 	<%-- 上面例子是默认回调的处理，也可定义自定义回调方法，在调用的元素上增加successDo属性  --%>
 *
 * ****************************** select 下拉框（shield-select）**********************************
 * 	<%-- 下拉框，增加样式shield-select【必需】，selected【非必需】为选中，disabled【非必需】为禁用，通过设定属性 shield-search【非必需】 来开启搜索匹配功能，v【非必需】表示默认值--%>
	<select name="interest" class="shield-select" <%-- shield-search --%> <%-- v="1" --%> >
		<option value="">所有</option>
		<option value="-1" disabled="disabled">无效选项</option>
		<option value="0" >充值</option>
		<option value="1" selected>转账</option>
		<!-- 通过optgroup分组 -->
		<optgroup label="城市记忆">
	    	<option value="你工作的第一个城市">你工作的第一个城市？</option>
	  	</optgroup>
	  	<optgroup label="学生时代">
		    <option value="你的工号">你的工号？</option>
		    <option value="你最喜欢的老师">你最喜欢的老师？</option>
	  	</optgroup>
 * </select>
 * ****************************** checkbox 复选框（shield-checkbox）**********************************
 * 	<!-- 复选框 ，增加样式shield-checkbox【必需】，title【非必需】定义显示文本，如果使用原始样式shd-skin【非必需】="primary"（shd-skin="switch"表示开关） disabled【非必需】禁用， checked【非必需】选中 -->
	<input type="checkbox" name="searchself" title="自己" class="shield-checkbox" shd-skin="primary">
	<!-- <input type="checkbox" name="searchself" title="自己" class="shield-checkbox" disabled> -->
	<!-- <input type="checkbox" name="searchself" title="自己" class="shield-checkbox" checked> -->
	<!-- <input type="checkbox" name="searchself" title="自己" class="shield-checkbox" shd-skin="primary"> -->
 * 
 * ****************************** 开关（shield-checkbox的变形）**********************************
 * 	<!-- 开关 ，增加样式shield-checkbox【必需】，title【非必需】定义显示文本用“|”连接不同显示文本，其中“|”前面的文字为开启状态显示文字，“|”后面的文字为关闭状态显示文字，
 * 	shd-skin【必需】="switch" disabled【非必需】禁用， checked【非必需】选中 -->
	<input type="checkbox" name="open" class="shield-checkbox" shd-skin="switch"> 	<!-- 无标题 -->
	<input type="checkbox" name="open" title="开启|关闭" class="shield-checkbox" shd-skin="switch" checked> <!-- 有标题，切换时分别显示“开启”、“关闭”，默认选中，即开启 -->
	<input type="checkbox" name="open" title="开启|关闭" class="shield-checkbox" shd-skin="switch" disabled> <!-- 有标题，切换时分别显示“开启”、“关闭”，不可用 -->
 *
 ***************************** radio 单选框（shield-radio）**********************************
	<!-- 单选框 ，增加样式shield-radio【必需】title【非必需】定义显示文本， disabled【非必需】禁用，checked【非必需】选中 -->
	<input type="radio" name="searchselfRadio" title="自己" value="0" class="shield-radio" <%-- disabled --%>>
	<input type="radio" name="searchselfRadio" title="全部" value="1" class="shield-radio" checked="checked">
 * 
 * ****************************** 可折叠容器（shield-collapsable）**********************************
 * <%-- 可折叠容器shield-collapsable，为图标区域shield-icon,需要有标题区class="shield-colla-title"，内容区：class="shield-colla-content" --%>
	<div class="shield-colla-item shield-collapsable" style="width: 100%;">
		<div class="shield-colla-title">用户信息</div>
		<div class="shield-colla-content shield-show"></div>
	</div>
 *
 * ****************************** 翻页（shield-page）**********************************
 * <!-- 分页，增加class为shield-page【必需】，templid为模板id【非必需】，与模板中的templid对应一般不添加即可，curPage【必需】为当前页数，totalPage【必需】为总页数，totalCount【必需】为总条数，curColor【非必需】为选中项的背景色
	pageUrl【非必需】为连接地址，如果分页内容包含在form中此项非必填，反之，此项必填，groups【非必需】定义连续页码的个数，默认5，
	target【非必需】翻页后新页面覆盖的内容区，不写默认找最近的form，form存在则直接提交form，form不存在则找当前显示的内容区
	first【非必需】定义页数过多翻到后面页面是首页显示文字，默认1，false不显示; last定义页数过多时尾页显示文字，默认最后一页数，false不显示 ; prev定义上一页显示文字，默认“上一页”; next定义下一页显示文字，默认“下一页” -->
	<div id="demo1" style="text-align: right;" class="shield-page" <%-- templid="shield-page-1" --%><%-- curColor="red" --%> 
		<%-- last="false" first="false" --%> <%-- next="NEXT" prev="PREV" --%>
		pageUrl="${ctxPath}${pageContext.request.requestURI }?p=" curPage="${empty param.p ? 1 :param.p }" totalPage="17" totalCount="200">
	</div>
 *
 * ****************************** tab多页签容器（shield-tab）**********************************
 * <!-- 分页，增加class为shield-tab【必需】，v为默认选中的tab下标（从0开始）【非必需】 -->
	<div class="shield-tab shield-tab-brief admin113-tab-brief01" v="${t }">
	<ul class="shield-tab-title">
		<li class="shield-this">个人信息</li>
		<li>修改密码</li>
		<li>密保问题</li>
	</ul>
	<div class="shield-tab-content p0">
		<div class="shield-tab-item shield-show"></div>
		<div class="shield-tab-item"></div>
		<div class="shield-tab-item"></div>
	</div>
</div>
 *
 ****************************** 更新状态（ajax）**********************************
 *	典型场景：启用、禁用、是否查看状态更新
 *	<%-- 增加样式shieldAjax【必需】，viewTitle【非必需】用于弹出确认信息可不加 --%>
 * 	<a data-url="/analyticsback/updateUrlSetStatus?url=${curtainModel.url }&status=0" class="shieldAjax" viewTitle="确定要更新URL设置状态吗?"><input type="button" class="usstate0" value="禁用"></a>
 * 	
 * 	<%-- 上面例子是默认回调的处理，也可定义自定义回调方法，在调用的元素上增加successDo属性  --%>
 * 	<a data-url="/analyticsback/updateUrlSetStatus?url=${curtainModel.url }&status=0" class="shieldAjax" successDo="shieldExtend12" viewTitle="确定要更新URL设置状态吗?">
 * 	<script type="text/javascript">
 * 		<%-- 自定义方法由三种  --%>
 * 		<%-- 1、覆盖全局方法 ShieldJS.HtmlAction.successDoMethod.shieldExtend,， shieldExtend 为successDo属性的值，shieldExtend为固定名称   --%>
 * 		<%-- 2、增加全局方法 ShieldJS.HtmlAction.successDoMethod.shieldExtend12， shieldExtend12 为successDo属性的值，shieldExtend12也可以命名为其他名字 --%>
 * 		<%-- 2、增加局部方法，下面的例子即是，shieldExtend12 为successDo属性的值，shieldExtend12也可以命名为其他名字 --%>
 * 		var shieldExtend12 = function(contentE, searchForm, menuliSelect, dialog, oldRefresh, result){
 *		if (dialog) {
 *			dialog.remove();
 *		}
 *		if (searchForm) {
 *			if (!oldRefresh) { //兼容旧框架的json返回，不重复刷新
 *				searchForm.submit();
 *			}
 *		} else if (menuliSelect) {
 *			menuliSelect.click();
 *		}
 *	};
 *	</script>
 *
 ****************************** 删除（ajax）**************************************
 *	典型场景：删除（回调同上）
 *	<%-- 增加样式shieldAjax【必需】，viewTitle【非必需】用于弹出确认信息可不加 --%>
 * 	<a data-url="/analyticsback/deleteUrlset?url=${curtainModel.url }" class="shieldAjax" viewTitle="确定要删除该URL设置信息吗?"><input type="button" class="usdelete" value="删除"/></a>	
 * 
 ****************************** 带选择参数器的 ajax 操作 ************************************
 *	典型场景：批量操作显示、隐藏，选择页面参数（jQuery选择器）（回调同上）
 *	<%-- ajax操作的基础上，增加了paramSelector 用来增加批量操作的选择器（本例中意思为选择checkbox中name为id的值） --%>
 * 	<a data-url="/lawyer/batchDelete4LawyerConsult" paramSelector=":checkbox[name='id']" class="shieldAjax" viewTitle="删除信息会把对应的回复全部删除，确定要删除选中的咨询信息吗?">
 * 		<input type="button" class="doDelete" value="删除">
 * 	</a>
 * 
 * 	<%-- 具体示例可参考      审核管理》企业推广管理 --%>
 * 	<%-- 参数选择器可以使用内置变量 {selectRows}，直接提交选中行的参数，需要接口checkbox的样式shieldSelectRow一起使用--%>
 * 	<%-- 添加样式shieldSelectRow用来标记选中行 ，方便选中--%>
 *	<td><input type="checkbox" class="shieldSelectRow" name="companyId" value="${company.id}"></td>
 *	<%-- 提交的shieldAjax，注意此处的参数选择器，paramSelector里面有参数{selectRows}表示复选框选中行的数据作为参数提交，如果只想选一部分可以在里面增加选择器
 *		{selectRows>:checkbox}意思为只提交选中行的的checkbox作为参数,用'>'连接选择器，里面的选择器为jQuery选择器 --%>
 *	<input type="button" value="添加" data-url="/extensions/batchAdd" paramSelector="{selectRows},#baseSearch #type" class="addAll shieldAjax" />
 *
 ****************************** A链接 shieldA ********************************************
 *	典型场景：打开新页面附加参数, 如预览，具体示例见账户管理》政府配置》企业前台展示》预览(已改为shieldSubmit，效果一致，可改为shieldA，shieldSubmit必须在form内部参数与from一致可不写参数，shieldA必须有参数不一定在form内)
 *	<%-- 增加样式shieldA，paramSelector为参数选择器遵循jQuery选择器语法，data-url链接 --%>
 * 	<!-- <a class="shieldA" paramSelector="#govCreditCustomForm" data-url="/govCreditCustom/toPreview" target="_blank"><input type="button" style="margin-left: 10px;" class="preview" value="预览"></a> -->
 *	<input type="button" style="margin-left: 10px;" class="preview shieldA" paramSelector="#govCreditCustomForm" data-url="/govCreditCustom/toPreview" value="预览">
			
 ****************************** 弹出窗修改、添加 shieldDialog *********************************
 *	典型场景：添加、修改、查看（保存后默认关闭当前窗口，并刷新，若不需要关闭在调用处添加属性successDo="shieldDonothing"）
 *	<%-- 增加样式shieldDialog，viewTitle用于弹出框的标题，viewWidth表示弹出框宽度，高度自动调节
 *	confirm为确认信息（非必需）confirm="是否确认执行该操作？"--%>
 * 	<a data-url="/analyticsback/toAddUrlset?url=${curtainModel.url }" class="shieldDialog" viewTitle="修改URL设置" viewWidth="400"><input type="button" class="usedit" value="编辑"/></a>	
 *
 * ****************************** 查找带回 shieldLookup *********************************
 *	典型场景：用于弹出窗口选择条件（与shieldDialog用法类似，多了id与name的定义，参考示例：审核管理》企业推广》企业推广详情，“选择”按钮的处理）
 *	<%-- 添加样式shieldLookupId，返回id --%>
 *	<input type="hidden" id="shareId" name="shareId" class="shieldLookupId">
 *	<%-- 添加样式shieldLookupName，返回显示 --%>
 *	<b>推广标题：</b><input type="text" id="shareTitle" name="shareTitle" class="shieldLookupName" readonly="readonly" style="width:15em;">
 *	<%-- 增加样式shieldLookup，viewTitle用于弹出框的标题，viewWidth表示弹出框宽度，高度自动调节
 *	confirm为确认信息（非必需）confirm="是否确认执行该操作？"--%>
 * 	<a data-url="/analyticsback/toAddUrlset?url=${curtainModel.url }" class="shieldLookup" viewTitle="修改URL设置" viewWidth="400"><input type="button" class="usedit" value="编辑"/></a>	
 *
 *	<%-- 弹出页处理 --%>
 *	<td class="pl10">
 *		<%-- 增加样式selectBtn表示为选择按钮，标明是选择按钮，dataId用于传递id，dataName用于显示名称 --%>
 *		<input class="infoAdd selectBtn" type="button" dataId="${model.id }" dataName="${model.title }" value="选择">
 *	</td>
 *
 ****************************** 页面局部加载 shieldLoad ************************************
 *	典型场景：内容页面中存在正常显示为，可搜索企业并对搜索企业结果进行处理，具体示例见审核管理》企业推广管理
 * 	<form action="/limitCompany/searchCompany" method="post" class="formcom searchForm partSearchForm">
 *		<div id="showCompanyList" name="showCompanyList"class="showCompanyList">
 *		……
 *			<%-- 增加样式shieldLoad data-url定义查询路径 增加了paramSelector 用来做参数选择器 ，搜索结果加载到target="#searchResultF" --%>
 *			<input class="doSearchCom shieldLoad" data-url="/limitCompany/searchCompany" paramSelector=".partSearchForm" 
 *				type="button" name="doSearchCom"id="doSearchCom" value="查询" target="#searchResultF">
 *			<%-- 搜索结果加载到的区域 target="#searchResultF" --%>
 *			<div id="searchResultF" name="searchResultF" class="searchResultF"></div>		
 *		</div>
 *	 </form>
 * 
 ****************************** 打开新窗口 shieldOpenWindow *********************************
 *	典型场景：付费、第三方认证（需打开新页面，当前页面可能随着页面打开修改页面显示），具体示例见账户管理》账户信息》充值，账户管理》账号绑定等
 *	<a data-url="/account/toRecharge" class="shieldOpenWindow" viewTitle="充值" submitTips="充值完成后请到【账户管理】--【收支明细】列表查看具体信息！" viewWidth="400">
 *		<input type="button" value="充值" class="toRecharge">
 *	</a>
 *	<%-- 提交页面，class="contentBody" --%>
 *	<div class="contentBody">
 *		<%-- 提交页面的表单，target="_blank" class="saveForm" --%>
 *		<form action="/account/pkiPay" method="post" target="_blank" class="saveForm">
 *		……
 *		<div class="opt"><input type="submit" value="提交"></div>
 *		</form>
 *	</div>		
 *
 * ****************************** 带参数提交按钮 shieldSubmit ************************************
 *  典型场景：审核通过、审核退回（根据审核结果传递不同参数）
 * 	<input type="hidden" name="state" id="state" />
 * 	<%-- shieldParams 中增加需要赋的值，注意键对应的是input的name，点击时自动触发所在form的提交并根据参数赋值 --%>
 * 	<input type="button" class="shieldSubmit" shieldParams="{state:0}" value="审核通过">
 *	<input type="button" class="shieldSubmit" shieldParams="{state:1}" value="退回申请">
 *
 ****************************** 上传图片 ****************************************
 *  典型场景：图片上传
 * 	<div class="clearfix imgUpLoad">
 * 		<%-- name属性与 上传按钮的inputFileName属性值一致，保存上传后图片地址 --%>
 *		<input name="image" type="hidden" value="${model.image }"/>
 *		<div class="img">
 *			<%-- 上传成功后显示图片的元素，添加样式imgShow --%>
 *			<img id="imageNews" name="imageNews" class="imgShow" width="70" src="${model.image }">
 *		</div>					
 *		<div class="option">
 *			<%-- 增加样式 class="shieldImageUpload"，定义上传路径submitUrl，图片存放input的name：inputFileName，如果需要其他参数可添加submitUrlParams属性，
 *			形式如submitUrlParams="companyId=123"，表示在原路径后增加companyId参数，值为123 --%>
 *			<span class="ms" style="margin-left:0px"><input type="button" class="imageUpload shieldImageUpload" 
 *			submitUrl="indexImgUpload" inputFileName="image" value="上传图片"/>
 *			<%-- 增加样式 class="shieldImgDelete"，删除按钮 --%>
 *			<input type="button" value="删除" class="imgsourceDelete shieldImgDelete" style="display: ${not empty model.imgsource?'':'none' }"/><br>
 *			</span><br/>
 *			<div style="color:red">支持的图片类型：JPG,JPEG,GIF,BMP,PNG</div>
 *		</div>
 *	</div>
 *
 ****************************** 多图上传 ************************************
 *	典型场景：评论图片上传
 *	<td>
 *		<div id="divImgUl" >
 *			<%-- 图片容器 class="shieldImgUL"--%>
 *			<ul class="shieldImgUL">
 *				<%-- 添加按钮 class="imgUpLiBefore"--%>
 *	        	<li class="imgUpLiBefore"><input type="button" class="imgUploadScHandle" id="uploadScPic" /></li>
 *			</ul>
 *		</div>
 *		<div class="con_bottom">
 *			<div class="img">
 *				<span id="uploadImgc">
 *					<%-- name属性与 上传按钮的inputFileName属性值一致，保存上传后图片地址 --%>
 *					<input class="inputxt scImg" name="photourls" type="hidden" value=""/>
 *					<%-- 增加样式 class="shieldMultiImgUpload"，定义上传路径submitUrl，图片存放input的name：inputFileName --%>
 *					<input type="button" class="shieldMultiImgUpload" value="上传" submitUrl="indexImgUpload" inputFileName="photourls"/>
 *				</span>
 *			</div>
 *		</div>
 *	</td>
 *
 ****************************** 下载附件 ****************************************
 * 	典型场景：下载附件，具体示例见审核管理》简历管理
 * 	<tr>
 *		<td style="width:20%;"><label>个人简历：</label></td>
 *		<td colspan="3">
 *			<%-- 增加样式shieldDownload downloadUrl为下载url existUrl为下载前先判断文件是否存在的地址，参数通过shieldParams传递 --%>
 *			<a class="recruitDownload shieldDownload" shieldParams="{id:${model.resumeId},resumeId:${model.id }}"
 *					downloadUrl="downloadRecruit" existUrl="isExitRecruitDownFile">${model.resumeName }</a>
 *		</td>
 *	</tr>
 * 
 ****************************** 复选框全选 ****************************************
 * 	典型场景：复选框全选（选择当前页面的所有复选框），具体示例见审核管理》推广企业管理、特殊企业管理、行业新闻管理等
 * 	<%-- 添加id checkall --%>	
 * 	<th><input type="checkbox" id="checkall"/></th>
 *	……	
 *	<%-- 页面需要有checkbox --%>	
 *	<td><input type="checkbox" name="id" value="${model.id}" /></td>
 *
 ****************************** 关联select ****************************************
 *	典型场景：关键词类型为替换是输入替换词，其他不显示，具体示例可参考账户管理》配置网址》更新抓取状态，审核管理》关键词管理》修改、添加
 *	以下为option联动示例
 *	<%--选择1时显示 id为dealBySpider的元素 选择2是显示id为yaunyinSpider的元素--%>
 *	<select id="state" name="state" class="shieldSelectRelated">
 *		<option value="">--请选择--</option>
 *		<option value="1" relatedTo="#dealBySpider">通过</option>
 *		<option value="2" relatedTo="#yaunyinSpider">退回</option>
 *	</select>
 *
 *	下面为select联动示例，具体示例参考审核管理》简历管理》搜索
 *	<%-- 联动select,relatedTo为联动目标（jQuery选择器，本例中为id为job的select），relatedUrl为联动url，其中department={}代表department为当前select选中的值，
 *		主要为了参数名与select名字不一致时，也可以使用参数选择器paramSelector，paramAdapter为参数适配器，将relatedUrl返回的json（注意必须为json格式）的字段适配为
 *		select可使用的字段，适配器中id为option的值name为option的显示文本如果字段名本身为id和name，此属性可不写，下面的示例中可用写为paramAdapter="name=jobName"，id省略表示与默认值一致 --%>
 *	<select name="department" id="department" class="shieldSelectRelated" relatedTo="#job" relatedUrl="/resume/getJobList?department={}" paramAdapter="id#name=jobName">
 *		<option value="0">请选择</option>
 *		<option value="1" relatedTo="#jobspan">技术部</option>
 *		<option value="2">信息采编部</option>
 *		<option value="3">客服部</option>
 *		<option value="4">招商部</option>
 *	</select>
 *	<span id="jobspan"><b>招聘职位：</b>
 *	<select name="job" id="job">
 *		<%-- 样式为 noshield，说明联动时不覆盖，一般用于类似搜索全部的“请选择” --%>
 *		<option value="0" class="noshield">请选择</option>
 *	</select></span>
 *
 * ****************************** 关联checkbox或radio ****************************************
 *	典型场景：checkbox选中时提交按钮可用，反选时不可用，具体示例参考审核管理》认证服务》品牌认证，交费功能
 *	<%-- shieldCheckedRelated关联操作checkbox或radio元素，relatedTo为关联到的元素，relatedToType="disabled"表示将目标元素更改disabled属性（其他值：disabledAndHide及不写）  --%>
 *	<sapn style="float: left;padding: 3px;"><input type="checkbox" id="acceptSetBtn" class="shieldCheckedRelated" relatedTo="#buySubmit" relatedToType="disabled">我已阅读并接受此规定</sapn>
 *		<input type="button" id="buySubmit" class="shieldSubmit" value="购买" disabled="disabled">
 *
 ****************************** 图片select组件 **********************************
 *  典型场景：缩略图、头像（图片作为select的下拉内容），具体示例可参考新闻发布的缩略图
 *	<%-- ddslick父容器 class="ddslickSelectContainer" --%>
 *	<td colspan="3" class="ddslickSelectContainer">
 *	<c:if test="${not empty imgSrcList }">
 *		<div style="margin: 10px 5px;">
 *		<%-- ddslick值 class="ddslickSelectValue" --%>
 *		<input id="newsimgSource" class="ddslickSelectValue" name="image" type="hidden" value="${model.image }">
 *		<%-- ddslick选择 class="ddslickSelect" --%>
 *		<select id="demo-htmlselect" name="imgSource1" class="ddslickSelect">
 *		<%-- data-imagesrc属性为图片地址 --%>
 *	<c:forEach items="${imgSrcList }" var="imgsrc">
 *	       	<option value="${imgsrc }" data-imagesrc="${imgsrc }" ${model.image==imgsrc?'selected':'' }></option>
 *	</c:forEach>
 *	       </select>
 *		</div>
 *	</c:if>
 *  </td>
 *
 ****************************** 多tab页签 **********************************
 *  典型场景：档案相关处理（进入内容区域后头部有多个页签，点击页签后进入页面区内容），具体示例可参考团队管理点击节点后页面、账户管理的过期提醒等
 *	<%-- shieldHeaderUrl 针对内部url一致只是参数不一致的情况（例如通用页面），配置的是公共部分,区别的部分再返回页面中v属性处理
 *	clickExtendFn="newsItemClickExtendFn" 为自定义的节点点击扩展方法，调用action前的处理
 * 	errCallbackFn="checkShopExist" 为自定义的元素点击失败后的扩展回调方法 completeCallbackFn="checkShopExist" 为自定义的元素点击完成后的扩展回调方法，用法同errCallbackFn --%>
 *	<li class="partyGovManageOpenPublish  menu" data-url="/partyGovOpenPublishManage/toOpenPublish" shieldHeaderUrl="/partyGovOpenPublishManage/toSearch">
 *		<img src="images/page.gif">&nbsp;&nbsp;<span>公开信息管理</span>
 *	</li>
 *	<%-- v来区分类型【必需】，传参是格式为 type=1，增加shieldHeader样式 --%>			
 *  <div class="openPublishHeader clearfix header shieldHeader">
 *		<a href="javascript:void(0)" v="1">领导信息</a>
 *		<a href="javascript:void(0)" v="2" errCallbackFn="checkShopExist">内设机构</a>
 *		<a href="javascript:void(0)" v="3" clickExtendFn="newsItemClickExtendFn">直属机构</a>
 *  </div>
 *  <%-- 增加shieldTabContent样式 --%>
 *	<div class="contentBody shieldTabContent"></div>
 *	<%-- 可能用到搜索表单不自动提交的情况，notAutoSubmit 表示不自动提交，默认searchForm会自动搜索，可能造成搜索两次 --%>
 *	<form action="/company/toShowCompanyPay" t="searchResult" class="searchForm" notAutoSubmit="true">
 *		……
 *	</form>
 * 	<script>
 * 	<%-- 自定义节点点击点击失败后的扩展回调方法 --%>
 *  <%-- data：返回内容，thisContentE：内容dom（jquery对象，包括后面的），shieldHeader：头部header，curHeaderA：当前选中头部A，
 *  menuliSelect：左侧栏选中的menu navMenuSelect：导航栏选中的menu 内置变量：contentE内容区（jquery对象） --%>
 *	var checkShopExist = function(data, thisContentE, shieldHeader, curHeaderA, menuliSelect, navMenuSelect){
 *		var json = data;
 *		var message = json.message;
 *		if(message.indexOf("创建店铺") > 0){ // 返回信息中包含 创建店铺
 *			contentE.find(".toshowMallShop").click(); // 进入店铺创建页面
 *			return false;
 *		}
 *		return true;
 *	}
 *	<%-- 自定义节点点击扩展方法，返回布尔型，true表示继续走框架的处理，false不再执行 --%>
 *	<%-- thisContentE：内容dom（jquery对象，包括后面的），shieldHeader：头部header，curHeaderA：当前选中头部A，
 *  menuliSelect：左侧栏选中的menu navMenuSelect：导航栏选中的menu 内置变量：contentE内容区（jquery对象） --%>
 *	function newsItemClickExtendFn(thisContentE, shieldHeader, curHeaderA, menuliSelect, navMenuSelect){
 *		var id = treeNode.id;
 *		var newsType = treeNode.newsType;
 *		if (newsType != null) {
 *			if (newsType == 21) {
 *				if (id == 1000) {
 *					treeNode.nodeClickUrl = "/specialTopic/toSearch"; // 专题配置特殊处理
 *					<%-- 返回true继续走后续处理 --%>
 *					return true;
 *
 *					//完全自己处理的情况
 *					//ajax.get("/specialTopic/toSearch", {}, contentE,function() {
 *						//内容区处理
 *						//if(!ShieldJS.core.contentAreaDeal(contentE, null, null)){ //shield.core.js
 *							//return false;
 *						//}
 *					//});
 *					<%-- 返回false不再执行框架方法 --%>
 *					//return false;
 *				}
 *			}
 *		}
 *		return true;
 *	}
 ****************************** 树形菜单 **********************************
 *  典型场景：新闻、团队管理，具体示例可参考团队管理、新闻发布
 *	<div class="content">
 *		<%-- shieldZTree树形结构菜单 data-url返回结果  nodeClickUrl表示点击节点的url selectNode表示默认选中的节点（key=value形式或直接写value，直接写value默认认为选择的是第N个节点（从0开始））
 *		 clickExtendFn="newsItemClickExtendFn" 为自定义的节点点击扩展方法
 *		paramAdapter 代表追加到url中的节点返回的参数，多个参数用#号分隔，本例中typeId=id#newsType 代表取node中id的属性作为参数传递，传递时参数名为typeId，newsType参数名不变，
 *		即最终拼出的url为/publish/header?typeId=${node的id属性值}&newsType=${newsType的值} 
 *		也可以直接在node中增加nodeClickUrl属性： map.put("nodeClickUrl", "/specialTopic/toSearch"); // 专题配置特殊处理
 *		注意：异步点击事件参数需要预处理时可以增加asyncsetting属性，定一个json格式字符串，或者自定义initZTree(treeE, jsTreeSetting)预处理方法
 *		注意data-url方法中需要返回的为json格式，返回的字段要求有name（显示名称）,id(唯一编码),pId(父类id)，其他字段根据情况确定，如nodeClickUrl（点击后的链接）
 *		--%>
 *		// 例子：初始化ztree参数（覆盖即可）
		var initZTree = function(treeE, jsTreeSetting){
			var showfreezeusers = 0;
			var showquitusers = 0;
			jsTreeSetting.async = {
				enable : true,
				url : "/team/list",
				autoParam : [ "id" ],
				otherParam: {"showfreeze":showfreezeusers, "showquit":showquitusers}
			};
		};
 *		<ul id="typeList" class="ztree shieldZTree" data-url="/publish/typeList" selectNode="rownum=2"  clickExtendFn="newsItemClickExtendFn"
 *			nodeClickUrl="/publish/header" paramAdapter="typeId=id#newsType">
 *		</ul>
 *	</div>
 * 	<script>
 * 	<%-- 自定义节点点击扩展方法，返回布尔型，true表示继续走框架的处理，false不再执行 --%>
 * 	<%-- e：点击事件，treeId：树dom的id，treeNode点击的节点，contentE内容区（jquery对象） --%>
 *	function newsItemClickExtendFn(e, treeId, treeNode, contentE){
 *		var id = treeNode.id;
 *		var newsType = treeNode.newsType;
 *		if (newsType != null) {
 *			if (newsType == 21) {
 *				if (id == 1000) {
 *					treeNode.nodeClickUrl = "/specialTopic/toSearch"; // 专题配置特殊处理
 *					<%-- 返回true继续走后续处理 --%>
 *					return true;
 *
 *					//完全自己处理的情况
 *					//ajax.get("/specialTopic/toSearch", {}, contentE,function() {
 *						//内容区处理
 *						//if(!ShieldJS.core.contentAreaDeal(contentE, null, null)){ //shield.core.js
 *							//return false;
 *						//}
 *					//});
 *					<%-- 返回false不再执行框架方法 --%>
 *					//return false;
 *				}
 *			}
 *		}
 *		return true;
 *	}
 *	</script>
 * ****************************** cron定时任务选择器shield-cron **********************************
 *	典型场景：复杂js的实现
 *	<!-- 定时器，增加class shield-cron -->
 *	<input class='shield-cron' type="text" value="0 0/1 * * * ?"></div>
 *	<!-- 可选样式qtip-jtools,qtip-tipped,qtip-bootstrap,qtip-tipsy,qtip-youtube,qtip-default,qtip-light,qtip-dark,qtip-red,qtip-green,qtip-blue更多参考http://qtip2.com/demos -->
 *	<!-- hide定义触发隐藏的动作,show定义触发显示的动作 -->
 *	<input type="text" id="shieldCronValue" class="qtips" content="test" titlebutton="true" title="12311" qtipsClass="qtip-light qtip-rounded" hide="unfocus" show="click scroll">
 *							
 ****************************** 扩展自定义方法 **********************************
 *	典型场景：复杂js的实现
 *	<%-- 调用shield.extend.jd的shieldDialogCallback["sciTechReport"] （需自己定义）shieldDonothing 非必需表示当前提交后什么都不做（默认关闭dialog刷新搜索结果） --%>
 * 	<div class="shieldDialogCallback shieldDonothing" callback="sciTechReport">
 * 		……
 * 	</div>
 * 
 ********************************************************************************
 */