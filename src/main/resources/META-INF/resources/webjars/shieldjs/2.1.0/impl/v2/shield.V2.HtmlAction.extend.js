ShieldJS.setVersion("V2");
//覆盖
var ShieldJS_V2 = {
	templ : {SELECT:{},CHECKBOX:{},SWITCH:{},RADIO:{},PAGE:{}}
	,templPath : ""
	,initTempl : function() {
		if (!this.templPath) {
			this.templPath = this.getJsPath(); //方法来源自core/shield.main.js会找此文件路径
			if (this.templPath.indexOf("core") != -1) { //未压缩
				this.templPath += "../impl/v2/templ";
            } else {
            	this.templPath += "../templ"; //压缩后可能需要自定义模板路径
			}
		}
		console.log("js file path: " + this.templPath);
		this.loadfile(ShieldJS.templPath+'/select.html', function(content){
			ShieldJS.templ.SELECT['default'] = content;
			var pageEles = content.split("<separator/>");
			if (pageEles.length > 0) {
				$.each(pageEles, function(i, n) {
					var thisPageEle = n;
					if (thisPageEle) {
						if (thisPageEle.indexOf("templid")==-1 || thisPageEle.indexOf('templid=="default"') != -1 || thisPageEle.indexOf('templid==""') != -1) {
							ShieldJS.templ.SELECT['default'] = thisPageEle;
						} else {
							// 括号表示组。访问可以用group[index]来访问每组的信息  
							var linkRegx = /.+templid="(.+?)"/;  
							var group = thisPageEle.match(linkRegx);
							if (group && group.length == 2) {
								var tempid = group[1];
								ShieldJS.templ.SELECT[tempid] = thisPageEle;
							}
						}
					}
				});
			}
			ShieldJS.debug(ShieldJS.templPath+"/select.html content: " + ShieldJS.templ.SELECT); 
		});
		this.loadfile(ShieldJS.templPath+'/checkbox.html', function(content){
			ShieldJS.templ.CHECKBOX['default'] = content;
			var pageEles = content.split("<separator/>");
			if (pageEles.length > 0) {
				$.each(pageEles, function(i, n) {
					var thisPageEle = n;
					if (thisPageEle) {
						if (thisPageEle.indexOf("templid")==-1 || thisPageEle.indexOf('templid=="default"') != -1 || thisPageEle.indexOf('templid==""') != -1) {
							ShieldJS.templ.CHECKBOX['default'] = thisPageEle;
						} else {
							// 括号表示组。访问可以用group[index]来访问每组的信息  
							var linkRegx = /.+templid="(.+?)"/;  
							var group = thisPageEle.match(linkRegx);
							if (group && group.length == 2) {
								var tempid = group[1];
								ShieldJS.templ.CHECKBOX[tempid] = thisPageEle;
							}
						}
					}
				});
			}
			ShieldJS.debug(ShieldJS.templPath+"/checkbox.html content: " + ShieldJS.templ.CHECKBOX); 
		});
		this.loadfile(ShieldJS.templPath+'/switch.html', function(content){
			ShieldJS.templ.SWITCH['default'] = content;
			var pageEles = content.split("<separator/>");
			if (pageEles.length > 0) {
				$.each(pageEles, function(i, n) {
					var thisPageEle = n;
					if (thisPageEle) {
						if (thisPageEle.indexOf("templid")==-1 || thisPageEle.indexOf('templid=="default"') != -1 || thisPageEle.indexOf('templid==""') != -1) {
							ShieldJS.templ.SWITCH['default'] = thisPageEle;
						} else {
							// 括号表示组。访问可以用group[index]来访问每组的信息  
							var linkRegx = /.+templid="(.+?)"/;  
							var group = thisPageEle.match(linkRegx);
							if (group && group.length == 2) {
								var tempid = group[1];
								ShieldJS.templ.SWITCH[tempid] = thisPageEle;
							}
						}
					}
				});
			}
			ShieldJS.debug(ShieldJS.templPath+"/switch.html content: " + ShieldJS.templ.SWITCH); 
		});
		this.loadfile(ShieldJS.templPath+'/radio.html', function(content){
			ShieldJS.templ.RADIO['default'] = content;
			var pageEles = content.split("<separator/>");
			if (pageEles.length > 0) {
				$.each(pageEles, function(i, n) {
					var thisPageEle = n;
					if (thisPageEle) {
						if (thisPageEle.indexOf("templid")==-1 || thisPageEle.indexOf('templid=="default"') != -1 || thisPageEle.indexOf('templid==""') != -1) {
							ShieldJS.templ.RADIO['default'] = thisPageEle;
						} else {
							// 括号表示组。访问可以用group[index]来访问每组的信息  
							var linkRegx = /.+templid="(.+?)"/;  
							var group = thisPageEle.match(linkRegx);
							if (group && group.length == 2) {
								var tempid = group[1];
								ShieldJS.templ.RADIO[tempid] = thisPageEle;
							}
						}
					}
				});
			}
			ShieldJS.debug(ShieldJS.templPath+"/radio.html content: " + ShieldJS.templ.RADIO); 
		});
		this.loadfile(ShieldJS.templPath+'/page.html', function(content){
			ShieldJS.templ.PAGE['default'] = content;
			var pageEles = content.split("<separator/>");
			if (pageEles.length > 0) {
				$.each(pageEles, function(i, n) {
					var thisPageEle = n;
					if (thisPageEle) {
						if (thisPageEle.indexOf("templid")==-1 || thisPageEle.indexOf('templid=="default"') != -1 || thisPageEle.indexOf('templid==""') != -1) {
							ShieldJS.templ.PAGE['default'] = thisPageEle;
						} else {
							// 括号表示组。访问可以用group[index]来访问每组的信息  
							var linkRegx = /.+templid="(.+?)"/;  
							var group = thisPageEle.match(linkRegx);
							if (group && group.length == 2) {
								var tempid = group[1];
								ShieldJS.templ.PAGE[tempid] = thisPageEle;
							}
						}
					}
				});
			}
			ShieldJS.debug(ShieldJS.templPath+"/page.html content: " + ShieldJS.templ.PAGE); 
		});
	}
	,initExtend : function(options) {
		if (options.templPath) {
			this.templPath = options.templPath;
		}
		ShieldJS.initTempl();
	}
	,loadingHtml:'<div class="admin113-loop"><i class="shield-icon shield-anim shield-anim-rotate shield-anim-loop">&#xe64c;</i><br>加载中</div>'
}
ShieldJS = $.extend(true, ShieldJS, ShieldJS_V2);
var colContentEle = $('.admin113-colu-content');
//cron表达式
ShieldJS.HtmlAction.addHTMLEleAction("shield-cron", ".shield-cron", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.each(function() {
	    var selectEthis = $(this);
	    var params = {
	    };
	    var targetE = getjQueryObj(selectEthis.attr("target"));
		var execTimesUrl = selectEthis.attr("execTimesUrl")||"";
		if (execTimesUrl) {
			params.execTimesUrl = execTimesUrl;
		}
	    selectEthis.cronGen(params);
    });
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if(!$.fn.cronGen){
			ShieldJS.error("检测到shield-cron元素缺少依赖JS库：cronGen.js！");
			return false;
		}
		dealEle.each(function() {
		    var selectEthis = $(this);
		    if (!selectEthis.is(":input")) {
		    	dealEle = dealEle.not(selectEthis);
		    	ShieldJS.error("检测到shield-cron元素不是input元素，请修改！"+selectEthis.prop("outerHTML"));
				return false; //true=continue，false=break。 
            }
	    });
		// 可进一步筛选
		return dealEle; //返回符合条件的jQuery对象
	}
	return dealEle.length > 0 ? dealEle : false;
},"cron表达式选择器");
//highCharts图
ShieldJS.HtmlAction.addHTMLEleAction("highChartsContainer", "#highChartsContainer", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var container = findAndMarkEle(contentE, this.expression);
	if(container.length>0){
		var intimeacctimeout;
		var loadInterval = function(container,series,series1,series2,series3,time){
			if(intimeacctimeout && container.is(":hidden") ){
				clearTimeout(intimeacctimeout);
			}else{
				if(hasAttr(container,"refreshUrl")){
					$.ajax({
						type: "POST",
						url: container.attr("refreshUrl"),
						dataType: "json",
						async: true,
						success: function(json){
							var x = json.xName, // current time
							xNext = json.xNameNext,
							y = json.data.count1,
							y1 = json.data.count2,
							y2 = json.data.count3,
							y3 = json.data.count4;
							saveOrupdatPoint(series, x, y,xNext);
							saveOrupdatPoint(series1, x, y1,xNext);
							saveOrupdatPoint(series2, x, y2,xNext);
							saveOrupdatPoint(series3, x, y3,xNext);
							intimeacctimeout = setTimeout(function() {
								loadInterval(container,series,series1,series2,series3,time);
							}, time);
						}
					});
				}
			}
		};
		var saveOrupdatPoint = function(series,x,y,xNext){
			var exist = false;
			for(var index=series.data.length-2;index<series.data.length;index++){
				var data = series.data[index];
				if(data.category==x || data.name==x){
					if(index==series.data.length-1){
						series.addPoint([xNext, 0], true, true);
					}
					exist = true;
					data.y=y;
					data.update();
					break;
				}
			}
			if(!exist){
				series.addPoint([x, y], true, true);
				series.addPoint([xNext, 0], true, true);
			}
		};
		
		if(hasAttr(container,"dataUrl")){ //有链接才处理
			var dataUrl = container.attr("dataUrl");
			container.html(ShieldJS.loadingHtml);
			var titleSuffix = "";
			if(hasAttr(container,ShieldJS.css.TITLE)){ //标题后缀
				titleSuffix = container.attr(ShieldJS.css.TITLE);
			}
			var yAxisTitle = "";
			if(hasAttr(container,"yAxisTitle")){ //y轴名称
				yAxisTitle = container.attr("yAxisTitle");
			}
			var xAxisTitle = "";
			if(hasAttr(container,"xAxisTitle")){ //x轴名称
				xAxisTitle = container.attr("xAxisTitle");
			}
			var tooltip = "";
			if(hasAttr(container,"tooltip")){ //tooltip名称
				tooltip = container.attr("tooltip");
			}
			var chartSetting = {
					//type: 'spline'
			}
			if(hasAttr(container,"refresh") && container.attr("refresh")=='true'){ //刷新
				var refreshMils = 3000; //默认3s刷新一次
				if(hasAttr(container,"refreshMils")){ //刷新时间，单位毫秒
					refreshMils = container.attr("refreshMils");
				}
				chartSetting = {
						events: {
							load: function() {
								var chart = this;
								var series = chart.series[0];
								var series1 = chart.series[1];
								var series2 = chart.series[2];
								var series3 = chart.series[3];
								loadInterval(container,series,series1,series2,series3,refreshMils);
							}
						}
				};
			}
			
			ShieldJS.ajax.post(dataUrl, contentE.find('form.searchForm').serialize(), function(json){
				json = $.parseJSON(json);
				container.highcharts({
					chart: chartSetting,
					title: {
						text: json.title + titleSuffix
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						title: {
							text: xAxisTitle
						},
						categories: json.xNames
					},
					yAxis: {
						title: {
							text: yAxisTitle
						},
						min: 0
					},
					tooltip: {
						crosshairs: true,
						shared: true,
						headerFormat: '<span style="font-size: 14px">{point.key}'+tooltip+'</span><br/>'
					},
					series: json.data
				});
			});
		}
	}
},"highCharts图");

//树形结构全选反选等按钮操作
ShieldJS.HtmlAction.addHTMLEleAction("shieldZtreeContainer" , function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var treeCheckNode = function(e) {
		var zTree = $.fn.zTree.getZTreeObj("permissionzTree"),
		type = e.data.type,
		nodes = zTree.getNodes();
		if (type == "checkAllTrue") {
			zTree.checkAllNodes(true);
		} else if (type == "checkAllFalse") {
			zTree.checkAllNodes(false);
		} else {
			var allnodes = zTree.transformToArray(nodes); //全部节点
			for (var i=0, l=allnodes.length; i<l; i++) {
				if (type == "toggle") {
					zTree.checkNode(allnodes[i], null, false, false);
				} else if (type == "togglePS") {
					zTree.checkNode(allnodes[i], null, true, false);
				}
			}
		}
	};
	var treeExpandNode = function(e) {
		var zTree = $.fn.zTree.getZTreeObj("permissionzTree"),
		type = e.data.type,
		nodes = zTree.getSelectedNodes();
		if (type == "expandAll") {
			zTree.expandAll(true);
		} else if (type == "collapseAll") {
			zTree.expandAll(false);
		}
	};
	findAndMarkEle(contentE, ".shieldZtreeContainer .allSelect").bind("click", {type:"checkAllTrue"}, treeCheckNode); //全选
	findAndMarkEle(contentE, ".shieldZtreeContainer .unAllSelect").bind("click", {type:"checkAllFalse"}, treeCheckNode); //全不选
	findAndMarkEle(contentE, ".shieldZtreeContainer .unSelect").bind("click", {type:"toggle"}, treeCheckNode); //反选
	findAndMarkEle(contentE, ".shieldZtreeContainer .expandAll").bind("click", {type:"expandAll"}, treeExpandNode); //全部展开
	findAndMarkEle(contentE, ".shieldZtreeContainer .collapseAll").bind("click", {type:"collapseAll"}, treeExpandNode); //全部收起
	//树形结构需考虑插件化
},"树形结构全选反选等按钮操作");
//扩展方法
//日期，调用My97DatePicker的日期插件
ShieldJS.HtmlAction.addHTMLEleAction("shieldDatePicker", "input.Wdate", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		try {
			WdatePicker;
      } catch (e) {
      	ShieldJS.error("缺少依赖JS库：My97DatePicker！"+dealEle.html());
      	return false;
      }
		var startDate = findAndMarkEle(contentE, 'input[init="startDate"]');
		var nowDate = findAndMarkEle(contentE, 'input[init="nowDate"]');
		if (startDate.length > 0) {
			startDate.each(function() {
				var $this = $(this); 
				var pattern = $this.attr("dateFmt");
				if (pattern) {
					$this.val(dateFormat(new Date(), pattern));
				} else {
					$this.val(dateFormat(new Date(), 'yyyy-MM-01'));
				}
			});
		}
		if (nowDate.length > 0) {
			nowDate.each(function() {
				var $this = $(this); 
				var pattern = $this.attr("dateFmt");
				if (pattern) {
					$this.val(dateFormat(new Date(), pattern));
				} else {
					$this.val(dateFormat(new Date(), 'yyyy-MM-dd'));
				}
			});
		}
		dealEle.each(function() {
			var $this = $(this);
			var options = {};
			var pattern = $this.attr("dateFmt");
			var onpicked = $this.attr("onpicked");
			if (pattern) {
				options.dateFmt = pattern;
			}
			if (onpicked) {
				options.onpicked = onpicked;
			} else {
				options.onpicked = function(dp) {
	                $(this).triggerHandler("change");//触发自定义change事件
              };
			}
			$this.click(function() {
				WdatePicker(options);
			});
		});
	}
}, "日期，调用My97DatePicker的日期插件");
//select下拉框
ShieldJS.HtmlAction.addHTMLEleAction("shield-select", ".shield-select", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.ShieldJSSelect();
	
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle = $.fn.ShieldJSSelect.filter(dealEle);
	return dealEle.length > 0 ? dealEle : false;
}, "select下拉框");
//checkbox复选框
ShieldJS.HtmlAction.addHTMLEleAction("shield-checkbox", ".shield-checkbox", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.ShieldJSCheckbox();
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle = $.fn.ShieldJSCheckbox.filter(dealEle);
	return dealEle.length > 0 ? dealEle : false;
},"checkbox复选框");
//radio单选框
ShieldJS.HtmlAction.addHTMLEleAction("shield-radio", ".shield-radio", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.ShieldJSRadio();
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle = $.fn.ShieldJSRadio.filter(dealEle);
	return dealEle.length > 0 ? dealEle : false;
}, "radio单选框");
//分页处理
ShieldJS.HtmlAction.addHTMLEleAction("shield-page", ".shield-page", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.ShieldJSPage({
		onSubmit: function(targetE) {
			ShieldJS.core.bindHtmlAction(targetE, menuli, menu, searchForm, shieldDialog); //内部继续绑定
	    }
	});
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle = $.fn.ShieldJSPage.filter(dealEle);
	return dealEle.length > 0 ? dealEle : false;
}, "分页处理");
//可折叠的容器
ShieldJS.HtmlAction.addHTMLEleAction("shield-collapsable", ".shield-collapsable", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.each(function() {
		var thisSelectE = $(this);
		
		var titleEle = thisSelectE.find(".shield-colla-title"); 
		var contentEle = thisSelectE.find(".shield-colla-content");
		var contentEleHeight = contentEle.height();
		var shieldCheckboxEle = thisSelectE.next();
		titleEle.on("click", ".block-icon", function() {
			var spendTime = 600; //变化用时
			if (contentEle.hasClass(ShieldJS.css.SHOW)) { //可见变为不可见
				contentEleHeight = contentEle.height(); //高度校正
				contentEle.animate({height: '0px',opacity: 0}, spendTime);
				$(this).html('<i class="shield-icon">&#xe6b6;</i>'); //<>展开图标
				setTimeout(function() {
					contentEle.toggleClass(ShieldJS.css.SHOW);
				}, spendTime);
			} else { //不可见变为可见
				contentEle.toggleClass(ShieldJS.css.SHOW);
				contentEle.animate({height: contentEleHeight+'px',opacity: 1}, spendTime);
				$(this).html('<i class="shield-icon">&#xe6ae;</i>'); //><收起图标
			}
		});
  });
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
			var $this = $(this);
			var titleEle = $this.find(".shield-colla-title"); 
			var contentEle = $this.find(".shield-colla-content");
			if (titleEle.find(".block-icon").length == 0) { //图标区，没有则创建
				titleEle.append('<span class="block-icon"></span>');
			}
			titleEle.find(".block-icon").each(function() {
				var iEle = $(this).find("i");
				if (iEle.length == 0) { //增加最小化图标><收起图标
					$(this).append('<i class="shield-icon">&#xe6ae;</i>');
				}
			});
			if (contentEle.length == 0) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-collapsable元素缺少标题容器shield-colla-title属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
			if (contentEle.length == 0) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-timer-close元素缺少内容容器shield-colla-content属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
      });
	}
	return dealEle.length > 0 ? dealEle : false;
}, "可折叠的容器");
ShieldJS.HtmlAction.addHTMLEleAction("shield-timer-close", ".shield-timer-close", function(selectE, menuli, menu, searchForm, shieldDialog){
	var time = getTimeAttr(selectE, "time");
	var timer = setTimeout(function() {
		var closeTime = getTimeAttr(selectE, "closeTime", 2000); //关闭用时，默认2秒
		if (closeTime > 0) {
			//selectE.animate({height: '0px',opacity: 0}, closeTime);
			selectE.slideUp(closeTime);
			setTimeout(function() {
				selectE.remove();
			}, closeTime);
      } else { //-1立刻移除
      	selectE.remove();
      }
	}, time);
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
			var $this = $(this);
			if (!hasAttr($this, "time")) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-timer-close元素缺少time（xx时间后关闭）属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
      });
	}
	return dealEle.length > 0 ? dealEle : false;
}, "div定时关闭");
ShieldJS.HtmlAction.addHTMLEleAction("shield-close", ".shield-close", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.click(function(){
			dealEle.closest(".shield-content").remove();
			//return false;
		});
	}
}, "div关闭");
//NavTab处理开始
//以NavTabNav方式打开(动作处理)
ShieldJS.HtmlAction.addHTMLEleAction("shieldNavTab", ".shieldNavTab", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		var siderLiMenusE = dealEle;
		
		siderLiMenusE.each(function(event) {
			var siderMenuClickEle = $(this); //选中的左侧菜单栏
			var tabid = siderMenuClickEle.data("relId");
			if (!tabid) {
				ShieldJS.error("检测到shieldNavTab元素缺少data-rel-id属性，请检查！"+siderMenuClickEle.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
			
			siderMenuClickEle.click(function() {
				if (hasAttr(siderMenuClickEle,"clickExtendFn")) { //扩展点击事件
					var clickExtendFnName = siderMenuClickEle.attr("clickExtendFn");
					clickExtendFnName = eval(clickExtendFnName);
					if ($.isFunction(clickExtendFnName)) {
						if(!clickExtendFnName(event, siderMenuClickEle, contentE)){
							//返回false则不再继续执行，true继续执行原逻辑
							return false;
						}
					}
				}
				
				if (siderMenuClickEle.data("url")) {//有链接才处理
					var hrefmenu = siderMenuClickEle.data("url");
					var tabTitle = siderMenuClickEle.attr(ShieldJS.css.TITLE);
					if (!tabTitle) {
						var menuTitleE = siderMenuClickEle.find(".shield-menu-title");
						if (menuTitleE.length >0) {
							tabTitle = menuTitleE.text();
						} else {
							tabTitle = siderMenuClickEle.text();
						}
					}
					//主内容区添加tab
					var targetEle = getjQueryObj(ShieldJS.core.options.contentNavTab);
					if(siderMenuClickEle.data("target")){
						targetEle = getjQueryObj(siderMenuClickEle.data("target"));
					}
					var result = ShieldJS.tab.add(targetEle, tabid, tabTitle, "content");
					tabContentShowEle = result.contentE; //内容区
					
					ShieldJS.avtiveTabContentE = tabContentShowEle;
					ShieldJS.activeTabTitleE = result.titleE;
					
					var relType = "";
					if(siderMenuClickEle.data("relType")){
						relType = siderMenuClickEle.data("relType"); //处理iframe的情况
					}
					var params={};
					if (relType == 'iframe') {
						changeFrameHeight = function (tabid) {
							var ifm = document.getElementById("iframe" + tabid);
							if (ifm) {
								ifm.height = ShieldJS.contentMainE.height();
							} else {
								ShieldJS.error("id为iframe" + tabid + "的iframe不存在！");
							}
							//alert($(ifm.document).find("body").length);
							$(ifm.document).find("body").onresize = function(){ alert(123);changeFrameHeight(tabid);}
						}
						var relHeight;
						if(siderMenuClickEle.data("height")){
							relHeight = parseInt(siderMenuClickEle.data("height")); //高度
						}
						var relHeightStr = ' onload="changeFrameHeight(\'' + tabid + '\')" ';
						if (relHeight) {
							relHeightStr = ' height="'+relHeight+'" ';
						}
						tabContentShowEle.html("").append('<iframe id="iframe' + tabid + '" width="100%" ' + relHeightStr + ' frameborder="0" scrolling="auto"></iframe>');
						$("#iframe" + tabid, tabContentShowEle).attr("src", hrefmenu+"?taddek="+Math.random());
					} else {
                  		var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector(siderMenuClickEle, params); //参数选择器
						if (!checkParams) {
							return false;
						} else {
							params = checkParams;
						}
						params = getParams(siderMenuClickEle, params);
						ShieldJS.ajax.get(tabContentShowEle, hrefmenu, params, function() {
	                  		//内容区处理
	                  		if(!ShieldJS.core.contentAreaDeal(tabContentShowEle, siderMenuClickEle)){
	                  			return false;
	                  		}
	                  		if (relHeight) {
	                  			tabContentShowEle.height(relHeight);
	                        }
                  		}, function(){ //失败移除
                  			ShieldJS.tab.del(contentE.find(".shield-content-tab"), tabid);
                  		});
                  	}
					return false; //阻止冒泡
				}
          });
			
		});
	}
}, "以NavTabNav方式打开(动作处理)");
//nav样式处理
ShieldJS.HtmlAction.addHTMLEleAction("shield-nav", ".shield-nav", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.ShieldJSNav();
	}
}, "nav样式处理");
//Nav展开收起
ShieldJS.HtmlAction.addHTMLEleAction("shield-nav-unfold", ".shield-nav-unfold", function(contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		//展开收起
		dealEle.each(function() {
			var $this = $(this);
			var sideNavEle = $this.closest(".shield-side");
			$this.click(function(){
				sideNavEle.find(".shield-nav-side-info").each(function() {
		            var $thisSideNavEle = $(this);
					if ($thisSideNavEle.find("div.title").length == 0) {
						var title = $thisSideNavEle.closest(".shield-nav-item").find("span.title").text();
						$thisSideNavEle.prepend('<div class="title">'+title+'</div>');
					}
	            });
				var oldWidth = sideNavEle.width();
				var oldLeft = ShieldJS.contentMainE.offset().left;
				sideNavEle.toggleClass("admin113-side-h"); //该样式表示收起侧边
				if (sideNavEle.hasClass("admin113-side-h")) { //收起
					ShieldJS.contentMainE.css("left", sideNavEle.width() + (oldLeft-oldWidth));
					var selectedItemEle = sideNavEle.find(".shield-menu.shield-this");
					var selectedItemEleParE = selectedItemEle.closest(".shield-nav-item");
					var selectedParEleOffset = selectedItemEleParE.offset();
					// 定位
//					selectedItemEleParE.find(".admin113-nav-info").addClass("admin113-nav-info-show");
					
//					selectedItemEleParE.find(".shield-caret").offset({top : selectedParEleOffset.top+selectedItemEleParE.height()/3});
//					var winHeight = $(window).height();
//					var sideInfoHeight = selectedItemEleParE.find(".shield-nav-side-info").height();
//					var sideInfoTop = selectedParEleOffset.top;
//					if (winHeight < selectedParEleOffset.top+sideInfoHeight) { //超出窗口高度
//						sideInfoTop = winHeight - sideInfoHeight;
//	                }
//					selectedItemEleParE.find(".shield-nav-side-info").offset({top : sideInfoTop});
	            } else { //展开
	            	sideNavEle.find(".admin113-nav-info").removeClass("admin113-nav-info-show");
	            	ShieldJS.contentMainE.css("left", sideNavEle.width() + (oldLeft-oldWidth));
	            }
			});
			//收起时，点击图标及父类元素，展开
			var shieldNavIconEles = findAndMarkEle(contentE, ".shield-nav-item");
			shieldNavIconEles.bind("click",function(){
				if (sideNavEle.hasClass("admin113-side-h")) { //收起时
					sideNavEle.find(".shield-nav-unfold").click();
				}
			});
			// 鼠标移入移出
			shieldNavIconEles.hover(function() {
				if (sideNavEle.hasClass("admin113-side-h")) { //收起时
					var hoverNavItemEle = $(this).closest(".shield-nav-item");
					var hoverNavItemEleOffset = hoverNavItemEle.offset();
					var navInfoHoverEle = hoverNavItemEle.find(".admin113-nav-info");
					navInfoHoverEle.addClass("admin113-nav-info-show");
					navInfoHoverEle.find(".shield-caret").offset({top : hoverNavItemEleOffset.top+hoverNavItemEle.height()/3});
					var winHeight = $(window).height();
					var sideInfoHeight = navInfoHoverEle.find(".shield-nav-side-info").height();
					var sideInfoTop = hoverNavItemEleOffset.top;
					if (winHeight < sideInfoTop+sideInfoHeight) { //超出窗口高度
						sideInfoTop = winHeight - sideInfoHeight;
					}
					navInfoHoverEle.find(".shield-nav-side-info").offset({top : sideInfoTop});
				}
          },function(){
          	if (sideNavEle.hasClass("admin113-side-h")) { //收起时
          		var hoverNavItemEle = $(this).closest(".shield-nav-item");
          		var navInfoHoverEle = hoverNavItemEle.find(".admin113-nav-info");
          		navInfoHoverEle.removeClass("admin113-nav-info-show");
          	}
          });
      });
	}
}, "Nav展开收起");
//Nav处理结束
//Tab处理开始
ShieldJS.HtmlAction.addHTMLEleAction("shield-tab", ".shield-tab", function(selectEs, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	ShieldJS.tab.init(selectEs);
},  function (contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	var shieldTabE = findAndMarkEle(contentE, this.expression);
	var result = ShieldJS.tab.check(shieldTabE);
	if (!result.hasErr && result.objEle) {
		return result.objEle;
	}
	return false;
}, "Tab处理");
//Tab处理结束
//第三方插件
ShieldJS.HtmlAction.addHTMLEleAction("ddslickSelect", ".ddslickSelect", function (selectEs, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	selectEs.each(function(){
		var ddslickSelectE = $(this);
		var container = ddslickSelectE.closest(".ddslickSelectContainer");
		ddslickSelectE.ddslick({ width: 125,height:200,onSelected: function(selectedData){ 
			var defaultPhoto = container.find(".dd-selected-value").val();
			container.find(".ddslickSelectValue").val(defaultPhoto);
		}})
	});
},  function (contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	var ddslickSelectE = findAndMarkEle(contentE, this.expression);
	var eleName = "ddslick第三方插件";
	if (ddslickSelectE.length > 0) {
		ddslickSelectE.each(function(){
			var ddslickSelectE = $(this);
			var container = ddslickSelectE.closest(".ddslickSelectContainer");
			if (container.length == 0) {
				ShieldJS.error(eleName + "未定义父类容器：请在父容器中添加class：ddslickSelectContainer");
				return false;
			} 
			if (container.find(".ddslickSelectValue").length == 0) {
				ShieldJS.error(eleName + "未定义选择后图片地址保存容器：请在对应的input容器上添加class：ddslickSelectValue");
				return false;
			} 
		});
		return ddslickSelectE;
	}
	return false;
}, "图片下拉选择(.ddslickSelectContainer .ddslickSelectValue .ddslickSelect)");
ShieldJS.HtmlAction.addHTMLEleAction("qtips", ".qtips", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	findAndMarkEle(contentE, this.expression).each(function() {
		var $this = $(this);
		var options = {};
		options.content = {
			text: $this.attr("content")
			, title: {
				text: $this.attr("title"),
				button: ($this.attr("titlebutton")=='true')||false
			}
		};
		options.style = {
			classes: $this.attr("qtipsClass")|| 'qtip-blue qtip-shadow qtip-rounded'     		
		};
		options.events = {
			visible: function(event, api) {
				//api.reposition(event);
			}
			,move: function(event, api) {  
	           // alert( event.pageX + '/' + event.pageY );  
	        }  
		};
		options.position = {
			target: 'event'
			,my: 'top center' /*指向图片在qtips的位置*/
			,at: 'bottom center' /*指向图片在元素的位置*/
			,viewport: $(window)
			,adjust: {resize:false, screen: false}
		};
		options.show = {
			target: $this
			,effect: function(offset) {
				$(this).slideDown(150);
			}
		};
		if (hasAttr($this, "show")) { //显示的触发事件
			options.show.event = $this.attr("show");
		}
		options.hide = {
			target: $this
		};
		if (hasAttr($this, "hide")) { //隐藏的触发事件
			options.hide.event = $this.attr("hide")
		}
		$this.qtip(options);
	});
},"qtips提示框");
/** 刷新图片 ，典型场景：点击按钮刷新图片
*	<%-- 图片容器 --%>
* 	<img class="signImgFcGainPersion" src="${fcGainPersionModel.gainimg}?20170103" width="135" height="170">
*	……
*	<%-- 样式shieldRefreshImg绑定该事件，增加refreshTo属性，指向需要刷新的图片 --%>		
*	<input type="button" title="点击该按钮刷新取证人签字图片" value="刷新图片" class="shieldRefreshImg button" refreshTo=".signImgFcGainPersion"/>
*/
ShieldJS.HtmlAction.addHTMLEleAction("shieldRefreshImg", ".shieldRefreshImg", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.unbind("click").click(function(){
			var $this = $(this);
			if (hasAttr($this,"refreshTo")) {
	            var refreshTo = getjQueryObj($this.attr("refreshTo"));
	            if (refreshTo.is("img")) {
	                var src = refreshTo.attr("src");
	                if (src.indexOf("randomparam")!=-1) {
	                    src = replaceParamVal(src,"randomparam", Math.random());
                    } else {
                    	if (src.indexOf("?")!=-1) {
                    		src +=  "&";
                    	} else {
                    		src +=  "?";
						}
                    	src +=  "randomparam=" + Math.random();
					}
	                refreshTo.attr("src",src);
                } else {
                	ShieldJS.alert("消息提示", "刷新的元素不是图片！", "error");
				}
            }else {
            	ShieldJS.alert("消息提示", "shieldRefreshImg图片刷新组件未定义刷新的图片元素：请添加refreshTo属性，关联刷新元素！", "error");
			}
			return false;
		});
	}
},"刷新图片操作");
/** 
* 无缓存图片	
*<%-- 样式shieldImgNoCache绑定该事件 --%>		
*	<img class="signImgFcGainPersion shieldImgNoCache" src="${fcGainPersionModel.gainimg }?20170103" width="135" height="170">
*/
ShieldJS.HtmlAction.addHTMLEleAction("shieldImgNoCache", ".shieldImgNoCache", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function(){
			var $this = $(this);
            if ($this.is("img")) {
            	var src = $this.attr("src");
            	if (hasAttr($this,"datasrc")) {
            		src = $this.attr("datasrc");
            	}
                if (src.indexOf("randomparam")!=-1) {
                    src = replaceParamVal(src,"randomparam", Math.random());
                } else {
                	if (src.indexOf("?")!=-1) {
                		src +=  "&";
                	} else {
                		src +=  "?";
					}
                	src +=  "randomparam=" + Math.random();
				}
                $this.attr("src",src);
            } else {
            	ShieldJS.alert("消息提示", "设定的元素不是图片！", "error");
			}
		});
	}
}, "无缓存图片");
/**
 * roundabout 3D层叠翻页效果
 * @field
 * 必需：
 *  第一个参数为自定义html动作处理的名字，注意与已有的是否冲突，如果冲突会用新的覆盖旧的
 *  第二个参数为js动作初始方法
 *  （均为jQuery对象）
 *  selectE:处理的jquery对象（checkFn中返回的对象），menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第二个参数为验证html元素是否合法（如是否缺少相关联的父子容器或存储容器），不合法返回false，合法返回合格条件的jQuery对象
 *  （均为jQuery对象）
 *  contentE:处理的页面对象，menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 */
ShieldJS.HtmlAction.addHTMLEleAction("roundabout", ".roundabout", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			var $this = $(this);
			var params= {};
			params = getParams($this,params);
			$this.roundabout(params).show();
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
},"roundabout 3D层叠翻页效果");
/**
 * shield-icon处理
 * @param selectE
 * @param menuli
 * @param menu
 * @param searchForm
 * @param shieldDialog
 * @returns
 */
ShieldJS.HtmlAction.addHTMLEleAction("shield-icon", ".shield-icon", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			var $this = $(this);
			var params= {};
			params = getParams($this, params);
			var hasTitle = hasAttr($this, "title");
			var title = "";
			if (!$this.html()) {
				if ($this.hasClass("show")) {
					$this.html("&#xe61d;");
					title = "查看";
	            } else if ($this.hasClass("manage")) {
					$this.html("&#xe609;");
					title = "管理";
	            } else if ($this.hasClass("audit")) {
					$this.html("&#xe68c;");
					title = "审核";
	            } else if ($this.hasClass("del")) {
					$this.html("&#xe60a;");
					title = "删除";
	            } else if ($this.hasClass("freeze")) {
					$this.html("&#xe605;");
					title = "冻结";
	            } else if ($this.hasClass("disable")) {
					$this.html("&#xe671;");
					title = "停用";
	            } else if ($this.hasClass("unfreeze")) {
					$this.html("&#xe613;");
					title = "解除冻结";
	            } else if ($this.hasClass("enable")) {
					$this.html("&#xe66a;");
					title = "启用";
	            } else if ($this.hasClass("add")) {
					$this.html("&#xe695;");
					title = "添加";
	            }
            }
			if (!hasTitle && title) {
				$this.attr("title", title);
			}
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
},"shield-icon处理");
//时间差计算
ShieldJS.HtmlAction.addHTMLEleAction("timeDiffContainer", ".timeDiffContainer", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			$(this).kvTimeDiff({
				/** 开始时间 */
				timeStart:".timeStart"
				/** 注意是结束时间 */
				,timeEnd:".timeEnd"
				/** 输出比较结果 */
				,timeDiffShow:".timeDiffShow"
			})
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
},"时间差计算");
//用于给不同tr增加status状态样式
ShieldJS.HtmlAction.addHTMLEleAction("shield-tr-status-class", ".shield-tr-status-class", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			var tableE = $(this);
			tableE.before(
					'<div>'+
					'	<table class="shield-table">'+
					'		<colgroup>'+
					'			<col width="120">'+
					'			<col width="">'+
					'			<col width="">'+
					'			<col width="">'+
					'		</colgroup>'+
					'		<tr>'+
					'			<td >颜色标示说明：</td>'+
					'			<td class="progstatetr yellowtr">冻结</td>'+
					'			<td class="progstatetr graytr">停用</td>'+
					'			<td></td>'+
					'		</tr>'+
					'	</table>'+
					'</div>');
			tableE.find("tr").each(function name() {
	            var trE = $(this);
	            if (hasAttr(trE, "status")) {
	            	var status = trE.attr("status");
	                if (status == -1) { //删除
	                	trE.addClass("");
                    } else if (status == 2) { //停用
                    	trE.addClass("graytr");
                    } else if (status == 3) { //冻结
                    	trE.addClass("yellowtr");
                    }
                }
            });
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
}, "用于给不同tr增加status状态样式");
ShieldJS.HtmlAction.addHTMLEleAction("shield-area-data", ".shield-area-data", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	
	var getAreaData = function(ele, parQhdm, value, read, callback) {
		if (ele.length > 0) {
			var areaMethod = ctxPath+"/area/get";
			$.getJSON(areaMethod, {
				'code' : parQhdm,
				'v' : value,
				'r' : read
			}, function(json) {
				var html = '<option value="">请选择</option>';
				if (json) {
					for ( var i = 0; i < json.length; i++) {
						html += '<option value="' + json[i].areaCode + '" parCode="'+ json[i].parentCode +'" ';
						if (json[i].selected) {
							html += ' selected="'+ json[i].selected +'" ';
						}
						if (json[i].disabled) {
							html += ' read="'+ json[i].disabled +'" ';
						}
						html += ' >';
						html += json[i].areaName + '</option>';
					}
					//alert(html)
				}
				ele.hide().html(html).show();
				if (ele.val()) { //有值时触发下一个
					getAreaData(ele.next(), ele.val(), value, read);
                }
				var readE = ele.find("option[read]");
				if (readE.length > 0) {
					ele.before("<span>" + readE.text() + "</span>");
					ele.hide();
				}
				if (callback && $.isFunction(callback)) {
					callback(ele, parQhdm, value, read);
                }
			});
        }
    };
	
	if (dealEle.length > 0) {
		if (!ctxPath) { //项目路径
			ctxPath = "";
		}
		dealEle.each(function() {
			var areaE = $(this);
			var areaSelectEs = areaE.find('select');
			var value = areaE.attr("v");
			var read = areaE.attr("r");
			if (areaSelectEs) {
				var ele = areaSelectEs.eq(0);
				var parQhdm = "000000";
				
				areaSelectEs.each(function(i, n) {
					var $this = $(n);
					if (!$this.html()) {
						$this.html('<option value="">请选择</option>');
                    }
					$this.change(function() {
						var t = $(this);
						var regionDm = t.val();
						getAreaData(t.next(), regionDm, null, read);
						// 取消
						while (!regionDm && t.prev("select").length > 0) {
							t = t.prev();
							regionDm = t.val();
						}
						areaE.find('.regionDm').val(regionDm).triggerHandler("change"); //触发自定义change事件
						var areaText = "";
						var nameSelectE = areaSelectEs.filter(function(index) {
							return index <= i;
						});
						nameSelectE.find("option:selected").each(function() {
							areaText += $(this).text();
	                    });
						areaText = areaText.replace(/请选择/g,"")
						areaE.find('.regionMc').val(areaText).triggerHandler("change"); //触发自定义change事件
					});
                });
				
				getAreaData(ele, parQhdm, value, read);
			}
        });
	}
}, "地区数据获取（select）");
//表格间行变色
ShieldJS.HtmlAction.addHTMLEleAction("shield-table-odd", ".shield-table-odd", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle.each(function() {
		var $this = $(this);
		$('tr:odd', $this).addClass("trodd");//间行变色
    });
},"表格间行变色");
//通用操作
ShieldJS.HtmlAction.addHTMLEleAction("shield-common-init",function(contentE, menuli, menu, searchForm, shieldDialog){
},"通用操作");
