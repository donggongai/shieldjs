ShieldJS.setVersion("Zi");
//覆盖
var ShieldJS_Zi = {
	//覆盖消息提醒方法
	alert : function(sa_title,sa_message,sa_type) {
		swal({
			title: sa_title,
			text: sa_message,
			type: sa_type
		});
	}
	//覆盖确认框
	,confirm : function(title, message, ok, cancel){
		swal({   
			title: title
			,text: message
			,confirmButtonText: "确定"
				,cancelButtonText: "取消"
					,type: "info"
						,showCancelButton: true
						,closeOnConfirm: false
						,showLoaderOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				if (ok && $.isFunction(ok)) {
					ok();
				}
			} else {
				if (cancel && $.isFunction(cancel)) {
					cancel();
				}
			}
		}
		); 
	}
	,loadingHtml : '<div class="admin113-loop"><img src="/metronic/assets/global/img/loading.gif"><br>加载中</div>'
}
ShieldJS = $.extend(true, ShieldJS, ShieldJS_Zi);
//菜单扩展点击事件，使用了addtabs插件，dom上直接绑定，不在js中处理
ShieldJS.core.leftCommonMenuClickDoing = function(event, liMenuContainerE, siderMenuClickEle, contentE) {
	liMenuContainerE.find("li").removeClass("active open");
	liMenuContainerE.find(".selected").remove();
	siderMenuClickEle.addClass("active open");
	liMenuContainerE.find(".title").after("<span class='selected' ></span>");
	return true; //扩展点击事件，返回false则不执行后续方法，返回true则继续执行后面的方法
};
//初始化bootstrap-addtabs
$.addtabs({
	loadCallback : function(contentE, titleE) { //成功后的回调
		ShieldJS.avtiveTabContentE = contentE;
		ShieldJS.activeTabTitleE = titleE;
		ShieldJS.core.bindHtmlAction(contentE); //成功后增加绑定事件
    }
});
//覆盖成功后的回调方法，还需要完善
ShieldJS.HtmlAction.successDoMethod.shieldRefresh = function(contentE, searchForm, menuliSelect, dialog, oldRefresh){ //刷新，关闭dialog，并刷新搜索表单(默认)
	if (dialog) {
		dialog.remove();
	}
	var shieldZTreeSearchE;
	var bootStrapTableE;
	if (ShieldJS.avtiveTabContentE && ShieldJS.avtiveTabContentE.length >0) {
		shieldZTreeSearchE = ShieldJS.avtiveTabContentE.find(".shieldZTreeSearch"); //树形结构
		bootStrapTableE = ShieldJS.avtiveTabContentE.find(".bootStrapTable"); //bootStrapTable表格
    }
	if (shieldZTreeSearchE && shieldZTreeSearchE.length > 0) {
		shieldZTreeSearchE.eq(0).click();
    } else if (bootStrapTableE && bootStrapTableE.length > 0) {
		bootStrapTableE.eq(0).bootstrapTable('refresh');
    } else if (searchForm) {
		if (!oldRefresh) { //兼容旧框架的json返回，不重复刷新
			searchForm.submit();
		}
	} else if (menuliSelect) {
		menuliSelect.click();
	}
};
//重置
ShieldJS.HtmlAction.addHTMLEleAction("reset", ".reset", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
   		dealEle.click(function(){
            $(this).closest("form")[0].reset();
   		});
	}
},"重置表单内容");
//数字动画计数器
ShieldJS.HtmlAction.addHTMLEleAction("data-counter", "[data-counter='counterup']", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
   		if (!$().counterUp) {
   			ShieldJS.error("检测到data-counter元素缺少依赖JS库：jquery.counterup.js！");
			return;
        }
   		dealEle.each(function(){
            $(this).counterUp({
                delay: 10,
                time: 1000
            });
   		});
	}
},"计数器动态效果");
//横向时间轴
ShieldJS.HtmlAction.addHTMLEleAction("cd-horizontal-timeline", ".cd-horizontal-timeline", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
   		if (!$().horizontalTimeline) {
   			ShieldJS.error("检测到data-counter元素缺少依赖JS库：horizontal-timeline.js！");
			return;
        }
   		dealEle.horizontalTimeline();
	}
},"横向时间轴");
//bootStrapTable表格
ShieldJS.HtmlAction.addHTMLEleAction("bootStrapTable", ".bootStrapTable", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.each(function() {
		var tableE = $(this);
		// 参数适配
		var paramAdaptNames = paramNameAdapt(tableE);
		var ajaxUrl = tableE.data("url");
		var tableParams = getParams(tableE);
		/*var columns=[{checkbox: true}
        ,{title: 'id',field: 'id',align: 'center',valign: 'middle',visible:false}
        ,{title: '登录账号',field: 'loginName',align: 'center',valign: 'middle'}
        ,{title: '用户名称',field: 'userName',align: 'center',valign: 'middle'}
        ,{title: '用户性别',field: 'gender',align: 'center',valign: 'middle',formatter:function (value,row,index) {
            if(value==1){return "男";}else if(value==0){return "女";}else{return "未知";}
        }}
        ,{title: '用户状态',field: 'isLock',align: 'center',formatter:function (value,row,index) {
if(value==1){return "正常";}else if(value==-1){return "锁定";}else{return "未知";}
}}];*/
		var columns= [];
		var columnser = tableE.find("tr.bootStrapTable_columns");
		var showUrl = tableE.data("showurl");
		var updUrl = tableE.data("updurl");
		var delUrl = tableE.data("delurl");
		columnser.find("th,td").each(function(i, n) {
			var ele = $(n);
			var trparams;
	        if (hasAttr(ele, "checkbox")) {
	        	trparams = {checkbox: true};
            } else if (hasAttr(ele, "data-operate")) { //操作
            	var operate = ele.data("operate");
            	var relId = ele.data("rel")||"batabs_new";
            	
	        	trparams = {title: ele.attr("title")||'操作',field: ele.data("key")||'id',align: ele.data("align")||'center',formatter:function (value,row,index) {
	    			var id = value;
	                var result = '';
	                var urlparams = {id:id};
	                var thisShowUrl = showUrl;
	        		var thisUpdUrl = updUrl;
	        		var thisDelUrl = delUrl;
	        		var showName = ele.attr("showName")||(ShieldJS.avtiveTabContentE && ShieldJS.avtiveTabContentE.find(".title")?ShieldJS.avtiveTabContentE.find(".title").text():"");
	                for (var key in paramAdaptNames) {
	                	if (key == "id") {
	                		urlparams[paramAdaptNames[key]] = id;
                        }
	                }
	                if ($.isEmptyObject(urlparams)) {
	                	urlparams.id = id;
                    }
	                var paramStr = $.param(urlparams);
	                if (thisShowUrl.indexOf("?") > -1) {
	                	thisShowUrl += "&";
                    }else {
                    	thisShowUrl += "?";
                    }
	                thisShowUrl += paramStr;
	                if (thisUpdUrl.indexOf("?") > -1) {
	                	thisUpdUrl += "&";
                    }else {
	                	thisUpdUrl += "?";
                    }
	                thisUpdUrl += paramStr;
	                if (!operate || operate.indexOf("S")!=-1) {
	                	result += '<button data-addtab="'+relId+'_show_'+id+'" title="'+showName+'查看" data-rel-type="new" data-url="'+thisShowUrl+'" class="btn btn-xs green"><span class="glyphicon glyphicon-search"></span></button>';
                    }
	                if (!operate || operate.indexOf("U")!=-1) {
	                	result += '<button data-addtab="'+relId+'_update'+id+'" title="'+showName+'编辑" data-rel-type="new" data-url="'+thisUpdUrl+'" class="btn btn-xs blue"><span class="glyphicon glyphicon-pencil"></span></button>';
	                }
	                if (!operate || operate.indexOf("D")!=-1) {
	                	result += '<button href="javascript:;" class="btn btn-xs red shieldAjax" title="'+showName+'删除"><span class="glyphicon glyphicon-remove"></span></button>';
	                }

	                return result;
	    		}};
            } else {
            	trparams = getParams(ele, trparams);
            	if (trparams.switchV) { //switch转换
	                var switchV = trparams.switchV;
	                var switchVs = switchV.split('|');
	                var functionStr = '';
	                var functionStrElse = '';
	                for (var i = 0; i < switchVs.length; i++) {
	                	var switchVsiValue = switchVs[i];
	                    if (switchVsiValue.indexOf('=')==-1) {
	                    	if (switchVsiValue == "$value") { //原值输出
	                    		functionStrElse += 'else{return value;}';
                            } else if (switchVsiValue.indexOf("$DateFormat(")==0) { //日期格式化
                            	// 括号表示组。访问可以用group[index]来访问每组的信息  
            					var linkRegx = /\$DateFormat\((.+?)\)/;
            					var group = switchVsiValue.match(linkRegx);
            					if (group && group.length == 2) {
            						var dataFormater = group[1];
            						functionStrElse += 'else{return new Date(value).format("'+dataFormater+'")}';
            					}else {
            						functionStrElse += 'else{return new Date(value).format("yyyy-MM-dd hh:mm:ss")}';
								}
                            } else {
                            	functionStrElse += 'else{return "'+switchVsiValue+'";}';
							}
                        } else {
                        	 var switchVvs = switchVsiValue.split('=');
                        	 if (functionStr) {
                        		 functionStr += 'else '
                        	 }
                        	 if (switchVvs[0]=="$empty") {
                        		 functionStr += 'if(!value)';
                        	 }else {
                        		 functionStr += 'if(value=="'+switchVvs[0]+'")';
                        	 }
                        	 functionStr += '{return "'+switchVvs[1]+'";}';
						}
                    }
	                eval('trparams.formatter = function(value,row,index) {'+functionStr+functionStrElse+'}');
	                
                }
			}
	        columns.push(trparams);
        });
		
		var _defaults = {
				method: 'get',
				cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
				dataType: "json",
				striped: false,	 //是否显示行间变色
				sortable: true,
//				selectItemName: 'btSelectItem', //复选框选中参数名，默认带
				sortOrder: "asc",  //排序方式,需要在列中定义sortable: true改列才会排序
				pagination: true,	//在表格底部显示分页工具栏
				pageSize: 10, //每页的记录行数（*）
				pageNumber: 1, //初始化加载第一页，默认第一页,并记录
				pageList: [10, 15, 20], //可供选择的每页的行数（*）
				idField: "id",  //标识哪个字段为id主键
				showToggle: true,   //名片格式
				cardView: false,//设置为True时默认显示名片（card）布局
				singleSelect: false,//复选框只能选择一条记录
				search: false,//是否显示右上角的搜索框
				clickToSelect: true,//点击行即可选中单选/复选框
				sidePagination: "server",//分页方式：client客户端分页，server服务端分页（*）
				queryParamsType: "", //参数格式,发送标准的RESTFul类型的参数请求
				strictSearch: true,
				showColumns: true,     //是否显示所有的列
				showRefresh: true,     //是否显示刷新按钮
				minimumCountColumns: 2,    //最少允许的列数
				responseHandler: function (res) {
					return {
						"rows": res.dataList,
						"total": res.total
					};
				},
				silent: false,  //刷新事件必须设置
				formatNoMatches: function () {  //没有匹配的结果
					return '无符合条件的记录';
				},
				rowStyle: function (row, index) {
					return {classes: "cursorHand"}
				}
				,columns : columns
				,url : ajaxUrl
				,queryParams : function (params) {
					var queryParams = ShieldJS.HtmlAction.checkAnGetParamSelector(tableE, {}); //参数选择器
			        //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			        var _queryParams = {
			            rows: params.pageSize,                         //页面大小
			            page: params.pageNumber,   //页码
			            sort: params.sortName,      //排序列名  
			            sortOrder: params.sortOrder //排位命令（desc，asc） 
			        };
			        queryParams = $.extend(true, tableParams||{},_queryParams, queryParams);
			        return queryParams;
                }
				,onLoadSuccess: function () {
                },
                onLoadError: function () {
                	ShieldJS.alert("消息提示", "数据加载失败！", "error");
                },
                onDblClickRow: function (row, $element) {
                    var id = row.id;
                    alert(id);;
                }
		};
//		_defaults.columns = columns;
//		_defaults.url = ajaxUrl,
//		_defaults.queryParams = queryParams(tableE);
		var settings = getSettings(tableE);
		var modeloptions = {};
		if (tableE.data("simple")==""|| tableE.data("simple")=="true") { //该data默认值true
			modeloptions.showToggle = false; //名片格式
			modeloptions.showColumns = false; //是否显示所有的列
			modeloptions.showRefresh = false; //是否显示刷新按钮
        }
		var options = $.extend(true, _defaults, tableE.data(), modeloptions, settings); //顺序
		tableE.data("btableSettings", options)
		tableE.bootstrapTable(options);
		ShieldJS.core.bindHtmlAction(tableE, null, null, null); //内部继续加载
		
    });
	var dealEle = findAndMarkEle(ShieldJS.avtiveTabContentE, ".bootStrapTableSearchBtn"); //搜索
	if (dealEle.length > 0) {
		dealEle.each(function(i, n){
			var selectEthis = $(this);
			var targetEle;
			if (selectEthis.data("target")) { //有多个bootstrapTable时一定要定义
				targetEle = getjQueryObj(selectEthis.data("target"));
			}
			if (!targetEle) {
				targetEle = selectE.eq(i); //target没有根据下标对应
			}
			selectEthis.click(function() {
				searchTable(targetEle);
			});
		});
	}
	
	function searchTable(tableEle) {
		tableEle.bootstrapTable('refresh', tableEle.bootstrapTable("getOptions")); //刷新Table，Bootstrap Table 会自动执行重新查询
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	var eleName = "bootStrapTable元素"; 
	if (dealEle.length > 0) {
		dealEle.each(function(){
			var selectEthis = $(this);
			if (!selectEthis.data("url")) {
				dealEle = dealEle.not(selectEthis);
				ShieldJS.error(eleName + "未定义data-url属性：请添加");
				return false; //true=continue，false=break。 
			}
			if (selectEthis.find("tr.bootStrapTable_columns").length == 0) {
				dealEle = dealEle.not(selectEthis);
				ShieldJS.error(eleName + "未定义列参数定义器：请添加class为bootStrapTable_columns的tr");
				return false; //true=continue，false=break。 
			}
		});
		return dealEle; //返回符合条件的jQuery对象
	}
	return dealEle.length > 0 ? dealEle : false;
},"bootStrapTable插件处理");

//定时任务统一参数
$.fn.cronGen.defaultOptions = $.extend(true, $.fn.cronGen.defaultOptions, {
	direction : 'right'
	// 提交按钮样式
	,submitBtnClass : 'btn blue'
	// 显示值的input框样式
	,showInputClass : 'form-control'
	// 弹出按钮style
	,inputGroupBtnStyle : ''
	// 弹出按钮html
	,selectBtn : '<span class="input-group-btn btn-right"><button class="btn green"><i class="fa fa-calendar-check-o"></i></button></span>'
});
ShieldJS.HtmlAction.addHTMLEleAction("shield-cron", ".shield-cron", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.each(function() {
	    var selectEthis = $(this);
	    var params = {
	    };
		var execTimesUrl = selectEthis.attr("execTimesUrl")||"";
		if (execTimesUrl) {
			params.execTimesUrl = execTimesUrl;
		}
	    selectEthis.cronGen(params);
    });
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if(!$.fn.cronGen){
			ShieldJS.error("检测到shield-cron元素缺少依赖JS库：cronGen.js！");
			return false;
		}
		dealEle.each(function() {
		    var selectEthis = $(this);
		    if (!selectEthis.is(":input")) {
		    	dealEle = dealEle.not(selectEthis);
		    	ShieldJS.error("检测到shield-cron元素不是input元素，请修改！"+selectEthis.prop("outerHTML"));
				return false; //true=continue，false=break。 
            }
	    });
		// 可进一步筛选
		return dealEle; //返回符合条件的jQuery对象
	}
	return dealEle.length > 0 ? dealEle : false;
},"cron表达式插件");
//highCharts图
ShieldJS.HtmlAction.addHTMLEleAction("highChartsContainer", "#highChartsContainer", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var container = findAndMarkEle(contentE, this.expression);
	if(container.length>0){
		var intimeacctimeout;
		var loadInterval = function(container,series,series1,series2,series3,time){
			if(intimeacctimeout && container.is(":hidden") ){
				clearTimeout(intimeacctimeout);
			}else{
				if(hasAttr(container,"refreshUrl")){
					$.ajax({
						type: "POST",
						url: container.attr("refreshUrl"),
						dataType: "json",
						async: true,
						success: function(json){
							var x = json.xName, // current time
							xNext = json.xNameNext,
							y = json.data.count1,
							y1 = json.data.count2,
							y2 = json.data.count3,
							y3 = json.data.count4;
							saveOrupdatPoint(series, x, y,xNext);
							saveOrupdatPoint(series1, x, y1,xNext);
							saveOrupdatPoint(series2, x, y2,xNext);
							saveOrupdatPoint(series3, x, y3,xNext);
							intimeacctimeout = setTimeout(function() {
								loadInterval(container,series,series1,series2,series3,time);
							}, time);
						}
					});
				}
			}
		};
		var saveOrupdatPoint = function(series,x,y,xNext){
			var exist = false;
			for(var index=series.data.length-2;index<series.data.length;index++){
				var data = series.data[index];
				if(data.category==x || data.name==x){
					if(index==series.data.length-1){
						series.addPoint([xNext, 0], true, true);
					}
					exist = true;
					data.y=y;
					data.update();
					break;
				}
			}
			if(!exist){
				series.addPoint([x, y], true, true);
				series.addPoint([xNext, 0], true, true);
			}
		};
		
		if(hasAttr(container,"dataUrl")){ //有链接才处理
			var dataUrl = container.attr("dataUrl");
			container.html(ShieldJS.loadingHtml);
			var titleSuffix = "";
			if(hasAttr(container,ShieldJS.css.TITLE)){ //标题后缀
				titleSuffix = container.attr(ShieldJS.css.TITLE);
			}
			var yAxisTitle = "";
			if(hasAttr(container,"yAxisTitle")){ //y轴名称
				yAxisTitle = container.attr("yAxisTitle");
			}
			var xAxisTitle = "";
			if(hasAttr(container,"xAxisTitle")){ //x轴名称
				xAxisTitle = container.attr("xAxisTitle");
			}
			var tooltip = "";
			if(hasAttr(container,"tooltip")){ //tooltip名称
				tooltip = container.attr("tooltip");
			}
			var chartSetting = {
					//type: 'spline'
			}
			if(hasAttr(container,"refresh") && container.attr("refresh")=='true'){ //刷新
				var refreshMils = 3000; //默认3s刷新一次
				if(hasAttr(container,"refreshMils")){ //刷新时间，单位毫秒
					refreshMils = container.attr("refreshMils");
				}
				chartSetting = {
						events: {
							load: function() {
								var chart = this;
								var series = chart.series[0];
								var series1 = chart.series[1];
								var series2 = chart.series[2];
								var series3 = chart.series[3];
								loadInterval(container,series,series1,series2,series3,refreshMils);
							}
						}
				};
			}
			
			ShieldJS.ajax.post(dataUrl, contentE.find('form.searchForm').serialize(), function(json){
				json = $.parseJSON(json);
				container.highcharts({
					chart: chartSetting,
					title: {
						text: json.title + titleSuffix
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						title: {
							text: xAxisTitle
						},
						categories: json.xNames
					},
					yAxis: {
						title: {
							text: yAxisTitle
						},
						min: 0
					},
					tooltip: {
						crosshairs: true,
						shared: true,
						headerFormat: '<span style="font-size: 14px">{point.key}'+tooltip+'</span><br/>'
					},
					series: json.data
				});
			});
		}
	}
},"highCharts图插件处理");

//树形结构
ShieldJS.HtmlAction.addHTMLEleAction("shieldZtree", ".shieldZTree", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var ztreeMenuContainerE = findAndMarkEle(contentE, this.expression);
	if (ztreeMenuContainerE.length > 0) {
   		if (!$.fn.zTree) {
   			ShieldJS.error("检测到shieldZTree元素缺少依赖JS库：jquery.ztree.js！");
			return;
        }
   		// 参数适配
   		var paramAdapt = function(ele, treeNode) {
   			var paramNames = paramNameAdapt(ele);
   			var params = {}
   			for (var key in paramNames) {
   				params[paramNames[key]] = treeNode[key];
            }
			return params;
        }
   		var getFont = function(treeId, node) { //字体，用于从结果中增加样式
			return node.font ? node.font : {};
		}
   		var addHoverDom = function(treeId, treeNode) {
   			var containerE = $("#"+treeId);
   			var params = paramAdapt(containerE, treeNode);
   	        var aObj = $("#" + treeNode.tId + "_a");
   	        
   	        var addStr ="";
   	        var addBtn = $("#ztree_addBtn_"+treeNode.tId);
   	        var addUrl = containerE.data("urlAdd"); //修改大写
   	        if (addBtn.length == 0 && addUrl){
   	        	addStr = '<span class="button add shieldDialog" data-url="'+addUrl+'" id="ztree_addBtn_' + treeNode.tId + '" viewTitle="新增" viewWidth="800"></span>';
   	        }
   	        var updBtn = $("#ztree_updBtn_"+treeNode.tId);
   	        var updUrl = containerE.data("urlUpd");
   	        if (updBtn.length == 0 && updUrl) {
   	        	addStr += '<span class="button edit shieldDialog" data-url="'+updUrl+'" id="ztree_updBtn_' + treeNode.tId + '" viewTitle="修改" viewWidth="800"></span>';
   	        }
   	        var delBtn = $("#ztree_delBtn_"+treeNode.tId);
   	        var delUrl = containerE.data("urlDel");
   	        if (delBtn.length == 0 && delUrl) {
   	        	addStr += '<span class="button remove" id="ztree_delBtn_' + treeNode.tId + '" title="删除" ></span>';
   	        }
   	        if (addStr) {
   	        	aObj.append(addStr);
            }
   	        ShieldJS.core.bindHtmlAction(["shieldDialogOrLookup"], aObj, menuliSelect, navMenuSelect, searchForm, shieldDialog); //内部继续加载
	        delBtn = $("#ztree_delBtn_"+treeNode.tId);
   	        if (delBtn.length > 0){
   	        	delBtn.unbind().bind("click", function(){
   	        		var zTree = $.fn.zTree.getZTreeObj(treeId);
	   		         if(null!=treeNode.children && treeNode.children.length>0){
	   		            ShieldJS.alert("提示信息","不能删除父节点","error");
	   		             return false;
	   		         }
	   		         ShieldJS.confirm("删除确认", "确定删除 "+treeNode.name+"?", function(){
	   		        	 ShieldJS.ajax.post(delUrl, params, function(responseJson){
   	                         if(responseJson.success==true){//返回true
   	                        	 zTree.removeNode(treeNode);//动态删除该节点
   	                             ShieldJS.alert("操作成功！", responseJson.msg, "success");
   	                         }
   	                     }, function (data) {
   	                    	 ShieldJS.alert("操作失败！","系统错误！！！","error");
   	                     });
	   		         }, function() {
	   		            ShieldJS.alert("取消操作", "取消了当前操作", "info");
	   		         });
   	        		return false;
   	        	});
   	        } 
   	        return false;
   	    };
	   	 var removeHoverDom=function(treeId, treeNode) {
	         $("#ztree_addBtn_"+treeNode.tId).unbind().remove();
	         $("#ztree_updBtn_"+treeNode.tId).unbind().remove();
	         $("#ztree_delBtn_"+treeNode.tId).unbind().remove();
	     };
	     function onDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
	 		var zTree = $.fn.zTree.getZTreeObj(treeId);
	 		var allnodes = zTree.transformToArray(zTree.getNodes());
 			var msg = "检测到权限排序进行了改变，请记得更新排序！"
 			ShieldJS.alert(msg);
	     }
	     var jsTreeSetting = {
			data : {
				simpleData : {
					enable : true
				}
			},
			view : {
				addHoverDom: addHoverDom,
                removeHoverDom: removeHoverDom,
                selectedMulti: false,
				showIcon : true,
				fontCss: getFont,
				nameIsHTML: true
			},
			edit: {
				drag: {
					autoExpandTrigger: true
				},
                enable: true,
                showRemoveBtn: false,
				showRenameBtn: false
            },
			callback : {
				onDrop: onDrop,
				onClick : function(e, treeId, treeNode) {
					if(treeNode){
						var containerE = $("#"+treeId);
						if (hasAttr(containerE,"clickExtendFn")) { //扩展点击事件
							var clickExtendFnName = containerE.attr("clickExtendFn");
							clickExtendFnName = eval(clickExtendFnName);
							if($.isFunction(clickExtendFnName)){
								if(!clickExtendFnName(e, treeId, treeNode, contentE)){
									return false;
								}
							}
						}
						var url = treeNode.urlNode;
						if (url) { //内容中自带链接
							var params = {};
							ShieldJS.ajax.get(contentE, url, params, function() {
								//内容区处理
								if(!ShieldJS.core.contentAreaDeal(contentE, null, navMenuClickEle)){
									return false;
								}
								
							});
						} else if (containerE.data("urlNode")) { //ul中公共链接
							var url = containerE.data("urlNode");
							var paramAdapter = containerE.attr("paramAdapter");
							var params = {};
							if (paramAdapter) { //有值且不为空
								params = paramAdapt(containerE, treeNode); //进行参数适配
								
								var targetV = containerE.data("target");
								var targetE; //目标显示区，jQuery选择器
								if (targetV) {
									targetE = getjQueryObj(targetV);
                                } else {
                                	targetE = ShieldJS.avtiveTabContentE.find(".ztreeContent"); //默认
								}
								if (targetE.length > 0) {
									ShieldJS.ajax.get(targetE, url, params, function() {
										//内容区处理
										ShieldJS.core.bindHtmlAction(targetE);
									});
                                } else {
                                	if (targetV) {
                                		ShieldJS.error(""+treeId+"容器上定义的data-target属性未找到匹配的对象，请检查修正！");
                                    }else {
                                    	ShieldJS.error("未定义点击后内容显示区域：请在"+treeId+"容器上添加data-target属性并添加目标显示区域（jQuery选择器）");
									}
								}
							} else {
								ShieldJS.error("未定义参数选择属性：请在"+treeId+"容器上添加paramAdapter属性并添加参数名，多个参数用#号连接");
								return false;
							}
						} 
					}
				}
			}
		};
		var loadZtree = function() {
			ztreeMenuContainerE.each(function(i, n){
				var treeE = $(this);
				var treeSettings = getExtendSettings(treeE, jsTreeSetting);
				if(typeof initZTree != "undefined" && $.isFunction(eval("initZTree"))){
					initZTree(treeE, treeSettings); //初始化，可做参数或其他预处理
				}
				treeE.data("treesettings", treeSettings);
				if(treeE.data("url")){ //有链接才处理
					if(hasAttr(treeE, "asyncsetting")){ //异步请求
						treeSettings.async = eval(treeE.attr("asyncsetting"));
					}
					var url = treeE.data("url");
					var params = {};
					if (hasAttr(treeE,"paramSelector")) { //参数选择器
						params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params); //参数选择器 siderE.find(paramSelector).serializeJson();
						if ($.isEmptyObject(params)) {
							ShieldJS.alert("消息提示", "参数为空！", "error");
							return false;
						}
					}
					params.timestamp = new Date().getTime(); //时间戳防缓存
					params = getParams(treeE, params);
					ShieldJS.ajax.getJSON(url, params, function(json) {
						if (json.obj && json.obj.length > 0) {
							$.fn.zTree.init(treeE, treeSettings, json.obj);
							var treeEId = treeE.attr("id");
							var treeObj = $.fn.zTree.getZTreeObj(treeEId);
							if(hasAttr(treeE,"selectNode")){ //默认选中节点
								var selectNode = treeE.attr("selectNode")||0; //默认0
								var defaultName = "rownum"; //默认属性
								var selectNodeValue= selectNode;
								var node;
								if (selectNode.indexOf("=") != -1) {
									defaultName = selectNode.split("=")[0];
									selectNodeValue = selectNode.split("=")[1];
									node = treeObj.getNodeByParam(defaultName, selectNodeValue);
								} else{
									var allNodes = treeObj.getNodes();
									node = allNodes[selectNode];
								}
								treeObj.selectNode(node);
								treeObj.setting.callback.onClick(null, treeObj.setting.treeId, node);
							}
						} else {
							treeE.html('没有数据！');
						}
					}, function() {
						treeE.html(json.message);
                    });
				}
				
			});
			var searchEles = findAndMarkEle(contentE, '.shieldZTreeSearch'); //搜按钮
			if (searchEles.length > 0) {
				searchEles.each(function(i, n){
					var selectEthis = $(this);
					var treeE;
					if (selectEthis.data("target")) { //有多个ztree时定义
						treeE = getjQueryObj(selectEthis.data("target"));
	                }
					if (!treeE) {
						treeE = ztreeMenuContainerE.eq(i); //获取ztree对象
					}
					selectEthis.click(function(event){
						var params = {};
						if (hasAttr(treeE, "paramSelector")) { //参数选择器
							params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params); //参数选择器siderE.find(paramSelector).serializeJson();
							if ($.isEmptyObject(params)) {
								ShieldJS.alert("消息提示", "参数为空！", "error");
								return false;
							}
						}
						params.timestamp = new Date().getTime(); //时间戳防缓存
						params = getParams(treeE, params);
						if (hasAttr(selectEthis, "clickExtendFn")) { //扩展点击事件
							var clickExtendFnName = selectEthis.attr("clickExtendFn");
							clickExtendFnName = eval(clickExtendFnName);
							if($.isFunction(clickExtendFnName)){
								if(!clickExtendFnName(event, params, treeE, treeE.data("treesettings"))){
									return false;
								}
							}
						}
						//未扩展，仍执行原查询
						ShieldJS.ajax.getJSON(treeE.data("url"), params, function(json) {
							if (json.obj && json.obj.length > 0) {
								$.fn.zTree.init(treeE, jsTreeSetting, json.obj);
							} else {
								treeE.html('没有数据！');
							}
						}, function() {
							treeE.html(json.message);
	                        
                        });
					});
				});
			}
		};
		loadZtree();
	}
},"ztree插件处理");
//树形结构全选反选等按钮操作
ShieldJS.HtmlAction.addHTMLEleAction("shieldZtreeContainer", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var treeCheckNode = function(e) {
		var zTree = $.fn.zTree.getZTreeObj("permissionzTree"),
		type = e.data.type,
		nodes = zTree.getNodes();
		if (type == "checkAllTrue") {
			zTree.checkAllNodes(true);
		} else if (type == "checkAllFalse") {
			zTree.checkAllNodes(false);
		} else {
			var allnodes = zTree.transformToArray(nodes); //全部节点
			for (var i=0, l=allnodes.length; i<l; i++) {
				if (type == "toggle") {
					zTree.checkNode(allnodes[i], null, false, false);
				} else if (type == "togglePS") {
					zTree.checkNode(allnodes[i], null, true, false);
				}
			}
		}
	};
	var treeExpandNode = function(e) {
		var zTree = $.fn.zTree.getZTreeObj("permissionzTree"),
		type = e.data.type,
		nodes = zTree.getSelectedNodes();
		if (type == "expandAll") {
			zTree.expandAll(true);
		} else if (type == "collapseAll") {
			zTree.expandAll(false);
		}
	};
	findAndMarkEle(contentE, ".shieldZtreeContainer .allSelect").bind("click", {type:"checkAllTrue"}, treeCheckNode); //全选
	findAndMarkEle(contentE, ".shieldZtreeContainer .unAllSelect").bind("click", {type:"checkAllFalse"}, treeCheckNode); //全不选
	findAndMarkEle(contentE, ".shieldZtreeContainer .unSelect").bind("click", {type:"toggle"}, treeCheckNode); //反选
	findAndMarkEle(contentE, ".shieldZtreeContainer .expandAll").bind("click", {type:"expandAll"}, treeExpandNode); //全部展开
	findAndMarkEle(contentE, ".shieldZtreeContainer .collapseAll").bind("click", {type:"collapseAll"}, treeExpandNode); //全部收起
	//树形结构需考虑插件化
},"ztree全选展开等操作");
//扩展方法
//日期，调用My97DatePicker的日期插件
ShieldJS.HtmlAction.addHTMLEleAction("shieldDatePicker", "input.Wdate", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		try {
			WdatePicker;
      } catch (e) {
      	ShieldJS.error("缺少依赖JS库：My97DatePicker！"+dealEle.html());
      	return false;
      }
		var startDate = findAndMarkEle(contentE, 'input[init="startDate"]');
		var nowDate = findAndMarkEle(contentE, 'input[init="nowDate"]');
		if (startDate.length > 0) {
			startDate.each(function() {
				var $this = $(this); 
				var pattern = $this.attr("dateFmt");
				if (pattern) {
					$this.val(dateFormat(new Date(), pattern));
				} else {
					$this.val(dateFormat(new Date(), 'yyyy-MM-01'));
				}
			});
		}
		if (nowDate.length > 0) {
			nowDate.each(function() {
				var $this = $(this); 
				var pattern = $this.attr("dateFmt");
				if (pattern) {
					$this.val(dateFormat(new Date(), pattern));
				} else {
					$this.val(dateFormat(new Date(), 'yyyy-MM-dd'));
				}
			});
		}
		dealEle.each(function() {
			var $this = $(this);
			var options = {};
			var pattern = $this.attr("dateFmt");
			var onpicked = $this.attr("onpicked");
			if (pattern) {
				options.dateFmt = pattern;
			}
			if (onpicked) {
				options.onpicked = onpicked;
			} else {
				options.onpicked = function(dp) {
	                $(this).triggerHandler("change");//触发自定义change事件
              };
			}
			$this.click(function() {
				WdatePicker(options);
			});
		});
	}
},"日期，调用My97DatePicker的日期插件");
//div定时关闭
ShieldJS.HtmlAction.addHTMLEleAction("shield-timer-close", ".shield-timer-close", function(selectE, menuli, menu, searchForm, shieldDialog){
	var time = getTimeAttr(selectE, "time");
	var timer = setTimeout(function() {
		var closeTime = getTimeAttr(selectE, "closeTime", 2000); //关闭用时，默认2秒
		if (closeTime > 0) {
			//selectE.animate({height: '0px',opacity: 0}, closeTime);
			selectE.slideUp(closeTime);
			setTimeout(function() {
				selectE.remove();
			}, closeTime);
      } else { //-1立刻移除
      	selectE.remove();
      }
	}, time);
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
			var $this = $(this);
			if (!hasAttr($this, "time")) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-timer-close元素缺少time（xx时间后关闭）属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
      });
	}
	return dealEle.length > 0 ? dealEle : false;
},"div定时关闭");
//div关闭
ShieldJS.HtmlAction.addHTMLEleAction("shield-close", ".shield-close", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.click(function(){
			dealEle.closest(".shield-content").remove();
			//return false;
		});
	}
},"div关闭");
//第三方插件
//图片下拉选择(.ddslickSelectContainer .ddslickSelectValue .ddslickSelect)
ShieldJS.HtmlAction.addHTMLEleAction("ddslickSelect", ".ddslickSelect", function (selectEs, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	selectEs.each(function(){
		var ddslickSelectE = $(this);
		var container = ddslickSelectE.closest(".ddslickSelectContainer");
		ddslickSelectE.ddslick({ width: 125,height:200,onSelected: function(selectedData){ 
			var defaultPhoto = container.find(".dd-selected-value").val();
			container.find(".ddslickSelectValue").val(defaultPhoto);
		}})
	});
},  function (contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	var ddslickSelectE = findAndMarkEle(contentE, this.expression);
	var eleName = "ddslick第三方插件";
	if (ddslickSelectE.length > 0) {
		ddslickSelectE.each(function(){
			var ddslickSelectE = $(this);
			var container = ddslickSelectE.closest(".ddslickSelectContainer");
			if (container.length == 0) {
				ShieldJS.error(eleName + "未定义父类容器：请在父容器中添加class：ddslickSelectContainer");
				return false;
			} 
			if (container.find(".ddslickSelectValue").length == 0) {
				ShieldJS.error(eleName + "未定义选择后图片地址保存容器：请在对应的input容器上添加class：ddslickSelectValue");
				return false;
			} 
		});
		return ddslickSelectE;
	}
	return false;
},"图片下拉选择");
//qtips
ShieldJS.HtmlAction.addHTMLEleAction("qtips", ".qtips", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	findAndMarkEle(contentE, this.expression).each(function() {
		var $this = $(this);
		var options = {};
		options.content = {
			text: $this.attr("content")
			, title: {
				text: $this.attr("title"),
				button: ($this.attr("titlebutton")=='true')||false
			}
		};
		options.style = {
			classes: $this.attr("qtipsClass")|| 'qtip-blue qtip-shadow qtip-rounded'     		
		};
		options.events = {
			visible: function(event, api) {
				//api.reposition(event);
			}
			,move: function(event, api) {  
	           // alert( event.pageX + '/' + event.pageY );  
	        }  
		};
		options.position = {
			target: 'event'
			,my: 'top center' /*指向图片在qtips的位置*/
			,at: 'bottom center' /*指向图片在元素的位置*/
			,viewport: $(window)
			,adjust: {resize:false, screen: false}
		};
		options.show = {
			target: $this
			,effect: function(offset) {
				$(this).slideDown(150);
			}
		};
		if (hasAttr($this, "show")) {
			options.show.event = $this.attr("show");
      }
		options.hide = {
			target: $this
		};
		if (hasAttr($this, "hide")) {
			options.hide.event = $this.attr("hide")
      }
		$this.qtip(options);
	});
},"qtips插件处理");
//fullCalendar
ShieldJS.HtmlAction.addHTMLEleAction("fullCalendar", ".fullCalendar", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	findAndMarkEle(contentE, this.expression).each(function() {
		var $this = $(this);
		var options = {};
		if (!jQuery().fullCalendar) {
			ShieldJS.error("检测到fullCalendar元素缺少依赖JS库：fullcalendar.js！");
            return;
        }

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var h = {};
        /**  
         *  支持的视图
         *  today       今天 
         *  month       一页显示一月, 日历样式  
            basicWeek   一页显示一周, 无特殊样式  
            basicDay    一页显示一天, 无特殊样式  
            agendaWeek  一页显示一周, 显示详细的24小时表格  
            agendaDay   一页显示一天, 显示详细的24小时表格  
            listYear    年列表（V3.*支持）  
            listMonth   月列表（V3.*支持）  
            listWeek    周列表（V3.*支持）  
            listDay     日列表（V3.*支持）  
         */ 
        if ($this.width() <= 400) {
        	$this.addClass("mobile");
            h = {
                left: 'title, prev, next',
                center: '',
                right: 'today,month,agendaWeek,agendaDay,listMonth'
            };
        } else {
        	$this.removeClass("mobile");
            if (App.isRTL()) {
                h = {
                    right: 'title',
                    center: '',
                    left: 'prev,next,today,month,agendaWeek,agendaDay,listMonth'
                };
            } else {
                h = {
                    left: 'title',
                    center: '',
                    right: 'prev,next,today,month,agendaWeek,agendaDay,listMonth'
                };
            }
        }
        if (!$this.data("url")) {
			ShieldJS.error("检测到fullCalendar元素未定义url，请检查！");
            return;
        }
        var _default = {
        	firstDay : 1 //第一天为周一
        	,droppable: true
            ,header: h
            ,editable: true
            ,isRTL : false //从右到左显示模式，默认false
            ,eventOrder : "title" //多个相同的日程数据排序方式，String/Array/Function, 默认值: "title"  
            ,events : {
                url: $this.data("url"),
                error: function(){
                    ShieldJS.alert("错误提示", "日历事件加载失败！", "error");
                }
            }
        	,eventRender: function(event, element) { //当Event对象开始渲染时触发
	        	if (jQuery().qtip) { //鼠标悬停
	        		element.qtip({
	        			content: {
	        				text: '开始时间：'+$.fullCalendar.moment(event.start).format('YYYY年M月D日(HH时)(mm分)')
	        				+(event.end?"<br/>结束时间："+$.fullCalendar.moment(event.end).format('YYYY年M月D日(HH时)(mm分)'):"")
	        				+(event.description?"<br/>详　　情："+event.description:"")
	        				+(event.url?"<br/>链接地址："+event.url:"")
	        				, title: {
	        					text: ""+event.title
	        				}
	        			}
	        			,style: {
	        				classes: 'qtip-blue qtip-shadow qtip-rounded'     		
	        			}
	        			,position: {
	        				my: 'top center',
	        				at: 'bottom center'
	        			}
	        		});
                }
		    }
            ,eventClick : function(calEvent, jsEvent, view) { //日历事件区块，单击时触发  
		    	// alert(calEvent.ucid);
		    	if(!calEvent.url){
		    		ShieldJS.openDialog('system/user/user_add.html?id='+calEvent.id, {}, null, '添加日历事件');
		    	}
		    }
		    ,dayClick : function(dayDate, allDay, jsEvent, view){ //空白的日期区，单击时触发  
		    	ShieldJS.debug(dayDate);
		    }
		    ,eventResize : function(calEvent, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) { //日程大小调整完成并已经执行更新时触发
		    }
		    ,eventDrop: function(calEvent, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) { //日程拖拽停止并且已经拖拽到其它位置了
		    }
		    ,eventMouseover: function(calEvent, jsEvent, view){//鼠标在日程区块上时触发  
            }
		    ,eventMouseout: function(calEvent, jsEvent, view){//鼠标从日程区块离开时触发  
            }
        }
        var options = getExtendSettings($this, _default);
        $this.fullCalendar('destroy'); // 销毁旧实例
        $this.fullCalendar(options); //添加新实例，测试，静态数据待修改
	});
},"fullCalendar日历插件处理");
// 拖拽
ShieldJS.HtmlAction.addHTMLEleAction("draggable", ".draggable", function(contentE, menuli, menu, searchForm, shieldDialog){
	if (!jQuery().draggable) {
		ShieldJS.error("检测到draggable元素缺少依赖JS库：jquery-ui.js！");
        return;
    }
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
	        var $this = $(this);
			var eventObject = {
					title: $.trim($this.text()) // use the element's text as the event title
			};
			// store the Event Object in the DOM element so we can get to it later
			$this.data('eventObject', eventObject);
			// make the event draggable using jQuery UI
			var _default = {
					zIndex: 999,
					revert: true, // will cause the event to go back to its
					revertDuration: 0 //  original position after the drag
			}
			var options = getExtendSettings($this, _default);
			$this.draggable(options);
        })
	}
},"拖拽插件");
ShieldJS.HtmlAction.addHTMLEleAction("fileUpload", "input.fileUpload", function(contentE, menuli, menu, searchForm, shieldDialog){
	if (!jQuery().fileinput) {
		ShieldJS.error("检测到fileUpload元素缺少依赖的JS库：bootstrap-fileinput！");
        return;
    }
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
	        var $this = $(this);
	        var url = $this.data("url");
	        var allowedFileExtensions_defaultStr = "";
	        var allowedFileExtensions = null;
	        if ($this.hasClass("image")) {
	        	allowedFileExtensions = "jpg,png,gif";
            }
	        allowedFileExtensions_defaultStr = $this.data("accept") || allowedFileExtensions;
	        if (allowedFileExtensions_defaultStr) {
	        	allowedFileExtensions = allowedFileExtensions_defaultStr.split(",");
            }
	        if (url) {
	        	var _default = {
        			language              : $this.data("language")||'zh'
	        		,uploadUrl             : url
	        		,autoReplace           : $this.data("autoreplace")||true
	        		,maxFileCount          : $this.data("maxfilecount")||1
	        		,maxFileSize           : $this.data("maxfilesize")||2000
	        		,uploadAsync           : $this.data("uploadasync")||true //默认异步提交
	        		,enctype               : $this.data("enctype")||'multipart/form-data'
	        		,dropZoneTitle         : $this.data("dropzonetitle")||"可以拖拽文件到这里!!!"
	        		,allowedPreviewTypes   : ['image'] //允许图片预览，其他预览存在中文乱码的情况
	        		,allowedFileExtensions : allowedFileExtensions
	        		,browseClass           : $this.data("browseclass")||"btn green btn-outline sbold" //按钮样式
	        		,removeClass           : $this.data("removeclass")||"btn red btn-outline sbold"
	        		,uploadClass           : $this.data("uploadclass")||"btn purple btn-outline sbold"
	        		,previewFileIcon       : $this.data("previewfileicon")||"<i class='glyphicon glyphicon-king'></i>"
	        		,preferIconicPreview: true // 开启用图标替换预览效果,图片没有设置仍显示预览
	        		,previewFileIconSettings: { // configure your icon file extensions
        		       'doc': '<i class="fa fa-file-word-o text-primary"></i>'
        		       ,'xls': '<i class="fa fa-file-excel-o text-success"></i>'
        		       ,'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>'
        		       ,'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>'
        		       ,'zip': '<i class="fa fa-file-archive-o text-muted"></i>'
        		       ,'htm': '<i class="fa fa-file-code-o text-info"></i>'
        		       ,'mov': '<i class="fa fa-file-movie-o text-warning"></i>'
        		       ,'mp3': '<i class="fa fa-file-audio-o text-warning"></i>'
        		       ,'txt': '<i class="fa fa-file-text-o text-info"></i>'
        		       ,'code': '<i class="fa fa-file-code-o text-info"></i>'
	        		}
	        		,previewFileExtSettings: {
        		       'doc': function(ext) {
        		           return ext.match(/(doc|docx)$/i);
        		       }
        		       ,'xls': function(ext) {
        		           return ext.match(/(xls|xlsx)$/i);
        		       }
        		       ,'ppt': function(ext) {
        		           return ext.match(/(ppt|pptx)$/i);
        		       }
        		       ,'zip': function(ext) {
        		           return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        		       }
        		       ,'htm': function(ext) {
        		           return ext.match(/(htm|html)$/i);
        		       }
        		       ,'mov': function(ext) {
        		           return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        		       }
        		       ,'mp3': function(ext) {
        		           return ext.match(/(mp3|wav)$/i);
        		       }
        		       ,'txt': function(ext) {
        		           return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        		       }
        		       ,'code': function(ext) {
        		           return ext.match(/(xml|xsl|jsp|php|class|asp|sh|bat|cmd)$/i);
        		       }
	        		}
//	        		,layoutTemplates:{ // 预览图片按钮控制，这里屏蔽预览按钮
//	        		   	actionZoom:''
//	        		}

    	        }
    	        var options = getExtendSettings($this, _default);
	        	$this.fileinput(options).on("fileuploaded", function (e, data) {
	        		var res = data.response;
	        		if (res.success==true) {
	        			ShieldJS.alert("",res.msg,"success");
	        		} else {
	        			ShieldJS.alert("",res.msg,"error");
	        		}
	        	})
            }
        })
	}
},"bootstrap-fileinput文件上传");
// div滚动
ShieldJS.HtmlAction.addHTMLEleAction("scroller", ".scroller", function(contentE, menuli, menu, searchForm, shieldDialog){
	if (!jQuery().slimScroll) {
		ShieldJS.error("检测到scroller元素缺少依赖JS库：jquery.slimscroll.js！");
        return;
    }
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
	        var $this = $(this);
	        $this.slimScroll({height:  $this.data("height")+'px'});
        })
	}
},"div滚动插件（jquery.slimscroll）");
//设置select元素的值
var setVal_Select = function(selectE, value) {
	if (!value) {
		value = selectE.data("v");
    }
	if (value) {
		selectE.val(value).trigger("change");
		selectE.change();
	}
};
//===================================================== select2 begin =======================================================
//初始化select2元素
var initSelect2Eles = function(ele, changeFn) {
	if (!jQuery().select2) {
		ShieldJS.error("检测到scroller元素缺少依赖JS库：select2.full.js！");
        return;
    }
	var pageSize = ele.data("pagesize")||20;
    var _default = { 
    	multiple : false
    	,language: "zh-CN"
    	,minimumInputLength: -1//最少输入多少个字符后开始查询
//    	,allowClear: true
    };
    if (ele.data("nosearch")==""||ele.data("nosearch")=="true") {
    	_default.minimumResultsForSearch = -1; //不搜索，minimumInputLength为-1时
    	_default.minimumInputLength = -1;
    }
    if (ele.data("ajax")) {
    	_default.ajax = {
			url: ele.data("url"),
			dataType: 'json',
			data: function (params) {
				var query = { //请求的参数, 关键字和搜索条件之类的
					search: params.term //select搜索框里面的value
					,page: params.page||1
				}
				// Query paramters will be ?search=[term]&page=[page]
				query = $.extend(true, query, getParams(ele));
				return query;
			},
//        delay: 1500,
			processResults: function (data, params) {
				//返回的选项必须处理成以下格式
				//var results =  [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }];
				var results=[];
				if (data.success) {
					results = data.obj;
                }
				return {
					results: results  //必须赋值给results并且必须返回一个obj
					,pagination: {
						more: (params.page||1 * pageSize) < data.total||0
	             	}
				};
			}
			,cache: true
    	} 
    }
    var options = getExtendSettings(ele, _default);
    var areaObj = ele.select2(options);
    if (ele.data("url")) { //清空原来的options
    	ele.empty();//清空下拉框
    	fillSelectOptions(ele, changeFn);
    }
    setVal_Select(ele);
}
var fillSelectOptions = function(ele, changeFn) {
	if (!ele.data("ajax")) { //排除原插件的ajax调用
		//绑定Ajax的内容
		if (ele.data("url")) {
			ele.empty();//清空下拉框
			ShieldJS.ajax.getJSON(ele.data("url"), {}, function (data) {
				if (data.success) {
					ele.append("<option value='0'>&nbsp;请选择……</option>");
					$.each(data.obj, function (i, item) {
						ele.append("<option value='" + item.id + "'>&nbsp;" + item.text + "</option>");
					});
					setVal_Select(ele);
					if ($.isFunction(changeFn)) {
						ele.change(function() {
							changeFn(ele);
						});
					}
				} 
			});
        }
    }
}
ShieldJS.HtmlAction.addHTMLEleAction("select2", ".select2", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
	        var $this = $(this);
	        initSelect2Eles($this);
        });
	}
},"Select2控件（select2.full）");
//===================================================== select2 end =======================================================
ShieldJS.HtmlAction.addHTMLEleAction("area", ".area", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
	        var $thisArea = $(this);
	        if ($thisArea.find("select").length>0) {
	        	$thisArea.find("select").each(function(i, n) {
	                var $thisSelectE = $(this);
	                var changeFn;
	                if (i == 0) {
	                	$thisSelectE.data("url", "system/area/province.json"); //正式运行时需替换
	                	changeFn = function(selectE) {
	                		var index = $thisArea.find("select").index(selectE); 
	                		var nextE = $thisArea.find("select").eq(index+1);
	                		if (nextE.length>0) {
	                			if (index==0) {
	                				nextE.data("url", "system/area/city.json?id="+selectE.val()); //正式运行时需替换
                                }else {
                                	nextE.data("url", "system/area/county.json?id="+selectE.val()); //正式运行时需替换
								}
	                			fillSelectOptions(nextE, changeFn);
                            }
                        };
                    } else {
                    	$thisSelectE.append("<option value=''>&nbsp;选择区域……</option>");
                    }
	                $thisSelectE.data("width", "200px");
	                initSelect2Eles($thisSelectE, changeFn);
                });
            }
        });
	}
},"行政区域联动（select2.full）");
