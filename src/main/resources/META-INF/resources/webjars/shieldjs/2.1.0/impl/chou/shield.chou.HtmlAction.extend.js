ShieldJS.setVersion("Chou");
try {
	ctxPath;
} catch (e) {
	ctxPath = "";
}
// qtip z-index修改，防止被layer弹出窗遮盖
$.fn.qtip.zindex = 20191050;
// qtips常用样式
var QTIPS_CLS = new ShieldMap();
QTIPS_CLS.put("red","qtip-red qtip-shadow qtip-rounded");
QTIPS_CLS.put("yellow","qtip-yellow qtip-shadow qtip-rounded");
QTIPS_CLS.put("green","qtip-green qtip-shadow qtip-rounded");
QTIPS_CLS.put("blue","qtip-blue qtip-shadow qtip-rounded");
QTIPS_CLS.put("light","qtip-light qtip-shadow qtip-rounded"); //浅色
QTIPS_CLS.put("dark","qtip-dark qtip-shadow qtip-rounded"); //深色
QTIPS_CLS.put("cream","qtip-cream qtip-shadow qtip-rounded");
QTIPS_CLS.put("youtube","qtip-youtube qtip-shadow qtip-rounded");
QTIPS_CLS.put("jtools","qtip-jtools qtip-shadow qtip-rounded");
QTIPS_CLS.put("cluetip","qtip-cluetip qtip-shadow qtip-rounded");
QTIPS_CLS.put("tip","qtip-tip qtip-shadow qtip-rounded");
QTIPS_CLS.put("titlebar","qtip-titlebar qtip-shadow qtip-rounded");
QTIPS_CLS.put("default","qtip-default qtip-shadow qtip-rounded");
// 覆盖表单验证的验证样式，注意弹出框的情况，需要在关闭弹出框后销毁qtips
$.fn.kvvalidate.defaults.callback = function(success, ele, msg, options) {
	//ShieldJS.debug(msg);
	if(!success && options && options.alert) { //弹出窗错误信息
		ShieldJS.error(msg);
		return;
	}
	var $this = ele;
    var position = {
    	right_out : ['left center', 'right center'],
    	right_in : ['right center', 'right center'],
    	left : ['left center', 'left center'],
    	left_top : ['bottom left', 'left top']
    };
    var showPosition = $this.parents('.validate').data("position")||"right_out";
    var qtipCls = $this.parents('.validate').data("qtipsClass")||"yellow";
    var qtipsClassV = QTIPS_CLS.get(qtipCls)||qtipCls;
    
    var positionE = ele; //定位元素，验证元素有可能不可见
    if (!positionE.is(":visible")) { //不可见的处理
    	var errorE = positionE.next(".qtipsvalerrortips");
	    if (errorE.length > 0) {
	    	positionE = errorE;
        } else {
        	positionE.after('<span class="qtipsvalerrortips" description="隐藏元素表单错误提示用">&nbsp;&nbsp;</span>');
        	errorE = positionE.next();
        	positionE = errorE;
        }
    }
	if (success) {
		positionE.removeAttr("qtipsvalerror").qtip('destroy');
    } else {
    	$this.attr("qtipsvalerror", true); // 标示验证错误
    	var options = {
    		content : {
				text: msg
			},
			style : {
				classes : qtipsClassV     		
			},
			position : {
				my: position[showPosition][0],  /*指向图片在qtips的位置*/
                at: position[showPosition][1],  /*指向图片在元素的位置*/
				viewport: $(window)
			},
			show: {  
				event: false,
				ready: true
			},
			hide: false
    	};
    	// layer的tips
//    	layer.tips(msg, positionE, {
//   		 	tips: [2, '#acacac']
//   		});
    	// qtips存在兼容性的问题，ie7下不能显示，根据实际情况进行取舍
    	positionE.qtip(options);
    	setTimeout(function(){
    		positionE.qtip('destroy');
		}, 1200); //1.2秒后自动关闭，防止有些不能关闭的情况出现影响显示
	}
}
// 显示/隐藏侧边栏
$("#resize").click(function() {
	var barWidth = $(this).width();
	if (ShieldJS.siderE.is(":visible")) {
		ShieldJS.siderE.hide();
		$(this).css('left', 0);
		var contentLeft = ShieldJS.contentMainE.offset().left;
		ShieldJS.contentMainE.data("contentLeft", contentLeft);
		ShieldJS.contentMainE.css('left', barWidth);
	} else {
		ShieldJS.siderE.show();
		$(this).css('left', ShieldJS.siderE.width());
		ShieldJS.contentMainE.css('left', ShieldJS.contentMainE.data("contentLeft"));
	}
}).mousedown(function() {
	return false;
});
if(typeof layer == "undefined" ){
	ShieldJS.error("缺少依赖JS库：layer！");
}
/** 
* 判断浏览器是否支持某一个CSS3属性 
* @param {String} 属性名称 
* @return {Boolean} true/false 
* @version 1.0 
* @author ydr.me 
* 2014年4月4日14:47:19 
*/
 
function supportCss3(style) { 
	var prefix = ['webkit', 'Moz', 'ms', 'o'],
	i, 
	humpString = [], 
	htmlStyle = document.documentElement.style, 
	_toHumb = function (string) { 
		return string.replace(/-(\w)/g, function ($0, $1) {
			return $1.toUpperCase(); 
		}); 
	}; 
	 
	for (i in prefix) {
		humpString.push(_toHumb(prefix[i] + '-' + style)); 
	}
	 
	humpString.push(_toHumb(style)); 
	 
	for (i in humpString) {
		if (humpString[i] in htmlStyle) return true; 
	}
	 
	return false; 
}
var loadingHtml = '<img src="'+ctxPath+'/images/icon/loading.gif"/ class="loading">加载中...';
if (supportCss3('animation-delay')) {
	loadingHtml = '<div class="loader1"><div class="loadtext">加载中...</div><div class="rect1 loading"></div> <div class="rect2 loading"></div> <div class="rect3 loading"></div> <div class="rect4 loading"></div> <div class="rect5 loading"></div></div>';
}

var txtJump = {
	len: 0,
	autoindex : 0,
	txtDom: "",
	arrTxt: [],
	stopauto : false,
	
	init: function(obj) {
		this.obj = obj;
		this.txtDom = obj.innerHTML.replace(/\s+/g, "");
		this.len = this.txtDom.length;
		this.autoindex = 0;
		this.arrTxt = [];
		obj.innerHTML = "";
		this.addDom();
	},
	addDom: function() {
		for (var i = 0; i < this.len; i++) {
			var spanDom = document.createElement("span");
			spanDom.innerHTML = this.txtDom.substring(i, i + 1);
			this.obj.appendChild(spanDom);
			this.arrTxt.push(spanDom);
		};
		for (var j = 0; j < this.len; j++) {
			this.arrTxt[j].style.position = "relative";
		};
		this.strat();
	},
	// 自动触发下一个
	autotrigger:function(){
		if(this.autoindex<this.len){
			var thisO = this;
			var ele = $(this.arrTxt[this.autoindex]).filter(':visible');
			if (ele.length && !this.stopauto ){
				ele.triggerHandler("mouseover");
				if(ele.timeouttimer){ //如果存在先清空，防止重复执行
					clearTimeout(ele.timeouttimer);
					ele.timeouttimer = null;
				}
				
				ele.timeouttimer = setTimeout(function(){
					thisO.autoindex++;
					thisO.autotrigger();
				}, 600);
			} else {
				clearTimeout(ele.timeouttimer);
				ele.timeouttimer = null;
			} 
			
		} else {
			this.autoindex = 0;
			this.autotrigger();
		} 	
	},
	stop: function() { // 停止
		this.stopauto = true;
	},
	strat: function() {
		for (var i = 0; i < this.len; i++) {
			
			$(this.arrTxt[i]).off('mouseover').on('mouseover', function() {
				// this是dom对象
				this.stop = 0;
				this.speed = -1;
				var $this = this;
				if (this.timer) { //先清空
					clearInterval(this.timer);
				};
				this.timer = setInterval(function() {

					$this.stop += $this.speed; //0  += -1
					if ($this.stop <= -20) {
						$this.speed = 1;
					}

					$this.style.top = $this.stop + "px";

					if ($this.stop >= 0) {
						clearInterval($this.timer)
						$this.style.top = 0 + "px";
					};
				}, 15);
			});
		}
		this.stopauto = false;
		this.autotrigger();
	}
}
 


var block_layui;
var block_layui_timer;
// 覆盖
var ShieldJS_V2 = {
	// 覆盖消息提醒方法
	alert : function(title, message, type, yesFn) {
		var ops = {title:title, skin: 'layui-layer-chouui', zIndex:20000000 };
		if ("success" == type) {
			ops.icon = 1;
        } else if ("error" == type) {
        	ops.icon = 2;
        } else if ("info" == type) {
        	ops.icon = 0;
		} else if ("warning" == type) {
        	ops.icon = 0;
		} else {
			layer.msg(message);
			return;
		}
		layer.alert(message, ops, function(index){
			//do something
			if (yesFn && $.isFunction(yesFn)) {
				yesFn();
			}
			layer.close(index);
		});
	}
	// 覆盖确认框
	,confirm : function(title, message, ok, cancel){
		layer.confirm(message, {
			title:title
			,skin: 'layui-layer-chouui'
		}, function(index){
			if (ok && $.isFunction(ok)) {
				ok();
			}
			layer.close(index); //关闭
		}, function(index){
			if (cancel && $.isFunction(cancel)) {
				cancel();
			}
		});
	}
	,initExtend : function(options) {
	}
	,loadingHtml:loadingHtml
	,dialog: {
		removeCallback : function(dialogE) { //关闭弹出框的回调
			// 关闭qtips
			var qipsvalerrorEles = dialogE.find("[qtipsvalerror]");
			if (qipsvalerrorEles.length > 0) {
				qipsvalerrorEles.removeAttr("qtipsvalerror").qtip('destroy');
            }
			// 关闭弹出框后同时销毁ueditor
			var ueditorEles = dialogE.find("[ueditorinit]");
			if (ueditorEles.length > 0) {
				ueditorEles.each(function() {
	                var $this = $(this);
	                removeUeditor($this.attr("ueditorid"));
                })
            }
		}
	}
	/** 请求的遮罩，一般用于ajax请求 options
    * @type Function
    * @param ele {jQueryObj} 发起请求的jQuery对象
    * @param url {String} 请求链接
    * @param params {Object} 参数,json格式或者字符串格式
    * @param method {String} 请求方法
    * @param dataType {String} 返回结果类型
    * @param async {Boolean} 是否是异步请求
    */
    ,blockUI : function(options){
    	if(!options.target){ //没有目标对象，如果有那么应该是页面刷新的情况，不进行遮罩
    		if(block_layui){
		    	layer.close(block_layui);
	    	}
	    	if(block_layui_timer){
	    		clearTimeout(block_layui_timer);
	    	}
			block_layui_timer = setTimeout(function(){
		    	block_layui = layer.msg('努力处理中……', {
		    		skin: ShieldJS.blockUIclsName, icon: 16, shade: 0.3, time:0, 
		    	  	success: function(layero, index){
		    	  		options.layero = layero;
		    	  		options.layerindex = index;
				    	ShieldJS.blockUISuccess(options);
				  	}
		    	});
			}, 800 ); // 频繁弹出会影响使用体验，所以延时后再弹出，如果在此时间内完成了则会取消定时器

    	}
    }
    /** 去除遮罩
    @type Function
    */
    ,unblockUI : function(options){
    	if(block_layui_timer){
    		clearTimeout(block_layui_timer);
    		block_layui_timer = null;
    	}
    	if(block_layui){
	    	layer.close(block_layui);
    	}
    	block_layui = null;
		//txtJump.stop();
    }
    /** 遮罩打开后的操作
    @type Function
    */
    ,blockUISuccess : function(options){
    	var layero = options.layero ;
    	if(layero){
    		console.log($(layero).html())
    		var layerContentE = $(layero).find('.layui-layer-content');
    		//var iEle = layerContentE.find('i').clone(); //复制节点，
    		var text = layerContentE.text(); // 复制文本，并包裹
    		//layerContentE.empty().append(iEle).append('<div>'+text+'</div>');
    		layerContentE.contents().eq(1).wrap('<div class="textdiv"></div>'); 
			var txtDom = layerContentE.find('.textdiv')[0];
			//var txtDom = layerContentE.contents().eq(1).get(0);
			txtJump.init(txtDom);
    	}
    }
}
ShieldJS = $.extend(true, ShieldJS, ShieldJS_V2);
// 菜单扩展点击事件
ShieldJS.core.leftCommonMenuClickDoing = function(event, liMenuContainerE, siderMenuClickEle, contentE) {
	liMenuContainerE.find(".shield-menu").removeClass(ShieldJS.core.options.liMenuCurClass);
	siderMenuClickEle.addClass(ShieldJS.core.options.liMenuCurClass); //重置选中样式
	// 刷新前的操作
	// 关闭qtips
	contentE.find("[qtipsvalerror]").removeAttr("qtipsvalerror").qtip('destroy');
	
	var hrefmenu = siderMenuClickEle.data("url");
	if (hrefmenu) { //有链接才处理并且不为空字符串
		
		var relType = "";
		if(siderMenuClickEle.data("relType")){
			relType = siderMenuClickEle.data("relType"); // 处理iframe的情况
		}
		var params={};
		if (relType == 'iframe') {
			var tabid = 'shield_iframe_show';
			changeFrameHeight = function (tabid) {
				var ifm = $("#iframe" + tabid);
				if (ifm.length > 0) {
					var rootE;
					try {
						rootE = ifm.contents().find(":root"); //跨域获取不到
                    } catch (e) {
                    	// %c表示样式%s表示字符串
                    	window.console && window.console.log && console.log("%c%s","color: red; background: yellow;","错误：");
                    	ShieldJS.debug(e);
                    }
					var mainHeight = ShieldJS.contentMainE.height();
					var setHeight = rootE?rootE.height():mainHeight; //rootE[0].scrollHeight与height()相同
					
					setHeight = Math.max(setHeight, mainHeight-5); //取大的值
					ifm.height(setHeight);
					var iframeResizeFn = ShieldJS.winResizes["iframeResize"+tabid]; //resize置空
					if (!iframeResizeFn) {
						iframeResizeFn = function() {
							var ifm = $("#iframe" + tabid);
							if (ifm.length > 0) {
								changeFrameHeight(tabid);
							}
							return false;
                        }
						ShieldJS.winResizes["iframeResize"+tabid] = iframeResizeFn;
                    }
				} else {
					ShieldJS.error("id为iframe" + tabid + "的iframe不存在！");
				}
				// alert($(ifm.document).find("body").length);
			}
			var relHeight;
			if(siderMenuClickEle.data("height")){
				relHeight = parseInt(siderMenuClickEle.data("height")); // 高度
			}
			var relHeightStr = ' onload="changeFrameHeight(\'' + tabid + '\')" ';
			if (relHeight) {
				relHeightStr = ' height="'+relHeight+'" ';
			}
			contentE.html("").append('<iframe id="iframe' + tabid + '" width="100%" ' + relHeightStr + ' frameborder="0" scrolling="auto"></iframe>');
			var ifrmaeE = $("#iframe" + tabid, contentE);
			ifrmaeE.attr("src", hrefmenu+"?ifrran="+Math.random());
			ifrmaeE.on('load', function() { //绑定事件
                var iframeContentE = ifrmaeE.contents().find(":root"); //搜索根元素
                ShieldJS.core.contentAreaDeal(iframeContentE, siderMenuClickEle, siderMenuClickEle)
            });
		} else {
      		var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector(siderMenuClickEle, params); // 参数选择器
			if (!checkParams) {
				return false;
			} else {
				params = checkParams;
			}
			params = getParams(siderMenuClickEle, params);
			ShieldJS.ajax.get(contentE, hrefmenu, params, function() {
				// 内容区处理
				if(!ShieldJS.core.contentAreaDeal(contentE, siderMenuClickEle, siderMenuClickEle)){ //内容区处理，绑定事件
					return false;
				}
				if (relHeight) {
          			tabContentShowEle.height(relHeight);
                }
			}, function(data){ //失败移除
				var json;
				if (typeof data == 'object') { //js对象
					json = data;
				} else { //一般为string
					try {
						json = $.parseJSON(data);
					} catch (e) {
						json = '';
					}
				}
      			ShieldJS.error(json.message||"执行失败！");
      		});
      	}
		
		
	}
	return true; //扩展点击事件，返回false则不执行后续方法，返回true则继续执行后面的方法
};
//==================================================================================================================
ShieldJS.HtmlAction.addHTMLEleAction("td-input-required", "form td:contains('*'),form th:contains('*')", function(contentE, menuli, menu, searchForm, shieldDialog){
	var selectE = contentE.find(this.expression)
	selectE.each(function() {
	    var selectEthis = $(this);
	    var se = '[type=text],[type=password],[type=file],[type=hidden],select[name],textarea';
	    var notSe = ".ke-upload-file,.webuploader-element-invisible,.ignore"; //上传组件排除
	    selectEthis.closest("td,th").next().find(se).not(notSe).addClass('required');
    });
}, "td中必填项input的处理");
//cron表达式
//定时任务统一参数
$.fn.cronGen.defaultOptions = $.extend(true, $.fn.cronGen.defaultOptions, {
	type : "springcron",
	// 提交按钮样式
	//submitBtnClass : 'btn blue',
	// 显示值的input框样式
	//showInputClass : 'form-control',
	// 弹出按钮style
	//inputGroupBtnStyle : '',
	// 弹出按钮html
	selectBtn : '<button class="shield-btn shield-btn-primary shield-btn-small"><i class="shield-icon"></i></button>'
});
ShieldJS.HtmlAction.addHTMLEleAction("shield-cron", ".shield-cron", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.each(function() {
	    var selectEthis = $(this);
	    var params = {};
		var execTimesUrl = selectEthis.attr("execTimesUrl")||"";
		if (execTimesUrl) {
			params.execTimesUrl = execTimesUrl;
		}
		params = getExtendSettings(selectEthis, params); //data-xx可能会覆盖取到的值
	    selectEthis.cronGen(params);
    });
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if(!$.fn.cronGen){
			ShieldJS.error("检测到shield-cron元素缺少依赖JS库：cronGen.js！");
			return false;
		}
		dealEle.each(function() {
		    var selectEthis = $(this);
		    if (!selectEthis.is(":input")) {
		    	dealEle = dealEle.not(selectEthis);
		    	ShieldJS.error("检测到shield-cron元素不是input元素，请修改！"+selectEthis.prop("outerHTML"));
				return false; //true=continue，false=break。 
            }
	    });
		// 可进一步筛选
		return dealEle; //返回符合条件的jQuery对象
	}
	return dealEle.length > 0 ? dealEle : false;
},"cron表达式选择器");
//highCharts图
ShieldJS.HtmlAction.addHTMLEleAction("highChartsContainer", "#highChartsContainer", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var container = findAndMarkEle(contentE, this.expression);
	if(container.length>0){
		var intimeacctimeout;
		var loadInterval = function(container,series,series1,series2,series3,time){
			if(intimeacctimeout && container.is(":hidden") ){
				clearTimeout(intimeacctimeout);
			}else{
				if(hasAttr(container,"refreshUrl")){
					$.ajax({
						type: "POST",
						url: container.attr("refreshUrl"),
						dataType: "json",
						async: true,
						success: function(json){
							var x = json.xName, // current time
							xNext = json.xNameNext,
							y = json.data.count1,
							y1 = json.data.count2,
							y2 = json.data.count3,
							y3 = json.data.count4;
							saveOrupdatPoint(series, x, y,xNext);
							saveOrupdatPoint(series1, x, y1,xNext);
							saveOrupdatPoint(series2, x, y2,xNext);
							saveOrupdatPoint(series3, x, y3,xNext);
							intimeacctimeout = setTimeout(function() {
								loadInterval(container,series,series1,series2,series3,time);
							}, time);
						}
					});
				}
			}
		};
		var saveOrupdatPoint = function(series,x,y,xNext){
			var exist = false;
			for (var index = series.data.length-2; index<series.data.length; index++) {
				var data = series.data[index];
				if(data.category==x || data.name==x){
					if(index==series.data.length-1){
						series.addPoint([xNext, 0], true, true);
					}
					exist = true;
					data.y=y;
					data.update();
					break;
				}
			}
			if(!exist){
				series.addPoint([x, y], true, true);
				series.addPoint([xNext, 0], true, true);
			}
		};
		
		if(hasAttr(container,"dataUrl")){ //有链接才处理
			var dataUrl = container.attr("dataUrl");
			container.html(ShieldJS.loadingHtml);
			var titleSuffix = "";
			if(hasAttr(container,ShieldJS.css.TITLE)){ //标题后缀
				titleSuffix = container.attr(ShieldJS.css.TITLE);
			}
			var yAxisTitle = "";
			if(hasAttr(container,"yAxisTitle")){ //y轴名称
				yAxisTitle = container.attr("yAxisTitle");
			}
			var xAxisTitle = "";
			if(hasAttr(container,"xAxisTitle")){ //x轴名称
				xAxisTitle = container.attr("xAxisTitle");
			}
			var tooltip = "";
			if(hasAttr(container,"tooltip")){ //tooltip名称
				tooltip = container.attr("tooltip");
			}
			var chartSetting = {
					//type: 'spline'
			}
			if(hasAttr(container,"refresh") && container.attr("refresh")=='true'){ //刷新
				var refreshMils = 3000; //默认3s刷新一次
				if(hasAttr(container,"refreshMils")){ //刷新时间，单位毫秒
					refreshMils = container.attr("refreshMils");
				}
				chartSetting = {
						events: {
							load: function() {
								var chart = this;
								var series = chart.series[0];
								var series1 = chart.series[1];
								var series2 = chart.series[2];
								var series3 = chart.series[3];
								loadInterval(container,series,series1,series2,series3,refreshMils);
							}
						}
				};
			}
			
			ShieldJS.ajax.post(dataUrl, contentE.find('form.searchForm').serialize(), function(json){
				json = $.parseJSON(json);
				container.highcharts({
					chart: chartSetting,
					title: {
						text: json.title + titleSuffix
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						title: {
							text: xAxisTitle
						},
						categories: json.xNames
					},
					yAxis: {
						title: {
							text: yAxisTitle
						},
						min: 0
					},
					tooltip: {
						crosshairs: true,
						shared: true,
						headerFormat: '<span style="font-size: 14px">{point.key}'+tooltip+'</span><br/>'
					},
					series: json.data
				});
			});
		}
	}
},"highCharts图");

//树形结构
ShieldJS.HtmlAction.addHTMLEleAction("shieldZtree", ".shieldZTree", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var ztreeMenuContainerE = findAndMarkEle(contentE, this.expression);
	if (ztreeMenuContainerE.length > 0) {
   		if (!$.fn.zTree) {
   			ShieldJS.error("检测到shieldZTree元素缺少依赖JS库：jquery.ztree.js！");
			return;
        }
   		// 参数适配
   		var paramAdapt = function(ele, treeNode) {
   			var paramNames = paramNameAdapt(ele);
   			var params = {}
   			for (var key in paramNames) {
   				params[paramNames[key]] = treeNode[key];
            }
			return params;
        }
   		var getFont = function(treeId, node) { //字体，用于从结果中增加样式
			return node.font ? node.font : {};
		}
   		var addHoverDom = function(treeId, treeNode) {
   			var containerE = $("#"+treeId);
   			var params = paramAdapt(containerE, treeNode);
   			var paramsStr = jQuery.param(params);
   	        var aObj = $("#" + treeNode.tId + "_a");
   	         var addStr ="";
	   	     var showBtn = $("#ztree_showBtn_"+treeNode.tId);
		     var showUrl = containerE.data("urlShow"); //修改大写
		     if (showBtn.length == 0 && showUrl){
		    	if (showUrl.indexOf("?") != -1) {
		        	showUrl += "&"+ paramsStr;
		    	} else {
		    		showUrl += "?"+ paramsStr;
		    	}
	        	var canShow = true;
	        	if (treeNode.hasOwnProperty("canShow")) { //存在属性，包含属性
	        		canShow = treeNode.canShow;
	        	}
		        if (canShow) {
		        	addStr = '<span class="shieldDialog shield-icon show" data-url="'+showUrl+'" id="ztree_showBtn_' + treeNode.tId + '" viewTitle="查看" viewWidth="800"></span>';
		        }
		    }
   	        var addBtn = $("#ztree_addBtn_"+treeNode.tId);
   	        var addUrl = containerE.data("urlAdd"); //修改大写
   	        if (addBtn.length == 0 && addUrl){
   	        	if (addUrl.indexOf("?") != -1) {
   	        		addUrl += "&"+ paramsStr;
                } else {
   	        		addUrl += "?"+ paramsStr;
                }
   	        	var canAdd = true;
   	        	if (treeNode.hasOwnProperty("canAdd")) { //存在属性，包含属性
   	        		canAdd = treeNode.canAdd;
                }
   	        	if (canAdd) {
   	        		addStr += '<span class="shieldDialog shield-icon add" data-url="'+addUrl+'" id="ztree_addBtn_' + treeNode.tId + '" viewTitle="新增" viewWidth="800"></span>';
                }
   	        }
   	        var updBtn = $("#ztree_updBtn_"+treeNode.tId);
   	        var updUrl = containerE.data("urlUpd");
   	        if (updBtn.length == 0 && updUrl) {
   	        	if (updUrl.indexOf("?") != -1) {
   	        		updUrl += "&"+ paramsStr;
                } else {
                	updUrl += "?"+ paramsStr;
                }
   	        	var canUpdate = true;
   	        	if (treeNode.hasOwnProperty("canUpdate")) { //存在属性，包含属性
   	        		canUpdate = treeNode.canUpdate;
                }
   	        	if (canUpdate) {
   	        		addStr += '<span class="shieldDialog shield-icon update" data-url="'+updUrl+'" id="ztree_updBtn_' + treeNode.tId + '" viewTitle="修改" viewWidth="800"></span>';
                }
   	        }
   	        var delBtn = $("#ztree_delBtn_"+treeNode.tId);
   	        var delUrl = containerE.data("urlDel");
   	        if (delBtn.length == 0 && delUrl) {
   	        	var canDelete = true;
   	        	if (treeNode.hasOwnProperty("canDelete")) { //存在属性
   	        		canDelete = treeNode.canDelete;
                }
   	        	if (canDelete) {
   	        		addStr += '<span class="shield-icon del" id="ztree_delBtn_' + treeNode.tId + '" title="删除" ></span>';
                }
   	        }
   	        if (addStr) {
   	        	aObj.append(addStr);
            }
   	        ShieldJS.core.bindHtmlAction(["shieldDialogOrLookup", "shield-icon"], aObj, menuliSelect, navMenuSelect, searchForm, shieldDialog); //内部继续加载
	        delBtn = $("#ztree_delBtn_"+treeNode.tId);
   	        if (delBtn.length > 0){
   	        	delBtn.unbind().bind("click", function(){
   	        		var zTree = $.fn.zTree.getZTreeObj(treeId);
	   		         if(null!=treeNode.children && treeNode.children.length>0){
	   		        	 ShieldJS.alert("提示信息","不能删除父节点","error");
	   		             return false;
	   		         }
	   		         ShieldJS.confirm("删除确认", "确定删除 "+treeNode.name+"?", function(){
	   		        	 ShieldJS.ajax.post(delUrl, params, function(responseJson){
   	                         if(responseJson.success==true){//返回true
   	                        	 zTree.removeNode(treeNode);//动态删除该节点
   	                             //ShieldJS.alert("操作成功！", responseJson.message, "success"); //框架已处理，无需再提示
   	                         }
   	                     }, function (data) {
   	                    	 ShieldJS.alert("操作失败！","系统错误！！！","error");
   	                     });
	   		         }, function() {
	   		            ShieldJS.alert("取消操作", "取消了当前操作", "info");
	   		         });
   	        		return false;
   	        	});
   	        } 
   	        return false;
   	    };
	   	 var removeHoverDom=function(treeId, treeNode) {
	   		 $("#ztree_showBtn_"+treeNode.tId).unbind().remove();
	         $("#ztree_addBtn_"+treeNode.tId).unbind().remove();
	         $("#ztree_updBtn_"+treeNode.tId).unbind().remove();
	         $("#ztree_delBtn_"+treeNode.tId).unbind().remove();
	     };
	     function onDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
	 		var zTree = $.fn.zTree.getZTreeObj(treeId);
	 		var allnodes = zTree.transformToArray(zTree.getNodes());
 			var msg = "检测到排序进行了改变，请记得更新排序！";
 			if ($("#resetsorts").length > 0) {
 				if(!$("#resetsorts").val()){
 					if ($("#resettips").length > 0) {
 						$("#resettips").text(msg); //页面内容中提示
                    } else {
                    	ShieldJS.alert("提示信息",msg,"info"); //提示框提示
                    }
 					$(".resetTreeOrderbtn").show();
 				} 
 				$("#resetsorts").val("");
 				for(var i=0;i<allnodes.length;i++){
 					//id-pid-level
 					$("#resetsorts").val($("#resetsorts").val()+""+allnodes[i].id+"-"+(allnodes[i].pId||0)+"-"+allnodes[i].level+",");
 				}
            }
	     }
	     var jsTreeSetting = {
			data : {
				simpleData : {
					enable : true
				}
			},
			view : {
				addHoverDom: addHoverDom,
                removeHoverDom: removeHoverDom,
                selectedMulti: false,
				showIcon : true,
				fontCss: getFont,
				nameIsHTML: true
			},
			edit: {
				drag: {
					autoExpandTrigger: true
				},
                enable: true,
                showRemoveBtn: false,
				showRenameBtn: false
            },
			callback : {
				onDrop: onDrop,
				onClick : function(e, treeId, treeNode) {
					if(treeNode){
						var containerE = $("#"+treeId);
						if (hasAttr(containerE,"clickExtendFn")) { //扩展点击事件
							var clickExtendFnName = containerE.attr("clickExtendFn");
							clickExtendFnName = eval(clickExtendFnName);
							if($.isFunction(clickExtendFnName)){
								if(!clickExtendFnName(e, treeId, treeNode, contentE)){
									return false;
								}
							}
						}
						var url = treeNode.urlNode;
						if (url) { //内容中自带链接
							var params = {};
							ShieldJS.ajax.get(contentE, url, params, function() {
								//内容区处理
								if(!ShieldJS.core.contentAreaDeal(contentE, null, navMenuClickEle)){
									return false;
								}
								
							});
						} else if (containerE.data("urlNode")) { //ul中公共链接
							var url = containerE.data("urlNode");
							var paramAdapter = containerE.attr("paramAdapter");
							var params = {};
							if (paramAdapter) { //有值且不为空
								params = paramAdapt(containerE, treeNode); //进行参数适配
								
								var targetV = containerE.data("target");
								var targetE; //目标显示区，jQuery选择器
								if (targetV) {
									targetE = getjQueryObj(targetV);
                                } else {
                                	targetE = ShieldJS.contentMainE.find(".ztreeContent"); //默认
								}
								if (targetE.length > 0) {
									ShieldJS.ajax.get(targetE, url, params, function() {
										//内容区处理
										ShieldJS.core.contentAreaDeal(targetE);
									});
                                } else {
                                	if (targetV) {
                                		ShieldJS.error("id为"+treeId+"的容器上定义的data-target属性未找到匹配的对象，请检查修正！");
                                    }else {
                                    	ShieldJS.error("未定义点击后内容显示区域：请在id为"+treeId+"的容器上添加data-target属性并添加目标显示区域（jQuery选择器）");
									}
								}
							} else {
								ShieldJS.error("未定义参数选择属性：请在"+treeId+"容器上添加paramAdapter属性并添加参数名，多个参数用#号连接");
								return false;
							}
						} 
					}
				}
			}
		};
		var loadZtree = function() {
			ztreeMenuContainerE.each(function(i, n){
				var treeE = $(this);
				var treeSettings = getExtendSettings(treeE, jsTreeSetting);
				if(typeof initZTree != "undefined" && $.isFunction(eval("initZTree"))){
					initZTree(treeE, treeSettings); // 初始化，可做参数或其他预处理
				}
				treeE.data("treesettings", treeSettings);
				if(treeE.data("url")){ //有链接才处理
					if(hasAttr(treeE, "asyncsetting")){ //异步请求
						treeSettings.async = eval(treeE.attr("asyncsetting"));
					}
					var url = treeE.data("url");
					var params = {};
					if (hasAttr(treeE,"paramSelector")) { //参数选择器
						params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params); //参数选择器 siderE.find(paramSelector).serializeJson();
						if ($.isEmptyObject(params)) {
							ShieldJS.alert("消息提示", "参数为空！", "error");
							return false;
						}
					}
					params.timestamp = new Date().getTime(); //时间戳防缓存
					params = getParams(treeE, params);
					
					var method = treeE.data("method")||"get";
					
					if (method == "get") {
						ShieldJS.ajax.get(null, url, params, function(json) {
							successZtreeDo(treeE,treeSettings,json);
		            	}, function(json) {
		            		treeE.html(json.message);
		            	});
		            } else {
		            	ShieldJS.ajax.post(url, params, function(json) {
		            		successZtreeDo(treeE,treeSettings,json);
		            	}, function(json) {
		            		treeE.html(json.message);
		            	}, "json");
					}
					
				
				/*	ShieldJS.ajax.getJSON(url, params, function(json) {
						if (json.data && json.data.length > 0) {
							$.fn.zTree.init(treeE, treeSettings, json.data); 
							var treeEId = treeE.attr("id");
							var zTreeObj = $.fn.zTree.getZTreeObj(treeEId);
							if(hasAttr(treeE,"selectNode")){ //默认选中节点
								var selectNode = treeE.attr("selectNode")||0; //默认0
								var defaultName = "rownum"; //默认属性
								var selectNodeValue= selectNode;
								var node;
								if (selectNode.indexOf("=") != -1) {
									defaultName = selectNode.split("=")[0];
									selectNodeValue = selectNode.split("=")[1];
									node = zTreeObj.getNodeByParam(defaultName, selectNodeValue);
								} else{
									var allNodes = zTreeObj.getNodes();
									node = allNodes[selectNode];
								}
								zTreeObj.selectNode(node);
								zTreeObj.setting.callback.onClick(null, zTreeObj.setting.treeId, node);
							}
						} else {
							treeE.html('没有数据！');
						}
					}, function() {
						treeE.html(json.message);
                    });*/
				}
				
			});
			
			var successZtreeDo = function(treeE,treeSettings,json){
				if (json.data && json.data.length > 0) {
					$.fn.zTree.init(treeE, treeSettings, json.data); 
					var treeEId = treeE.attr("id");
					var zTreeObj = $.fn.zTree.getZTreeObj(treeEId);
					if(hasAttr(treeE,"selectNode")){ //默认选中节点
						var selectNode = treeE.attr("selectNode")||0; //默认0
						var defaultName = "rownum"; //默认属性
						var selectNodeValue= selectNode;
						var node;
						if (selectNode.indexOf("=") != -1) {
							defaultName = selectNode.split("=")[0];
							selectNodeValue = selectNode.split("=")[1];
							node = zTreeObj.getNodeByParam(defaultName, selectNodeValue);
						} else{
							var allNodes = zTreeObj.getNodes();
							node = allNodes[selectNode];
						}
						zTreeObj.selectNode(node);
						zTreeObj.setting.callback.onClick(null, zTreeObj.setting.treeId, node);
					}
				} else {
					treeE.html('没有数据！');
				}
			}
			
			
			// 选中节点操作
			var treeCheckNode = function(e, zTreeObj) {
				var type = e.data.type,
				nodes = zTreeObj.getNodes();
				if (type == "checkAllTrue") {
					zTreeObj.checkAllNodes(true);
				} else if (type == "checkAllFalse") {
					zTreeObj.checkAllNodes(false);
				} else {
					var allnodes = zTreeObj.transformToArray(nodes); //全部节点
					for (var i=0, l=allnodes.length; i<l; i++) {
						if (type == "toggle") {
							zTreeObj.checkNode(allnodes[i], null, false, false);
						} else if (type == "togglePS") {
							zTreeObj.checkNode(allnodes[i], null, true, false);
						}
					}
				}
				if (zTreeObj.setting.callback.onCheck && $.isFunction(zTreeObj.setting.callback.onCheck)) {
					zTreeObj.setting.callback.onCheck(null, zTreeObj.setting.treeId); //执行点击操作
				}
			};
			// 展开收起操作
			var treeExpandNode = function(e, zTreeObj) {
				var type = e.data.type;
				if (type == "expandAll") {
					zTreeObj.expandAll(true);
				} else if (type == "collapseAll") {
					zTreeObj.expandAll(false);
				}
			};
			//过滤ztree显示数据
			var nodeList; //用于存储被符合条件的结点
			function filter(zTreeObj){
				for (var n in nodeList) { //取消高亮
//					nodeList[n].font= {color:"black", "font-weight":"normal"};
					if (nodeList[n].orginame) {
						nodeList[n].name = nodeList[n].orginame;
                    }
					zTreeObj.updateNode(nodeList[n]);
	            }
				var allNode = zTreeObj.transformToArray(zTreeObj.getNodes());
				zTreeObj.hideNodes(allNode); //隐藏
			    //查找符合条件的叶子节点
				function filterFunc(node) {
		            var keywords = $("#keyword").val();
		            if ( node.name.indexOf(keywords) > -1){
		            	node.orginame = node.name;
		            	node.name = "<font style='font-weight:bold;color:red;'>" + node.name + "</font>";
		            	//node.font= {"color":"red", "font-weight":"bold"}; //不使用此方式，因为存在多次搜索的还原问题
	                	zTreeObj.updateNode(node);
		            	return true;
		            } 
		            return false;
		        };
		        var keywords = $("#keyword").val();
		        if (keywords) {
		            //var nodeList = zTreeObj.getNodesByParamFuzzy("text", keywords, null);
		            nodeList = zTreeObj.getNodesByFilter(filterFunc); //获取符合条件的叶子结点
		            nodeList = zTreeObj.transformToArray(nodeList);
		            
		            for (var n in nodeList) {
		            	if (nodeList.hasOwnProperty(n)) {
		            		findParent(zTreeObj, nodeList[n]);
		            	}
		            }
		            zTreeObj.showNodes(nodeList);
		        } else {
		        	zTreeObj.showNodes(allNode);
		        	zTreeObj.expandAll(false);
		        }
			};
			// 查找父节点
			function findParent(treeObj, node) {
		        treeObj.expandNode(node, true, false, false);
		        var pNode = node.getParentNode();
		        if (pNode != null) {
		            nodeList.push(pNode);
		            findParent(treeObj, pNode);
		        }
		    };
			var btnContainers = findAndMarkEle(contentE, '.shieldZtreeBtnContainer'); //搜索按钮容器
			if (btnContainers.length > 0) {
				btnContainers.each(function(i, n){
					var btnContainer = $(this);
					var treeE;
					if (btnContainer.data("target")) { //有多个ztree时定义
						treeE = getjQueryObj(btnContainer.data("target"));
	                }
					if (!treeE) {
						treeE = ztreeMenuContainerE.eq(i); //获取ztree对象
					}
					
					//处理按钮
					var searchEles = findAndMarkEle(btnContainer, '.shieldZTreeRefresh'); //刷新按钮
					if (searchEles.length > 0) { //1个容器只取1个按钮
						var selectEthis = searchEles;
						var treeEThis;
						if (selectEthis.data("target")) { //有多个ztree时定义
							treeEThis = getjQueryObj(selectEthis.data("target"));
		                }
						if (!treeEThis) {
							treeEThis = treeE; //获取ztree对象
						}
						selectEthis.click(function(event){
							var params = {};
							if (hasAttr(treeE, "paramSelector")) { //参数选择器
								params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params); //参数选择器siderE.find(paramSelector).serializeJson();
								if ($.isEmptyObject(params)) {
									ShieldJS.alert("消息提示", "参数为空！", "error");
									return false;
								}
							}
							params.timestamp = new Date().getTime(); //时间戳防缓存
							params = getParams(treeE, params);
							if (hasAttr(selectEthis, "clickExtendFn")) { //扩展点击事件
								var clickExtendFnName = selectEthis.attr("clickExtendFn");
								clickExtendFnName = eval(clickExtendFnName);
								if($.isFunction(clickExtendFnName)){
									if(!clickExtendFnName(event, params, treeE, treeE.data("treesettings"), zTreeObj)){
										return false;
									}
								}
							}
							//未扩展，仍执行原查询
							ShieldJS.ajax.getJSON(treeE.data("url"), params, function(json) {
								if (json.data && json.data.length > 0) {
									$.fn.zTree.init(treeE, jsTreeSetting, json.data);
								} else {
									treeE.html('没有数据！');
								}
							}, function() {
								treeE.html(json.message);
		                        
	                        });
						});
					}
					var searchEles = findAndMarkEle(btnContainer, '.shieldZTreeSearch'); //筛选按钮
					if (searchEles.length > 0) { //1个容器只取1个按钮
						var selectEthis = searchEles;
						var treeEThis;
						if (selectEthis.data("target")) { //有多个ztree时定义
							treeEThis = getjQueryObj(selectEthis.data("target"));
		                }
						if (!treeEThis) {
							treeEThis = treeE; //获取ztree对象
						}
						selectEthis.click(function(event){
							var params = {};
							if (hasAttr(treeE, "paramSelector")) { //参数选择器
								params = ShieldJS.HtmlAction.checkAnGetParamSelector(treeE, params); //参数选择器siderE.find(paramSelector).serializeJson();
								if ($.isEmptyObject(params)) {
									ShieldJS.alert("消息提示", "参数为空！", "error");
									return false;
								}
							}
							params.timestamp = new Date().getTime(); //时间戳防缓存
							params = getParams(treeE, params);
							if (hasAttr(selectEthis, "clickExtendFn")) { //扩展点击事件
								var clickExtendFnName = selectEthis.attr("clickExtendFn");
								clickExtendFnName = eval(clickExtendFnName);
								if($.isFunction(clickExtendFnName)){
									if(!clickExtendFnName(event, params, treeE, treeE.data("treesettings"), zTreeObj)){
										return false;
									}
								}
							}
							//未扩展，仍执行原查询
							var zTreeObj = $.fn.zTree.getZTreeObj(treeE.attr("id")); //根据id获取ztree对象,不可放到外面，可能还未初始化
							filter(zTreeObj);
						});
					}
					findAndMarkEle(btnContainer, ".allSelect").bind("click", {type:"checkAllTrue"}, function(e) {
						var zTreeObj = $.fn.zTree.getZTreeObj(treeE.attr("id")); //根据id获取ztree对象,不可放到外面，可能还未初始化
						treeCheckNode(e, zTreeObj);
                    }); //全选
					findAndMarkEle(btnContainer, ".unAllSelect").bind("click", {type:"checkAllFalse"}, function(e) {
						var zTreeObj = $.fn.zTree.getZTreeObj(treeE.attr("id")); //根据id获取ztree对象
						treeCheckNode(e, zTreeObj); //全不选
					});
					findAndMarkEle(btnContainer, ".unSelect").bind("click", {type:"toggle"}, function(e) {
						var zTreeObj = $.fn.zTree.getZTreeObj(treeE.attr("id")); //根据id获取ztree对象
						treeCheckNode(e, zTreeObj); //反选
					});
					findAndMarkEle(btnContainer, ".expandAll").bind("click", {type:"expandAll"}, function(e) {
						var zTreeObj = $.fn.zTree.getZTreeObj(treeE.attr("id")); //根据id获取ztree对象
						treeExpandNode(e, zTreeObj); //全部展开
					});
					findAndMarkEle(btnContainer, ".collapseAll").bind("click", {type:"collapseAll"}, function(e) {
						var zTreeObj = $.fn.zTree.getZTreeObj(treeE.attr("id")); //根据id获取ztree对象
						treeExpandNode(e, zTreeObj); //全部收起
					});
					// 排序按钮
					findAndMarkEle(btnContainer, ".resetTreeOrderbtn").bind("click", {type:"order"}, function(e) {
						var $this = $(this);
						var url = $this.data('url');
						var container = $this.parent();
						ShieldJS.ajax.post(url, {
							sorts : container.find('#resetsorts').val(),
							startIndex : container.find('#resetstartIndex').val(),
							asc : container.find('#resetasc').length>0?container.find('#resetasc').val():0,
							fieldName : container.find('#resetfieldName').val(),
							type : container.find('#resettype').val(),
							mode : container.find('#resetmode').val()
						}, function() {
							$this.closest("form").submit();
						})
					});
				});
			};
			
		};
		loadZtree();
	}
}, "ztree插件处理");
//扩展方法
//日期，调用My97DatePicker的日期插件
ShieldJS.HtmlAction.addHTMLEleAction("shieldDatePicker", "input.Wdate", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		try {
			WdatePicker;
      } catch (e) {
      	ShieldJS.error("缺少依赖JS库：My97DatePicker！"+dealEle.html());
      	return false;
      }
		var startDate = findAndMarkEle(contentE, 'input[init="startDate"]');
		var nowDate = findAndMarkEle(contentE, 'input[init="nowDate"]');
		if (startDate.length > 0) {
			startDate.each(function() {
				var $this = $(this); 
				var pattern = $this.attr("dateFmt");
				if (pattern) {
					$this.val(dateFormat(new Date(), pattern));
				} else {
					$this.val(dateFormat(new Date(), 'yyyy-MM-01'));
				}
			});
		}
		if (nowDate.length > 0) {
			nowDate.each(function() {
				var $this = $(this); 
				var pattern = $this.attr("dateFmt");
				if (pattern) {
					$this.val(dateFormat(new Date(), pattern));
				} else {
					$this.val(dateFormat(new Date(), 'yyyy-MM-dd'));
				}
			});
		}
		dealEle.each(function() {
			var $this = $(this);
			var options = {};
			var pattern = $this.attr("dateFmt");
			var onpicked = $this.attr("onpicked");
			if (pattern) {
				options.dateFmt = pattern;
			}
			if (onpicked) {
				options.onpicked = onpicked;
			} else {
				options.onpicked = function(dp) {
	                $(this).triggerHandler("change");//触发自定义change事件
              };
			}
			options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
			$this.click(function() {
				WdatePicker(options);
			});
		});
	}
}, "日期，调用My97DatePicker的日期插件");
//可折叠的容器
ShieldJS.HtmlAction.addHTMLEleAction("shield-collapsable", ".shield-collapsable", function(selectE, menuli, menu, searchForm, shieldDialog){
	selectE.each(function() {
		var thisSelectE = $(this);
		
		var titleEle = thisSelectE.find(".shield-colla-title"); 
		var contentEle = thisSelectE.find(".shield-colla-content");
		var contentEleHeight = contentEle.height();
		var shieldCheckboxEle = thisSelectE.next();
		titleEle.on("click", ".collapsable-icon", function() {
			var spendTime = 600; //变化用时
			if (contentEle.hasClass(ShieldJS.css.SHOW)) { //可见变为不可见
				contentEleHeight = contentEle.height(); //高度校正
				contentEle.animate({height: '0px',opacity: 0}, spendTime);
				$(this).removeClass("to_bottom").addClass('to_top'); //<>展开图标
				setTimeout(function() {
					contentEle.toggleClass(ShieldJS.css.SHOW);
					contentEle.toggle();
				}, spendTime);
			} else { //不可见变为可见
				contentEle.toggleClass(ShieldJS.css.SHOW).show();
				contentEle.animate({height: contentEleHeight+'px',opacity: 1}, spendTime);
				$(this).removeClass("to_top").addClass('to_bottom'); //><收起图标
			}
		});
  });
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
			var $this = $(this);
			var titleEle = $this.find(".shield-colla-title"); 
			var contentEle = $this.find(".shield-colla-content");
			if (titleEle.find(".collapsable-icon").length == 0) { //图标区，没有则创建
				titleEle.append('<div class="to_bottom collapsable-icon"></div>');
			}
			if (contentEle.length == 0) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-collapsable元素缺少标题容器shield-colla-title属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
			if (contentEle.length == 0) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-timer-close元素缺少内容容器shield-colla-content属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
      });
	}
	return dealEle.length > 0 ? dealEle : false;
}, "可折叠的容器");
ShieldJS.HtmlAction.addHTMLEleAction("shield-timer-close", ".shield-timer-close", function(selectE, menuli, menu, searchForm, shieldDialog){
	var time = getTimeAttr(selectE, "time");
	var timer = setTimeout(function() {
		var closeTime = getTimeAttr(selectE, "closeTime", 2000); //关闭用时，默认2秒
		if (closeTime > 0) {
			//selectE.animate({height: '0px',opacity: 0}, closeTime);
			selectE.slideUp(closeTime);
			setTimeout(function() {
				selectE.remove();
			}, closeTime);
      } else { //-1立刻移除
      	selectE.remove();
      }
	}, time);
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
			var $this = $(this);
			if (!hasAttr($this, "time")) {
				dealEle = dealEle.not($this);
				ShieldJS.error("检测到shield-timer-close元素缺少time（xx时间后关闭）属性，请检查！"+$this.prop("outerHTML"));
				return false; //true=continue，false=break。 
			}
      });
	}
	return dealEle.length > 0 ? dealEle : false;
}, "div定时关闭");
ShieldJS.HtmlAction.addHTMLEleAction("shield-close", ".shield-close", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.click(function(){
			dealEle.closest(".shield-content").remove();
			//return false;
		});
	}
}, "div关闭");
//第三方插件
ShieldJS.HtmlAction.addHTMLEleAction("ddslickSelect", ".ddslickSelect", function (selectEs, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	selectEs.each(function(){
		var ddslickSelectE = $(this);
		var container = ddslickSelectE.closest(".ddslickSelectContainer");
		var options = { 
				width: 125,
				height:200,
				onSelected: function(selectedData){
					var defaultPhoto = container.find(".dd-selected-value").val();
					container.find(".ddslickSelectValue").val(defaultPhoto);
		}};
		options = getExtendSettings(ddslickSelectE, options); //data-xx可能会覆盖取到的值
		ddslickSelectE.ddslick(options)
	});
},  function (contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog) {
	var ddslickSelectE = findAndMarkEle(contentE, this.expression);
	var eleName = "ddslick第三方插件";
	if (ddslickSelectE.length > 0) {
		ddslickSelectE.each(function(){
			var ddslickSelectE = $(this);
			var container = ddslickSelectE.closest(".ddslickSelectContainer");
			if (container.length == 0) {
				ShieldJS.error(eleName + "未定义父类容器：请在父容器中添加class：ddslickSelectContainer");
				return false;
			} 
			if (container.find(".ddslickSelectValue").length == 0) {
				ShieldJS.error(eleName + "未定义选择后图片地址保存容器：请在对应的input容器上添加class：ddslickSelectValue");
				return false;
			} 
		});
		return ddslickSelectE;
	}
	return false;
}, "图片下拉选择(.ddslickSelectContainer .ddslickSelectValue .ddslickSelect)");
// 属性data-title,data-content,data-qtips-class,data-show-event,data-hide-event,data-titlebutton
ShieldJS.HtmlAction.addHTMLEleAction("qtips", ".qtips", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	findAndMarkEle(contentE, this.expression).each(function() {
		var $this = $(this);
		var qtipCls = $this.data("qtipsClass")||"blue";
	    var qtipsClassV = QTIPS_CLS.get(qtipCls)||qtipCls;
		var options = {};
		options.content = {
				text: $this.data("cont")
				, title: {
					text: $this.data("title"),
					button: ($this.data("titlebutton")=='true') || false
				}
		};
		options.style = {
			classes: qtipsClassV
		};
		options.events = {
			visible: function(event, api) {
				//api.reposition(event);
			}
			,move: function(event, api) {  
	           // alert( event.pageX + '/' + event.pageY );  
	        }  
		};
		options.position = {
			target: 'event'
			,my: 'top center' /*指向图片在qtips的位置*/
			,at: 'bottom center' /*指向图片在元素的位置*/
			,viewport: $(window)
			,adjust: {resize:false, screen: false}
		};
		options.show = {
			target: $this
			,effect: function(offset) {
				$(this).slideDown(150);
			}
		};
//		if ($this.data("showEvent")) { //显示的触发事件
//			options.show.event = $this.data("showEvent");
//		}
		options.hide = {
			target: $this
		};
//		if ($this.data("hideEvent")) { //隐藏的触发事件
//			options.hide.event = $this.data("hideEvent")
//		}
		options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
		$this.qtip(options);
	});
}, "qtips提示框");
/** 刷新图片 ，典型场景：点击按钮刷新图片
*	<%-- 图片容器 --%>
* 	<img class="signImgFcGainPersion" src="${fcGainPersionModel.gainimg}?20170103" width="135" height="170">
*	……
*	<%-- 样式shieldRefreshImg绑定该事件，增加refreshTo属性，指向需要刷新的图片 --%>		
*	<input type="button" title="点击该按钮刷新取证人签字图片" value="刷新图片" class="shieldRefreshImg button" refreshTo=".signImgFcGainPersion"/>
*/
ShieldJS.HtmlAction.addHTMLEleAction("shieldRefreshImg", ".shieldRefreshImg", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.unbind("click").click(function(){
			var $this = $(this);
			if (hasAttr($this,"refreshTo")) {
	            var refreshTo = getjQueryObj($this.attr("refreshTo"));
	            if (refreshTo.is("img")) {
	                var src = refreshTo.attr("src");
	                if (src.indexOf("randomparam")!=-1) {
	                    src = replaceParamVal(src,"randomparam", Math.random());
                    } else {
                    	if (src.indexOf("?")!=-1) {
                    		src +=  "&";
                    	} else {
                    		src +=  "?";
						}
                    	src +=  "randomparam=" + Math.random();
					}
	                refreshTo.attr("src",src);
                } else {
                	ShieldJS.alert("消息提示", "刷新的元素不是图片！", "error");
				}
            }else {
            	ShieldJS.alert("消息提示", "shieldRefreshImg图片刷新组件未定义刷新的图片元素：请添加refreshTo属性，关联刷新元素！", "error");
			}
			return false;
		});
	}
},"刷新图片操作");
/** 
* 无缓存图片	
*<%-- 样式shieldImgNoCache绑定该事件 --%>		
*	<img class="signImgFcGainPersion shieldImgNoCache" src="${fcGainPersionModel.gainimg }?20170103" width="135" height="170">
*/
ShieldJS.HtmlAction.addHTMLEleAction("shieldImgNoCache", ".shieldImgNoCache", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function(){
			var $this = $(this);
            if ($this.is("img")) {
            	var src = $this.attr("src");
            	if (hasAttr($this,"datasrc")) {
            		src = $this.attr("datasrc");
            	}
                if (src.indexOf("randomparam")!=-1) {
                    src = replaceParamVal(src,"randomparam", Math.random());
                } else {
                	if (src.indexOf("?")!=-1) {
                		src +=  "&";
                	} else {
                		src +=  "?";
					}
                	src +=  "randomparam=" + Math.random();
				}
                $this.attr("src",src);
            } else {
            	ShieldJS.alert("消息提示", "设定的元素不是图片！", "error");
			}
		});
	}
}, "无缓存图片");
/**
 * roundabout 3D层叠翻页效果
 * @field
 * 必需：
 *  第一个参数为自定义html动作处理的名字，注意与已有的是否冲突，如果冲突会用新的覆盖旧的
 *  第二个参数为js动作初始方法
 *  （均为jQuery对象）
 *  selectE:处理的jquery对象（checkFn中返回的对象），menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 *  第二个参数为验证html元素是否合法（如是否缺少相关联的父子容器或存储容器），不合法返回false，合法返回合格条件的jQuery对象
 *  （均为jQuery对象）
 *  contentE:处理的页面对象，menuli：左侧栏menu，menu：导航栏menu，searchForm：搜索表单，shieldDialog：弹出框
 */
ShieldJS.HtmlAction.addHTMLEleAction("roundabout", ".roundabout", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			var $this = $(this);
			var params= {};
			params = getParams($this,params);
			$this.roundabout(params).show();
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
},"roundabout 3D层叠翻页效果");
/**
 * shield-icon处理
 * @param selectE
 * @param menuli
 * @param menu
 * @param searchForm
 * @param shieldDialog
 * @returns
 */
ShieldJS.HtmlAction.addHTMLEleAction("shield-icon", ".shield-icon", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			var $this = $(this);
			var params= {};
			params = getParams($this, params);
			var hasTitle = hasAttr($this, "title");
			var iconTitle = $this.data("iconTitle");
			var parentTitle = $this.attr("title"); //本元素的title
			var title = "";
			iconTitle = iconTitle || parentTitle;
			if (!$this.html()) { //没有内容时处理
				if ($this.hasClass("show")) {
					title = iconTitle||"查看";
					$this.html('<img src="' + ctxPath + '/images/icon/view.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("update")) {
					title = iconTitle||"修改";
					$this.html('<img src="' + ctxPath + '/images/icon/modify.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("audit")) {
					title = iconTitle||"审核";
					$this.html('<img src="' + ctxPath + '/images/icon/audit.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("del")) {
					title = iconTitle||"删除";
					$this.html('<img src="' + ctxPath + '/images/icon/delete.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("freeze")) {
					title = iconTitle||"冻结";
	            	$this.html('<img src="' + ctxPath + '/images/icon/freeze.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("unfreeze")) {
					title = iconTitle||"解除冻结";
					$this.html('<img src="' + ctxPath + '/images/icon/unfreeze.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("disable")) {
					title = iconTitle||"停用";
					$this.html('<img src="' + ctxPath + '/images/icon/disable.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("enable")) {
					title = iconTitle||"启用";
					$this.html('<img src="' + ctxPath + '/images/icon/enable.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("add")) {
					title = iconTitle||"添加";
					$this.html('<img src="' + ctxPath + '/images/icon/add.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("add2")) {
					title = iconTitle||"添加";
					$this.html('<img src="' + ctxPath + '/images/icon/add2.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("notice")) {
					title = iconTitle||"配置通知";
					$this.html('<img src="' + ctxPath + '/images/icon/tixing.png" class="c_p" title="' + title + '">');
	            } else if ($this.hasClass("refresh")) {
					title = iconTitle||"刷新";
					$this.html('<img src="' + ctxPath + '/images/icon/update.png" class="c_p" title="' + title + '">');
	            }else if ($this.hasClass("start")) {
					title = iconTitle||"启用";
					$this.html('<img src="' + ctxPath + '/images/icon/start.png" class="c_p" title="' + title + '">');
	            }else if ($this.hasClass("stop")) {
					title = iconTitle||"停用";
					$this.html('<img src="' + ctxPath + '/images/icon/stop.png" class="c_p" title="' + title + '">');
	            }
            }
			
			if (!hasTitle && title) {
				$this.attr("title", title);
			}
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
},"shield-icon处理");
//时间差计算
ShieldJS.HtmlAction.addHTMLEleAction("timeDiffContainer", ".timeDiffContainer", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			$(this).kvTimeDiff({
				/** 开始时间 */
				timeStart:".timeStart"
				/** 注意是结束时间 */
				,timeEnd:".timeEnd"
				/** 输出比较结果 */
				,timeDiffShow:".timeDiffShow"
			})
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
},"时间差计算");
//用于给不同tr增加status状态样式
ShieldJS.HtmlAction.addHTMLEleAction("shield-tr-status-class", ".shield-tr-status-class", function(selectE, menuli, menu, searchForm, shieldDialog){
	if (selectE.length > 0) {
		selectE.each(function(){
			var tableE = $(this);
			tableE.before(
					'<div>'+
					'	<table class="shield-table">'+
					'		<colgroup>'+
					'			<col width="">'+
					'			<col width="">'+
					'			<col width="">'+
					'		</colgroup>'+
					'		<tr>'+
					'			<td class="shield-status-td">颜色标示说明：</td>'+
					'			<td class="progstatetr yellowtr">冻结</td>'+
					'			<td class="progstatetr graytr">停用</td>'+
					'		</tr>'+
					'	</table>'+
					'</div>');
			tableE.find("tr").each(function name() {
	            var trE = $(this);
	            if (hasAttr(trE, "status")) {
	            	var status = trE.attr("status");
	                if (status == -1) { //删除
	                	trE.addClass("");
                    } else if (status == 2) { //停用
                    	trE.addClass("graytr");
                    } else if (status == 3) { //冻结
                    	trE.addClass("yellowtr");
                    }
                }
            });
		});
	}
}, function (contentE, menuli, menu, searchForm, shieldDialog) {
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		return dealEle; //返回符合条件的jQuery对象
	}
	return false; //默认不符合条件
}, "用于给不同tr增加status状态样式");
ShieldJS.HtmlAction.addHTMLEleAction("shield-area-data", ".shield-area-data", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	
	var showAreaData = function(ele, url, parQhdm, callback) {
		if (ele.length > 0) {
			if (parQhdm) {
	            
				var params = {'pcode' : parQhdm};
				
				var paramNames = {id:"id", name:"name", parentCode:"parentCode"}; //默认参数配置
				
				var paramAdapter = ele.attr("paramAdapter"); // 参数适配器，如id=areaCode#name=areaName，意思是讲返回结果的areaCode转换为显示的id，将areaName转换为显示的name
				if (paramAdapter) {
					var paramAdapters = paramAdapter.split("#"); //#号分隔
					for (var i=0;i<paramAdapters.length;i++) {
						var paramName = paramAdapters[i];
						if (paramName.indexOf("=")!=-1) {
							var _paramNames = paramName.split("="); //如果key与提交不一致用=号分隔
							var paramsKey = _paramNames[0];
							var jsonResutlKey = _paramNames[1];
							paramNames[paramsKey] = jsonResutlKey;
						} else {
							paramNames[paramName] = paramName;
						}
					}
				}
				//获取数据
				if(url.indexOf("{}") != 0 && parQhdm){
					url = url.replace("{}", parQhdm);
				}
				var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector(ele, params); //参数选择器
				if (!checkParams) {
					return false;
				} else {
					params = checkParams;
				}
				params = getParams(ele, params);
				
				$.getJSON(url, params, function(json) {
					var html = '<option value="">请选择</option>';
					if (json) {
						for ( var i = 0; i < json.length; i++) {
							html += '<option value="' + json[i][paramNames.id] + '" parCode="'+ json[i][paramNames.parentCode] +'" ';
							if (json[i].selected) {
								html += ' selected="'+ json[i].selected +'" ';
							}
							if (json[i].disabled) {
								html += ' disabled="'+ json[i].disabled +'" ';
							}
							html += ' >';
							html += json[i][paramNames.name] + '</option>';
						}
						//alert(html)
					}
					ele.hide().html(html).show();
					if(ele.data("disabled")){
						ele.prop("disabled", true);
					}
					// 有值时触发下一个
					if(!ele.data("initareav")){
						setInputValue(ele, null, true, function(){ // 第一个参数元素，第二个设置的值，第三个是否触发change方法，第四个完成后的回调
							if(ele.is(":disabled")){
								var text = ele.find("option:selected").text();
								var areavalue = ele.find("option:selected").val();
								if((text =='市辖区' || text == '县') && areavalue.substr(4, 2)=='00'){
									text = '';
								}
								ele.before("<span>"+text+"</span>");
								ele.hide();
							}
						});
						ele.attr("data-initareav", "initareav");
					} else { 
						// showAreaData(ele.next("select"), url, ele.val()); //继续显示下级
						ele.triggerHandler("change"); //触发自定义change事件
					}
					
					var readE = ele.find("option[read]");
					if (readE.length > 0) {
						ele.before("<span>" + readE.text() + "</span>");
						ele.hide();
					}
					if (callback && $.isFunction(callback)) {
						callback(ele, url, parQhdm);
					}
				});
            } else {
            	var html = '<option value="">请选择</option>';
                ele.hide().html(html).show();
                showAreaData(ele.next("select"), url, ele.val()); //继续往后设置
            }
        }
    };
	
	if (dealEle.length > 0) {
		try  { //项目路径
			ctxPath;
		} catch (e) {
			ctxPath = "";
		}
		dealEle.each(function() {
			var areaE = $(this); // 容器
			var url = areaE.data("url")||(ctxPath+"/area/get");
			var pid = areaE.data("pid")||"0"; //第一级的pid
			var areaSelectEs = areaE.find('select'); // 查找内部的select
			var value = areaE.attr("v")||areaE.data("v")||0;
			var limitV = areaE.data("limit")||0;
			var disabled = false; //不可用，只展示
			if (hasAttr(areaE, "disabled")) {
				disabled = (areaE.attr("disabled")!="false");
            }
			
			if (areaSelectEs.length > 0) {
				var valuesInit = []; //村、社区
				var valuesLimit = []; //限定区域
				if(value || limitV){
					var valueCode = value+"";
					var limitVCode = limitV+"";
					
					// 限定区域
					var limitprovince = limitVCode.substr(0, 2); //省
					var limitprovinceCode; //省 完整编码
					if (limitprovince && limitprovince>0) {
						limitprovinceCode = limitprovince+"0000";
					}
					var limitcity = limitVCode.substr(2, 2); //市
					var limitcityCode; //市 完整编码
					if (limitcity && limitcity>0) {
						limitcityCode = limitprovince+limitcity+"00";
					}
					var limitcounty = limitVCode.substr(4, 2); //区、县
					var limitcountyCode; //区、县 完整编码
					if (limitcounty && limitcounty>0) {
						limitcountyCode = limitprovince+limitcity+limitcounty+"";
					}
					var limittown = limitVCode.substr(6, 3); //乡、镇、街道
					var limittownCode; //乡、镇、街道 完整编码
					if (limittown && limittown>0) {
						limittownCode = limitprovince+limitcity+limitcounty+"000";
					}
					var limitvillage = limitVCode.substr(9, 3); //村、社区
					var limitvillageCode; //村、社区 完整编码
					if (limitvillage && limitvillage>0) {
						limitvillageCode = limitprovince+limitcity+limitcounty+limitvillage+"";
					}
					
					if (limitprovinceCode) {
						valuesLimit.push(limitprovinceCode);
                    }
					if (limitcityCode) {
						valuesLimit.push(limitcityCode);
                    }
					if (limitcountyCode) {
						valuesLimit.push(limitcountyCode);
                    }
					if (limittownCode) {
						valuesLimit.push(limittownCode);
                    }
					if (limitvillageCode) {
						valuesLimit.push(limitvillageCode);
                    }
					
					// 默认值
					var province = valueCode.substr(0, 2); //省
					var provinceCode; //省 完整编码
					if (province && province>0) {
						provinceCode = province+"0000";
					}
					var city = valueCode.substr(2, 2); //市
					var cityCode; //市 完整编码
					if (city && city>0) {
						cityCode = province+city+"00";
					}
					var county = valueCode.substr(4, 2); //区、县
					var countyCode; //区、县 完整编码
					if (county && county>0) {
						countyCode = province+city+county+"";
					}
					var town = valueCode.substr(6, 3); //乡、镇、街道
					var townCode; //乡、镇、街道 完整编码
					if (town && town>0) {
						townCode = province+city+county+"000";
					}
					var village = valueCode.substr(9, 3); //村、社区
					var villageCode; //村、社区 完整编码
					if (village && village>0) {
						villageCode = province+city+county+village+"";
					}
					
					if (province > 0 && limitprovince>0 && province!=limitprovince) { //省不相同，则后面的限制无效
						provinceCode = limitprovinceCode;
						cityCode = null;
						countyCode = null;
						townCode = null;
						villageCode = null;
                    }
					if (city > 0 && limitcity>0 && city!=limitcity) { //市不相同，则后面的限制无效
						cityCode = limitcityCode;
						countyCode = null;
						townCode = null;
						villageCode = null;
                    }
					if (county > 0 && limitcounty>0 && county!=limitcounty) { //区、县不相同，则后面的限制无效
						countyCode = limitcountyCode;
						townCode = null;
						villageCode = null;
                    }
					if (town > 0 && limittown>0 && town!=limittown) { //乡、镇、街道不相同，则后面的限制无效
						townCode = limittownCode;
						villageCode = null;
                    }
					if (village > 0 && limitvillage>0 && village!=limitvillage) { //村、社区不相同，则后面的限制无效
						villageCode = limitvillageCode;
                    }
					
					if (provinceCode) {
						valuesInit.push(provinceCode);
                    }
					if (cityCode) {
						valuesInit.push(cityCode);
                    }
					if (countyCode) {
						valuesInit.push(countyCode);
                    }
					if (townCode) {
						valuesInit.push(townCode);
                    }
					if (villageCode) {
						valuesInit.push(villageCode);
                    }
				}
				var ele = areaSelectEs.eq(0); //省
				var parQhdm = pid;
				
				areaSelectEs.each(function(i, n) {
					var $this = $(n);
					$this.attr("data-v", valuesInit[i]);
					$this.attr("data-disabled", disabled ? disabled : (valuesLimit[i]?true:false));
					if (!$this.html()) { // 增加默认option
						$this.html('<option value="">请选择</option>');
                    }
					$this.change(function() {
						var t = $(this);
						var regionDm = t.val();
						showAreaData(t.next("select"), url, regionDm, null); //获取下一级地区的显示
						// 如果选中的是空值，则取上一级的编码和名称显示，相当于取消当前级别地区
						while (!regionDm && t.prev("select").length > 0) {
							t = t.prev();
							regionDm = t.val();
						}
						// 将值赋给input
						areaE.find('.regionDm').val(regionDm).triggerHandler("change"); //触发自定义change事件
						var areaText = "";
						areaE.find("option:selected").each(function() {
							areaText += $(this).text();
	                    });
						areaText = areaText.replace(/请选择/g,"")
						areaE.find('.regionMc').val(areaText).triggerHandler("change"); //触发自定义change事件
					});
                });
				
				showAreaData(ele, url, parQhdm, value); // 初始化一波
			}
        });
	}
}, "地区数据获取（select）");
//表格间行变色
ShieldJS.HtmlAction.addHTMLEleAction("shield-table-odd", ".shield-table-odd", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle.each(function() {
		var $this = $(this);
		$('tr:odd', $this).addClass("listodd");//间行变色
    });
},"表格间行变色");
//百度编辑器ueditor
var ueditors = {};
ShieldJS.HtmlAction.ueditor = {
	// 简洁版
	simpleTheme : {
		toolbars:[[ 'bold', 'italic', 'underline', 'fontborder', '|', 'forecolor', 'backcolor']]
	},
	// 上传版
	uploadTheme : {
		toolbars:[[ 'bold', 'italic', 'underline', 'fontborder', '|', 'forecolor','backcolor','simpleupload', '|', 'insertimage', 'emotion','attachment']]
	}
}

ShieldJS.HtmlAction.addHTMLEleAction("ueditor", "textarea.ueditor,script.ueditor", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle.each(function() {
		var $this = $(this);
		var edId = $this.attr("id");
		$this.attr("ueditorinit","true");
		$this.attr("ueditorid", edId);
		//var index = $("#"+edId).index($this);
		//edId += index;
        if (ueditors[edId]) {
        	removeUeditor(edId);
        }
        var options ={};
        var dataTheme = $this.data("ueditorTheme"); //data定义的主题
        if (dataTheme) {
        	if (dataTheme == "simple") {
            	$.extend(true, options, ShieldJS.HtmlAction.ueditor.simpleTheme);
        	} else if (dataTheme == "upload") {
            	$.extend(true, options, ShieldJS.HtmlAction.ueditor.uploadTheme);
        	}
    	}
    	options = getExtendSettings($this, options);
        var ueditor = UE.getEditor(edId, options);
        ueditors[edId] = ueditor;
    });
},"百度编辑器ueditor的处理");
// 移除百度编辑器
var removeUeditor = function(edId){
	UE.delEditor(edId);
	delete ueditors[edId];
}
ShieldJS.HtmlAction.addHTMLEleAction("shield-strength", ".shield-strength", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	dealEle.each(function() {
		var $this = $(this);
		var edId = $this.attr("id");
		var html ='<div id="level" class="pw-strength">'+
		   	'<div class="pw-bar"></div>'+
		   	'<div class="pw-bar-on"></div>'+
		   	'<div class="pw-txt">'+
			'	<span>弱</span>'+
			'	<span>中</span>'+
			'	<span>强</span>'+
		   	'</div>'+ 
		'</div>';
		$this.after(html);
		var nextE = $this.next();
		$this.keyup(function() {
			// i -- 执行对大小写不敏感的匹配  g -- 执行全局匹配(查找所有匹配而非找到第一个匹配后停止) m -- 执行多行匹配
			// ?=n 正向匹配
			var strongRegex = /^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*\W).*$/g;
		  	var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*\d))|((?=.*[a-z])(?=.*\d))).*$", "g");
		  	var enoughRegex = new RegExp("(?=.{6,}).*", "g");
		  	
		  	var thisVal = $(this).val();
		  	if (strongRegex.test(thisVal)) {
		  		//密码为八位及以上并且字母数字特殊字符三项都包括,强度最强 
		  		nextE.removeClass('pw-weak');
		  		nextE.removeClass('pw-medium');
		   		nextE.removeClass('pw-strong');
		   		nextE.addClass('pw-strong'); 
		  	} else if (mediumRegex.test(thisVal)) {
		  		//密码为七位及以上并且字母、数字、特殊字符三项中有两项，强度是中等 
		   		nextE.removeClass('pw-weak'); 
		   		nextE.removeClass('pw-medium'); 
		   		nextE.removeClass('pw-strong'); 
		   		nextE.addClass('pw-medium'); 
		  	} else if (enoughRegex.test(thisVal)) {
		  		//如果密码为6为及以下，就算字母、数字、特殊字符三项都包括，强度也是弱的
		   		nextE.removeClass('pw-weak');
		  		nextE.removeClass('pw-medium');
		   		nextE.removeClass('pw-strong');
		   		nextE.addClass('pw-weak');
		  	} else {
		  		//密码小于六位的时候，密码强度图片都为灰色 
		   		nextE.removeClass('pw-weak');
		   		nextE.removeClass('pw-medium');
		   		nextE.removeClass('pw-strong');
		   		nextE.addClass('pw-defule');
		  	}
		  	return true;
		});
    });
},"密码强度验证工具");
//fullCalendar 参考：https://blog.csdn.net/ymnets/article/details/78661247
ShieldJS.HtmlAction.addHTMLEleAction("fullCalendar", ".fullCalendar", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	findAndMarkEle(contentE, this.expression).each(function() {
		var $this = $(this);
		var options = {};
		if (!jQuery().fullCalendar) {
			ShieldJS.error("检测到fullCalendar元素缺少依赖JS库：fullcalendar.js！");
            return;
        }

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var h = {};
        /**  
         *  支持的视图
         *  today       今天 
         *  month       一页显示一月, 日历样式  
            basicWeek   一页显示一周, 无特殊样式  
            basicDay    一页显示一天, 无特殊样式  
            agendaWeek  一页显示一周, 显示详细的24小时表格  
            agendaDay   一页显示一天, 显示详细的24小时表格  
            listYear    年列表（V3.*支持）  
            listMonth   月列表（V3.*支持）  
            listWeek    周列表（V3.*支持）  
            listDay     日列表（V3.*支持）  
         */ 
        if ($this.width() <= 400) {
        	$this.addClass("mobile");
            h = {
                left: 'prev, next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            };
        } else {
        	$this.removeClass("mobile");
            h = {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            };
        }
        if (!$this.data("url")) {
			ShieldJS.error("检测到fullCalendar元素未定义url，请检查！");
            return;
        }
        var updateEvent = function(url, params, title) {
        	ShieldJS.openDialog(url, params, function(ele, dialog) { //此处可优化，内部方法可默认实现
    			ShieldJS.form.initSubmit(ele, function(formE, targetE, data) {//成功后
					//dialog.remove();
					ShieldJS.HtmlAction.execSuccessDoMethod("shieldRefresh", ShieldJS.contentMainE, searchForm, menuliSelect, dialog);
				});
    			//ShieldJS.core.bindHtmlAction(ele, null, null, null, dialog); //内部继续加载，已经默认实现
            }, title);
        }
        var delEvent = function(url, params) {
			ShieldJS.HtmlAction.postData(url, params, contentE, searchForm, menuliSelect, shieldDialog, "shieldRefresh", ShieldJS.HtmlAction.clickCallback($this));
        }
        var _default = {
        	firstDay : 1 //第一天为周一
        	,droppable: true
            ,header: h
            ,fixedWeekCount:false //月视图时显示的周数，默认true。true每次固定显示6周，false则每月根据实际周数显示4，5，6周。
            ,editable: true
            ,isRTL : false //从右到左显示模式，默认false
            ,eventOrder : "title" //多个相同的日程数据排序方式，String/Array/Function, 默认值: "title" 
            ,eventLimit: true // allow "more" link when too many events
            // 使用function动态获取数据，start为开始时间，end为结束时间，这个不需要我们来设置，日历自动获取
            ,events : function (start, end, timezone, callback) {
    	      	console.log('ajax get from:' + start + ' to:' + end);
    			if ($this.data("url")) {
    				ShieldJS.ajax.getJSON($this.data("url"), {start:$.fullCalendar.formatDate(start,'YYYY-MM-DD'), end:$.fullCalendar.formatDate(end,'YYYY-MM-DD')}, function(json) {
    					if (json.data && json.data.length > 0) {
    						var events = [];
			                $.each(json.data, function(i, n){
			                	events.push(n);
			                });
    						// 内置方法，它可以将刚刚获取到的events数据渲染到日历中
    			            callback(events);

    			            // 张和祥20200713添加，用于日历加载完后的回调函数
							if ($this.data("eventsCallback") && $.isFunction(eval($this.data("eventsCallback")))) {
								eval($this.data("eventsCallback"))();
							}
    					}
    				});
                }
//
//                url: $this.data("url"),
//                error: function(){
//                    ShieldJS.alert("错误提示", "日历事件加载失败！", "error");
//                }
            }
        	,eventRender: function(event, element) { //当Event对象开始渲染时触发
	        	if (jQuery().qtip) { //鼠标悬停
	        		element.qtip({
	        			content: {
	        				text: '开始时间：'+$.fullCalendar.moment(event.start).format('YYYY年M月D日(HH时)(mm分)')
	        				+(event.end?"<br/>结束时间："+$.fullCalendar.moment(event.end).format('YYYY年M月D日(HH时)(mm分)'):"")
	        				+(event.content?"<br/>详　　情："+event.content:"")
	        				+(event.url?"<br/>链接地址："+event.url:"")
	        				, title: {
	        					text: ""+event.title
	        				}
	        			}
	        			,style: {
	        				classes: 'qtip-blue qtip-shadow qtip-rounded'     		
	        			}
	        			,position: {
	        				my: 'top center',
	        				at: 'bottom center'
	        			}
	        		}).contextMenu('myMenu', {
				    	onContextMenu: function(e) {	
				    		// 是否显示
				        	// if ($(e.target).hasClass('bluebg')) return true;
				    		return true;
				      	},
				      	onShowMenu: function(e, menu) {
				      		// alert(event.ucid);
				      		if (!event.repeatType || event.repeatType=="0") { //非重复事件不显示“删除全部”按钮
				      			$('#calanderinfodeleAll', menu).remove();
                            }
				      		// $('#split', menu).remove();//选中多项只能合并
				        	return menu;
				      	},
				      	bindings: {
			        		'calanderinfoedit': function(t) { // 编辑	
			        			updateEvent($this.data("urlUpd"), {cid:event.id}, "修改日历事件");
			        		},
			        		'calanderinfodele': function(t) { // 删除
			        			var thisE = $(this);
			        			ShieldJS.confirm("删除", "确定删除该事件吗？", function() {
			        				delEvent($this.data("urlDel"), {oid:event.oid});
                                });
			        		},
			        		'calanderinfodeleAll': function(t) { // 删除
			        			var thisE = $(this);
			        			ShieldJS.confirm("删除", "确定删除该事件的全部重复事件吗？", function() {
			        				delEvent($this.data("urlDel"), {cid:event.id});
                                });
			        		}
			  			}		
				    });
                }
		    }
            ,eventClick : function(calEvent, jsEvent, view) { //日历事件区块，单击时触发  
		    	//alert(calEvent.ucid);
		    	if(!calEvent.url && $this.data("urlUpd")){
		    		updateEvent($this.data("urlUpd"), {cid:calEvent.id}, "修改日历事件");
		    	}
		    }
		    ,dayClick : function(dayDate, allDay, jsEvent, view){ //空白的日期区，单击时触发  
		    	ShieldJS.debug(dayDate);
		    	if($this.data("urlUpd")){
		    		updateEvent($this.data("urlUpd"), {start : $.fullCalendar.formatDate(dayDate,'YYYY-MM-DD')}, "添加日历事件");
		    	}
		    }
		    ,eventResize : function(calEvent, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) { //日程大小调整完成并已经执行更新时触发
		    	ShieldJS.debug("eventResize:"+dayDelta)
		    }
		    ,eventDrop: function(calEvent, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) { //日程拖拽停止并且已经拖拽到其它位置了
		    	ShieldJS.debug("eventDrop:"+dayDelta)
		    }
		    ,eventMouseover: function(calEvent, jsEvent, view){//鼠标在日程区块上时触发  
            }
		    ,eventMouseout: function(calEvent, jsEvent, view){//鼠标从日程区块离开时触发  
            }
        }
        var options = getExtendSettings($this, _default);
        $this.fullCalendar('destroy'); // 销毁旧实例
        $this.fullCalendar(options); //添加新实例，测试，静态数据待修改
	});
},"fullCalendar日历插件处理");
// 拖拽
ShieldJS.HtmlAction.addHTMLEleAction("draggable", ".draggable", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if (!jQuery().draggable) {
			ShieldJS.error("检测到draggable元素缺少依赖JS库：jquery-ui.js！");
			return;
		}
		dealEle.each(function() {
	        var $this = $(this);
			var eventObject = {
					title: $.trim($this.text()) // use the element's text as the event title
			};
			// store the Event Object in the DOM element so we can get to it later
			$this.data('eventObject', eventObject);
			// make the event draggable using jQuery UI
			var options = getExtendSettings($this, {});
			$this.draggable(options);
        })
	}
}, "拖拽插件");
//拖拽
ShieldJS.HtmlAction.addHTMLEleAction("shieldDatetime", ".shieldDatetime", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if(typeof moment == "undefined"){ //判断lunar是否已经定义
			ShieldJS.error("检测到shieldDatetime元素缺少依赖JS库：moment.js！");
			return;
		}
		dealEle.each(function() {
			var $this = $(this);
			var datetime = $this.text();
			if (datetime) {
				var title = $this.data("title");
				if (!title) {
					$this.attr("title", title);
                }
				$this.data("olddata", datetime);
				var receivedDate = moment(datetime); //日期
				var formatVal = $this.data("momentFmt") || $this.data("format"); //后面的参数是为了防止前面参数冲突时;
				if (!formatVal) {
					formatVal = "YYYY-MM-DD HH:mm:ss";
                }
				// var diffDays = receivedDate.diff(moment(), 'days'); //不到24小时的为0
				var messageHtml = receivedDate.format(formatVal);
//				if (diffDays == 0) {
//				}
				var showtime= ($this.data("showtime")+""!="false");
				if(!showtime){
					messageHtml = "";
				}
				var fromnow= $this.data("fromnow")||"show";
				if ((fromnow != "none" && fromnow != "hide" && fromnow != "close") || !showtime) { //时间 和距现在的实际必显示一个
					if(showtime){
						messageHtml += ' ('
					}
					messageHtml += receivedDate.fromNow();
					if(showtime){
						messageHtml += ')';
					}
                }
				$this.text(messageHtml);
            }
        })
	}
}, "时间格式化显示（依赖moment.js）");
//通用操作
ShieldJS.HtmlAction.addHTMLEleAction("shield-common-init", function(contentE, menuli, menu, searchForm, shieldDialog){
	var tableEles = findAndMarkEle(contentE, "table");
	tableEles.each(function() {
	    var tableEle = $(this);
	    //增加title,排除父类含有class的noTableTitle_plugin
		var noTableTitlePare = tableEle.closest(".noTableTitle_plugin");
		if (noTableTitlePare.length === 0) {
			if (!tableEle.data("notitle")) {
				var tds = tableEle.find("td");
				tds.each(function(){
					var $this = $(this);
					if (!$this.data("notitle")) {
						var titleOld = $this.attr("title");
						if (!titleOld) {
							var titleV = $this.text();
							//titleV = titleV.replace(/\s*/g,"");
							$this.attr("title", titleV); 
						}
					}
				});
			}
		}
	});
	var textareaVEles = findAndMarkEle(contentE, "textarea[data-v]");
	textareaVEles.each(function() {
	    var textareaVEle = $(this);
	    var v = textareaVEle.data("v");
	    textareaVEle.html(v);
	})
}, "通用操作");
//================= tab插件 ===========================
ShieldJS.HtmlAction.addHTMLEleAction("multiTab", ".multi-tab_plugin", function(contentE, menuli, menu, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if (!jQuery().multiTab) {
			ShieldJS.error("检测到multi-tab_plugin元素缺少依赖JS库：jquery.multiTab.js！");
			return;
		}
		
		dealEle.each(function() {
	        var $this = $(this);
	        var options = {};
	        options = getExtendSettings($this, options);
	        $this.multiTab(options);
	        //$this.multiTabClose($this.find(".title").eq(2));
		});
	}
},"多页签插件");
// ================= 追加到tr后面
ShieldJS.HtmlAction.addHTMLEleAction("shieldAppendTr_plugin", ".shieldAppendTr_plugin", function(contentE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		dealEle.each(function() {
	        var $this = $(this);
	        $this.click(function(){
		        var url = $this.data("url");
		        
		        var targetEle = $(".shieldAppendTr");
		        if(targetEle.length == 0){
		        	targetEle = $('<tr class="shieldAppendTr"><td colspan="' + $this.closest("tr").children("td").length + '"></td></tr>');
		        }
		        $this.closest("tr").after(targetEle);
		        targetEle = targetEle.children("td");
				
	        	if (url) {
					var params = {};
					var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
					if (!checkParams) {
						return false;
					} else {
						params = checkParams;
					}
					params = getParams($this,params);
					var method = $this.data('method') || "get";
					
	        		if (method == "post") {
	        			ShieldJS.ajax.post(targetEle, url, params, function(data) {
	        				ShieldJS.core.bindHtmlAction(targetEle); //内部继续加载
	                    }, function(data) {
	                    }, null);
					} else {
						ShieldJS.ajax.get(targetEle, url, params, function(data) {
							ShieldJS.core.bindHtmlAction(targetEle); //内部继续加载
		                }, function(data) {
		                }, null);
					}
	            } 
	        });
      });
	}
},"追加到tr后面");
