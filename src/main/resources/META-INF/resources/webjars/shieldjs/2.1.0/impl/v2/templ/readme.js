//doT.js模板引擎
//常用语法

//{{ }}     for evaluation 模板标记符
//{{= }}    for interpolation 输出显示，默认变量名叫it
//{{! }}    for interpolation with encoding html标签转义后输出显示
//{{? }}    for conditionals 条件分支，if条件的简写
//{{~ }}    for array iteration 遍历数组
//{{# }}    for compile-time evaluation/includes and partials
//{{## #}}  for compile-time defines

//dot.js调用方式：
//var tmpText = doT.template(模板);
//tmpText(数据源);
    
//使用步骤
//数据源  ： json
//模板 ：   数据显示的格式
//区域 ：   数据显示的位置
//调用方试 ： 填充数据

// 参考：https://www.jianshu.com/p/f671201df514 https://jingyan.baidu.com/article/359911f573881657fe0306fe.html

//示例如下
// 页面定义
	<div>
	    ------赋值------
	</div>
	<div id="interpolation"></div>
	<div>
	    ------循环------
	</div>
	<div id="evaluation"></div>
	<div>
	    ------数组------
	</div>
	<div id="arraysation"></div>
	<div>
	    ------条件------
	</div>
	<div id="condition"></div>
// 模板定义
	<!--赋值-->
	<script id="interpolationtmpl" type="text/x-dot-template">
	    <div>{{=it.name}}</div>
	    <div>dot框架</div>
	</script>
	<!--循环map-->
	<script id="evaluationtmpl" type="text/x-dot-template">
	    {{ for(var prop in it) { }}
	    <div>键:{{= prop }}对应的值:{{= it[prop] }}</div>
	    {{ } }}
	</script>
	<!--数组-->
	<script id="arraystmpl" type="text/x-dot-template">
	    {{~it.interests:value:index}}
	    <div>下标：{{= index }}对应的值：{{= value }}!</div>
	    {{~}}
	</script>
	<!--条件-->
	<script id="conditionstmpl" type="text/x-dot-template">
	    {{? it.name }}
	    <div>name存在时走到这里, {{=it.name}}!</div>
	    {{?? !it.age === 0}}
	    <div>age等于零时走到这里!</div>
	    {{??}}
	    You are {{=it.age}}
	    {{?}}
	</script>
//在js中调用模板
<script>    
	/**
	 *  数据源  赋值
	 */
	var dataInter = {
	    "name" : "shuangqi1991",
	    "age" : 88
	};
	//调用方式
	var interText = doT.template($('#interpolationtmpl').html())(dataInter);
	$('#interpolation').html(interText);
	/**
	 * 数据源 map
	 */
	var dataEval = {
	    "name" : "shuangqi1991",
	    "age" : 88,
	    "interests" : ["basketball", "hockey", "photography"],
	    "contact" : {
	        "email" : "shuangqi1991@qq.com",
	        "phone" : "999999999"
	    }
	};
	//调用方式
	var evalText = doT.template($('#evaluationtmpl').html())(dataEval);
	$('#evaluation').html(evalText);
	/**
	 *  数据源 数组
	 */
	var arrEval = {
	    "interests" : ["basketball", "hockey", "photography"]
	};
	//调用方式
	var arrText = doT.template($('#arraystmpl').html())(arrEval);
	$('#arraysation').html(arrText);
	/**
	 *  数据源 条件
	 */
	var dataCondition = {
	    "name" : "ss",
	    "age" : 2
	};

	//调用方式
	var conText = doT.template($('#conditionstmpl').html())(dataCondition);
	$('#condition').html(conText);
</script>

//结果
<div>
------赋值------
</div>
<div id="interpolation"> <div>shuangqi1991</div> <div>dot框架</div> </div>
<div>
------循环------
</div>
<div id="evaluation">
	<div>键:name对应的值:shuangqi1991</div>
	<div>键:age对应的值:88</div>
	<div>键:interests对应的值:basketball,hockey,photography</div>
	<div>键:contact对应的值:[object Object]</div>
</div>
<div>
------数组------
</div>
<div id="arraysation">
	<div>下标：0对应的值：basketball!</div>
	<div>下标：1对应的值：hockey!</div>
	<div>下标：2对应的值：photography!</div>
</div>
<div>
------条件------
</div>
<div id="condition">
	<div>name存在时走到这里, ss!</div>
</div>