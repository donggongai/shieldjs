/**
* ShieldJSCheckbox
* @fileOverview 复选框包含开关，需要依赖shield.main.js以及tool/doT.js，templ/checkbox.html
* @author kevin
* @email donggongai@126.com
* @version 1.0
* @date 2018-3-16
*/
;(function($){
	/**
	* @author kevin
	* @constructor ShieldJSCheckbox
	* @description 菜单样式
	* @see The <a href="#">kevin</a>
	* @example
	*	$(".nav").ShieldJSCheckbox();
	* @since version 1.0
	*/
	$.fn.ShieldJSCheckbox = function(options){
		debug(this);
		if (this.length > 0) {
			/**
			 * @description {Json} 设置参数选项
			 * @field
			 */
			var options = $.extend({}, $.fn.ShieldJSCheckbox.defaults, options);
			/**
			 * @description 处理函数
			 */
			return this.each(function() {
				var thisSelectE = $(this);
				
				var templid = thisSelectE.attr("templid")||'default'; //模板id
				var eleClass="";
				var templData = {}; //模板数据
				templData.skin = "";
				var replaceHtml = '<div class="shield-unselect" ></div>';
				//var shieldCheckboxEle = thisSelectE.next();
				var skin = "check";
				if (hasAttr(thisSelectE, "shd-skin")) {
					var skin = thisSelectE.attr("shd-skin");
					templData.skin = skin;
					//shieldCheckboxEle.attr("shd-skin", skin);
				}
				var checkedClass;
				var title = thisSelectE.attr("title");
				if (!title) {
					title = "";
		        }
				templData.name = title;
				var titleOn = title;
				var titleOff = title;
				if (skin == 'switch') { //开关
					eleClass += "shield-form-switch";
					//shieldCheckboxEle.addClass("shield-form-switch");
					checkedClass = "shield-form-onswitch";
					if (title.indexOf("|") != -1) {
						var titleArr = title.split("|");
						titleOn = titleArr[0];
						titleOff = titleArr[1];
		            }
					templData.name = titleOff;
					//shieldCheckboxEle.html('<em>'+ titleOff +'</em><i class="shield-icon"></i>');
				} else { //普通checkbox
					eleClass += "shield-form-checkbox";
					//shieldCheckboxEle.addClass("shield-form-checkbox");
					checkedClass = "shield-form-checked";
					//shieldCheckboxEle.html('<span>'+ title +'</span><i class="shield-icon">&#xe6ad;</i>');
				}
				if (hasAttr(thisSelectE, "disabled")) { //样式追加
					eleClass += " shield-checkbox-disabled "+ShieldJS.css.DISABLED;
					//shieldCheckboxEle.addClass("shield-checkbox-disabled");
					//shieldCheckboxEle.addClass(ShieldJS.css.DISABLED);
				}
				templData.classes = eleClass;
				//调用模板
				var conText = "";
				if (skin == 'switch') { //开关
					conText = doT.template(ShieldJS.templ.SWITCH[templid])(templData);
				} else {
					conText = doT.template(ShieldJS.templ.CHECKBOX[templid])(templData);
				}
				ShieldJS.debug(conText);
				thisSelectE.after(conText);
				var shieldCheckboxEle = thisSelectE.next();
				//事件绑定
				shieldCheckboxEle.on("click", function() {
					if (!shieldCheckboxEle.hasClass(ShieldJS.css.DISABLED)) {
						var $this = $(this);
						$this.toggleClass(checkedClass);
						if (skin == 'switch') { //开关
							if ($this.hasClass(checkedClass)) {
								$this.find("em").text(titleOn);
							} else {
								$this.find("em").text(titleOff);
							}
						}
						thisSelectE.prop("checked", $this.hasClass(checkedClass)).triggerHandler("change",["false"]); //真实元素赋值,并触发绑定的change事件，false表示不再执行选中元素的点击防止死循环
						ShieldJS.debug("checkbox选中=="+thisSelectE.prop("checked"));
					}
					return false; //阻止冒泡
				});
				if (hasAttr(thisSelectE, "checked")) { //选中处理
					shieldCheckboxEle.click();
				}
				thisSelectE.hide(); //隐藏原始元素
				thisSelectE.change(function(event, changeShieldValue) { //值变化值将其赋值给处理元素
					changeShieldValue = changeShieldValue||"true";
					if (changeShieldValue=="true") { //防止死循环
						var $this = $(this);
						if ($this.prop("checked") != shieldCheckboxEle.hasClass(checkedClass)) { //选中处理
							shieldCheckboxEle.click();
						}
                    }
                });
			});
        }
	};
	/**
	* @description {Json} 默认参数
	* @field
	*/
	$.fn.ShieldJSCheckbox.defaults = {
		// 鼠标悬浮的菜单样式
		barClass : 'shield-nav-bar'
		// 条目样式
	    ,itemClass : 'shield-nav-item'
	    // 竖向样式
	    ,treeClass : 'shield-nav-tree'
	    // 竖向选中样式
		,treeItemdClass : 'shield-nav-itemed'
		// 展开收起样式，存在子菜单
		,moreClass : 'shield-nav-more'
		// 展开收起样式,显示子菜单时出现
		,moredClass : 'shield-nav-mored'
		// 子菜单样式
		,childClass : 'shield-nav-child'
		// icon图标样式
		,iconClass : 'shield-icon'
	};
	$.fn.ShieldJSCheckbox.filter = function(dealEle) {
		var showName = "shield-checkbox元素";
		if (dealEle.length > 0) {
			dealEle.each(function() {
				var $this = $(this);
				if (!$this.is(":checkbox")) {
					dealEle = dealEle.not($this);
					ShieldJS.error("检测到" + showName + "不是checkbox表单元素，请修改！"+$this.prop("outerHTML"));
					return false; //true=continue，false=break。 
				}
	        });
		}
		return dealEle;
	}
	/**
	* @description 输出选中对象的个数到控制台
	* @param {jQueryObject} $obj 选中对象
	*/
	function debug($obj) {
		if (window.console && window.console.log)
			window.console.log('ShieldJSPage selection count: ' + $obj.size());
	};
})(jQuery);