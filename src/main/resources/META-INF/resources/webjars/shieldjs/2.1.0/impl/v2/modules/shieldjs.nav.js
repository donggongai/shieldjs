/**
* ShieldJSNav
* @fileOverview 高亮显示
* @author kevin
* @email donggongai@126.com
* @version 1.0
* @date 2018-3-16
*/
;(function($){
	/**
	* @author kevin
	* @constructor ShieldJSNav
	* @description 菜单样式
	* @see The <a href="#">kevin</a>
	* @example
	*	$(".nav").ShieldJSNav();
	*	其中竖向默认选中不选中为实心圆和空心圆，可自定义，通过在<i class="shield-icon" switch="&#xe654;,&#xe655;"></i>定义switch属性，用“,”分隔未选中和选中效果，第一项为未选中图标，第二项为选中图标
	* @since version 1.0
	*/
	$.fn.ShieldJSNav = function(options){
		debug(this);
		if (this.length > 0) {
			/**
			 * @description {Json} 设置参数选项
			 * @field
			 */
			var options = $.extend({},$.fn.ShieldJSNav.defaults, options);
			/**
			 * @description 处理函数
			 */
			return this.each(function() {
				var navEle = $(this); //整个nav元素
				//使用了metadata插件获取元数据
				var o = $.metadata ? $.extend({}, options, navEle.metadata()) : options;
				
				navEle.append('<span class="' + o.barClass + '"></span>');
				var timer = {};
				// 竖向特殊处理
				navEle.children("." + o.itemClass).each(function() {
					var $this = $(this);
					var parE = navEle;
					if (parE.hasClass(o.treeClass)) { //处理竖向
						var showOrHideE = $this.find("." + o.moreClass); //展开收起
						if (showOrHideE.length>0) {
							showOrHideE.closest("a").click(function() {
								$(this).closest("li").toggleClass(o.treeItemdClass);
							});
						}
						//有子菜单显示ShieldJS.css.SHOW
						var childEle = findAndMarkEle($this, "." + o.childClass);
						if (childEle.length >0) {
							childEle.find("dd").each(function() {
								var chiledDdEle = $(this);
								var chiledTitleAEle =  chiledDdEle.find("a");
								if (chiledTitleAEle.length == 0) {
									ShieldJS.error("检测到." + o.childClass + "的dd元素缺少a元素不能继续处理，请检查！"+chiledDdEle.prop("outerHTML"));
									return false;
								}
								var iconSelected = "&#xe655;"; //实心圆
								var iconUnSelected = "&#xe699;"; //空心圆
								var shieldIconEle = chiledTitleAEle.find("." + o.iconClass);
								if (shieldIconEle.length == 0) { //如果没有图标则添加
									chiledTitleAEle.prepend('<i class="' + o.iconClass + '">'+ iconUnSelected +'</i>');
									shieldIconEle = chiledTitleAEle.find("." + o.iconClass);
									shieldIconEle.data("changeiconimg", true);
								}
								if (hasAttr(shieldIconEle, "switch")) { //定义了切换字符，第一个为非选中，第二个为选中
									shieldIconEle.data("changeiconimg", true);
									var shieldIconSwitchStr = shieldIconEle.attr("switch");
									var shieldIconSwitchVals = shieldIconSwitchStr.split(",");
									iconSelected = shieldIconSwitchVals[1];
									iconUnSelected = shieldIconSwitchVals[0];
									shieldIconEle.html(iconUnSelected); //未选中
								}
								shieldIconEle.data("changeiconSelected", iconSelected);
								shieldIconEle.data("changeiconUnSelected", iconUnSelected);
								chiledDdEle.click(function() {
									var ddEles = parE.find("dd").filter("."+ShieldJS.css.THIS);
									ddEles.removeClass(ShieldJS.css.THIS);
									$(this).addClass(ShieldJS.css.THIS); //重置选中样式
									ddEles.find("." + o.iconClass).each(function(){
										var thisShieldIconEle = $(this);
										if (thisShieldIconEle.data("changeiconimg")) { //切换图标
											thisShieldIconEle.html(thisShieldIconEle.data("changeiconUnSelected")); //原来选中的部分设置为空心圆或自定义
										}
									});
									if (shieldIconEle.data("changeiconimg")) { //切换图标
										shieldIconEle.html(shieldIconEle.data("changeiconSelected")); //现在选中的部分设置为实心圆或自定义
									}
								});
							});
						}
					} 
				});
				// 通用处理
				navEle.children("." + o.itemClass).on({
					mouseenter : function(){
						var $this = $(this);
						var thisOffset = $this.offset();
						var navBarE = navEle.find("." + o.barClass).show();
						var timerIndex = $this.data("timerIndex");
						if (timerIndex && timer[timerIndex]) { //定时器存在时取消
							$this.removeData("timerIndex");
							clearTimeout(timer[timerIndex]);
						}
						if (navEle.hasClass(o.treeClass)) { //竖向
							navBarE.offset({top:thisOffset.top, left:thisOffset.left}).height($this.children("a:first").height()); //高亮条
						} else { //横向
							navBarE.offset({left:thisOffset.left, top:thisOffset.top+$this.height()-navBarE.height()}).width($this.width()); //高亮条
							//有子菜单显示ShieldJS.css.SHOW
							var childEle = $this.children("." + o.childClass);
							if (childEle.length >0) {
								$this.children("a").find("." + o.moreClass).addClass(o.moredClass);
								childEle.addClass(ShieldJS.css.SHOW);
							}
							
						}
					}
					,mouseleave : function(){
						var $this = $(this);
						navEle.find("." + o.barClass).hide();
						if (navEle.hasClass(o.treeClass)) { //竖向
							
						} else { //横向
							var childEle = $this.children("." + o.childClass);
							if (childEle.length >0) {
								var timerIndex = new Date().getTime();
								$this.data("timerIndex",timerIndex);
								timer[timerIndex] = setTimeout(function() {
									$this.children("a").find("." + o.moreClass).removeClass(o.moredClass);
									childEle.removeClass(ShieldJS.css.SHOW);
									$this.removeData("timerIndex");
								}, 300); //延时0.3秒，用于父子元素的鼠标移动过渡
							}
						}
					}
					,click : function() {
						var $this = $(this);
						if (!navEle.hasClass(o.treeClass)) { //竖向不添加
							navEle.children("." + o.itemClass).removeClass(ShieldJS.css.THIS);
							$this.addClass(ShieldJS.css.THIS);
						}
					}
				});
			});    
        }
	};
	/**
	* @description {Json} 默认参数
	* @field
	*/
	$.fn.ShieldJSNav.defaults = {
		// 鼠标悬浮的菜单样式
		barClass : 'shield-nav-bar'
		// 条目样式
	    ,itemClass : 'shield-nav-item'
	    // 竖向样式
	    ,treeClass : 'shield-nav-tree'
	    // 竖向选中样式
		,treeItemdClass : 'shield-nav-itemed'
		// 展开收起样式，存在子菜单
		,moreClass : 'shield-nav-more'
		// 展开收起样式,显示子菜单时出现
		,moredClass : 'shield-nav-mored'
		// 子菜单样式
		,childClass : 'shield-nav-child'
		// icon图标样式
		,iconClass : 'shield-icon'
	};
	/**
	* @description {String} 格式化字符串
	*/
	$.fn.ShieldJSNav.format = function(txt) {
		return '<strong>' + txt + '</strong>';
	};
	/**
	* @description 输出选中对象的个数到控制台
	* @param {jQueryObject} $obj 选中对象
	*/
	function debug($obj) {
		if (window.console && window.console.log)
			window.console.log('ShieldJSNav selection count: ' + $obj.size());
	};
})(jQuery);