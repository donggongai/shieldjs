﻿// QuartZ Cron表达式参考https://www.cnblogs.com/free-dom/p/5801917.html
/*!
 * cronGen v1.0.0
 */
(function ($) {
    
    var resultsName = "";
    var inputElement;
    var displayElement;
    var valueShowElement;
    var execTimesUrl;
    $.fn.extend({
        cronGen: function (options) {
            if (options == null) {
              options = {};
            }
            options = $.extend({}, $.fn.cronGen.defaultOptions, options);
            execTimesUrl = options.execTimesUrl;
            //create top menu
            var cronContainer = $("<div/>", { id: "CronContainer"});
            var valueDiv = $("<div/>", { id: "CronGenValueDiv"});
            var valueDiv1 = $("<div/>", { id: "CronGenValueDiv1"});
            valueDiv1.html("<span>表达式：</span>");
            var CronGenValueInput = $('<input/>', { 'type': 'text', id: "CronGenValueInput", "class":"shield-input" });
            valueDiv1.append(CronGenValueInput);
            valueDiv.append(valueDiv1);
            valueDiv.append('<div class="CronGenValueExpDiv"><span>说　明：</span><span class="CronGenValueExplanation" style="width:460px; margin-left:0px;"></span>' +
            		'<span class="CronGenValueShow CronGenValueShowSecond"></span><span class="CronGenValueShow CronGenValueShowMinute"></span>' +
            		'<span class="CronGenValueShow CronGenValueShowHour"></span><span class="CronGenValueShow CronGenValueShowDayOfMonth"></span>' +
            		'<span class="CronGenValueShow CronGenValueShowMonth"></span><span class="CronGenValueShow CronGenValueShowDayOfWeek"></span>' +
            		'<span class="CronGenValueShow CronGenValueShowYear"></span></div>');
            valueShowElement = CronGenValueInput;
            var mainDiv = $("<div/>", { id: "CronGenMainDiv"});
            var topMenu = $("<ul/>", { "class": "nav nav-tabs", id: "CronGenTabs" });
            $('<li/>', { 'class': 'active' }).html($('<a id="SecondlyTab" href="#Secondly">秒</a>')).appendTo(topMenu);
            $('<li/>').html($('<a id="MinutesTab" href="#Minutes">分钟</a>')).appendTo(topMenu);
            $('<li/>').html($('<a id="HourlyTab" href="#Hourly">小时</a>')).appendTo(topMenu);
            $('<li/>').html($('<a id="DailyTab" href="#Daily">日</a>')).appendTo(topMenu);
            $('<li/>').html($('<a id="MonthlyTab" href="#Monthly">月</a>')).appendTo(topMenu);
            $('<li/>').html($('<a id="WeeklyTab" href="#Weekly">周</a>')).appendTo(topMenu);
            $('<li/>').html($('<a id="YearlyTab" href="#Yearly">年</a>')).appendTo(topMenu);
            $(topMenu).appendTo(mainDiv);

            //create what's inside the tabs
            var container = $("<div/>", { "class": "container-fluid" });
            var row = $("<div/>", { "class": "row-fluid" });
            var span12 = $("<div/>", { "class": "span12" });
            var tabContent = $("<div/>", { "class": "tab-content"});


            //creating the secondsTab
            var secondsTab = $("<div/>", { "class": "tab-pane active", id: "Secondly" });
            var seconds1 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "1", name : "second"}).appendTo(seconds1);
            $(seconds1).append("每秒 允许的通配符[, - * /]");
            $(seconds1).appendTo(secondsTab);
            
            var seconds2 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "2", name : "second"}).appendTo(seconds2);
            $(seconds2).append("周期 从");
            $("<input/>",{type : "text", id : "secondStart_0", value : "1", 'class' : "input-width"}).appendTo(seconds2);
            $(seconds2).append("-");
            $("<input/>",{type : "text", id : "secondEnd_0", value : "2", 'class' : "input-width"}).appendTo(seconds2);
            $(seconds2).append("秒");
            $(seconds2).appendTo(secondsTab);
            
            var seconds3 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "3", name : "second"}).appendTo(seconds3);
            $(seconds3).append("从");
            $("<input/>",{type : "text", id : "secondStart_1", value : "0", 'class' : "input-width"}).appendTo(seconds3);
            $(seconds3).append("秒开始,每");
            $("<input/>",{type : "text", id : "secondEnd_1", value : "1", 'class' : "input-width"}).appendTo(seconds3);
            $(seconds3).append("秒执行一次");
            $(seconds3).appendTo(secondsTab);
            
            var seconds4 = $("<div/>",{"class":"line line-border"});
            $("<input/>",{type : "radio", value : "4", name : "second", id: "sencond_appoint"}).appendTo(seconds4);
            $(seconds4).append("指定");
            $(seconds4).appendTo(secondsTab);
            
            var secondListHtml = "";
            for (var i = 0; i < 60; i++) {
            	if (i%10 == 0) {
            		secondListHtml +='<div class="imp secondList">';
                }
            	//Seconds(秒)：可以用数字0-59 表示，
            	secondListHtml +='<div class="cron-style"><input type="checkbox" disabled="disabled" value="'+i+'">'+("0" + i).substr(-2)+'</div>';
            	if ((i+1)%10 == 0) {
            		secondListHtml +='</div>';
            	}
            }
            $(secondsTab).append(secondListHtml);
            $("<input/>",{type : "hidden", id : "secondHidden"}).appendTo(secondsTab);
            $(secondsTab).appendTo(tabContent);
            
            //creating the minutesTab
            var minutesTab = $("<div/>", { "class": "tab-pane", id: "Minutes" });
            
            var minutes1 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "1", name : "min"}).appendTo(minutes1);
            $(minutes1).append("每分钟 允许的通配符[, - * /]");
            $(minutes1).appendTo(minutesTab);
            
            var minutes2 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "2", name : "min"}).appendTo(minutes2);
            $(minutes2).append("周期 从");
            $("<input/>",{type : "text", id : "minStart_0", value : "1", 'class' : "input-width"}).appendTo(minutes2);
            $(minutes2).append("-");
            $("<input/>",{type : "text", id : "minEnd_0", value : "2", 'class' : "input-width"}).appendTo(minutes2);
            $(minutes2).append("分钟");
            $(minutes2).appendTo(minutesTab);
            
            var minutes3 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "3", name : "min"}).appendTo(minutes3);
            $(minutes3).append("从");
            $("<input/>",{type : "text", id : "minStart_1", value : "0", 'class' : "input-width"}).appendTo(minutes3);
            $(minutes3).append("分钟开始,每");
            $("<input/>",{type : "text", id : "minEnd_1", value : "1", 'class' : "input-width"}).appendTo(minutes3);
            $(minutes3).append("分钟执行一次");
            $(minutes3).appendTo(minutesTab);
            
            var minutes4 = $("<div/>",{"class":"line line-border"});
            $("<input/>",{type : "radio", value : "4", name : "min", id: "min_appoint"}).appendTo(minutes4);
            $(minutes4).append("指定");
            $(minutes4).appendTo(minutesTab);
            
            var minListHtml = "";
            for (var i = 0; i < 60; i++) {
            	if (i%10 == 0) {
            		minListHtml +='<div class="imp minList">';
                }
            	//Minutes(分)：可以用数字0－59 表示，
            	minListHtml +='<div class="cron-style"><input type="checkbox" disabled="disabled" value="'+i+'">'+("0" + i).substr(-2)+'</div>';
            	if ((i+1)%10 == 0) {
           		 	minListHtml +='</div>';
            	}
            }
            $(minutesTab).append(minListHtml);
            $("<input/>",{type : "hidden", id : "minHidden"}).appendTo(minutesTab);
            $(minutesTab).appendTo(tabContent);
            
            //creating the hourlyTab
            var hourlyTab = $("<div/>", { "class": "tab-pane", id: "Hourly" });

            var hourly1 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "1", name : "hour"}).appendTo(hourly1);
            $(hourly1).append("每小时 允许的通配符[, - * /]");
            $(hourly1).appendTo(hourlyTab);
            
            var hourly2 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "2", name : "hour"}).appendTo(hourly2);
            $(hourly2).append("周期 从");
            $("<input/>",{type : "text", id : "hourStart_0", value : "1", 'class' : "input-width"}).appendTo(hourly2);
            $(hourly2).append("-");
            $("<input/>",{type : "text", id : "hourEnd_0", value : "2", 'class' : "input-width"}).appendTo(hourly2);
            $(hourly2).append("小时");
            $(hourly2).appendTo(hourlyTab);
            
            var hourly3 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "3", name : "hour"}).appendTo(hourly3);
            $(hourly3).append("从");
            $("<input/>",{type : "text", id : "hourStart_1", value : "0", 'class' : "input-width"}).appendTo(hourly3);
            $(hourly3).append("点开始,每");
            $("<input/>",{type : "text", id : "hourEnd_1", value : "1", 'class' : "input-width"}).appendTo(hourly3);
            $(hourly3).append("小时执行一次");
            $(hourly3).appendTo(hourlyTab);
            
            var hourly4 = $("<div/>",{"class":"line line-border"});
            $("<input/>",{type : "radio", value : "4", name : "hour", id: "hour_appoint"}).appendTo(hourly4);
            $(hourly4).append("指定");
            $(hourly4).appendTo(hourlyTab);
            
            var hourListHtml = "";
            for (var i = 0; i < 24; i++) {
            	if (i%10 == 0) {
            		hourListHtml +='<div class="imp hourList">';
                }
            	//Hours(时)：可以用数字0-23表示,
            	hourListHtml +='<div class="cron-style"><input type="checkbox" disabled="disabled" value="'+i+'">'+("0" + i).substr(-2)+'</div>';
            	if ((i+1)%10 == 0) {
            		hourListHtml +='</div>';
            	}
            }
            $(hourlyTab).append(hourListHtml);
//            $(hourlyTab).append('<div class="imp hourList"><input type="checkbox" disabled="disabled" value="0">00<input type="checkbox" disabled="disabled" value="1">01<input type="checkbox" disabled="disabled" value="2">02<input type="checkbox" disabled="disabled" value="3">03<input type="checkbox" disabled="disabled" value="4">04<input type="checkbox" disabled="disabled" value="5">05</div>');
//            $(hourlyTab).append('<div class="imp hourList"><input type="checkbox" disabled="disabled" value="6">06<input type="checkbox" disabled="disabled" value="7">07<input type="checkbox" disabled="disabled" value="8">08<input type="checkbox" disabled="disabled" value="9">09<input type="checkbox" disabled="disabled" value="10">10<input type="checkbox" disabled="disabled" value="11">11</div>');
//            $(hourlyTab).append('<div class="imp hourList"><input type="checkbox" disabled="disabled" value="12">12<input type="checkbox" disabled="disabled" value="13">13<input type="checkbox" disabled="disabled" value="14">14<input type="checkbox" disabled="disabled" value="15">15<input type="checkbox" disabled="disabled" value="16">16<input type="checkbox" disabled="disabled" value="17">17</div>');
//            $(hourlyTab).append('<div class="imp hourList"><input type="checkbox" disabled="disabled" value="18">18<input type="checkbox" disabled="disabled" value="19">19<input type="checkbox" disabled="disabled" value="20">20<input type="checkbox" disabled="disabled" value="21">21<input type="checkbox" disabled="disabled" value="22">22<input type="checkbox" disabled="disabled" value="23">23</div>');
            $("<input/>",{type : "hidden", id : "hourHidden"}).appendTo(hourlyTab);
            $(hourlyTab).appendTo(tabContent);
            

            //creating the dailyTab
            var dailyTab = $("<div/>", { "class": "tab-pane", id: "Daily" });

            var daily1 = $("<div/>",{"class":"line line_daily"});
            $("<input/>",{type : "radio", value : "1", name : "day"}).appendTo(daily1);
            $(daily1).append("每天 允许的通配符[, - * / L W]");
            $(daily1).appendTo(dailyTab);
            
            var daily5 = $("<div/>",{"class":"line line_daily"});
            $("<input/>",{type : "radio", value : "2", name : "day"}).appendTo(daily5);
            $(daily5).append("不指定");
            $(daily5).appendTo(dailyTab);
            
            var daily2 = $("<div/>",{"class":"line line_daily"});
            $("<input/>",{type : "radio", value : "3", name : "day"}).appendTo(daily2);
            $(daily2).append("周期 从");
            $("<input/>",{type : "text", id : "dayStart_0", value : "1", 'class' : "input-width"}).appendTo(daily2);
            $(daily2).append("-");
            $("<input/>",{type : "text", id : "dayEnd_0", value : "2", 'class' : "input-width"}).appendTo(daily2);
            $(daily2).append("日");
            $(daily2).appendTo(dailyTab);
            
            var daily3 = $("<div/>",{"class":"line line_daily"});
            $("<input/>",{type : "radio", value : "4", name : "day"}).appendTo(daily3);
            $(daily3).append("从");
            $("<input/>",{type : "text", id : "dayStart_1", value : "1", 'class' : "input-width"}).appendTo(daily3);
            $(daily3).append("日开始,每");
            $("<input/>",{type : "text", id : "dayEnd_1", value : "1", 'class' : "input-width"}).appendTo(daily3);
            $(daily3).append("天执行一次");
            $(daily3).appendTo(dailyTab);
            
            var daily6 = $("<div/>",{"class":"line line_daily"});
            $("<input/>",{type : "radio", value : "5", name : "day"}).appendTo(daily6);
            $(daily6).append("每月");
            $("<input/>",{type : "text", id : "dayStart_2", value : "1", 'class' : "input-width"}).appendTo(daily6);
            $(daily6).append("号最近的那个工作日");
            $(daily6).appendTo(dailyTab);
            
            var daily7 = $("<div/>",{"class":"line line_daily"});
            $("<input/>",{type : "radio", value : "6", name : "day"}).appendTo(daily7);
            $(daily7).append("每月最后一天");
            $(daily7).appendTo(dailyTab);
            
            var daily4 = $("<div/>",{"class":"line line_daily line-border"});
            $("<input/>",{type : "radio", value : "7", name : "day", id: "day_appoint"}).appendTo(daily4);
            $(daily4).append("指定");
            $(daily4).appendTo(dailyTab);
            
            var dayListHtml = "";
            for (var i = 0; i < 31; i++) {
            	var dayNum = i+1;
            	if (i%10 == 0) {
            		dayListHtml +='<div class="imp dayList">';
                }
            	//Day-of-Month(天)：可以用数字1-31 中的任一一个值，但要注意一些特别的月份
            	dayListHtml +='<div class="cron-style"><input type="checkbox" disabled="disabled" value="'+dayNum+'">'+("0" + dayNum).substr(-2)+'</div>';
            	if ((i+1)%10 == 0) {
            		dayListHtml +='</div>';
            	}
            }
            $(dailyTab).append(dayListHtml);
//            $(dailyTab).append('<div class="imp dayList"><input type="checkbox" disabled="disabled" value="1">01<input type="checkbox" disabled="disabled" value="2">02<input type="checkbox" disabled="disabled" value="3">03<input type="checkbox" disabled="disabled" value="4">04<input type="checkbox" disabled="disabled" value="5">05<input type="checkbox" disabled="disabled" value="6">06<input type="checkbox" disabled="disabled" value="7">07<input type="checkbox" disabled="disabled" value="8">08<input type="checkbox" disabled="disabled" value="9">09<input type="checkbox" disabled="disabled" value="10">10</div>');
//            $(dailyTab).append('<div class="imp dayList"><input type="checkbox" disabled="disabled" value="11">11<input type="checkbox" disabled="disabled" value="12">12<input type="checkbox" disabled="disabled" value="13">13<input type="checkbox" disabled="disabled" value="14">14<input type="checkbox" disabled="disabled" value="15">15<input type="checkbox" disabled="disabled" value="16">16<input type="checkbox" disabled="disabled" value="17">17<input type="checkbox" disabled="disabled" value="18">18<input type="checkbox" disabled="disabled" value="19">19<input type="checkbox" disabled="disabled" value="20">20</div>');
//            $(dailyTab).append('<div class="imp dayList"><input type="checkbox" disabled="disabled" value="21">21<input type="checkbox" disabled="disabled" value="22">22<input type="checkbox" disabled="disabled" value="23">23<input type="checkbox" disabled="disabled" value="24">24<input type="checkbox" disabled="disabled" value="25">25<input type="checkbox" disabled="disabled" value="26">26<input type="checkbox" disabled="disabled" value="27">27<input type="checkbox" disabled="disabled" value="28">28<input type="checkbox" disabled="disabled" value="29">29<input type="checkbox" disabled="disabled" value="30">30</div>');
//            $(dailyTab).append('<div class="imp dayList"><input type="checkbox" disabled="disabled" value="31">31</div>');
            $("<input/>",{type : "hidden", id : "dayHidden"}).appendTo(dailyTab);
            $(dailyTab).appendTo(tabContent);
            
            
            //creating the monthlyTab
            var monthlyTab = $("<div/>", { "class": "tab-pane", id: "Monthly" });

            var monthly1 = $("<div/>",{"class":"line line_monthly"});
            $("<input/>",{type : "radio", value : "1", name : "month"}).appendTo(monthly1);
            $(monthly1).append("每月 允许的通配符[, - * /]");
            $(monthly1).appendTo(monthlyTab);
            
//            无意义
//            var monthly2 = $("<div/>",{"class":"line line_monthly"});
//            $("<input/>",{type : "radio", value : "2", name : "month"}).appendTo(monthly2);
//            $(monthly2).append("不指定");
//            $(monthly2).appendTo(monthlyTab);
            
            var monthly3 = $("<div/>",{"class":"line line_monthly"});
            $("<input/>",{type : "radio", value : "3", name : "month"}).appendTo(monthly3);
            $(monthly3).append("周期 从");
            $("<input/>",{type : "text", id : "monthStart_0", value : "1", 'class' : "input-width"}).appendTo(monthly3);
            $(monthly3).append("-");
            $("<input/>",{type : "text", id : "monthEnd_0", value : "2", 'class' : "input-width"}).appendTo(monthly3);
            $(monthly3).append("月");
            $(monthly3).appendTo(monthlyTab);
            
            var monthly4 = $("<div/>",{"class":"line line_monthly"});
            $("<input/>",{type : "radio", value : "4", name : "month"}).appendTo(monthly4);
            $(monthly4).append("从");
            $("<input/>",{type : "text", id : "monthStart_1", value : "1", 'class' : "input-width"}).appendTo(monthly4);
            $(monthly4).append("月开始,每");
            $("<input/>",{type : "text", id : "monthEnd_1", value : "1", 'class' : "input-width"}).appendTo(monthly4);
            $(monthly4).append("月执行一次");
            $(monthly4).appendTo(monthlyTab);
            
            var monthly5 = $("<div/>",{"class":"line line_monthly line-border"});
            $("<input/>",{type : "radio", value : "5", name : "month", id: "month_appoint"}).appendTo(monthly5);
            $(monthly5).append("指定");
            $(monthly5).appendTo(monthlyTab);
            
            var monthListHtml = "";
            for (var i = 0; i < 12; i++) {
            	var monthNum = i+1;
            	if (i%10 == 0) {
            		monthListHtml +='<div class="imp monthList">';
                }
            	//Month(月)：可以用1-12 或用字符串  “JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV and DEC” 表示
            	monthListHtml +='<div class="cron-style"><input type="checkbox" disabled="disabled" value="'+monthNum+'">'+("0" + monthNum).substr(-2)+'</div>';
            	if ((i+1)%10 == 0) {
            		monthListHtml +='</div>';
            	}
            }
            $(monthlyTab).append(monthListHtml);
//            $(monthlyTab).append('<div class="imp monthList"><input type="checkbox" disabled="disabled" value="1">01<input type="checkbox" disabled="disabled" value="2">02<input type="checkbox" disabled="disabled" value="3">03<input type="checkbox" disabled="disabled" value="4">04<input type="checkbox" disabled="disabled" value="5">05<input type="checkbox" disabled="disabled" value="6">06</div>');
//            $(monthlyTab).append('<div class="imp monthList"><input type="checkbox" disabled="disabled" value="7">07<input type="checkbox" disabled="disabled" value="8">08<input type="checkbox" disabled="disabled" value="9">09<input type="checkbox" disabled="disabled" value="10">10<input type="checkbox" disabled="disabled" value="11">11<input type="checkbox" disabled="disabled" value="12">12</div>');
            $("<input/>",{type : "hidden", id : "monthHidden"}).appendTo(monthlyTab);
            $(monthlyTab).appendTo(tabContent);

            //creating the weeklyTab
            var weeklyTab = $("<div/>", { "class": "tab-pane", id: "Weekly" });
            
            var weekly1 = $("<div/>",{"class":"line line_weekly"});
            $("<input/>",{type : "radio", value : "1", name : "week"}).appendTo(weekly1);
            $(weekly1).append("每周 允许的通配符[, - * / L #]");
            $(weekly1).appendTo(weeklyTab);
            
            var weekly2 = $("<div/>",{"class":"line line_weekly"});
            $("<input/>",{type : "radio", value : "2", name : "week"}).appendTo(weekly2);
            $(weekly2).append("不指定");
            $(weekly2).appendTo(weeklyTab);
            
            var weekly3 = $("<div/>",{"class":"line line_weekly"});
            $("<input/>",{type : "radio", value : "3", name : "week"}).appendTo(weekly3);
            $(weekly3).append("周期 从星期");
            $("<input/>",{type : "text", id : "weekStart_0", value : "1", 'class' : "input-width"}).appendTo(weekly3);
            $(weekly3).append("-");
            $("<input/>",{type : "text", id : "weekEnd_0", value : "2", 'class' : "input-width"}).appendTo(weekly3);
            $(weekly3).append("");
            $(weekly3).appendTo(weeklyTab);
            
            var weekly4 = $("<div/>",{"class":"line line_weekly"});
            $("<input/>",{type : "radio", value : "4", name : "week"}).appendTo(weekly4);
            $(weekly4).append("每月第");
            $("<input/>",{type : "text", id : "weekStart_1", value : "1", 'class' : "input-width"}).appendTo(weekly4);
            $(weekly4).append("个星期");
            $("<input/>",{type : "text", id : "weekEnd_1", value : "1", 'class' : "input-width"}).appendTo(weekly4);
            $(weekly4).appendTo(weeklyTab);
            
            var weekly5 = $("<div/>",{"class":"line line_weekly"});
            $("<input/>",{type : "radio", value : "5", name : "week"}).appendTo(weekly5);
            $(weekly5).append("每月最后一个星期");
            $("<input/>",{type : "text", id : "weekStart_2", value :"1", 'class' : "input-width"}).appendTo(weekly5);
            $(weekly5).appendTo(weeklyTab);
            
            var weekly6 = $("<div/>",{"class":"line line_weekly line-border"});
            $("<input/>",{type : "radio", value : "6", name : "week", id: "week_appoint"}).appendTo(weekly6);
            $(weekly6).append("指定");
            $(weekly6).appendTo(weeklyTab);
            
            var weekListHtml = '<div class="imp weekList">';
            for (var i = 0; i < 7; i++) {
            	var weekday = i+1;
            	var weekdayval = $.fn.cronGen.tools.getWeekDayVal(weekday);
            	var weekdayLabel = "";
            	switch (weekday) {
	                case 1:
	                	weekdayLabel = "星期一";
		                break;
	                case 2:
	                	weekdayLabel = "星期二";
		                break;
	                case 3:
	                	weekdayLabel = "星期三";
		                break;
	                case 4:
	                	weekdayLabel = "星期四";
		                break;
	                case 5:
	                	weekdayLabel = "星期五";
		                break;
	                case 6:
	                	weekdayLabel = "星期六";
		                break;
	                case 7:
	                	weekdayLabel = "星期日";
		                break;
                }
            	//Day-of-Week(每周)：可以用数字1-7表示（1 ＝ 星期日,2=星期一）或用字符口串“SUN, MON, TUE, WED, THU, FRI and SAT”表示
            	weekListHtml +='<div class="cron-style weekday"><input type="checkbox" disabled="disabled" value="'+weekdayval+'">'+weekdayLabel+'</div>';
            }
            weekListHtml +='</div>';
            $(weeklyTab).append(weekListHtml);
//            $(weeklyTab).append('<div class="imp weekList"><input type="checkbox" disabled="disabled" value="1">1<input type="checkbox" disabled="disabled" value="2">2<input type="checkbox" disabled="disabled" value="3">3<input type="checkbox" disabled="disabled" value="4">4<input type="checkbox" disabled="disabled" value="5">5<input type="checkbox" disabled="disabled" value="6">6<input type="checkbox" disabled="disabled" value="7">7</div>');
            
            $("<input/>",{type : "hidden", id : "weekHidden"}).appendTo(weeklyTab);
            $(weeklyTab).appendTo(tabContent);

            //creating the yearlyTab
            var yearlyTab = $("<div/>", { "class": "tab-pane", id: "Yearly" });
            
            var yearly1 = $("<div/>",{"class":"line line_yearly"});
            $("<input/>",{type : "radio", value : "1", name : "year"}).appendTo(yearly1);
            $(yearly1).append("不指定 允许的通配符[, - * /] 非必填");
            $(yearly1).appendTo(yearlyTab);
            
            var yearly3 = $("<div/>",{"class":"line"});
            $("<input/>",{type : "radio", value : "2", name : "year"}).appendTo(yearly3);
            $(yearly3).append("每年");
            $(yearly3).appendTo(yearlyTab);
            
            var yearly2 = $("<div/>",{"class":"line line_yearly year-border"});
            $("<input/>",{type : "radio", value : "3", name : "year"}).appendTo(yearly2);
            $(yearly2).append("周期从");
            $("<input/>",{type : "text", id : "yearStart_0", value : "2018", 'class' : "input-width"}).appendTo(yearly2);
            $(yearly2).append("-");
            $("<input/>",{type : "text", id : "yearEnd_0", value : "2019", 'class' : "input-width"}).appendTo(yearly2);
            $(yearly2).append("年");
            $(yearly2).appendTo(yearlyTab);
            $("<input/>",{type : "hidden", id : "yearHidden"}).appendTo(yearlyTab);
            $(yearlyTab).appendTo(tabContent);
            
            $(tabContent).appendTo(span12);

            //creating the button and results input           
            //resultsName = $(this).prop("id");
            //$(this).prop("name", resultsName);
            
            var mainBtnDiv = $("<div/>", { "class": "CronGenBtnDiv"});
            var mainBtnDivHtml = '<div class="button-bg"><input type="button" ';
            if (options.submitBtnClass) { //默认"shield-btn"
            	mainBtnDivHtml += ' class="'+options.submitBtnClass+'"';
            }
            mainBtnDivHtml += ' id="submitbtn" value="确定"/></div>';
            if (execTimesUrl) { //最近几次运行时间
            	mainBtnDivHtml += '<div id="excuteTimeDiv"><span>最近5次运行时间</span><div class="excuteTimeDivContent"><div/>';
            }
            mainBtnDiv.html(mainBtnDivHtml);

            $(span12).appendTo(row);
            $(row).appendTo(container);
            $(container).appendTo(mainDiv); //主内容各个选项区
            $(mainBtnDiv).appendTo(mainDiv); //主内容按钮区
            $(cronContainer).append(valueDiv); //值
            $(cronContainer).append(mainDiv);
            
            var that = $(this);

            // Hide the original input
            that.hide();

            // Replace the input with an input group
            var $g = $("<div>").addClass("input-group input-group-cronGen");
            // Add an input
            var $i = $("<input>", { type: 'text', placeholder: 'cron表达式...'}).addClass(options.showInputClass).val($(that).val());
            valueShowElement.val($(that).val()); //自定义显示
            $i.appendTo($g);
            $i.prop("readOnly", true); //只读
            // Add the button
            var $b = $(options.selectBtn);
            // Put button inside span
            var $s = $('<span style="' + options.inputGroupBtnStyle + '"></span>').addClass('input-group-btn');
            $b.appendTo($s);
            $s.appendTo($g);

            $(this).before($g);

            inputElement = that;
            displayElement = $i;
            // layer弹出框
            $b.on('click', function (e) {
            	e.preventDefault();
            	var index = layer.open({
            		type: 1,
            		skin: 'layui-layer-rim', //加上边框
            		maxHeight : 500, //最大高度，只有当高度自适应时，设定才有效。
            		area: '595px', //宽度
            		title : "Cron选择器",
            		content: $(cronContainer).html()
            	});
            	valueShowElement = $("#CronGenValueInput");
            	valueShowElement.change(function() {
            		var value = $(this).val();
            		// Update original control
                    inputElement.val(value); //返回值回写输入框（不可见）
                	displayElement.val(value); //显示输入框（可见）
                	if (execTimesUrl) { //最近几次运行时间
                		if (console && console.log) {
                			console.log("execTimesUrl==="+execTimesUrl);
                        }
    	            	//设置最近五次运行时间
    	            	$.ajax({
    	            		type: 'get',
    	            		url: execTimesUrl,
    	            		dataType: "json",
    	            		data: { "CronExpression" : valueShowElement.val() },
    	            		success: function (result) {
    	            			// success表示是否成功，message返回提示信息，object返回结果
    	            			if (result.success) {
    	            				var data = result.data;
    	            				if (data && data.length > 0) {
    	            					var strHTML = "<ul>";
    	            					for (var i = 0; i < data.length; i++) {
    	            						strHTML += "<li>" + data[i] + "</li>";
    	            					}
    	            					strHTML +="</ul>"
    	            					$(".excuteTimeDivContent").html(strHTML);
    	            				} else {
        	            				$(".excuteTimeDivContent").html(result.message);
        	            			}
                                } else {
    	            				$(".excuteTimeDivContent").html(result.message);
    	            			}
    	            		}
    	            	});
                	}
                	// 解释表达式 
                	var vals = value.replace(/  /g," ").split(" ");
                	if (vals.length >= 6) {
	                    showHtml = $.fn.cronGen.tools.explainCron(value);
                		$(".CronGenValueExplanation").html(showHtml);
                    }
                });
            	valueShowElement.val(displayElement.val()).triggerHandler("change");
                
                $.fn.cronGen.tools.cronParse(inputElement.val());
                
                //绑定指定事件
                $.fn.cronGen.tools.initChangeEvent();
                
                $('#CronGenTabs a').click(function (e) {
                	$('#CronGenTabs li.active').removeClass("active");
                	$(this).closest("li").addClass("active");
                    $("#CronGenMainDiv").find(".tab-pane").removeClass("active");
                    $("#CronGenMainDiv").find(".tab-pane"+$(this).attr("href")).addClass("active");
                    //generate();
                    return false;
                });
                $("#CronGenMainDiv :input").not(":button,:submit").change(function (e) {
                    generate();
                });
                $("#CronGenMainDiv :input").not(":button,:submit").focus(function (e) {
                    generate();
                });
                $("#CronGenMainDiv #submitbtn").click(function (e) { //关闭窗口
                	layer.close(index);
                });
                
            });
            
//            $b.popover({
//                html: true,
//                content: function () {
//                    return $(cronContainer).html();
//                },
//                template: '<div class="popover" style="max-width:500px !important; width:425px;left:-341.656px;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
//                placement: options.direction
//
//            }).on('click', function (e) {
//                e.preventDefault();
//                $.fn.cronGen.tools.cronParse(inputElement.val());
//                
//                //绑定指定事件
//                $.fn.cronGen.tools.initChangeEvent();
//                
//                
//                $('#CronGenTabs a').click(function (e) {
//                    e.preventDefault();
//                    $(this).tab('show');
//                    //generate();
//                });
//                $("#CronGenMainDiv select,input").change(function (e) {
//                    generate();
//                });
//                $("#CronGenMainDiv input").focus(function (e) {
//                    generate();
//                });
//                //generate();
//            });
            return;
        }
    });

    var generate = function () {

        var activeTab = $("ul#CronGenTabs li.active a").prop("id");
        var results = "";
        switch (activeTab) {
            case "SecondlyTab":
                switch ($("input:radio[name=second]:checked").val()) {
                    case "1":
                        $.fn.cronGen.tools.everyTime("second");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                        $.fn.cronGen.tools.cycle("second");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.startOn("second");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "4":
                    	$.fn.cronGen.tools.initCheckBox("second");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
            case "MinutesTab":
                switch ($("input:radio[name=min]:checked").val()) {
                    case "1":
                        $.fn.cronGen.tools.everyTime("min");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                        $.fn.cronGen.tools.cycle("min");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.startOn("min");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "4":
                    	$.fn.cronGen.tools.initCheckBox("min");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
            case "HourlyTab":
                switch ($("input:radio[name=hour]:checked").val()) {
                    case "1":
                       $.fn.cronGen.tools.everyTime("hour");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                       $.fn.cronGen.tools.cycle("hour");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.startOn("hour");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "4":
                    	$.fn.cronGen.tools.initCheckBox("hour");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
            case "DailyTab":
                switch ($("input:radio[name=day]:checked").val()) {
                    case "1":
                        $.fn.cronGen.tools.everyTime("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                        $.fn.cronGen.tools.unAppoint("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.cycle("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "4":
                        $.fn.cronGen.tools.startOn("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "5":
                        $.fn.cronGen.tools.workDay("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "6":
                        $.fn.cronGen.tools.lastDay("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "7":
                    	$.fn.cronGen.tools.initCheckBox("day");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
            case "WeeklyTab":
                switch ($("input:radio[name=week]:checked").val()) {
                    case "1":
                        $.fn.cronGen.tools.everyTime("week");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                        $.fn.cronGen.tools.unAppoint("week");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.cycle("week");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "4":
                        $.fn.cronGen.tools.weekOfDay("week");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "5":
                        $.fn.cronGen.tools.lastWeek("week");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "6":
                    	$.fn.cronGen.tools.initCheckBox("week");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
            case "MonthlyTab":
                switch ($("input:radio[name=month]:checked").val()) {
                    case "1":
                        $.fn.cronGen.tools.everyTime("month");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                        $.fn.cronGen.tools.unAppoint("month");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.cycle("month");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "4":
                        $.fn.cronGen.tools.startOn("month");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "5":
                    	$.fn.cronGen.tools.initCheckBox("month");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
            case "YearlyTab":
                switch ($("input:radio[name=year]:checked").val()) {
                    case "1":
                        $.fn.cronGen.tools.unAppoint("year");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "2":
                        $.fn.cronGen.tools.everyTime("year");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                    case "3":
                        $.fn.cronGen.tools.cycle("year");
                        results = $.fn.cronGen.tools.cronResult();
                        break;
                }
                break;
        }
        // Update display
        valueShowElement.val(results).triggerHandler("change");
        
        console.log("val==="+valueShowElement.val());
    };

    
    $.fn.cronGen.tools = {
		//转换为星期对应的值 Day-of-Week(每周)：可以用数字1-7表示（1 ＝ 星期日,2=星期一）或用字符口串“SUN, MON, TUE, WED, THU, FRI and SAT”表示
		getWeekDayVal : function (weekday) {
	    	weekday = parseInt(weekday);
	    	var weekdayval = weekday+1;
	    	if (weekday == 7) {
	    		weekdayval = 1;
	        }
	    	return weekdayval;
	    },
	    //根据对应值返回星期数 值1返回7（星期日）2返回1（星期一）
	    getWeekDay : function (weekdayval) {
	    	weekdayval = parseInt(weekdayval);
	    	var weekday = weekdayval - 1;
	    	if (weekday == 0) {
	    		weekday = 1;
	        }
	    	return weekday;
	    },
        /**
         * 每周期
         */
        everyTime : function(dom){
            $("#"+dom+"Hidden").val("*");
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 不指定
         */
        unAppoint : function(dom){
            var val = "?";
            if (dom == "year")
            {
                val = "";
            }
            $("#"+dom+"Hidden").val(val);
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 周期
         */
        cycle : function(dom){
            var start = $("#"+dom+"Start_0").val();
            var end = $("#"+dom+"End_0").val();
            if (dom=='week') {
            	start = this.getWeekDayVal(start);
            	end = this.getWeekDayVal(end);
            } 
            $("#"+dom+"Hidden").val(start + "-" + end);
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 从开始
         */
        startOn : function(dom) {
            var start = $("#"+dom+"Start_1").val();
            var end = $("#"+dom+"End_1").val();
            $("#"+dom+"Hidden").val(start + "/" + end);
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 最后一天
         */
        lastDay : function(dom){
            $("#"+dom+"Hidden").val("L");
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 第x个星期x
         */
        weekOfDay : function(dom){
            var start = $("#"+dom+"Start_1").val();
            var end = $("#"+dom+"End_1").val(); 
            $("#"+dom+"Hidden").val(this.getWeekDayVal(end) + "#" + start ); //6#3" or "FRI#3"指这个月第3个周五(6指周五，3指第3个)
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 最后一个周x
         */
        lastWeek : function(dom){
            var start = $("#"+dom+"Start_2").val();
            $("#"+dom+"Hidden").val(this.getWeekDayVal(start)+"L");
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        /**
         * 工作日
         */
        workDay : function(dom) {
            var start = $("#"+dom+"Start_2").val();
            $("#"+dom+"Hidden").val(start + "W");
            $.fn.cronGen.tools.clearCheckbox(dom);
        },
        initChangeEvent : function(){
            var secondList = $(".secondList").children().find(":checkbox");
            $("#sencond_appoint").click(function(){
                if (this.checked) {
                    if (secondList.filter(":checked").length == 0) {
                        secondList.eq(0).prop("checked", true);
                    }
                    secondList.eq(0).change();
                }
            });
        
            secondList.change(function() {
                var sencond_appoint = $("#sencond_appoint").prop("checked");
                if (sencond_appoint) {
                    var vals = [];
                    secondList.each(function() {
                        if (this.checked) {
                            vals.push(this.value);
                        }
                    });
                    var val = "?";
                    if (vals.length > 0 && vals.length < 59) {
                        val = vals.join(","); 
                    }else if(vals.length == 59){
                        val = "*";
                    }
                    $("#secondHidden").val(val);
                }
            });
            
            var minList = $(".minList").children().find(":checkbox");
            $("#min_appoint").click(function(){
                if (this.checked) {
                    if (minList.filter(":checked").length == 0) {
                        minList.eq(0).prop("checked", true);
                    }
                    minList.eq(0).change();
                }
            });
            
            minList.change(function() {
                var min_appoint = $("#min_appoint").prop("checked");
                if (min_appoint) {
                    var vals = [];
                    minList.each(function() {
                        if (this.checked) {
                            vals.push(this.value);
                        }
                    });
                    var val = "?";
                    if (vals.length > 0 && vals.length < 59) {
                        val = vals.join(",");
                    }else if(vals.length == 59){
                        val = "*";
                    }
                    $("#minHidden").val(val);
                }
            });
            
            var hourList = $(".hourList").children().find(":checkbox");
            $("#hour_appoint").click(function(){
                if (this.checked) {
                    if (hourList.filter(":checked").length == 0) {
                        hourList.eq(0).prop("checked", true);
                    }
                    hourList.eq(0).change();
                }
            });
            
            hourList.change(function() {
                var hour_appoint = $("#hour_appoint").prop("checked");
                if (hour_appoint) {
                    var vals = [];
                    hourList.each(function() {
                        if (this.checked) {
                            vals.push(this.value);
                        }
                    });
                    var val = "?";
                    if (vals.length > 0 && vals.length < 24) {
                        val = vals.join(",");
                    }else if(vals.length == 24){
                        val = "*";
                    }
                    $("#hourHidden").val(val);
                }
            });
            
            var dayList = $(".dayList").children().find(":checkbox");
            $("#day_appoint").click(function(){
                if (this.checked) {
                    if (dayList.filter(":checked").length == 0) {
                        dayList.eq(0).prop("checked", true);
                    }
                    dayList.eq(0).change();
                }
            });
            
            dayList.change(function() {
                var day_appoint = $("#day_appoint").prop("checked");
                if (day_appoint) {
                    var vals = [];
                    dayList.each(function() {
                        if (this.checked) {
                            vals.push(this.value);
                        }
                    });
                    var val = "?";
                    if (vals.length > 0 && vals.length < 31) {
                        val = vals.join(",");
                    }else if(vals.length == 31){
                        val = "*";
                    }
                   $("#dayHidden").val(val);
                }
            });
            
            var monthList = $(".monthList").children().find(":checkbox");
            $("#month_appoint").click(function(){
                if (this.checked) {
                    if (monthList.filter(":checked").length == 0) {
                        monthList.eq(0).prop("checked", true);
                    }
                    monthList.eq(0).change();
                }
            });
            
            monthList.change(function() {
                var month_appoint = $("#month_appoint").prop("checked");
                if (month_appoint) {
                    var vals = [];
                    monthList.each(function() {
                        if (this.checked) {
                            vals.push(this.value);
                        }
                    });
                    var val = "?";
                    if (vals.length > 0 && vals.length < 12) {
                        val = vals.join(",");
                    }else if(vals.length == 12){
                        val = "*";
                    }
                    $("#monthHidden").val(val);
                }
            });
            
            var weekList = $(".weekList").children().find(":checkbox");
            $("#week_appoint").click(function(){
                if (this.checked) {
                    if (weekList.filter(":checked").length == 0) {
                        weekList.eq(0).prop("checked", true);
                    }
                    weekList.eq(0).change();
                }
            });
            
            weekList.change(function() {
                var week_appoint = $("#week_appoint").prop("checked");
                if (week_appoint) {
                    var vals = [];
                    weekList.each(function() {
                        if (this.checked) {
                            vals.push(this.value);
                        }
                    });
                    var val = "?";
                    if (vals.length > 0 && vals.length < 7) {
                        val = vals.join(",");
                    }else if(vals.length == 7){
                        val = "*";
                    }
                   $("#weekHidden").val(val);
                }
            });
        },
        initObj : function(strVal, strid){
            var ary = null;
            if (strVal) {
            	var objRadio = $("input[name='" + strid + "'");
            	if (strVal == "*") {
            		objRadio.eq(0).attr("checked", "checked");
            	} else if (strVal.split('-').length > 1) {
            		ary = strVal.split('-');
            		objRadio.eq(1).attr("checked", "checked");
            		$("#" + strid + "Start_0").val(ary[0]);
            		$("#" + strid + "End_0").val(ary[1]);
            	} else if (strVal.split('/').length > 1) {
            		ary = strVal.split('/');
            		objRadio.eq(2).attr("checked", "checked");
            		$("#" + strid + "Start_1").val(ary[0]);
            		$("#" + strid + "End_1").val(ary[1]);
            	} else {
            		objRadio.eq(3).attr("checked", "checked");
            		if (strVal != "?") {
            			ary = strVal.split(",");
            			for (var i = 0; i < ary.length; i++) {
            				$("." + strid + "List input[value='" + ary[i] + "']").attr("checked", "checked");
            			}
            			$.fn.cronGen.tools.initCheckBox(strid);
            		}
            	}
            }
        },
        initDay : function(strVal) {
            var ary = null;
            var objRadio = $("input[name='day'");
            if (strVal == "*") {
                objRadio.eq(0).attr("checked", "checked");
            } else if (strVal == "?") {
                objRadio.eq(1).attr("checked", "checked");
            } else if (strVal.split('-').length > 1) {
                ary = strVal.split('-');
                objRadio.eq(2).attr("checked", "checked");
                $("#dayStart_0").val(ary[0]);
                $("#dayEnd_0").val(ary[1]);
            } else if (strVal.split('/').length > 1) {
                ary = strVal.split('/');
                objRadio.eq(3).attr("checked", "checked");
                $("#dayStart_1").val(ary[0]);
                $("#dayEnd_1").val(ary[1]);
            } else if (strVal.split('W').length > 1) {
                ary = strVal.split('W');
                objRadio.eq(4).attr("checked", "checked");
                $("#dayStart_2").val(ary[0]);
            } else if (strVal == "L") {
                objRadio.eq(5).attr("checked", "checked");
            } else {
                objRadio.eq(6).attr("checked", "checked");
                ary = strVal.split(",");
                for (var i = 0; i < ary.length; i++) {
                    $(".dayList input[value='" + ary[i] + "']").attr("checked", "checked");
                }
                $.fn.cronGen.tools.initCheckBox("day");
            }
        },
        initMonth : function(strVal) {
            var ary = null;
            var objRadio = $("input[name='month'");
            if (strVal == "*") { //每月
            	objRadio.filter(function(index) {
            		return $(this).val() == 1;
            	}).attr("checked", "checked");
            } else if (strVal == "?") { //不指定
            	objRadio.filter(function(index) {
          		  	return $(this).val() == 2;
            	}).attr("checked", "checked");
            } else if (strVal.split('-').length > 1) { //范围
                ary = strVal.split('-');
                objRadio.filter(function(index) {
                	return $(this).val() == 3;
              	}).attr("checked", "checked");
                $("#monthStart_0").val(ary[0]);
                $("#monthEnd_0").val(ary[1]);
            } else if (strVal.split('/').length > 1) { //周期
                ary = strVal.split('/');
                objRadio.filter(function(index) {
                	return $(this).val() == 4;
              	}).attr("checked", "checked");
                $("#monthStart_1").val(ary[0]);
                $("#monthEnd_1").val(ary[1]);

            } else {
            	objRadio.filter(function(index) {
            		return $(this).val() == 5;
              	}).attr("checked", "checked");

                ary = strVal.split(",");
                for (var i = 0; i < ary.length; i++) {
                    $(".monthList input[value='" + ary[i] + "']").attr("checked", "checked");
                }
                $.fn.cronGen.tools.initCheckBox("month");
            }
        },
        initWeek : function(strVal) {
            var ary = null;
            var objRadio = $("input[name='week'");
            if (strVal == "*") { //每周
                objRadio.eq(0).attr("checked", "checked");
            } else if (strVal == "?") { //不指定
                objRadio.eq(1).attr("checked", "checked");
            } else if (strVal.split('-').length > 1) { //周期
                ary = strVal.split('-');
                objRadio.eq(2).attr("checked", "checked");
                $("#weekStart_0").val(this.getWeekDay(ary[0]));
                $("#weekEnd_0").val(this.getWeekDay(ary[1]));
            } else if (strVal.split('#').length > 1) { //第x个周x
                ary = strVal.split('#');
                objRadio.eq(3).attr("checked", "checked");
                $("#weekStart_1").val(ary[1]);
                $("#weekEnd_1").val(this.getWeekDay(ary[0]));
            } else if (strVal.split('L').length > 1) { //每月最后一个周x
                ary = strVal.split('L');
                objRadio.eq(4).attr("checked", "checked");
                $("#weekStart_2").val(this.getWeekDay(ary[0]));
            } else {
                objRadio.eq(5).attr("checked", "checked");
                ary = strVal.split(",");
                for (var i = 0; i < ary.length; i++) {
                    $(".weekList input[value='" + ary[i] + "']").attr("checked", "checked");
                }
                $.fn.cronGen.tools.initCheckBox("week");
            }
        },
        initYear : function(strVal) {
            var ary = null;
            var objRadio = $("input[name='year'");
            if (strVal == "*") {
                objRadio.eq(1).attr("checked", "checked");
            } else if (strVal.split('-').length > 1) {
                ary = strVal.split('-');
                objRadio.eq(2).attr("checked", "checked");
                $("#yearStart_0").val(ary[0]);
                $("#yearEnd_0").val(ary[1]);
            }
        },
        cronParse : function(cronExpress) {
            //获取参数中表达式的值
            if (cronExpress) {
                var regs = cronExpress.split(' ');
                $("input[name=secondHidden]").val(regs[0]);
                $("input[name=minHidden]").val(regs[1]);
                $("input[name=hourHidden]").val(regs[2]);
                $("input[name=dayHidden]").val(regs[3]);
                $("input[name=monthHidden]").val(regs[4]);
                $("input[name=weekHidden]").val(regs[5]);

                $.fn.cronGen.tools.initObj(regs[0], "second");
                $.fn.cronGen.tools.initObj(regs[1], "min");
                $.fn.cronGen.tools.initObj(regs[2], "hour");
                $.fn.cronGen.tools.initDay(regs[3]);
                $.fn.cronGen.tools.initMonth(regs[4]);
                $.fn.cronGen.tools.initWeek(regs[5]);

                if (regs.length > 6) {
                    $("input[name=yearHidden]").val(regs[6]);
                    $.fn.cronGen.tools.initYear(regs[6]);
                }
            }
    	},
        cronResult : function() {
            var result;
            var second = $("#secondHidden").val();
            second = second== "" ? "*":second;
            var minute = $("#minHidden").val();
            minute = minute== "" ? "*":minute;
            var hour = $("#hourHidden").val();
            hour = hour== "" ? "*":hour;
            var day = $("#dayHidden").val();
            day = day== "" ? "*":day;
            var month = $("#monthHidden").val();
            month = month== "" ? "*":month;
            var week = $("#weekHidden").val();
            week = week== "" ? "?":week;
            var year = $("#yearHidden").val();
            var item = [];
            item.push(second);
            item.push(minute);
            item.push(hour);
            item.push(day);
            item.push(month);
            item.push(week);
            if (year!="") {
                item.push(year);
            }
            //修复表达式错误BUG，如果后一项不为* 那么前一项肯定不为为*，要不然就成了每秒执行了
            //获取当前选中tab
            var currentIndex = $("#CronGenTabs li.active").index();
            //当前选中项之前的如果为*，则都设置成0
            for (var i = currentIndex; i >= 1; i--) {
            	if (item[i] != "*" && item[i - 1] == "*") {
            		if ( i-1 == 5) { //周，只能为?
            			item[i- 1] = "?";
            		} else {
            			item[i - 1] = "0";
            		}
            	}
            }
            //当前选中项之后的如果不为*则都设置成*
            if (item[currentIndex] == "*") {
            	for (var i = currentIndex + 1; i < item.length; i++) {
            		if (i == 5) { //周，只能为?
            			item[i] = "?";
            		} else {
            			item[i] = "*";
            		}
            	}
            }
            if (item[5] != "?") { //Day-of-Month和Day-of-Week中，指“没有具体的值”。当两个子表达式其中一个被指定了值以后，为了避免冲突，需要将另外一个的值设为“?”
            	item[3] = "?";
            	if (item[4] == "0") {
                	item[4] = "*";
                }
            }
            if (item[3] == "0") { //Day-of-Month和Day-of-Week中，指“没有具体的值”。当两个子表达式其中一个被指定了值以后，为了避免冲突，需要将另外一个的值设为“?”
            	item[3] = "?";
            }
            if (item[4] == "0") { //月
            	item[4] = "*";
            }
            if (item[3] == "?" && item[5] == "?") { //只能有一个
            	item[3] = "*";
            }
            result =item.join(" ");
            return result;
        },
        clearCheckbox : function(dom){
        	//清除选中的checkbox
            var list = $("."+dom+"List").children().find(":checkbox").filter(":checked");
            if ($(list).length > 0) {
            	$.each(list, function(index){
            		$(this).attr("checked", false);
            		$(this).attr("disabled", "disabled");
            		$(this).change();
            	});
            }
        },
        // cron表达式解释为字符串
        explainCron : function(value) {
        	var vals = value.replace(/  /g," ").split(" ");
        	var showHtml = "";
        	if (vals.length >= 6) {
                var secondVal = vals[0];
                var minuteVal = vals[1];
                var hourVal = vals[2];
                var dayOfMonthVal = vals[3];
                var monthVal = vals[4];
                var dayOfWeekVal = vals[5];
                var yearVal;
                if (vals.length >= 7) {
                	yearVal = vals[6];
                }
                if (yearVal) {
                    if (yearVal == "*") {
                    	showHtml += "每年";
                    } else {
                    	showHtml += yearVal+"年";
                    }
                }
                if (monthVal) {
                	if (monthVal == "*") {
                		showHtml += "每月";
                	} else if (monthVal == "?") {
                		showHtml += "";
                	} else if (monthVal.split('-').length > 1) { //范围
                		var ary = monthVal.split('-');
                		showHtml += ary[0] + "月到" + ary[1] + "月" ;
                	} else if (monthVal.split('/').length > 1) { //周期
                		var ary = monthVal.split('/');
                		showHtml += "从" + ary[0] + "月开始每" + ary[1] + "个月" ;
                	}  else {
                		var ary = monthVal.split(",");
                		for (var i = 0; i < ary.length; i++) {
                			showHtml += ary[i] + "月";
                		}
                	}
                }
                if (dayOfWeekVal) {
                    if (dayOfWeekVal == "*") {
                    	showHtml += "每周";
                    } else if (dayOfWeekVal == "?") {
                    	showHtml += "";
                    } else if (dayOfWeekVal.split('-').length > 1) { //范围
                        var ary = dayOfWeekVal.split('-');
                        showHtml += "星期" + $.fn.cronGen.tools.getWeekDay(ary[0]) + "到星期" + $.fn.cronGen.tools.getWeekDay(ary[1]) + " ";
                    } else if (dayOfWeekVal.split('#').length > 1) { //第x个周x
                    	var ary = dayOfWeekVal.split('#'); //第一个为周x第二个为第x个
                        showHtml += "第" + ary[1] + "个星期" + $.fn.cronGen.tools.getWeekDay(ary[0]) + " ";
                    } else if (dayOfWeekVal.split('L').length > 1) { //每月最后一个周x
                    	var ary = dayOfWeekVal.split('L');
                    	showHtml += "最后一个星期" + $.fn.cronGen.tools.getWeekDay(ary[0]) + " ";
                    } else {
                    	var ary = dayOfWeekVal.split(",");
                    	for (var i = 0; i < ary.length; i++) {
                            showHtml += "星期" + $.fn.cronGen.tools.getWeekDay(ary[i]) + " ";
                        }
                    }
                }
                if (dayOfMonthVal) {
                    if (dayOfMonthVal == "*") {
                    	showHtml += "每天";
                    } else if (dayOfMonthVal == "?") {
                    	showHtml += "";
                    } else if (dayOfMonthVal.split('-').length > 1) { //范围
                        var ary = dayOfMonthVal.split('-');
                        showHtml += ary[0] + "号到" + ary[1] + "号";
                    } else if (dayOfMonthVal.split('/').length > 1) { //周期
                        var ary = dayOfMonthVal.split('/');
                        showHtml += "从"+ ary[0] + "号开始每" + ary[1] + "天";
                    } else if (dayOfMonthVal.split('W').length > 1) { //x号最近的那个工作日
                    	var ary = dayOfMonthVal.split('W'); //第一个为周x第二个为第x个
                        showHtml += ary[0] + "号最近的工作日";
                    } else if (dayOfMonthVal.split('L').length > 1) { //每月最后一天
                    	showHtml += "最后一天";
                    } else {
                    	var ary = dayOfMonthVal.split(",");
                    	for (var i = 0; i < ary.length; i++) {
                            showHtml += ary[i]+"号";
                        }
                    }
                }
                if (hourVal) {
                    if (hourVal == "*") {
                    	showHtml += "每小时";
                    } else if (hourVal.split('-').length > 1) { //范围
                        var ary = hourVal.split('-');
                        showHtml += ary[0] + "点到" + ary[1] + "点";
                    } else if (hourVal.split('/').length > 1) { //周期
                        var ary = hourVal.split('/');
                        showHtml += "从"+ ary[0] + "点开始每" + ary[1] + "个小时";
                    }  else {
                    	var ary = hourVal.split(",");
                    	for (var i = 0; i < ary.length; i++) {
                            showHtml += ary[i]+"点";
                        }
                    }
                }
                if (minuteVal) {
                    if (minuteVal == "*") {
                    	showHtml += "每分钟";
                    } else if (minuteVal.split('-').length > 1) { //范围
                        var ary = minuteVal.split('-');
                        showHtml += ary[0] + "分到" + ary[1] + "分";
                    } else if (minuteVal.split('/').length > 1) { //周期
                        var ary = minuteVal.split('/');
                        showHtml += "从"+ ary[0] + "分开始每" + ary[1] + "分钟";
                    }  else {
                    	var ary = minuteVal.split(",");
                    	for (var i = 0; i < ary.length; i++) {
                            showHtml += ary[i] + "分";
                        }
                    }
                }
                if (secondVal) {
                    if (secondVal == "*") {
                    	showHtml += "每秒";
                    } else if (secondVal.split('-').length > 1) { //范围
                        var ary = secondVal.split('-');
                        showHtml += ary[0] + "秒到" + ary[1] + "秒";
                    } else if (secondVal.split('/').length > 1) { //周期
                        var ary = secondVal.split('/');
                        showHtml += "从"+ ary[0] + "秒开始每" + ary[1] + "秒";
                    }  else {
                    	var ary = secondVal.split(",");
                    	for (var i = 0; i < ary.length; i++) {
                            showHtml += ary[i] + "秒";
                        }
                    }
                }
                
                showHtml = showHtml.replace("每年每月","每月").replace("每月每天","每天").replace("每天每小时","每小时").
                	replace("每月每周","每周").replace("每周每小时","每小时").replace("每小时每分钟","每分钟").replace("每分钟每秒","每秒");
                
            }
        	return showHtml;
        },
        initCheckBox : function(dom) {
        	//移除checkbox禁用
            var list = $("."+dom+"List").children().find(":input");
            if (list.length > 0) {
            	$.each(list, function(index){
            		$(this).removeAttr("disabled");
            	});
            }
        }
    };
    
    //默认参数
    $.fn.cronGen.defaultOptions = {
        direction : 'bottom'
        //确定按钮样式
        ,submitBtnClass : 'shield-btn'
        ,showInputClass : 'shield-input'
        ,inputGroupBtnStyle : 'float:left;'
        //选择按钮html
        ,selectBtn : '<button class="shield-btn shield-btn-primary shield-btn-small"><i class="shield-icon">&#xe6f4;</i></button>'
    };
})(jQuery);
