/**
* ShieldJSRadio
* @fileOverview 单选框，需要依赖shield.main.js以及tool/doT.js，templ/radio.html
* @author kevin
* @email donggongai@126.com
* @version 1.0
* @date 2018-3-16
*/
;(function($){
	/**
	* @author kevin
	* @constructor ShieldJSRadio
	* @description 菜单样式
	* @see The <a href="#">kevin</a>
	* @example
	*	$(".nav").ShieldJSRadio();
	* @since version 1.0
	*/
	$.fn.ShieldJSRadio = function(options){
		debug(this);
		if (this.length > 0) {
			/**
			 * @description {Json} 设置参数选项
			 * @field
			 */
			var options = $.extend({}, $.fn.ShieldJSRadio.defaults, options);
			/**
			 * @description 处理函数
			 */
			return this.each(function() {
				var thisSelectE = $(this);
				var templData = {}; //模板数据
				var eleClass="";
				//var replaceHtml = '<div class="shield-unselect" ></div>';
				//thisSelectE.after(replaceHtml);
				//var shieldRadioEle = thisSelectE.next();
				var templid = thisSelectE.attr("templid")||'default'; //模板id
				var title = thisSelectE.attr("title");
				if (!title) {
					title = "";
		        }
				templData.name = title;
				templData.divname = thisSelectE.attr("name")+"";
				templData.icon = "&#xe699"; //空心圆（未选中）&#xe655; //实心圆
				//shieldRadioEle.addClass("shield-form-radio");
				//shieldRadioEle.attr("divname", thisSelectE.attr("name")+"");
				var checkedClass = "shield-form-radioed";
				//shieldRadioEle.html('<i class="shield-anim shield-icon">&#xe699;</i><span>'+title+'</span>');
				if (hasAttr(thisSelectE, "disabled")) { //样式追加
					eleClass += "shield-radio-disabled "+ShieldJS.css.DISABLED;
					//shieldRadioEle.addClass("shield-radio-disabled");
					//shieldRadioEle.addClass(ShieldJS.css.DISABLED);
				}
				templData.classes = eleClass;
				//调用模板
				var conText = doT.template(ShieldJS.templ.RADIO[templid])(templData);
				ShieldJS.debug(conText);
				thisSelectE.after(conText);
				var shieldRadioEle = thisSelectE.next();
				//事件绑定
				shieldRadioEle.on("click", function() {
					var $this = $(this);
					if (!$this.hasClass(ShieldJS.css.DISABLED)) {
						$this.addClass(checkedClass);
						var iCheckedClass = "shield-anim-scaleSpring";
						var parE = $this.closest("from");
						if (parE.length == 0) {
							parE = $this.parent();
		                }
						var otherRadioEles = parE.find("[divname='"+$this.attr("divname")+"']").not($this);
						otherRadioEles.each(function() {
							$(this).removeClass(checkedClass); //其他处理过的radio移除选中
						});
						var otherIEles = otherRadioEles.find("i");
						otherIEles.each(function() {
							$(this).removeClass(iCheckedClass).html("&#xe699;"); //设为空心圆
						});
						var thisIEle = $this.find("i");
						thisIEle.html("&#xe655;"); //设为实心圆
						thisIEle.addClass(iCheckedClass);
						
						// 其他设为false
						var otherInputEles = parE.find(":input[name='"+thisSelectE.attr("divname")+"']").not(thisSelectE);
						otherIEles.each(function() {
							$(this).prop("checked", false).triggerHandler("change",["false"]); //真实元素赋值,并触发绑定的change事件，false表示不再执行选中元素的点击防止死循环
						});
						thisSelectE.prop("checked", true).triggerHandler("change",["false"]); //真实元素赋值,并触发绑定的change事件，false表示不再执行选中元素的点击防止死循环
						ShieldJS.debug("radio选中=="+thisSelectE.prop("checked")+" radio值=="+$("[name='"+thisSelectE.attr("name")+"']:checked").val());
					}
					return false; //阻止冒泡
				});
				if (hasAttr(thisSelectE, "checked")) { //选中处理
					shieldRadioEle.click();
				}
				thisSelectE.hide(); //隐藏原始元素
				thisSelectE.change(function(event, changeShieldValue) { //值变化值将其赋值给处理元素
					changeShieldValue = changeShieldValue||"true";
					if (changeShieldValue=="true") { //防止死循环
						var $this = $(this);
						if ($this.prop("checked") != shieldRadioEle.hasClass(checkedClass)) { //选中处理
							shieldRadioEle.click();
						}
                    }
                });
			});
        }
	};
	/**
	* @description {Json} 默认参数
	* @field
	*/
	$.fn.ShieldJSRadio.defaults = {
		// 鼠标悬浮的菜单样式
		barClass : 'shield-nav-bar'
		// 条目样式
	    ,itemClass : 'shield-nav-item'
	    // 竖向样式
	    ,treeClass : 'shield-nav-tree'
	    // 竖向选中样式
		,treeItemdClass : 'shield-nav-itemed'
		// 展开收起样式，存在子菜单
		,moreClass : 'shield-nav-more'
		// 展开收起样式,显示子菜单时出现
		,moredClass : 'shield-nav-mored'
		// 子菜单样式
		,childClass : 'shield-nav-child'
		// icon图标样式
		,iconClass : 'shield-icon'
	};
	$.fn.ShieldJSRadio.filter = function(dealEle) {
		var showName = "shield-radio元素";
		if (dealEle.length > 0) {
			dealEle.each(function() {
				var $this = $(this);
				if (!$this.is(":radio")) {
					dealEle = dealEle.not($this);
					ShieldJS.error("检测到" + showName + "不是radio表单元素，请修改！"+$this.prop("outerHTML"));
					return false; //true=continue，false=break。 
				}
	        });
		}
		return dealEle;
	}
	/**
	* @description 输出选中对象的个数到控制台
	* @param {jQueryObject} $obj 选中对象
	*/
	function debug($obj) {
		if (window.console && window.console.log)
			window.console.log('ShieldJSPage selection count: ' + $obj.size());
	};
})(jQuery);