// emoji表情，组件使用
Touchjs.ueditors = {};
Touchjs.emoji = {
	emojiIcons_old : {
		alias: {weixiao:"微笑",xixi:"嘻嘻",haha:"哈哈",keai:"可爱",kelian:"可怜",wabi:"挖鼻",chijing:"吃惊",haixiu:"害羞",jiyan:"挤眼",bizui:"闭嘴",bishi:"鄙视",aini:"爱你",lei:"泪",touxiao:"偷笑",qinqin:"亲亲",shengbing:"生病",taikaixin:"太开心",baiyan:"白眼",youhengheng:"右哼哼",zuohengheng:"左哼哼",xu:"嘘",shuai:"衰",tu:"吐",haqian:"哈欠",baobao:"抱抱",nu:"怒",yiwen:"疑问",chanzui:"馋嘴",baibai:"拜拜",sikao:"思考",han:"汗",kun:"困",shui:"睡",qian:"钱",shiwang:"失望",ku:"酷",se:"色",heng:"哼",guzhang:"鼓掌",yun:"晕",beishang:"悲伤",zhuakuang:"抓狂",heixian:"黑线",yinxian:"阴险",weiqu:"委屈",numa:"怒骂",hufen:"互粉",shudaizi:"书呆子",fennu:"愤怒",ganmao:"感冒",xin:"心",shangxin:"伤心",zhu:"猪",xiongmao:"熊猫",tuzi:"兔子",ok:"OK",ye:"耶",good:"GOOD",no:"NO",zan:"赞",lai:"来",ruo:"弱",caonima:"草泥马",shenma:"神马",jiong:"囧",fuyun:"浮云",geili:"给力",weiguan:"围观",weiwu:"威武",huatong:"话筒",lazhu:"蜡烛",dangao:"蛋糕",fahongbao:"发红包"},
		title: {weixiao:"微笑",xixi:"嘻嘻",haha:"哈哈",keai:"可爱",kelian:"可怜",wabi:"挖鼻",chijing:"吃惊",haixiu:"害羞",jiyan:"挤眼",bizui:"闭嘴",bishi:"鄙视",aini:"爱你",lei:"泪",touxiao:"偷笑",qinqin:"亲亲",shengbing:"生病",taikaixin:"太开心",baiyan:"白眼",youhengheng:"右哼哼",zuohengheng:"左哼哼",xu:"嘘",shuai:"衰",tu:"吐",haqian:"哈欠",baobao:"抱抱",nu:"怒",yiwen:"疑问",chanzui:"馋嘴",baibai:"拜拜",sikao:"思考",han:"汗",kun:"困",shui:"睡",qian:"钱",shiwang:"失望",ku:"酷",se:"色",heng:"哼",guzhang:"鼓掌",yun:"晕",beishang:"悲伤",zhuakuang:"抓狂",heixian:"黑线",yinxian:"阴险",weiqu:"委屈",numa:"怒骂",hufen:"互粉",shudaizi:"书呆子",fennu:"愤怒",ganmao:"感冒",xin:"心",shangxin:"伤心",zhu:"猪",xiongmao:"熊猫",tuzi:"兔子",ok:"OK",ye:"耶",good:"GOOD",no:"NO",zan:"赞",lai:"来",ruo:"弱",caonima:"草泥马",shenma:"神马",jiong:"囧",fuyun:"浮云",geili:"给力",weiguan:"围观",weiwu:"威武",huatong:"话筒",lazhu:"蜡烛",dangao:"蛋糕",fahongbao:"发红包"}
	},
	emojiIcons_tieba : {
		alias: {1:"hehe",2:"haha",3:"tushe",4:"a",5:"ku",6:"lu",7:"kaixin",8:"han",9:"lei",10:"heixian",11:"bishi",12:"bugaoxing",13:"zhenbang",14:"qian",15:"yiwen",16:"yinxian",17:"tu",18:"yi",19:"weiqu",20:"huaxin",21:"hu",22:"xiaonian",23:"neng",24:"taikaixin",25:"huaji",26:"mianqiang",27:"kuanghan",28:"guai",29:"shuijiao",30:"jinku",31:"shengqi",32:"jinya",33:"pen",34:"aixin",35:"xinsui",36:"meigui",37:"liwu",38:"caihong",39:"xxyl",40:"taiyang",41:"qianbi",42:"dnegpao",43:"chabei",44:"dangao",45:"yinyue",46:"haha2",47:"shenli",48:"damuzhi",49:"ruo",50:"OK"},
		title:{1:"呵呵",2:"哈哈",3:"吐舌",4:"啊",5:"酷",6:"怒",7:"开心",8:"汗",9:"泪",10:"黑线",11:"鄙视",12:"不高兴",13:"真棒",14:"钱",15:"疑问",16:"阴脸",17:"吐",18:"咦",19:"委屈",20:"花心",21:"呼~",22:"笑脸",23:"冷",24:"太开心",25:"滑稽",26:"勉强",27:"狂汗",28:"乖",29:"睡觉",30:"惊哭",31:"生气",32:"惊讶",33:"喷",34:"爱心",35:"心碎",36:"玫瑰",37:"礼物",38:"彩虹",39:"星星月亮",40:"太阳",41:"钱币",42:"灯泡",43:"茶杯",44:"蛋糕",45:"音乐",46:"haha",47:"胜利",48:"大拇指",49:"弱",50:"OK"}
	}
}
Touchjs.modules = {
	valueDefault : {
		expression : ":input[data-v]",
		fn: function(ele) {
			var dealEle = findAndMarkEle(ele, this.expression);
			if(dealEle.length > 0){
				dealEle.each(function() {
					var $this = $(this);
					var value = $this.data("v");
					if(typeof value != 'undefined'){ // 定义了就执行
						setInputValue($this);
						// 自定义加载完成后回调
						Touchjs.loadCallback($this, "valuedefault");
	                }
				});
			}
		}
		,description : "默认值处理"
	},
	showAndHide : {
		expression : ".touchjs-showhide",
		fn: function(ele) {
			var expression = this.expression;
			var dealEle = findAndMarkEle(ele, expression);
			if(dealEle.length > 0){
				dealEle.each(function() {
					var $this = $(this);
					
					var target = $this.data("target"); // 内容取不到通过元素取
					if (target) {
	                    
						var targetE = getjQueryObj(target);
						
						if (targetE.length>0) {
							var showMethodFns = $this.data("show")||"mouseenter"; //如click mouseenter
							var hideMethodFns = $this.data("hide")||"mouseleave"; //如unfocus
							
							
							var showMethods = showMethodFns.split(" ");
							var hideMethods = hideMethodFns.split(" ");
							
							$.each(showMethods, function(i, showMethod) {
								$this.on(showMethod+".touchjs_hover",function(){
									targetE.show(); //添加
								});
							});
							$.each(hideMethods, function(i, hideMethod) {
								
								if(('' + hideMethod).indexOf('unfocus') > -1) { //不能获取焦点，变动到html的鼠标点击上
									$this.on("mousedown.touchjs_hover touchstart.touchjs_hover", function(){ //点击到自己身上时不冒泡
										return false;
									});
									$this.closest('html').on("mousedown.touchjs_hover touchstart.touchjs_hover", function() {
										targetE.hide();
									});
								} else {
									
									$this.on(hideMethod+".touchjs_hover",function(){
										targetE.hide();
									});
								}
							});
	                    } else {
	                    	Touchjs.log(expression+"定义的target："+target+"不存在！");
	                    }
	                } else {
	                	Touchjs.log(expression+"未定义target");
	                }
					
					
				});
			}
		}
		,description : "切换显示（如移入显示，移出隐藏）"
	},
	// 编辑器【使用了百度编辑器】
	ueditor : {
		expression : "textarea.ueditor,script.ueditor,.touchjs-editor",
		fn: function(ele) {
			var dealEle = findAndMarkEle(ele, this.expression);
			if(dealEle.length > 0){
				if(typeof UE == "undefined" ){
					Touchjs.error("缺少依赖JS库：ueditor！"+dealEle.html());
					return;
				}
				
				dealEle.each(function() {
					var $this = $(this);
					var edId = $this.attr("id");
					$this.attr("ueditorid", edId);
					if (Touchjs.ueditors[edId]) {
						removeUeditor(edId);
					}
					var ueditor = UE.getEditor(edId);
					Touchjs.ueditors[edId] = ueditor;
					setTimeout(function(){
						// ueditor偷偷的把ctrl+enter方法进行了表单提交处理，此处重新处理
						$("#"+edId).find("iframe").contents().find(":root").find("body").off('keydown.touchjs').on('keydown.touchjs', function(e){
							var key = e.which;
							if (event.ctrlKey || event.metaKey) {
								if (key == Touchjs.keyCode.ENTER) { // 回车
									var formE = $this.closest("form");
									if (formE.length>0 && formE.find(":submit").length > 0) { // 有提交按钮
										formE.submit();
	                                }
									return false;
								}
							}
						});
					}, 100);
					// 自定义加载完成后回调
					Touchjs.loadCallback($this, "editor");
				});
			}
		}
		,description : "百度编辑器"
	},
	// 头像功能
	avatar : {
		expression : ".touchjs-avatar",
		fn: function(ele) {
			var avatarEles = findAndMarkEle(ele, this.expression);
			avatarEles.each(function() {
				var defaultAvatar = ctxPath +"/bbs/v2/images/portrait.gif";
								
				var $this = $(this);
				var src = $this.attr("src");
				var colors = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#f1c40f",
                	"#e67e22", "#e74c3c", "#eca0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"];
				
				//显示第一个字作为头像
				if($this.data("dashow")){
					var headImgE = $this;
			        if(!src && headImgE.is("img")){ //没有路径时,并且需要是图片
			        	var canvas = document.createElement("canvas");
				        var context = canvas.getContext('2d');
				        if(context){ //支持canvas才继续，否则不需要继续了
				        	var uname = headImgE.data("dashow")||"无名氏";
				        	var width = headImgE.data("dawidth")||headImgE.attr("width")||90;
					        var name = uname.charAt(0);
					        var fontSize =  Math.round(width/2);
					        var fontWeight = 'bold';
					        canvas.width = width;
					        canvas.height = width;
					        
					        var narrs = String(uname).toUpperCase().split(" ");
							// 去第一个字符，如果不存在则返回？
							var firstchar = narrs.length == 1 ? narrs[0] ? narrs[0].charAt(0) : "?" : narrs[0].charAt(0) + narrs[1].charAt(0);
							window.devicePixelRatio && (width *= window.devicePixelRatio); //该 Window 属性 devicePixelRatio 能够返回当前显示设备的物理像素分辨率与 CSS 像素分辨率的比率。此值也可以解释为像素大小的比率：一个 CSS 像素的大小与一个物理像素的大小的比值。简单地说，这告诉浏览器应该使用多少个屏幕的实际像素来绘制单个 CSS 像素。
							var charCode = ("?" == firstchar ? 72 : firstchar.charCodeAt(0) ) - 64;
							var colorIndexr = charCode % 20;
							context.fillStyle = colors[colorIndexr - 1];
				        	
					        //context.fillStyle = '#1abc9c';
					        context.fillRect(0, 0, canvas.width, canvas.height);
					        context.fillStyle = '#FFF';
					       	context.font = fontWeight + ' ' + fontSize + 'px sans-serif';
							//context.font = fontWeight + ' ' + fontSize + "px Arial";
					        context.textAlign = 'center';
					        //context.textBaseline="middle";
					        //(text,x,y,maxWidth); text输出的文本。x:开始绘制文本的x坐标位置（相对于画布）。y开始绘制文本的 y 坐标位置（相对于画布） maxWidth:可选。允许的最大文本宽度，以像素计。
					        //var fillx = width/2;
					        //context.fillText(name, fillx, fillx); 
					        if(25 > width ){
					        	context.fillText(firstchar, width / 2, width / 1.37);
					        } else if("U" === firstchar || "V" === firstchar || "W" === firstchar){
					        	context.fillText(firstchar, width / 2, width / 1.42); 
					        } else {
					     		context.fillText(firstchar, width / 2, width / 1.48);
					        }
					        defaultAvatar = canvas.toDataURL("image/png");
					        headImgE.width(width);
				        }
						canvas = null;
			        } else if(!headImgE.is("img")){
			        	ShieldJS.error("avatar组件不是img元素！");
			        }
				} 
				
				if (!src) { // 没有图片时加载默认图片
					$this.attr("src", defaultAvatar);
		        }
				$this.on('error', function(){ // 图片加载错误时加载默认图片
					$this.attr('src', defaultAvatar);
					$this.off('error'); // 取消绑定，防止一直加载
				});
				
				// 自定义加载完成后回调
				Touchjs.loadCallback($this, "avatar");
			});
		}
		,description : "头像"
	},
	ajax : {
		expression : ".touchjs-ajax",
		touch : function(ele, containerE, opreateName) {
			var $this = ele;
			var url = $this.data("url");
			var method = $this.data("method");
			var params = getParams( $this, {});
			params = $.extend(true, params, $this.paramSelector());
			//params = getExtendSettings( $this, params);
			delete params["url"];//删除属性
			//delete params["title"];//删除属性
			delete params["shieldParams"];//删除属性
			//console.log("params")
			//console.log(params)
			
			var dialog = Touchjs.dialog;
			var successDo = successDo||Touchjs.successDo.refresh;;
			if (hasAttr($this, "successDo")) { //form上保存成功后的处理
				successDo = $this.attr("successDo");
			}
			// 查找是否存在点击元素和搜索表单
			var clickEle = $this.data("successdo-click");
			if (clickEle) {
				clickEle = getjQueryObj(clickEle);
	        }
			var searchForm = $this.data("successdo-searchform");
			if (searchForm) {
				searchForm = getjQueryObj(searchForm);
	        }
			// function(url, params, containerE, invokeEle, searchForm, clickEle, shieldDialog, successDo, methodType, successCallback)
			Touchjs.postData(url, params, containerE, $this, searchForm, clickEle, dialog, successDo, method, function(){
				// 自定义加载完成后回调
				Touchjs.loadCallback($this, "ajax");
			});
	    },
		deal : function(ele, expression, namespace, defaultConfirmMsg, opreateName) {
			var ajax = this; 
			Touchjs.dynamicBind(ele, "click", namespace, expression, function(thisE) {
				var $this = thisE;
				
				var confirmMsg = $this.data("title")||defaultConfirmMsg;
				if (confirmMsg=="none"||confirmMsg=="no") { //不显示确认信息
					ajax.touch($this, ele, opreateName);
	            } else {
	            	Touchjs.confirm("确认信息", confirmMsg, function(){
	            		ajax.touch($this, ele);
	            	}, function() {
	            		
	            	});
	            	
	            }
			});
		},
		fn: function(ele) {
			this.deal(ele, this.expression, ".Touchjs.ajax", "确定执行该操作？", "操作");
		},
		description : "ajax操作"
	},
	// 删除功能
	del : {
		expression : ".touchjs-del",
		fn: function(ele) {
			Touchjs.modules.ajax.deal(ele, this.expression, ".Touchjs.del", "确定删除该信息吗？", "删除");
		}
		,description : "ajax删除"
	},
	// 翻页，改自林昊写的方法
	page : {
		expression : ".touchjs-page a",
		fn: function(ele) {
			var namespace = ".Touchjs.page";
			var expression = this.expression;
			
			function checkpage(pageNum, totalpage) {
				if (pageNum - 1 < 0)
					pageNum = 1;
				if (totalpage && pageNum > totalpage )
					pageNum = totalpage;
				return pageNum;
			}
			
			Touchjs.dynamicBind(ele, "click", namespace, expression, function(thisE) {
				var $this = thisE;
				var parE = $this.closest(".touchjs-page");
			//ele.off("click"+namespace, this.expression).on("click"+namespace, this.expression, function() {
				//var $this = $(this);
				
				if ($this.closest(".no-common-page").length == 0) { // 如果有no-common-page则不执行通用的翻页操作
					var pageNum = $this.data("v");
					if (pageNum) {
						// 3种情况
						// 1、包含自动提交表单的，替换页码后触发表单提交事件
						// 2、包含pjax的，替换页码后触发pjax的点击事件
						// 3、其他情况，原url替换页面后直接刷新页面
						// 自动提交表单
						var formE = $this.closest('.touchjs-submitform'); //情况1：在表单内则提交表单
						if (formE.length>0) {
							var pageNumE = formE.find('.pageNum');
							pageNum = checkpage(pageNum);
							pageNumE.val(pageNum);
							formE.submit();
							/**返回顶部--用户提现效果 */
							$('html, body').animate({ scrollTop: formE.offset().top - 10 }, 300);
						} else {
							// 普通url
							var url = parE.data("pageurl")||$this.data("pageurl")||location.href;
							var pageUrl = changeURLArg(url, "p", pageNum);
							if ($this.closest("[data-pjax-target]").length > 0) { //情况2：检查上级是否存在pjax
								var pjaxUrlEle = $this.closest("[data-pjax-url]"); // 指定url
								if (pjaxUrlEle.length > 0){ //对指定的url进行页码替换
									url = pjaxUrlEle.data("pjax-url")||pjaxUrlEle.data("url")||pageUrl;
									pageUrl = changeURLArg(url, "p", pageNum);
								}
								$this.data("url", pageUrl); // 设置url
								return Touchjs.modules.pjax.doPjax($this);
							}
							
							location.replace(pageUrl); // 情况3
						}
	                }
				}
				// 自定义加载完成后回调
				Touchjs.loadCallback($this, "page");
			});
			/** changeURLArg 替换url参数，没有的话就添加，有的话就进行修改，动态添加是？还是&   */
			/** url     ：  页面地址栏URL            */
			/** arg     ：  参数名字，这里是pageNum   */
			/** arg_val ：  要替换的值               */
			var changeURLArg = function(url, arg, arg_val){
				var pattern = arg+'=([^&]*)';
				var replaceText = arg+'='+arg_val;
				if (url.match(pattern)) {
					var tmp='/('+ arg+'=)([^&]*)/gi';
					tmp=url.replace(eval(tmp), replaceText);
					return tmp;
				} else {
					if(url.match('[\?]')){
						return url+'&'+replaceText;
					}else{
						return url+'?'+replaceText;
					}
				}
			}
		}
		,description : "翻页，改自林昊写的方法"
	},
	// 日期插件
	date : {
		expression : "input.Wdate,.touchjs-date,.touchjs-datetime",
		fn: function(ele) {
			var dealEle = findAndMarkEle(ele, this.expression);
			if (dealEle.length > 0) {
				try {
					WdatePicker;
				} catch (e) {
					Touchjs.error("缺少依赖JS库：My97DatePicker！"+dealEle.html(), e);
					return false;
				}
				var startDate = findAndMarkEle(ele, 'input[init="startDate"]');
				var nowDate = findAndMarkEle(ele, 'input[init="nowDate"]');
				if (startDate.length > 0) {
					startDate.each(function() {
						var $this = $(this); 
						var pattern = $this.data("datefmt");
						if (pattern) {
							$this.val(dateFormat(new Date(), pattern));
						} else {
							$this.val(dateFormat(new Date(), 'yyyy-MM-01'));
						}
					});
				}
				if (nowDate.length > 0) {
					nowDate.each(function() {
						var $this = $(this); 
						var pattern = $this.data("datefmt");
						if (pattern) {
							$this.val(dateFormat(new Date(), pattern));
						} else {
							$this.val(dateFormat(new Date(), 'yyyy-MM-dd'));
						}
					});
				}
				dealEle.each(function() {
					var $this = $(this);
					var options = {};
					var pattern = $this.data("datefmt");
					if ($this.hasClass("touchjs-datetime")) {
						pattern = "yyyy-MM-dd HH:mm:ss"; //设置显示格式
		            }
					var onpicked = $this.data("onpicked");
					if (pattern) {
						options.dateFmt = pattern;
					}
					if (onpicked) {
						options.onpicked = onpicked;
					} else {
						options.onpicked = function(dp) {
			                $(this).triggerHandler("change");//触发自定义change事件
		              };
					}
					options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
					//console.log(options)
					$this.on("click.Touchjs.wdate", function() {
						WdatePicker(options);
					});
					// 自定义加载完成后回调
					Touchjs.loadCallback($this, "date");
				});
			}
		}
		,description : "日期插件"
	},
	// 日期格式化
	fmttime : {
		expression : ".touchjs-fmttime",
		fn: function(ele) {
			var dealEles = findAndMarkEle(ele, this.expression);
			
			if (dealEles.length > 0) {
				if(typeof moment == "undefined"){ //判断moment是否已经定义,语法参考http://momentjs.cn/docs/
					Touchjs.error("检测到shieldDatetime元素缺少依赖JS库：moment.js！");
					return;
				}
				dealEles.each(function() {
					var $this = $(this);
					var datetime = $this.text();
					if (datetime) {
						var title = $this.data("title");
						if (!title) {
							$this.attr("title", title);
	                    }
						$this.data("olddata", datetime);
						var receivedDate = moment(datetime); //日期
						var formatVal = $this.data("momentFmt") || $this.data("format") || $this.data("fmt"); //后面的参数是为了防止前面参数冲突时;
						if (!formatVal) {
							formatVal = "YYYY-MM-DD HH:mm:ss";
		                }
						var messageHtml = receivedDate.format(formatVal); //格式化
						var showtime= ($this.data("showtime")+""!="false");
						if(!showtime){
							messageHtml = "";
						}
						var fromnow= $this.data("fromnow")||"show";
						if ((fromnow != "none" && fromnow != "hide" && fromnow != "close") || !showtime) { //时间 和距现在的实际必显示一个
							if(showtime){
								messageHtml += ' ('
							}
							messageHtml += receivedDate.fromNow();
							if(showtime){
								messageHtml += ')';
							}
	                    }
						$this.text(messageHtml);
		            }
					// 自定义加载完成后回调
					Touchjs.loadCallback($this, "fmttime");
		        })
			}
		}
		,description : "时间格式化显示（依赖moment.js）"
	},
	// 搜索自动补全
	autoSuggest : {
		expression : "input.touchjs-autoSuggest",
		fn: function(ele) {
			var eles = findAndMarkEle(ele, this.expression);
			eles.each(function() {
	            var $this = $(this);
	            var options = {}; // {offset:{left:-36, top:5}};
	            options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
	            $this.autoSuggest(options);
	            // 自定义加载完成后回调
				Touchjs.loadCallback($this, "autosuggest");
	        });
		}
		,description : "搜索自动补全"
	},
	// emoji表情插入
	emoji: {
		expression : ".touchjs-emoji",
		fn: function(ele) {
			var dealEles = findAndMarkEle(ele, this.expression);
			
			if (dealEles.length > 0) {
				if (!jQuery().emoji) {
					Touchjs.error("检测到"+this.expression+"元素缺少依赖JS库：jquery.emoji.js！");
		            return;
		        }
				//调用用
				var emojiIcons = [{
					name: "旧表情",
					path: ctxPath + "/bbs/v2/lib/emoji/img/old/",
					maxNum: 73,
					file: ".gif",
					placeholder: "\[{alias}\]",
					alias: Touchjs.emoji.emojiIcons_old.alias,
					title: Touchjs.emoji.emojiIcons_old.title
				},{
					name: "贴吧表情",
					path: ctxPath + "/bbs/v2/lib/emoji/img/tieba/",
					maxNum: 50,
					file: ".jpg",
					placeholder: ":{alias}:",
					alias: Touchjs.emoji.emojiIcons_tieba.alias,
					title: Touchjs.emoji.emojiIcons_tieba.title
				}, {
					name: "QQ表情",
					path: ctxPath + "/bbs/v2/lib/emoji/img/qq/",
					maxNum: 91,
					file: ".gif",
					placeholder: "#qq_{alias}#"
				}];
				
				dealEles.each(function() {
					var $this = $(this);
					var button = $this.data("button");
					var options = {
						button: button,
						showTab: true,
						animation: 'slide',
						icons: emojiIcons};
					options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
					$(button).on("click", function() {
						var btnEle = $(this);
						var init = btnEle.data("init");
						if (!init) {
							btnEle.data("init", "init");   
							$this.emoji(options);
						}
					});
					// 自定义加载完成后回调
					Touchjs.loadCallback($this, "emoji");
				});
			}
		}
		,description : "emoji表情插入"
	},
	// emoji解析
	emojiParse : {
		expression : ".touchjs-emojiParse",
		fn: function(ele) {
			var dealEles = findAndMarkEle(ele, this.expression);
			
			if (dealEles.length > 0) {
				if (!jQuery().emojiParse) {
					Touchjs.error("检测到"+this.expression+"元素缺少依赖JS库：jquery.emoji.js！");
		            return;
		        }
				// 解析用
				var emojiIconsParse = [{
					name: "旧表情",
					path: ctxPath + "/bbs/v2/lib/emoji/img/old/",
					maxNum: 73,
					file: ".gif",
					placeholder: "\\[{alias}\\]",
					alias: Touchjs.emoji.emojiIcons_old.alias,
					title: Touchjs.emoji.emojiIcons_old.title
				},{
					name: "贴吧表情",
					path: ctxPath + "/bbs/v2/lib/emoji/img/tieba/",
					maxNum: 50,
					file: ".jpg",
					placeholder: "\\:{alias}\\:",
					alias: Touchjs.emoji.emojiIcons_tieba.alias,
					title: Touchjs.emoji.emojiIcons_tieba.title
				}, {
					name: "QQ表情",
					path: ctxPath + "/bbs/v2/lib/emoji/img/qq/",
					maxNum: 91,
					file: ".gif",
					placeholder: "#qq_{alias}#"
				}];
				
				dealEles.each(function() {
					var $this = $(this);
					var options = {icons: emojiIconsParse};
					options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
					$this.emojiParse(options);
					// 自定义加载完成后回调
					Touchjs.loadCallback($this, "emojiparse");
				});
			}
		}
		,description : "emoji解析"
	},
	// 弹出框
	dialog : {
		expression : ".touchjs-dialog",
		callback : function(ele, dialogE, dialog) {
			if (ele.data("cache")) { // 设置缓存
				// 扩展弹出框关闭的方法，关闭后保留html内容
				dialogE.data("extendRemove", function(index, layeroEle){
					var html = dialogE.find(".layui-layer-content").formhtml();
					ele.attr("cachehtml", true);
					ele.data("cachehtml", html); //关闭后保存html
				});
	        }
			var successDo = Touchjs.successDo.refresh;
			if (hasAttr(ele, "successDo")) { //保存成功后的处理
				successDo = ele.attr("successDo");
			}
			if (!dialog) {
				dialog = Touchjs.dialog;
	        }
			Touchjs.form.initSubmit(dialogE, function(formE, targetE, data) {//成功后
				Touchjs.formSubmitCallback(successDo, formE, targetE, data, null, null, dialog);
			}); 
			// 自定义加载完成后回调
			Touchjs.loadCallback(ele, "dialog");
	    },
		fn: function(ele) {
			var namespace = this.expression;
			var expression = namespace;
			Touchjs.dynamicBind(ele, "click", namespace, expression, function(thisE) {
				var $this = thisE;
			//ele.off("click"+namespace, this.expression).on("click"+namespace, this.expression, function() {
				//var $this = $(this);
				
				var dialog = Touchjs.dialog;
				
				var url = $this.data("url");
				var title = $this.data("title")||"详情";
				var params = getParams($this, {});
				params = $.extend(true, params, $this.paramSelector());
				var width = $this.data("width")||600;
				var options = {area: width+'px'};
				options = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
				
				if ($this.data("cache")) { // 使用缓存,打开一次后不再重复发起请求
					
					// 点击右上角关闭按钮,重置表单，防止不想提交的进入表单里
					options.cancel = function(index, layero){
						var layeroEle = $(layero);
						layeroEle.find("form").each(function() {
	                        $(this)[0].reset(); 
	                    });
						dialog.close();
						return true; 
					};
	                
					if ($this.data("cachehtml")) { //已经缓存过了，直接显示内容
						var html = $this.data("cachehtml");
						Touchjs.dialog.open(title, html, function(dialogE, index) {
							Touchjs.modules.dialog.callback($this, dialogE, dialog)
	                    }, options);
	                    return;
	                }
	            }
				// 普通查询
				var method = $this.data("method");
				
				if(url){
					
					if (method == "post") {
						Touchjs.ajax.post(url, params,  function(data) {
							Touchjs.dialog.open(title, data, function(dialogE, index) {
								Touchjs.modules.dialog.callback($this, dialogE, dialog)
	                        }, options);
						});
					} else {
						Touchjs.ajax.get(url, params,  function(data) {
							Touchjs.dialog.open(title, data, function(dialogE, index) {
								Touchjs.modules.dialog.callback($this, dialogE, dialog)
	                        }, options);
						});
					}
					
					/*if (method == "post") {
						$.post(url, params, function(data) {
							Touchjs.dialog.open(title, data, function(dialogE, index) {
								Touchjs.modules.dialog.callback($this, dialogE, dialog)
	                        }, options);
						});
					} else {
						$.get(url, params, function(data) {
							Touchjs.dialog.open(title, data, function(dialogE, index) {
								Touchjs.modules.dialog.callback($this, dialogE, dialog)
	                        }, options);
						});
					}*/
				} else if($this.data("target")){ //直接显示内容
					Touchjs.dialog.open(title, getjQueryObj($this.data("target")).html(), function(dialogE, index) {
						// 赋值
						if(!$.isEmptyObject(params)){ //非空
							for ( var key in params) {
								var pvalue = params[key];
								dialogE.find('#'+key).val(pvalue); // id
								dialogE.find('[name="'+key+'"]').val(pvalue); // name
								dialogE.find('[value-to="'+key+'"]').val(pvalue); // value-to属性
							}
						}
						// 回调
						Touchjs.modules.dialog.callback($this, dialogE, dialog)
	                }, options);
				}
				
				
				
			});
				
		}
		,description : "弹出框"
	},
	// 提示
	tips : {
		expression : ".touchjs-tips",
		fn: function(ele) {
			var dealEles = findAndMarkEle(ele, this.expression);
			
			if (dealEles.length > 0) {
				dealEles.each(function() {
					var $this = $(this);
					var content = $this.data("content"); //取内容
					if (!content) {
	                    var source = $this.data("target"); // 内容取不到通过元素取
	                    var targetE = $this.next(source);
	                    if(targetE.length == 0){ // 先后再前，再找同辈元素
	                    	targetE = $this.prev(source);
	                    }
	                    if(targetE.length == 0){ // 再找同辈元素
	                    	targetE = $this.siblings(source);
	                    }
	                    if(targetE.length == 0){ // 依然找不到直接整个dom搜索
	                    	targetE = $(source);
	                    }
	                    content = targetE.html();
	                }
					if (content) {
						var options = getExtendSettings($this, {}); //data-xx可能会覆盖取到的值
						Touchjs.tips($this, content, options);
						// 自定义加载完成后回调
						Touchjs.loadCallback($this, "tips");
	                }
	            });
				
			}
				
		}
		,description : "提示框"
	},
	// pjax局部刷新
	pjax : {
		expression : ".touchjs-pjax",
		doPjax : function(ele) {
			var beforeFn = ele.data("before"); //前置处理
			var continueFn = true;
			if (beforeFn && $.isFunction(beforeFn)) {
				continueFn = beforeFn();
	        }
			if (continueFn) {
				var parentE = ele.closest("[data-pjax-target]");
				if (parentE.length == 0) {
					parentE = ele.closest("[data-pjax-url]");
	            }
				
				var url = ele.data("url");
				if (!url) {
					url = ele.attr("href");
	            }
				if (!url) {
					url = parentE.data("pjax-url");
	            }
				var params = getParams(ele, {});
				var extendParams = {};
				if (hasAttr(ele, "paramSelector")) {
					extendParams = ele.paramSelector();
	            } else if (parentE.length > 0 && hasAttr(parentE, "paramSelector")){
	            	extendParams = parentE.paramSelector();
	            }
				params = $.extend(true, params, extendParams);
				
				var target = ele.data("target");
				if (!target) {
					target = parentE.data("pjax-target");
	            }
				
				if (!$.isEmptyObject(params)) {
					if (url.indexOf("?")==-1) {
						url += '?'; 
					} else {
						url += '&';
					}
					url += $.param(params);
				}
				//console.log(url);
				//console.log(target);
				var options = getExtendSettings(ele, {}); //data-xx可能会覆盖取到的值
				options = params = $.extend(true, options, {url: url, container: target});
				
				$.pjax(options);
				return false; //阻止a标签的默认动作
	        }
			return true;
	    },
		fn: function(ele) {
			var dealEles = findAndMarkEle(ele, this.expression);
			
			if (dealEles.length > 0) {
				if (!jQuery().pjax) {
					Touchjs.error("检测到"+this.expression+"元素缺少依赖JS库：jquery.pjax.js！");
					return;
				}
				dealEles.each(function() {
					var $this = $(this);
					$this.on("click.touchjs-pjax", function() {
						return Touchjs.modules.pjax.doPjax($this);
					});
					// 自定义加载完成后回调
					Touchjs.loadCallback($this, "pjax");
	            });
				
			}
				
		}
		,description : "pjax局部刷新【必须加到a元素上，否则无效】"
	},
	// 局部载入页面
	load : {
		expression : ".touchjs-load",
		fn: function(ele) {
			var dealEles = findAndMarkEle(ele, this.expression);
			
			if (dealEles.length > 0) {
				// 加载数据
				var loadData = function(method, ele, url, params) {
					var hasJSTmpl = false||ele.data("jstmpl")||ele.find(".jstmpl").length;//js模板
					var jsTmplHtml = ""; //js模板内容
					var jsTmplResultHtml = "";
					
					if(hasJSTmpl){
						jsTmplHtml = ""||ele.data("jstmpl")||ele.find(".jstmpl").text(); //js模板内容
						if (ele.find(".jstmpl").length>0) {
							ele.data("jstmpl", ele.find(".jstmpl").text());
	                    }
					}
						
					ele.html(Touchjs.loadingHtml); //加载中
					
					// 成功后的回调
					var successload = function(ele) {
						dealWidth(ele);
						ele.find(".closebtn").on("click", function() {
							closeTarget(targetEle); //关闭
	                    });
						Touchjs.touch(ele);
						// 自定义加载完成后回调
						Touchjs.loadCallback(ele, "load");
	                }
					
					if (method == "post") {
						if(hasJSTmpl){ //使用js模板的情况
							Touchjs.ajax.post(url, params,  function(result) {
								var fieldName = ele.data("field")||"resultList"; //字段
								// jsTmplResultHtml = doT.template(jsTmplHtml)(result.data[fieldName]); //处理js模板
								// function(str, data, ele)
								jsTmplResultHtml = Touchjs.template(jsTmplHtml, result.data[fieldName], ele); //处理js模板
								ele.html(jsTmplResultHtml);
								successload(ele);
							});
						} else {
							Touchjs.ajax.post(ele, url, params,  function() {
								successload(ele);
							});
						}
					} else {
						if(hasJSTmpl){
							Touchjs.ajax.get(url, params,  function(result) {
								var fieldName = ele.data("field")||"resultList"; //字段
								// function(str, data, ele)
								jsTmplResultHtml = Touchjs.template(jsTmplHtml, result.data[fieldName], ele); //处理js模板
								ele.html(jsTmplResultHtml); //ele.find(".jstmpl").replaceWith(jsTmplResultHtml);
								successload(ele);
							});
						} else {
							Touchjs.ajax.get(ele, url, params,  function() {
								successload(ele);
							});
						}
					}
	            }
				// 宽度计算
	            var dealWidth = function(targetEle){
	            	var targetEleWidth = targetEle.data("width");
					if (targetEleWidth) {
						var prevEle = targetEle.prev();
						if (prevEle.find(expression).length>0) { //前一个元素包含load组件时才处理，否则不处理
							var orgWidth = prevEle.data("org--width");
							var widthPrev = orgWidth;
							if (orgWidth) {
								widthPrev = orgWidth;
							} else {
								widthPrev = prevEle.outerWidth(true); //包括外边框
								prevEle.data("org--width", widthPrev);
							}
							var widthTotal = widthPrev ; // + targetEle.outerWidth(true)
							
							targetEle.css("width", targetEleWidth);
							prevEle.width(widthTotal - targetEle.outerWidth(true) -10);
	                    }
						
	                }
	            }
	            var closeTarget = function(targetEle){
					var prevEle = targetEle.prev();
					var orgWidth = prevEle.data("org--width");
					if (orgWidth) {
						targetEle.hide();
						prevEle.width(orgWidth);
	                } 
	            }
				
				dealEles.each(function() {
					var $this = $(this);
					var url = $this.data("url");
					var params = getParams($this, {"_tr":1});
					params = $.extend(true, params, $this.paramSelector());
					
					var method = $this.data("method")||"get";
					var target = $this.data("target");
					var targetE = $this;
					if (target) {
						targetE = getjQueryObj(target);
	                }
					
					loadData(method, targetE, url, params);
					// 刷新,
					var refresh = $this.data("refresh"); //获取刷新元素
					if (refresh) {
						var refreshEle = getjQueryObj(refresh);
						if(refreshEle.length > 0){
							refreshEle.on("click", function() {
								var refreshParams = $.extend(true, params, $this.paramSelector()); //获取最新参数
								refreshParams = $.extend(true, refreshParams, $this.data("refreshParams")||{}); //data-refresh-params刷新时额外的参数
								loadData(method, targetE, url, refreshParams);
	                        });
						} else{
							Touchjs.debug("touchjs-load的刷新选择器没有匹配到元素："+refresh);
						}
	                }
					
					/*$this.load(url, params, function(){
						*//**由于图像处理未作异步加载处理，需重新调用一下工具类 *//*
						Touchjs.touch($this);
					});*/
				});
			}
				
		}
		,description : "局部载入页面"
	},
	/**	a标签附加参数(如果不是新打开的页面,为Get方式提交需要注意参数长度) */
	a : {
		expression : '.touchjs-a',
		fn : function(contentE){
			findAndMarkEle(contentE, this.expression).each(function() {
				var $this = $(this);
				if($this.data("url")){ //有链接才处理
					$this.click(function() {
						var url = $this.data("url");
						var params={};
						var checkParams = $this.paramSelector(); //参数选择器
						if (!checkParams) {
							return true;
						} else {
							params = checkParams;
						}
						params = getParams($this, params);
						console.log(params);
						var method = $this.data("method")||"get";
						var target = $this.data("target")||$this.attr("target");
						if (target) {
							if (target == "_blank") {
								openNewPage($this, url, params); //shield.util.js 中的方法
								// 自定义点击回调
								Touchjs.loadCallback($this, "a");
							} else {
								var targetE = getjQueryObj(target);
								console.log("post");
								if (method == "post") {
									Touchjs.ajax.post(targetE, url, params,  function() {
										Touchjs.touch($this);
									});
								} else {
									console.log("get");
									Touchjs.ajax.get(targetE, url, params,  function() {
										Touchjs.touch($this);
									});
								}
							}
	                    } else {
	                    	if (!$.isEmptyObject(params)) {
	                    		if (url.indexOf("?")==-1) {
	                    			url += '?'; 
	                    		} else {
	                    			url += '&';
	                    		}
	                    		url += $.param(params);
	                    	}
	                    	window.location.href = url;
	                    }
	//						return false;
					});
				} 
	        });
		}
		,description : "a标签附加参数(如果不是新打开的页面,为Get方式提交需要注意参数长度)"
	},
	/**	打开链接 */
	openLink : {
		expression : '.touchjs-openlink',
		fn : function(selectE){
			var namespace = this.expression;
			 selectE.off("click"+namespace).on("click"+namespace, function() {
				 var $this = $(this);
				 var linkSrcE = $this;
				 var url = '';
				 if (hasAttr($this, "baseOn")) { //基于元素
					 var baseOnValue = $this.attr("baseOn");
					 var baseOnValueArrs = baseOnValue.split(">");
					 if (baseOnValueArrs.length == 2) {
						 linkSrcE = $this.closest(baseOnValueArrs[0]).find(baseOnValueArrs[1]);
						 if (linkSrcE.is(":input")) {
							 url = linkSrcE.val();
						 } else {
							 url = linkSrcE.text();
						 }
					 }
				 } else {
					 var prevE = $this.prev();
					 if (prevE.is(":input")) { // 类型为text的input或select  if (prevE.is(":text") || prevE.is("select")) {
						 url = prevE.val();
					 } else {
						 url = prevE.text();
					 }
				 }
				 url = formatUrl(url);
				 if (url == '') {
					 Touchjs.alert("消息提示", '空连接,不能打开!', "error");
				 } else {
					$this.after("<a href='" + url + "' target='_blank'></a>");
					var a = $this.next();
					a[0].click(); //直接用a.click()不执行
					a.remove();
					// 自定义点击回调
					Touchjs.loadCallback($this, "openlink");
				 }
				return false;
			});
		},
		checkFn : function(contentE){
			var dealEle = findAndMarkEle(contentE, this.expression);
			var eleName = "openLink元素";
			if (dealEle.length > 0) {
				dealEle.each(function(i, n){
					var $this = $(this);
					if (hasAttr($this, "baseOn")) { //基于元素
						 var baseOnValue = $this.attr("baseOn");
						 var baseOnValueArrs = baseOnValue.split(">");
						 if (baseOnValueArrs.length == 2) {
							 linkSrcE = $this.closest(baseOnValueArrs[0]).find(baseOnValueArrs[1]);
							 if(linkSrcE.length == 0){
								 Touchjs.error(eleName + "未定义链接源元素：请检查baseOn的值定义是否正确（父元素选择器+“>”+源元素选择器）");
								 Touchjs.log(eleName + "未定义链接源元素：请检查baseOn的值定义是否正确（父元素选择器+“>”+源元素选择器）"+baseOnValue+$this.prop("outerHTML"));
								 return false;
							 }
						 }
					 }
				});
			}
			return dealEle.length > 0 ? dealEle : false;
		}
		,description : "打开链接"
	},
	/** 刷新 */
	refresh : {
		expression : '.touchjs-refresh',
		fn : function(ele) {
			var dealEle = findAndMarkEle(ele, this.expression);
			var namespace = this.expression;
			if (dealEle.length > 0) {
				dealEle.each(function(i, n){
					var $this = $(this);
					$this.off("click"+namespace).on("click"+namespace, function() {
						// 待实现
						Touchjs.successDoMethod.refresh(contentE);
						// 自定义点击回调
						Touchjs.loadCallback($this, "refresh");
						return false;
					});
				});
			}
		}
	 	,description : "刷新"
	},
	/** 带参数提交（form基础上点击不同按钮提交不同结果） */
	submit : {
		expression : '.touchjs-submit',
		fn : function(contentE, dialog){
			var dealEle = findAndMarkEle(contentE, this.expression);
			var namespace = this.expression;
			if (dealEle.length > 0) {
				dealEle.each(function(i, n){
					var $this = $(this);
					$this.off("click"+namespace).on("click"+namespace, function() {
						var $this = $(this);
						var params={};
						
						var checkParams = $this.paramSelector(); //参数选择器
						if (!checkParams) {
							return false;
						} else {
							params = checkParams;
						}
						
						params = getParams($this,params);
						var formE = $this.closest('form');
						if (formE.length > 0){
							for(var key in params){
								formE.find("input[name='"+key+"']").val(params[key]);
							}
							if (!dialog) { //dialog不存在时才处理，存在时默认处理
								Touchjs.form.initSubmit(contentE, function(formE, targetE, data){
									// 初始化页面内容
									//提交完成后刷新当前页面
									var successDo = Touchjs.successDo.refresh;
									Touchjs.formSubmitCallback(successDo, formE, targetE, data, null, formE, null);
									// 自定义点击回调
									Touchjs.loadCallback($this, "submit");
								});
							}
							formE.submit();
						}
						return false;
					});
				});
			}
		}
		,description : "带参数提交（基于form，点击不同按钮提交不同结果）"
	},
	/** 表单重置 */
	reset : {
		expression : '.touchjs-reset',
		fn : function(contentE, dialog){
			var dealEle = findAndMarkEle(contentE, this.expression);
			var namespace = this.expression;
			if (dealEle.length > 0) {
				dealEle.each(function(i, n){
					var $this = $(this);
					$this.off("click"+namespace).on("click"+namespace, function() {
						var formE = $this.closest('form');
						if (formE.length > 0){
							formE[0].reset();
							formE.find(":input:not(:reset)").each(function() {
								$(this).triggerHandler("reset");
	                        });
						}
						// 自定义点击回调
						Touchjs.loadCallback($this, "reset");
						return false;
					});
				});
			}
		}
		,description : "表单重置"
	},
	/** 关联其他元素的select
	 * @example 1
	 * //选择1时显示 id为dealBySpider的元素 选择2是显示id为yaunyinSpider的元素
	 * <select id="state" name="state" class="shieldSelectRelated">
			<option value="">--请选择--</option>
			<option value="1" relatedTo="#dealBySpider">通过</option>
			<option value="2" relatedTo="#yaunyinSpider">退回</option>
		</select>
	 * @example 2
		<!-- 联动select,relatedTo为联动目标（jQuery选择器，本例中为id为job的select），relatedUrl为联动url，其中department={}代表department为当前select选中的值，
		主要为了参数名与select名字不一致时，也可以使用参数选择器paramSelector，paramAdapter为参数适配器，将relatedUrl返回的json（注意必须为json格式）的字段适配为
		select可使用的字段，适配器中id为option的值name为option的显示文本如果字段名本身为id和name，此属性可不写，下面的示例中可用写为paramAdapter="name=jobName"，id省略表示与默认值一致 -->
		<select name="department" id="department" class="shieldSelectRelated" relatedTo="#job" relatedUrl="/resume/getJobList?department={}" paramAdapter="id#name=jobName">
		    <option value="0">请选择</option>
		    <option value="1" relatedTo="#jobspan">技术部</option>
		    <option value="2">信息采编部</option>
		    <option value="3">客服部</option>
		    <option value="4">招商部</option>
		</select>
		<span id="jobspan"><b>招聘职位：</b>
		<select name="job" id="job">
		    <!-- 样式为 noshield，说明联动时不覆盖 -->
		    <option value="0" class="noshield">请选择</option>
		</select></span>
	
	*/
	selectRelated : {
		expression : '.touchjs-selectRelated',
		fn: function(selectE, menuliSelect, navMenuSelect, searchForm, shieldDialog){
			var shieldSelectRelated = selectE;
			shieldSelectRelated.each(function() {
				var $this = $(this);
				var autochange = $this.data("autochange");
				var hasAutoChange = autochange || (autochange==false) || (autochange=="false");
				if (!hasAutoChange) {
					if (hasAttr($this, "relatedTo")) { //select级别的不自动change
						autochange = false;
					} else { // option级别的自动change
						autochange = true;
					}
				}
				
				$this.change(function(){
					setTimeout(function() {
						var selectVal = $this.val()+"";
						
						var paramNames = {id:"id",name:"name"}; //默认参数配置
						if (hasAttr($this, "relatedTo") && selectVal!="" && selectVal!="null") { //select级别联动
							var relatedTo = $this.attr("relatedTo");
							var relatedToE = getjQueryObj(relatedTo);
							var relatedUrl = $this.attr("relatedUrl");
							
							var paramAdapter = $this.attr("paramAdapter");
							if (paramAdapter) {
								var paramAdapters = paramAdapter.split("#"); //#号分隔
								for (var i=0;i<paramAdapters.length;i++) {
									var paramName = paramAdapters[i];
									if (paramName.indexOf("=")!=-1) {
										var _paramNames = paramName.split("="); //如果key与提交不一致用=号分隔
										var paramsKey = _paramNames[0];
										var jsonResutlKey = _paramNames[1];
										paramNames[paramsKey] = jsonResutlKey;
									} else {
										paramNames[paramName] = paramName;
									}
								}
	                        }
							//获取数据
							if(relatedUrl.indexOf("{}") != 0){
								relatedUrl = relatedUrl.replace("{}",selectVal);
							}
							var params={};
							var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector($this, params); //参数选择器
							if (!checkParams) {
								return false;
							} else {
								params = checkParams;
							}
							params = getParams($this, params);
							if (relatedToE.is("select")) { // 联动对象是select，则应该返回的是json格式
								$.getJSON(relatedUrl, params, function(json) {
									var html = "";
									if (json) {
										for ( var i = 0; i < json.length; i++) {
											html += '<option value="' + json[i][paramNames.id] + '">';
											html += json[i][paramNames.name] + '</option>';
										}
									}
									relatedToE.find("option:not(.noshield)").remove();
									relatedToE.append(html);
									
									if(!relatedToE.data("initareav")){ //赋值
										var value = relatedToE.data("v");
										if (value) {
											if (value == "clear") { //清空值
												value = "";
											}
											//relatedToE.val(value);
											relatedToE.find("option[value='"+value+"']").attr("selected", "selected");
											relatedToE.triggerHandler("change"); //触发change方法
	                                    }
										relatedToE.attr("data-initareav", "initareav");
									}
									
								});
	                        } else { // 联动对象不是select，直接返回页面
	                        	var method = $this.data("method")||"post";
								if (method == "post") {
									ShieldJS.ajax.post(relatedToE, relatedUrl, params, function(data) {
										ShieldJS.form.initSubmit(relatedToE, function(formE, targetE, data){
											//改写form的submit方法（util.js）
										});
										//页面处理
										ShieldJS.core.bindHtmlAction(relatedToE, menuliSelect, navMenuSelect, searchForm);
										// 自定义点击回调
										ShieldJS.HtmlAction.clickCallback($this);
									}, null, null, true);
								} else {
									ShieldJS.ajax.get(relatedToE, relatedUrl, params,  function() {
										ShieldJS.form.initSubmit(relatedToE, function(formE, targetE, data){
											//改写form的submit方法（util.js）
										});
										//页面处理
										ShieldJS.core.bindHtmlAction(relatedToE, menuliSelect, navMenuSelect, searchForm);
										// 自定义点击回调
										ShieldJS.HtmlAction.clickCallback($this);
									});
								}
	                        }
						}
						//option联动
						$this.find("option").each(function(){
							var $thisOption = $(this);
							var opetionVal = $thisOption.val();
							if (hasAttr($thisOption, "relatedTo")) { //有属性才处理
								var relatedTo = $thisOption.attr("relatedTo");
								var relatedToE = getjQueryObj(relatedTo);
								if (opetionVal === selectVal) {
									relatedToE.find(":input").prop("disabled",false);
									relatedToE.show();
								} else {
									//表单设为不可用
									relatedToE.find(":input").prop("disabled",true);
									relatedToE.hide();
								}
							}
						});
						
					}, 1); //加定时器是因为通过utils赋值的select不加取不到值
				});
				
				// 获取上级数据
				function getHigherLevel(ele){
					
					var higherLevel = ele.data("higher"); //倒推上级
					var higherUrl = ele.data("higher-url"); //倒推上级
					
						
					if (higherUrl) { //select级别联动
						var selectVal = ele.data("v");
						if (selectVal) {
							var relatedUrl = ele.attr("relatedUrl"); //下级
							var paramAdapter = ele.attr("paramAdapter"); //下级
							var eleId = ele.attr("id");
							try {
								higherIndex ++;
							}catch (e) {
								higherIndex = 0;
							}
							if (!eleId) {
								eleId = "gethigherContainer"+higherIndex;
								ele.attr("id", eleId);
							}
							var paramNames = {id:"id",name:"name", pid:"pid"}; //默认参数配置
							var higherReqUrl = higherUrl;
							
							var paramAdapter = ele.attr("paramAdapter");
							if (paramAdapter) {
								var paramAdapters = paramAdapter.split("#"); //#号分隔
								for (var i=0;i<paramAdapters.length;i++) {
									var paramName = paramAdapters[i];
									if (paramName.indexOf("=")!=-1) {
										var _paramNames = paramName.split("="); //如果key与提交不一致用=号分隔
										var paramsKey = _paramNames[0];
										var jsonResutlKey = _paramNames[1];
										paramNames[paramsKey] = jsonResutlKey;
									} else {
										paramNames[paramName] = paramName;
									}
								}
							}
							//获取数据
							if(higherUrl.indexOf("{}") != 0){
								higherReqUrl = higherReqUrl.replace("{}", selectVal);
							}
							var params={};
							var checkParams = ShieldJS.HtmlAction.checkAnGetParamSelector(ele, params); //参数选择器
							if (!checkParams) {
								return false;
							} else {
								params = checkParams;
							}
							params = getParams(ele, params);
							$.getJSON(higherReqUrl, params, function(json) {
								var html = "";
								if (json) {
									var highPid ;
									for ( var i = 0; i < json.length; i++) {
										highPid = json[i][paramNames.pid];
										html += '<option value="' + json[i][paramNames.id] + '">';
										html += json[i][paramNames.name] + '</option>';
									}
									var attrs = {id: "hlselect", "class":"shieldSelectRelated", "data-higher-url":higherUrl, "data-v":highPid, "relatedTo":"#"+eleId, "relatedUrl":relatedUrl }
									if (paramAdapter) {
										attrs.paramAdapter = paramAdapter;
	                                }
									higherE =  $("<select/>", attrs);
									ele.html(html);
									higherE.append(html);
									ele.before(higherE);
									
									if(!ele.data("initareav")){ //赋值
										var value = ele.data("v");
										if (value) {
											if (value == "clear") { //清空值
												value = "";
											}
											ele.val(value);
											// ele.triggerHandler("change"); //触发change方法
										}
										ele.attr("data-initareav", "initareav");
									}
									ShieldJS.core.bindHtmlAction(higherE.parent(), menuliSelect, navMenuSelect, searchForm); //绑定元素
								} else{
									ele.remove();
								}
								
								
							});
						} else {
							ele.remove();
						}
					}
						
				}
				getHigherLevel($this);
				
				if (autochange) {
					$this.triggerHandler("change");//触发自定义change事件
	            }
	        });
		},
		checkFn : function(contentE) { //检查函数
			var dealEle = findAndMarkEle(contentE, this.expression);
			var eleName = "联动select元素shieldSelectRelated";
			if(dealEle.length > 0 ) {
				dealEle.each(function(){
					var $this = $(this);
					if (hasAttr($this,"relatedTo")) { //select级别的联动（区别于option级别）
						var relatedTo = $this.attr("relatedTo");
						var relatedToE = getjQueryObj(relatedTo);
						if (relatedToE.length == 0) {
							Touchjs.error(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！");
							Touchjs.console(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！"+$this.prop("outerHTML"));
							return false;
						}
						if (!hasAttr($this,"relatedUrl")) {
							Touchjs.error(eleName + "未定义数据获取地址：relatedUrl属性");
							Touchjs.console(eleName + "未定义数据获取地址：relatedUrl属性"+$this.prop("outerHTML"));
							return false;
						}
						if (relatedToE.is("select") && !hasAttr($this,"paramAdapter")) {
							Touchjs.error(eleName + "未定义参数适配：paramAdapter属性");
							Touchjs.console(eleName + "未定义参数适配：paramAdapter属性"+$this.prop("outerHTML"));
							return false;
						}
					}
				});
			};
			return dealEle.length > 0 ? dealEle : false;
		}
		,description : "关联其他元素的select"
	},
	
	/** 关联其他元素的checkbox或radio
	 * @example 
	 * // checkedRelated关联操作checkbox或radio元素，relatedTo为关联到的元素，relatedToType="disabled"表示将目标元素更改disabled属性（其他值：disabledAndHide及不写）
	 * <sapn style="float: left;padding: 3px;"><input type="checkbox" id="acceptSetBtn" class="shieldCheckedRelated" relatedTo="#buySubmit" relatedToType="disabled">我已阅读并接受此规定</sapn>
	 * <input type="button" id="buySubmit" class="shieldSubmit" value="购买" disabled="disabled">*/
	checkedRelated : {
		expression : '.touchjs-checkedRelated',
		fn: function(selectE){
			
			selectE.each(function(){
				var thisSelectE = $(this);
				
				var shieldCheckedRelated = thisSelectE;
				var changeDealE; //如果是radio需处理为同一父类的radio
				if (shieldCheckedRelated.is(":radio")) { //单选框
					var formE = shieldCheckedRelated.closest("form");
					changeDealE = formE.find(":radio[name='"+shieldCheckedRelated.attr("name")+"']");
				}
				if (!changeDealE) {
					changeDealE = shieldCheckedRelated;
				}
				changeDealE.each(function() {
					var bindE = $(this);
					if (!bindE.hasClass("touchjs-checkedRelated-init")) {
						bindE.bind("change",function(){
							var $thisCheckedE = $(this);
							if (changeDealE) { //radio需处理同一父级的所有radio元素，否则点击其他的radio不会触发上一次选中radio的change事件
								$thisCheckedE = changeDealE;
							}
							$thisCheckedE.filter(":not(:checked)").each(function(){
								var $this = $(this);
								changeEvent($this);
							});
							$thisCheckedE.filter(":checked").each(function(){
								var $this = $(this);
								changeEvent($this);
							});
						});
						bindE.addClass("touchjs-checkedRelated-init");
	                }
	            });
				
				shieldCheckedRelated.triggerHandler("change");//触发自定义change事件
			});
			// 显示隐藏元素
			function showRelate2E(relatedToE, checked) {
				if (checked) {
					relatedToE.show();
	            } else {
	            	relatedToE.hide();
				}
	        }
			// 禁用启用元素
			function enableRelate2E(relatedToE, checked) {
				relatedToE.prop("disabled", !checked); //关联对象设置为启用/禁用
				relatedToE.find(":input").each(function() { //关联对象里面的input设置为启用/禁用
					var $thisInput = $(this);
					var disabled = $thisInput.prop("disabled"); // 获取初始状态
					var data_enabled = $thisInput.data("enabled"); //data中存在启用值
					var data_disabled = $thisInput.data("disabled"); //data中存在禁用值
					if (!data_enabled && !data_disabled) { //都没有设置 或者 都是false（后一种不会存在）
						if (disabled) {
							data_disabled = true; //禁用
							$thisInput.data("disabled", true);
	                    } else {
	                    	$thisInput.data("enabled", true); //该值无实际用处，只是表明已经初始化过了
	                    }
					}
					if (checked) { //可用，如果选中了，且原来就是禁用的则继续禁用，原来为正常则启用
						$thisInput.prop("disabled", data_disabled?true:false);
	                } else { //不可用,如果未选中，则禁用此元素
	                	$thisInput.prop("disabled", !checked);
	                }
	            });
	        }
			
			function changeEvent($this) {
				setTimeout(function() {
					var checked = $this.prop("checked");
					
					if (hasAttr($this, "relatedTo")) { //关联元素
						var relatedTo = $this.attr("relatedTo");
						var relatedToE = getjQueryObj(relatedTo);
						
						var relatedToType = "disabledandhide"; //默认设为不可用并隐藏
						if (hasAttr($this, "relatedToType")) { //定义关联状态，目前有3种：仅设为不可用、仅隐藏、设为不可用并隐藏
							relatedToType = $this.attr("relatedToType").toLowerCase(); //转小写
						}
						if (relatedToType == "disabled") { //仅设为不可用
							enableRelate2E(relatedToE, checked);
							
						} else if (relatedToType == "disabledandhide") { //设为不可用并隐藏
							enableRelate2E(relatedToE, checked);
							showRelate2E(relatedToE, checked);
						} else { //仅隐藏
							
							showRelate2E(relatedToE, checked);
						}
						if (checked) {// 参数传递
							var datas = $this.data(); //用data存储
							for ( var key in datas) {
								relatedToE.find('[name="'+key+'"]').val(datas[key]);
							}
	                    }
					}
				}, 1); //加定时器是防止utils干扰
	        }
			
			
		},
		checkFn : function(contentE) { //检查函数
			var dealEle = findAndMarkEle(contentE, this.expression);
			var eleName = "联动checkbox或radio元素checkedRelated";
			if(dealEle.length > 0 ) {
				dealEle.each(function(){
					var $this = $(this);
					if (hasAttr($this,"relatedTo")) {
						var relatedTo = $this.attr("relatedTo");
						var relatedToE = getjQueryObj(relatedTo);
						if (!relatedToE || relatedToE.length == 0) {
							Touchjs.error(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！");
							Touchjs.console(eleName + "联动元素未找到：请检查relatedTo属性是否定义错误！"+$this.prop("outerHTML"));
							return false;
						}
					}
				});
			}
			return dealEle.length > 0 ? dealEle : false;
		}
		,description : "关联其他元素的checkbox或radio"
	},
	/** 全选 */
	checkAll : {
		expression : '.touchjs-checkall',
		fn: function(contentE){
			findAndMarkEle(contentE, this.expression).change(function() {
				var $this = $(this);
				var checked = $this.is(':checked');
				var rangeE = $this.closest("table");
				if (hasAttr($this, "rangeEle")) {
					var rangeTo = $this.attr("rangeEle");
					rangeE = $this.closest(rangeTo);
					if (rangeE.length == 0) {
						Touchjs.error("全选组件上级处理范围元素未找到：请检查rangeEle属性是否定义错误！");
						Touchjs.console("全选组件上级范围元素未找到：请检查rangeEle属性是否定义错误！"+$this.prop("outerHTML"));
						return false;
					}
				}
				rangeE.find(':checkbox').each(function() {
					$(this).prop("checked", checked);
				});
			});
		}
		,description : "全选"
	},
	/** 用于标记选中行，一般用于checkbox上用于选中行传参 */
	selectRow : {
		expression : '.touchjs-selectrow',
		fn: function(contentE){
			findAndMarkEle(contentE, this.expression).change(function() {
				var $this = $(this);
				var checked = $this.is(':checked');
				var checkClass = ShieldJS.HtmlAction.names.shieldSelectRowByshieldCheck;
				var rowEle = $this.closest("tr");
				rowEle.toggleClass(checkClass);
			});
		}
		,description : "用于标记选中行，一般用于checkbox上用于选中行传参"
	},
	/** 用于表单自动提交 */
	autoSubmitForm : {
		expression : '.touchjs-submitform',
		fn: function(contentE) {
			findAndMarkEle(contentE, this.expression).each(function(i, n) {
				var $this = $(this);
				$this.submit();
				//页面处理
				// 自定义点击回调
				Touchjs.loadCallback($this, "autosubmitform");
			});
		}
		,description : "自动提交表单"
	},
	/** 无数据tr处理 */
	noDataTr : {
		expression : '.touchjs-nodatatr',
		fn: function(contentE) {
			findAndMarkEle(contentE, this.expression).each(function(i, n) {
				var $this = $(this);
				var pervTr = $this.prev();
				$this.find("th,td").attr("colspan", pervTr.find("th,td").length);
			});
		}
		,description : "无数据tr处理"
	},	
	/** 根据文件id下载文件，可能需要覆盖，需要根据项目的实现重写 */
	download : {
		expression : '.touchjs-download',
		fn : function(contentE){
			var dealEle = findAndMarkEle(contentE, this.expression);
			if (dealEle.length > 0) {
				var $ = jQuery;
				dealEle.each(function() {
					var ele = $(this);
					ele.click(function() {
						var eleData = ele.data();
						var _default = {
							// 判断是否存在地址
							url: ctxPath +'/isExitDownFile',
							// 下载文件地址
							downUrl: ctxPath +'/downloadAtt'
						};
						var options = getExtendSettings(ele, _default);
						var params = {};
						params = getParams(ele, params);
						var id = ele.attr('id');
						if (!id) {
							id =  params.id;
						}
						params.id = id;
						Touchjs.ajax.getJSON(options.url, params, function(data) {
							window.location.href = options.downUrl + '?id=' + id;
						});
					});
				});
			}
		}
		,description : "根据文件id下载文件"
	},	
	/** 用于显示所有已经注册的touchJS的html扩展动作方法 */
	allModules : {
		expression : '.touchjs-allmodules',
		fn: function(contentE){
			findAndMarkEle(contentE, this.expression).each(function(i, n) {
				var $this = $(this);
				var actions = $.extend({}, Touchjs.modules);
				var tablecls = "allmodules "; //table的class
				tablecls += $this.data("class")||"";
				var extendUrl = $this.data("extendUrl"); //扩展url，查看示例
				var htmlStr = '<table class="'+tablecls+'">'+
								 '<tr class="headertr"><th class="touchjs_headertr_xh"></th><th class="touchjs_headertr_method">方法名</th><th class="touchjs_headertr_ex">表达式</th><th class="touchjs_headertr_des">描述</th>';
				if (extendUrl) {
					htmlStr += '<th class="touchjs_headertr_demo">示例</th>';
	            }
				htmlStr += '</tr>';
				var index = 0;
				for(var actionName in actions){
					index++;
					htmlStr += '<tr class="datatr"><td class="touchjs_datatr_xh">'+index+'</td><td class="touchjs_datatr_method">'+actionName+'</td><td class="touchjs_datatr_ex">'+(actions[actionName].expression||"")+'</td><td class="touchjs_datatr_des">'+(actions[actionName].description||"")+'</td>';
					if (extendUrl) {
						htmlStr += '<td class="touchjs_datatr_demo"><a data-url="'+$this.data("extendUrl")+actionName+'" class="touchjs-dialog" data-title="查看信息" data-width="800">查看示例</td>';
	                }
					htmlStr += '</tr>';
				}
				htmlStr += '</table>';
				var tableE = $(htmlStr);
				tableE.find("td").each(function(i, n) {
					$(this).data("html", $(this).html());
				});
				if(extendUrl){
					Touchjs.load(["dialog"], tableE); //加载dialog方法
				}
				var _defaults = {
					/**前景色*/
					foreground: 'red',    
					/**背景色*/
				    background: 'yellow'    
				};
				//搜索
				$(".showAllModulesSearch").off("keyup.alltoujs").on("keyup.alltoujs", contentE, function() {
	                var $search = $(this);
	                var searchKey = $.trim($search.val());
	                if (searchKey) {
	                	var options = $.extend(true, _defaults, getSettings($search));
	                	tableE.find("tr.datatr").hide();
	                	tableE.find("tr.datatr").filter(function() {
	                		if ($(this).text().toLowerCase().indexOf($search.val().toLowerCase())!=-1) {
	                			var reg = new RegExp("("+$search.val()+")", "gi"); //全局并忽略大小写
	                			$(this).find("td").not(".touchjs_datatr_demo").each(function(i, n) { //排除示例
	                				var newstr = $(n).data("html").replace(reg, '<strong style="background-color: '+options.background+';color: '+options.foreground+';">$1</strong>');
	                				$(n).html(newstr);
	                            });
	                			return true;
	                		}
	                		return false; 
	                	}).show();
	                } else {
	                	tableE.find("tr.datatr").show().find("td").not(".touchjs_datatr_demo").each(function(i, n) {
							$(this).html($(this).data("html"));
						});
					}
	            });
				$this.append(tableE);
			});
		}
		,description : "展示所有的注册方法"
	}
};
/** 添加htmlEle动作处理函数
 * @param name {String} 方法名称
 * @param expression {String} 表单式
 * @param fn {String} 处理函数
 * @param checkFn {String} 检查函数【可无】
 * @param description {String} 方法描述 */
Touchjs.addModule = function (name, expression, fn, checkFn, description) {
	if(this.modules[name]){
		Touchjs.log(name + "动作处理方法被覆盖！");
	}
	// 复制对象
    var copiedObject = $.extend({}, this.modules[name]);
    var expressionV = arguments[1]; //重载，expression可能不存在
    var indexRemove = 0;
    if ($.isFunction(expression)) {
    	expressionV = "";
    	indexRemove++;
    }
    var fnV = arguments[2-indexRemove]; //重载，checkFn函数可能不存在
    var checkFnV = arguments[3-indexRemove];
    var descriptionV = arguments[4-indexRemove];
    if (typeof(checkFnV) == "string") {
    	descriptionV = checkFnV;
    	checkFnV = null;
    }
	this.modules[name] = {
		expression : expressionV || "" //表达式
		,fn : fnV //处理函数
		,checkFn : checkFnV //检查函数，是否缺少属性等，如果不合法返回false
		,description : descriptionV || copiedObject.description //方法描述
	};
};
Touchjs.load = function(ele, methodNames, options) {
	var actions = $.extend({}, Touchjs.modules);
	var foreachObj = actions;
	//获取html元素action处理
	if (methodNames && $.isArray(methodNames)) {
		foreachObj = methodNames;
    }
	if (!options && methodNames && !$.isArray(methodNames)) { //没有设置methodNames，则第二个参数为options
		options = methodNames;
		methodNames = null;
    }
	if (options == null) {
		options = {};
	}
	var options = $.extend(true, {}, this.options, options);
	options = getExtendSettings(ele, options);
	
	var hasError = false;
	var bindBeforeCallbackE = findAndMarkEle(ele, '[bindBefore]'); //绑定前处理
	
	if (bindBeforeCallbackE.length > 0) { // 有搜索表单时的处理(class="searchForm")
		var callbackFn = bindBeforeCallbackE.attr("bindBefore");
		callbackFn = eval(callbackFn); //字符串转换为方法
		if ($.isFunction(callbackFn)) {
			callbackFn(bindBeforeCallbackE); //需要自定义
		}
	}
	
	for (var actionName in foreachObj) {
		if (methodNames) {
			actionName = foreachObj[actionName]; //如果是数组，actionName返回的下标，再取出名字
        }
		
		try {
			// 无checkFn函数的直接调用
			if (!$.isFunction(actions[actionName].checkFn)) {
				actions[actionName].fn(ele);
			} else if ($.isFunction(actions[actionName].checkFn)) {
				// 有checkFn函数的检测完后调用
				var selectEles = actions[actionName].checkFn(ele);
				if (selectEles) {
					actions[actionName].fn(selectEles);
				}
			}
		} catch (e) {
			hasError = true;
			Touchjs.debug("方法异常！");
			Touchjs.error("方法异常："+actionName, e);
			if(options && $.isFunction(options.loadErrorCallback) ){ //成功错误回调函数
				options.loadErrorCallback(actionName, e);
			}
			break;
		}
		
	}
	
	var bindAfterCallbackE = findAndMarkEle(ele, '[bindAfter]'); //绑定后处理
	if (bindAfterCallbackE.length > 0) { // 自定义回调处理
		var callbackFn = bindAfterCallbackE.attr("bindAfter");
		callbackFn = eval(callbackFn); //字符串转换为方法
		if ($.isFunction(callbackFn)) {
			callbackFn(bindAfterCallbackE); //需要自定义
		}
	}
	if(!hasError && options && $.isFunction(options.loadSuccCallback) ){ //成功回调函数
		options.loadSuccCallback(ele);
	}
	if(options && $.isFunction(options.loadCallback) ){ //回调函数
		options.loadCallback(ele);
	}
	
};
Touchjs.touch = function(ele, methodNames, options) {
	// 提交表单完成后执行方法
	Touchjs.form.initSubmit(ele, function(formE, targetE, data){
		// 初始化页面内容
		if (targetE && targetE.length > 0) {
			Touchjs.load(targetE, methodNames, options);
		}
	});
	Touchjs.load(ele, methodNames, options);
};
// =============================== 扩展组件 ===============================
Touchjs.addModule("pagetop", ".touchjs-pagetop", function(ele) {
	var $view = $('html,body'), $backTop = findAndMarkEle(ele, this.expression);
	var top = $backTop.data("top")||100; //滚动条距离顶部高度多少px时显示
	$backTop.css("cursor"," pointer");
	$backTop.hide(); //一开始隐藏
	$(window).scroll(function(){
		//当window的scrolltop距离大于1时，go to 
		if($(this).scrollTop() > top){
			$backTop.fadeIn(1000);
		}else{
			$backTop.fadeOut(1000);
		}
	});
	$backTop.click(function(){ //点击返回顶部
		$view.animate({
			'scrollTop':0
		},800);
		return false;
	});
}, "返回顶部");
Touchjs.addModule("multiTab", ".touchjs-multiTab", function(ele) {
	var dealEle = findAndMarkEle(ele, this.expression);
	if (dealEle.length > 0) {
		if (!jQuery().multiTab) {
			Touchjs.error("检测到multi-tab_plugin元素缺少依赖JS库：jquery.multiTab.js！");
			return;
		}
		
		dealEle.each(function() {
	        var $this = $(this);
	        var options = {
	        	itemSuccess : function(contentEle, titleEle){
		        	//内容区处理
					Touchjs.touch(contentEle);
					// 增加翻页处理
					Touchjs.dynamicBind(contentEle, "click1", ".tabpage", "[data-tab-page] a", function(thisE) {
						var $this = thisE;
						var parE = $this.closest(".touchjs-page");
						
						var pageNum = $this.data("v");
						if (pageNum) {
							titleEle.data("p", pageNum);
							titleEle.triggerHandler("refresh");
							/**返回顶部--用户提现效果 */
							$('html, body').animate({ scrollTop: 0 }, 300);
                        }
					});
					
	        	}
	        };
	        options = getExtendSettings($this, options);
	        $this.multiTab(options);
	        // 翻页
	        
	        //$this.multiTabClose($this.find(".title").eq(2));
		});
	}
}, "多页签插件");
Touchjs.addModule("required", "form td:contains('*'),form th:contains('*')", function(contentE){
	var selectE = contentE.find(this.expression)
	selectE.each(function() {
	    var selectEthis = $(this);
	    var se = '[type=text],[type=password],[type=file],[type=hidden],select[name],textarea'; //需要处理的元素
	    var notSe = ".ke-upload-file,.webuploader-element-invisible,.ignore"; //排除的元素，上传组件排除
	    selectEthis.closest("td,th").next().find(se).not(notSe).addClass('required');
    });
}, "td中必填项input的处理");
// 加载table上，表示可拖拽，需要拖拽的td添加样式dragHandle表明可拖拽，如果要使用onDrop方法，则tr上必须定义id -->
//resetsortsEle排序值存放元素(jquery选择器)，resettips 排序提示语(字符) resettipsEle排序提示语显示元素(jquery选择器),resetbtnEle排序按钮(jquery选择器)
//动态添加的行不能处理 TODO
Touchjs.addModule("tableDnD", ".touchjs-tablednd", function(contentE){
	var dealEle = findAndMarkEle(contentE, this.expression);
	if (dealEle.length > 0) {
		if (!jQuery().tableDnD) {
			Touchjs.error("检测到"+this.expression+"元素缺少依赖JS库：jquery.tableDnD.js！");
		}
		// 如果某一些行不想拖动，只需要给这一行添加class nodrop和nodrag
		dealEle.each(function() {
			var $this = $(this);
			var options = getExtendSettings($this, {
				onDragClass: "myDragClass", //选中拖动一行时候的样式
				onDrop: function(table, row) { //表示拖动成功后执行的方法,要使用该方法tr上必须有id属性,该方法默认将tr的id作为取值
					console.log(1)
					var sortsESelector = $this.data("resetsortsEle")||"#resetsorts"; //排序值
					var sortsE = contentE.find(sortsESelector);
					if (sortsE.length > 0) { //排序后序号存放元素
						if(!sortsE.val()){
							var msg = $this.data("resettips")||"检测到列表中排序进行了改变，请记得更新排序！"
							var resettipsEleSelector = $this.data("resettipsEle")||"#resettips"; //排序提示
							if ($(resettipsEleSelector).length > 0) {
		 						$(resettipsEleSelector).text(msg); //页面内容中提示
		                    } else {
		                    	Touchjs.alert("提示信息", msg, "info"); //提示框提示
		                    }
							var resetOrderbtnSelector = $this.data("resetbtnEle")||".resetOrderbtn";
							$(resetOrderbtnSelector).show();
						}
						var orders = "";
						var rows = table.tBodies[0].rows;
						for (var i=0; i<rows.length; i++) {
							orders += rows[i].id+",";
						}
						sortsE.val(orders);
					}
				},
				dragHandle: ".dragHandle",
				onDragStart: function(table, row) {console.log("123")} 
			});
			$this.tableDnD(options);
			$this.on("mouseover mouseout", "td.dragHandle",function(event){
				if(event.type == "mouseover"){
					//鼠标悬浮
					$(this).addClass('showDragHandle');
				}else if(event.type == "mouseout"){
					//鼠标离开
					$(this).removeClass('showDragHandle');
				}
			}); //鼠标停留样式
			
		});
	}
}, "tableDnD拖拽插件");

