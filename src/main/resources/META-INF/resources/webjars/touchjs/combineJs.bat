%~d0
cd %~dp0
java -jar ../tool/compiler.jar --js touchjs.core.js touchjs.ajax.js touchjs.dialog.js touchjs.form.js ^
 touchjs.modules.js ^
 --js_output_file touchjs.min.js
