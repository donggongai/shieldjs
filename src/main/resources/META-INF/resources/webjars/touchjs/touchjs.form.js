/**    框架的form处理,依赖jquery.form.js
 * @constructor Touchjs.form */
Touchjs.form = {
    /**    初始化当前元素的表单
     * @param ele {jQueryObj} 需要处理的jQuery对象
     * @param callback {Function} 表单提交后的回调 (formE, targetE, data)
     * @param checkFn {Function} 对表单进行检查筛选，返回true表示通过
     */
    initSubmit : function(ele, callback, checkFn) {
        var touchjsForm = this;
        if (ele.length > 0) {
            var formEs = (ele[0].tagName != 'FORM') ? ele.find('form') : ele;
            formEs = formEs.not(".nottouch"); //排除不处理的
            formEs.each(function() {
                touchjsForm.initOneSubmit($(this), callback, checkFn);
            });
        }
    },
    /**    初始化单个表单
     * @param formE {jQueryObj} form标签的jQuery格式对象
     * @param callback {Function} 表单提交后的回调
     * @param checkFn {Function} 对表单进行检查筛选，返回true表示通过
     */
    initOneSubmit : function(formE, callback, checkFn) {
        //_blank 打开新窗口的不处理
        var target = "";
        if(hasAttr(formE,"target")){
            target = formE.attr("target");
        }
        if (target != "_blank") {
        	formE.addClass("ajaxform_touch");
        	if (!jQuery().ajaxSubmit) {
				Touchjs.debug("检测到缺少依赖JS库：jquery.form.js！不能自动处理为ajax提交表单！");
	        }
            formE.off('submit.touchjs').on("submit.touchjs", function() {
                if(!target){
                    target = formE.attr('shieldTarget'); //touchjs
                }
                var targetE = target ? findEle(formE, target) : null; //修改
                if (targetE && targetE.length == 0) {
                    targetE = target ? $(target) : null; //修改
                    if (targetE.length > 1) {
                    	Touchjs.error("表单提交目标对象超过1个，不执行覆盖操作！target: "+target + " 数目："+targetE.length);
                        targetE = null;
                    }
                }
                tjSubmitClickEle = formE.find(':submit:enabled,.shieldSubmit:enabled,.touchjsSubmit:enabled'); //排除disabled,可能有多个
                if (!tjSubmitClickEle.is(':disabled') ) {
                    
                   // var formTemp = [];
                    var options = {
						beforeSubmit : function(params, invokerEle, options) {
							if (Touchjs.form.validate(formE)) {
								if ((typeof checkFn != 'function' || checkFn(formE))) {
									if (targetE) {
										targetE.html(Touchjs.loadingHtml);
									}
									// 防止重复提交
									tjSubmitClickEle.prop('disabled', true);
									
									// 清除嵌套表单
									//formE.find('form').each(function() {
										//var formE = $(this);
									   // formTemp.push([ formE.prev(), formE ]);
									   // formE.remove();
									//});
									//验证通过后的回调
									if (formE.attr("afterValidate")) {
										var afterValidateFn = formE.attr("afterValidate");
										afterValidateFn = eval(afterValidateFn); //字符串转换为方法
										if ($.isFunction(afterValidateFn)) {
											afterValidateFn(formE, targetE, params);
										}
									}
									return true;
								}
							}
							
							return false;
						},
						success : function(data, textStatus, jqXHR) {
							tjSubmitClickEle.prop('disabled', false);
							var contentType = jqXHR.getResponseHeader("content-type") || "";

							// 分析一下callback的执行，
							// 1、如果json不为空，即返回结果为json对象，还存在2中情况，根据是否存在message 和 success属性区分，
							//  1.1 存在则是提示信息，弹出确认框，点击确定后执行callback
							//  1.2 不存在，则单纯是json数据，直接调用callback
							// 2、 如果json对象为空，则是html对象，直接调用callback
							var json = Touchjs.data2JsonObj(data, contentType);
							
							if (Touchjs.checkError(data, contentType, $.isFunction(callback)?function(){
								return callback(formE, targetE, data);
							}:null)) { //检查是否成功
								if (targetE) {
									targetE.html(data);
									//内容区处理
									Touchjs.touch(targetE);
								}
								if (!json && $.isFunction(callback)) { //非json对象
									callback(formE, targetE, data)
								} else if (json && !(json.success && json.message) && $.isFunction(callback)) { //是json对象但没有提示信息
									callback(formE, targetE, data)
								}
								// 成功时的回调
								if (formE.attr("successCallback")) {
									var successCallbackFn = formE.attr("successCallback");
									successCallbackFn = eval(successCallbackFn); //字符串转换为方法
									if($.isFunction(successCallbackFn)){
										successCallbackFn(formE, targetE, data);
									}
								}
							} else { //失败时的回调										
								if (formE.attr("errorCallback")) {
									var errorCallbackFn = formE.attr("errorCallback");
									errorCallbackFn = eval(errorCallbackFn); //字符串转换为方法
									if($.isFunction(errorCallbackFn)){
										errorCallbackFn(formE, targetE);
									}
								}
							}
							// 完成时的回调，不管是否成功
							if (formE.attr("completeCallback")) {
								var completeCallbackFn = formE.attr("completeCallback");
								completeCallbackFn = eval(completeCallbackFn); //字符串转换为方法
								if($.isFunction(completeCallbackFn)){
									completeCallbackFn(formE, targetE, data);
								}
							}
							// 还原嵌套表单
							//for ( var i = 0; i < formTemp.length; i++) {
								//var obj = formTemp[i];
								//if (obj[0].append) {
									//obj[0].after(obj[1]);
								//}
							//}
						}
                    };
                    setTimeout(function() {
                        formE.ajaxSubmit(options);
                    }, 1);
                    
                }
                return false;
            });
        }
        this.initOneSubmitExtend(formE, target);
    },
    /**    初始化单个表单的扩展方法（默认将当前页码设置为1）
     * @param formE {jQueryObj} form标签的jQuery格式对象
     * @param target {String} 表单提交后目标对象
     */
    initOneSubmitExtend : function(formE, target) { //扩展方法
        formE.find(":submit").click(function() { //执行查询时把页数设置为1
            var pageNum = formE.find(".pageNum");
            if (pageNum.length > 0) {
                pageNum.val(1);
            }
        });
    },
    /**    表单验证
     * @param formE {jQueryObj} 待验证的form对象
     * @returns {Boolean} 验证通过时返回true，否则返回false */
    validate : function(formE) { //扩展方法
    	var options = {};
    	options = getExtendSettings(formE, options); 
        return formE.kvvalid(options);
    }
}