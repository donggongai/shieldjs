/** 框架封装的ajax方法 
 * @class Touchjs.ajax */
Touchjs.ajax = {
	/**    ajax请求对象 */
    requests : {
        singleRequest : null,
        mutiRequest : null,
        clearRequest : function() {
            this.singleRequest = null;
            this.mutiRequest = null;
        }
    },
    abortLastReq : function() {
        if (this.requests && this.requests.singleRequest) {
            this.requests.singleRequest.abort();
            this.requests.singleRequest = null;
            //Touchjs.debug("abort ajax request");
        }
    },
    /**    ajax全局处理方法，可覆盖 */
    global : {
        error : function(response, textStatus, errorThrown) {
//	            第一个参数 response（jqXHR ） 在jQuery1.5版本以后则开始使用jqXHR对象，该对象是一个超集，就是该对象不仅包括XMLHttpRequest对象，还包含其他更多的详细属性和信息
//	            readyState :当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
//	            status  ：返回的HTTP状态码，比如常见的404,500等错误代码。
//	            statusText ：对应状态码的错误信息，比如404错误信息是not found,500是Internal Server Error。
//	            responseText ：服务器响应返回的文本信息
//	            第二个参数  textStatus（String）：返回的是字符串类型，表示返回的状态，根据服务器不同的错误可能返回下面这些信息："timeout"（超时）, "error"（错误）, "abort"(中止), "parsererror"（解析错误），还有可能返回空值。
//	            第三个参数 errorThrown（String）：也是字符串类型，表示服务器抛出返回的错误信息，如果产生的是HTTP错误，那么返回的信息就是HTTP状态码对应的错误信息，比如404的Not Found,500错误的Internal Server Error。
            var responseText = response.responseText;
            Touchjs.debug(responseText);
            var contentType = response.getResponseHeader("content-type") || "";
            Touchjs.checkError($.parseJSON(responseText), contentType);
            if (tjSubmitClickEle) {
                tjSubmitClickEle.removeAttr('disabled');
            }
            undisabledBtn();
        },
        complete : function(response, textStatus) {
        	Touchjs.ajax.requests.clearRequest();
           
        }
    },
    dealParamsAjax : function(method, ele, url, params, success, error, dataType, async) {
        // shift arguments if data argument was omitted
        if (typeof(ele) == "string") {
            async = dataType;
            dataType = error;
            error = success;
            success = params;
            params = url;
            url = ele;
            ele = undefined;
        }
        return this.ajax(ele, url, params, success, error, method, dataType, async);
    },
    /**    ajax请求
     * @param ele {jQueryObj} 发起请求的jQuery对象
     * @param url {String} 请求链接
     * @param params {Object} 参数,json格式或者字符串格式
     * @param success {Function} 成功的回调方法，注意区别于ajax原生的success，该方法为后台返回的成功方法
     * @param error {Function} 失败的回调方法，注意区别于ajax原生的error，该方法为后台返回的失败方法
     * @param method {String} 请求方法
     * @param dataType {String} 返回结果类型
     * @param async {Boolean} 是否是异步请求 */
    ajax : function(ele, url, params, success, error, method, dataType, async) {
        if (typeof(ele) == "string") {
            async = dataType;
            dataType = error;
            error = success;
            success = params;
            params = url;
            url = ele;
            ele = undefined;
        }
        disabledBtn();
        if (!params) {
            params = {};
        }
        var requestedTime = new Date().getTime();
        params.r = requestedTime;
        if (ele) {
            ele.html(Touchjs.loadingHtml);
        }
        if (params.singleRequest) {
            this.abortLastReq();
        }
        if (async == null) {
            async = true;
        }
        var request = $.ajax({
            url : url,
            type: method,
            dataType: dataType,
            data : params,
            traditional : true,
            async : async, //异步(不写默认值ie下有问题)
            beforeSend:function(XMLHttpRequest){
                var tokenEle = $("[id='csrfTokenInput']:last"); // #csrfTokenInput只能取一个
                if (Touchjs.contentMainE && Touchjs.contentMainE.find("iframe").length>0) { //如果有iframe则取iframe里的
                	tokenEle = tokenEle.add(Touchjs.contentMainE.find("iframe").contents().find(":root").find("[id='csrfTokenInput']:last"));
                }
                if (tokenEle.length > 0) {
                	var tokenEle = tokenEle.last();
                    XMLHttpRequest.setRequestHeader(tokenEle[0].name, tokenEle.val());
                }
                XMLHttpRequest.setRequestHeader("X-Requested-Time", requestedTime);
            },
            success : function(htmlData, textStatus, jqXHR) {
                undisabledBtn();
                if (tjSubmitClickEle) {
                    tjSubmitClickEle.removeAttr('disabled');
                }
                var contentType = jqXHR.getResponseHeader("content-type") || "";
                
                // 分析一下callback的执行，
                // 1、如果json不为空，即返回结果为json对象，还存在2中情况，根据是否存在message 和 success属性区分，
                //  1.1 存在则是提示信息，弹出确认框，点击确定后执行callback
                //  1.2 不存在，则单纯是json数据，直接调用callback
                // 2、 如果json对象为空，则是html对象，直接调用callback
                var json = Touchjs.data2JsonObj(htmlData, contentType);
                    
                if (Touchjs.checkError(htmlData, contentType, $.isFunction(success)?function(){ //只有是json格式时才会走
                	return success(htmlData);
                }:null)) {
                    if (ele) {
                        ele.html(htmlData);
                    }
                    // initPage();// 初始化页面
                    if (!json && $.isFunction(success)) { //非json对象
                    	success(htmlData);
                    } else if (json && !(json.success && json.message) && $.isFunction(success)) { //存在属性，包含属性
                    	success(htmlData);
    	        	}
                } else {
                    if (ele) {
                        ele.html('');
                    }
                    if (typeof error == 'function') {
                        error(htmlData);
                    }
                }
            },
            complete:function(response, textStatus){
                //请求结束方法增强处理  ,隐藏遮罩层
                //App.unblockUI(ele);
            	Touchjs.ajax.global.complete(response, textStatus);
            },
            error: function (response, textStatus, errorThrown) { 
            	Touchjs.ajax.global.error(response, textStatus, errorThrown);
            }
        });
        if (params.mutiRequest) {
            this.requests.mutiRequest = request;
        } else { 
            this.requests.singleRequest = request;
        }
    },
    /**    ajax发起get请求
     * @param ele {jQueryObj} 发起请求的jQuery对象
     * @param url {String} 请求链接
     * @param params {Object} 参数,json格式或者字符串格式
     * @param success {Function} 成功的回调方法，注意区别于ajax原生的success，该方法为后台返回的成功方法
     * @param error {Function} 失败的回调方法，注意区别于ajax原生的error，该方法为后台返回的失败方法
     * @param dataType {String} 返回结果类型，参考jQuery的ajax说明
     * @param async {Boolean} 是否是异步请求 */
    get : function(ele, url, params, success, error, dataType, async) {
        this.dealParamsAjax("get", ele, url, params, success, error, dataType, async);
    }
    /**    ajax发起post请求
     * @param ele {jQueryObj} 发起请求的jQuery对象【非必需】
     * @param url {String} 请求链接
     * @param params {Object} 参数,json格式或者字符串格式
     * @param success {jQueryObj} 成功的回调方法，注意区别于ajax原生的success，该方法为后台返回的成功方法
     * @param error {jQueryObj} 失败的回调方法，注意区别于ajax原生的error，该方法为后台返回的失败方法
     * @param dataType {jQueryObj} 返回结果类型，参考jQuery的ajax说明
     * @param async {jQueryObj} 是否是异步请求 */
    ,post : function(ele, url, params, success, error, dataType, async) {
        this.dealParamsAjax("post", ele, url, params, success, error, dataType, async);
    }
    /**    ajax获取json对象
     * @param url {String} 请求链接
     * @param params {Object} 参数,json格式或者字符串格式
     * @param success {Function} 成功的回调方法，注意区别于ajax原生的success，该方法为后台返回的成功方法
     * @param error {Function} 失败的回调方法，注意区别于ajax原生的error，该方法为后台返回的失败方法
     * @param async {Boolean} 是否是异步请求 */
    ,getJSON : function(url, params, success, error, async) {
        this.dealParamsAjax("get", null, url, params, success, error, "json", async);
    }
}
//button不可用
var disabledBtn = function() {
    if(Touchjs.contentMainE && Touchjs.contentMainE.length > 0){
    	Touchjs.contentMainE.find(":button,:submit").not(".ignoredis").not(":disabled").each(function() {
	        var $this = $(this);
	        $this.data("tj-disabled", "deal"); //标记
	        $this.prop("disabled", true);
        });
    }
    if(Touchjs.dialogE && Touchjs.dialogE.length > 0){
    	Touchjs.dialogE.find(":button,:submit").not(".ignoredis").not(":disabled").each(function() {
	        var $this = $(this);
	        $this.data("tj-disabled", "deal"); //标记
	        $this.prop("disabled", true);
        });
    }
};
//button恢复可用
var undisabledBtn = function() {
    if(Touchjs.contentMainE && Touchjs.contentMainE.length > 0){
    	Touchjs.contentMainE.find(":button,:submit").not(".ignoredis").filter(function() {
	        return $(this).data("tj-disabled"); //只处理带标记的
        }).each(function() {
	        var $this = $(this);
	        $this.removeData("tj-disabled"); //移除标记
	        $this.prop("disabled", false);
        });
    }
    if(Touchjs.dialogE && Touchjs.dialogE.length > 0){
    	Touchjs.dialogE.find(":button,:submit").not(".ignoredis").filter(function() {
	        return $(this).data("tj-disabled"); //只处理带标记的
        }).each(function() {
	        var $this = $(this);
	        $this.removeData("tj-disabled"); //移除标记
	        $this.prop("disabled", false);
        });
    }
};